<!DOCTYPE html>
<html>
<head>
    <title>Authentication Sample</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">    
</head>
<body>

<div class="container">

    <div class="row">
        <div class="col-lg-12">
            
            <h1>Authentication Example</h1>

            <hr />

        </div>

        <div class="col-lg-4 col-lg-offset-4">
            
            
            <form id="login-form" role="form">

                <fieldset>
                    <legend>Login</legend>
                
                    <div class="form-group">
                        <label>Email</label>
                        <input name="email" type="text" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input name="password" type="password" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Login" class="form-control btn btn-primary">
                    </div>
                    <div class="form-group">
                        <div class="message-login"></div>
                    </div>
                </fieldset>
            </form>

            <hr />

        </div> <!-- /.col-lg-4 -->

    </div> <!-- EO .row -->

    <div class="row">
        
        <div class="col-lg-4 col-lg-offset-4">
            
            <form id="create-user" role="form">
                <fieldset>
                    <legend>Register</legend>
                    <div class="form-group">
                        <label>Email</label>
                        <input name="email" type="text" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input name="password" type="password" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>First Name</label>
                        <input name="first_name" type="text" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input name="last_name" type="text" class="form-control" required>
                    </div>
                    
                    <input type="hidden" name="business_name" value="BEC" />

                    <input type="hidden" name="job_title" value="Associate" />

                    <input type="hidden" name="business_category" value="BEC" />

                    <div class="form-group">
                        <input type="submit" value="Register" class="form-control btn btn-primary">
                    </div>

                    <div class="form-group">
                        <div class="message-register"></div>
                    </div>

                </fieldset>
                
            </form>

        </div>

       
    </div>

</div> <!-- /.container -->

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script type="text/javascript">
$(function() {

    // Login 
    // Debug in Console
    $('#login-form').on('submit', function(e) {

        e.preventDefault();

        form_data = $(this).serialize();

        $.ajax({
            /**
             * Staging base_path: http://staging-tne.herokuapp.com/
             * Production base_path: https://thenetworkingeffect.herokuapp.com/
             */
            url: '/api/v1/auth/login',
            method: 'POST',
            data: form_data,
            dataType: 'json',
            success: function(data){
                $(".message-login").addClass("alert alert-success").html("Login successful - check console for response data including token.");
                console.log(data);
                localStorage.setItem('JWTtoken', data.token);
            },
            error: function (request, status, error) {
                $(".message-login").addClass("alert alert-danger").html(request.responseText);
                console.error(request.responseText);
            }
        });

    }); //EO form
    // EO Regular Authentication

    //CREATE USER
    $('#create-user').on('submit', function(e) {

        e.preventDefault();

        form_data = $(this).serialize();
        
        $.ajax({
            url: '/api/v1/auth/register',
            method: 'POST',
            data: form_data,
            dataType: 'json',
            success: function(data){
                $(".message-register").addClass("alert alert-success").html("Login successful - check console for response data including token.");
                console.error(data);
            },
            error: function (request, status, error) {
                $(".message-register").addClass("alert alert-danger").html(request.responseText);
                console.log(request.responseText);
            }
        });
    }); //EO form
    //EO CREATE USER - ADMIN


}); // EO DOM
</script>

<!-- / Scripts -->
</body>
</html>