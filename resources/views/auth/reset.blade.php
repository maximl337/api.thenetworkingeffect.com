@include('_templates._header')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">

            <form class="form" role="form" method="POST" action="/password/reset">
                {!! csrf_field() !!}

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group">
                    <input placeholder="your_email@example.com" class="form-control" type="email" name="email" value="{{ old('email') }}">
                </div>

                <div class="form-group">
                    <input placeholder="New Password" class="form-control" type="password" name="password">
                </div>

                <div class="form-group">
                    <input placeholder="Confirm new password" class="form-control" type="password" name="password_confirmation">
                </div>

                <div class="form-group">
                    <button  class="form-control btn btn-primary" type="submit">
                        Reset Password
                    </button>
                </div>
            </form>

        </div>

         @include ('errors.list')
    </div>
</div>
@include('_templates._footer')