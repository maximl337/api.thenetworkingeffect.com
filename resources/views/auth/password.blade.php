@include('_templates._header')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
            
                <form class="form" role="form" method="POST" action="/password/email">
                    {!! csrf_field() !!}

                    <div class="form-group">
                        <input placeholder="your_email@example.com" class="form-control" type="email" name="email" value="{{ old('email') }}" required="required">

                        <p style="color: white; font-weight: bold; padding: 5px;" class="helper-block">An email with a link to reset your password will be sent to the registered email. Follow the instructions in the email to reset your password.</p>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary form-control">
                            Send Password Reset Link
                        </button>
                    </div>  


                    <div class="form-group">
                        
                       
                           @if (session('status'))
                                <p class="alert alert-success text-center">{{ session('status') }}</p>
                            @endif 
                        

                    </div>
                </form>

            </div>

            @include ('errors.list')
        </div>
    </div>
    
@include('_templates._footer')