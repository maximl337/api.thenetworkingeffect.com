<?php

$filename = 'user-cities.csv';

$fp = fopen('user-cities.csv', 'w');

fputcsv($fp, ['id', 'first_name', 'last_name', 'email', 'city', 'latitude', 'longitude']);

foreach ($users as $fields) {

	$fields = (array) $fields;

    fputcsv($fp, $fields);
}

fclose($fp);

	header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($filename).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filename));
    readfile($filename);


