<form id="register" method="POST" action="{{ url('api/v1/oauth/register') }}" role="form">

	<fieldset>
		<legend>Register</legend>

		{!! csrf_field() !!}

		<div class="form-group">
			<label for="business_name">Name</label>
			<input id="business_name" class="form-control" type="text" name="name" value="{{ old('business_name') }}" placeholder="Enter your Company name" />
		</div>
		
		<div class="form-group">
			<label for="email">Email</label>
			<input id="email" class="form-control" type="text" name="email" value="{{ old('email') }}" placeholder="Enter your email" />
		</div>

		<div class="form-group">
			<label for="password">Password</label>
			<input id="password" class="form-control" type="password" name="password" />
		</div>

		<div class="form-group">
			<label for="password_confirmation">Confirm password</label>
			<input id="password_confirmation" class="form-control" type="password" name="password_confirmation" />
		</div>
		<div class="form-group">
			<input id="" class="form-control btn btn-primary" type="submit" value="Create" />
		</div>

	</fieldset>
	
</form>

<br />
<div class="text-center">
	<a href="{{ url('api/v1/oauth/login') }}">Already have an account?</a>
</div>
