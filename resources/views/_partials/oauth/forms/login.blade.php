<form id="login" method="POST" action="{{ url('api/v1/oauth/login') }}" role="form">


	<fieldset>
		<legend>Login</legend>
		<div class="form-group">
			<label for="email">Email</label>
			<input id="email" class="form-control " type="email" name="email" value="{{ old('email') }}" placeholder="Enter your email" />
		</div>
		
		<div class="form-group">
			<label for="password">Password</label>
			<input id="password" class="form-control" type="password" name="password" placeholder="Enter your password" />
		</div>

		<div class="form-group">
			<input id="" class="form-control btn btn-primary" type="submit" value="Submit" />
		</div>
	</fieldset>
	{!! csrf_field() !!}

	
</form>

<br />
<div class="text-center">
	<a href="{{ url('api/v1/oauth/register') }}">Create an account</a>
</div>
