<h1>The Networking Effect - Password reset robot</h1> <br />

<p>You recently requested to reset your password.</p>

<p>Click here to reset your password: <br />
<a href="{{ 'https://www.thenetworkingeffect.com/forgot_password.php?token='.$token }}">{{ 'https://www.thenetworkingeffect.com/forgot_password.php?token='.$token }}</a>
</p>

<p>If you did not request this then simply ignore this email and your password will remain unchanged.</p>