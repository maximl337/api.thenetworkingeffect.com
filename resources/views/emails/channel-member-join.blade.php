<p>A User has joined a channel.</p>
<p>Member ID: {{ $data['user']->id }}</p>
<p>Member Email: {{ $data['user']->email }}</p>
<p>Channel: {{ $data['channel']->name }}</p>