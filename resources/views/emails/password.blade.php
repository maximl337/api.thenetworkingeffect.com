<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<!-- NAME: 1 COLUMN - BANDED -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Forgot password request</title>
        
    <style type="text/css">
		body,#bodyTable,#bodyCell{
			height:100% !important;
			margin:0;
			padding:0;
			width:100% !important;
		}
		table{
			border-collapse:collapse;
		}
		img,a img{
			border:0;
			outline:none;
			text-decoration:none;
		}
		h1,h2,h3,h4,h5,h6{
			margin:0;
			padding:0;
		}
		p{
			margin:1em 0;
			padding:0;
		}
		a{
			word-wrap:break-word;
		}
		.mcnPreviewText{
			display:none !important;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
			line-height:100%;
		}
		table,td{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		#outlook a{
			padding:0;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		body,table,td,p,a,li,blockquote{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		#bodyCell{
			padding:0;
		}
		.mcnImage,.mcnRetinaImage{
			vertical-align:bottom;
		}
		.mcnTextContent img{
			height:auto !important;
		}
	/*
	tab Page
	section background style
	tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.
	*/
		body,#bodyTable{
			/*editable*/background-color:#F2F2F2;
		}
	/*
	tab Page
	section background style
	tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.
	*/
		#bodyCell{
			/*editable*/border-top:0;
		}
	/*
	tab Page
	section heading 1
	tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
	style heading 1
	*/
		h1{
			/*editable*/color:#606060 !important;
			display:block;
			/*editable*/font-family:Helvetica;
			/*editable*/font-size:40px;
			/*editable*/font-style:normal;
			/*editable*/font-weight:bold;
			/*editable*/line-height:125%;
			/*editable*/letter-spacing:-1px;
			margin:0;
			/*editable*/text-align:left;
		}
	/*
	tab Page
	section heading 2
	tip Set the styling for all second-level headings in your emails.
	style heading 2
	*/
		h2{
			/*editable*/color:#404040 !important;
			display:block;
			/*editable*/font-family:Helvetica;
			/*editable*/font-size:26px;
			/*editable*/font-style:normal;
			/*editable*/font-weight:bold;
			/*editable*/line-height:125%;
			/*editable*/letter-spacing:-.75px;
			margin:0;
			/*editable*/text-align:left;
		}
	/*
	tab Page
	section heading 3
	tip Set the styling for all third-level headings in your emails.
	style heading 3
	*/
		h3{
			/*editable*/color:#606060 !important;
			display:block;
			/*editable*/font-family:Helvetica;
			/*editable*/font-size:18px;
			/*editable*/font-style:normal;
			/*editable*/font-weight:bold;
			/*editable*/line-height:125%;
			/*editable*/letter-spacing:-.5px;
			margin:0;
			/*editable*/text-align:left;
		}
	/*
	tab Page
	section heading 4
	tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
	style heading 4
	*/
		h4{
			/*editable*/color:#808080 !important;
			display:block;
			/*editable*/font-family:Helvetica;
			/*editable*/font-size:16px;
			/*editable*/font-style:normal;
			/*editable*/font-weight:bold;
			/*editable*/line-height:125%;
			/*editable*/letter-spacing:normal;
			margin:0;
			/*editable*/text-align:left;
		}
	/*
	tab Preheader
	section preheader style
	tip Set the background color and borders for your email's preheader area.
	*/
		#templatePreheader{
			/*editable*/background-color:#FFFFFF;
			/*editable*/border-top:0;
			/*editable*/border-bottom:0;
		}
	/*
	tab Preheader
	section preheader text
	tip Set the styling for your email's preheader text. Choose a size and color that is easy to read.
	*/
		.preheaderContainer .mcnTextContent,.preheaderContainer .mcnTextContent p{
			/*editable*/color:#606060;
			/*editable*/font-family:Helvetica;
			/*editable*/font-size:11px;
			/*editable*/line-height:125%;
			/*editable*/text-align:left;
		}
	/*
	tab Preheader
	section preheader link
	tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
	*/
		.preheaderContainer .mcnTextContent a{
			/*editable*/color:#606060;
			/*editable*/font-weight:normal;
			/*editable*/text-decoration:underline;
		}
	/*
	tab Header
	section header style
	tip Set the background color and borders for your email's header area.
	*/
		#templateHeader{
			/*editable*/background-color:#FFFFFF;
			/*editable*/border-top:0;
			/*editable*/border-bottom:0;
		}
	/*
	tab Header
	section header text
	tip Set the styling for your email's header text. Choose a size and color that is easy to read.
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*editable*/color:#606060;
			/*editable*/font-family:Helvetica;
			/*editable*/font-size:15px;
			/*editable*/line-height:150%;
			/*editable*/text-align:left;
		}
	/*
	tab Header
	section header link
	tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
	*/
		.headerContainer .mcnTextContent a{
			/*editable*/color:#6DC6DD;
			/*editable*/font-weight:normal;
			/*editable*/text-decoration:underline;
		}
	/*
	tab Body
	section body style
	tip Set the background color and borders for your email's body area.
	*/
		#templateBody{
			/*editable*/background-color:#FFFFFF;
			/*editable*/border-top:0;
			/*editable*/border-bottom:0;
		}
	/*
	tab Body
	section body text
	tip Set the styling for your email's body text. Choose a size and color that is easy to read.
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*editable*/color:#606060;
			/*editable*/font-family:Helvetica;
			/*editable*/font-size:15px;
			/*editable*/line-height:150%;
			/*editable*/text-align:left;
		}
	/*
	tab Body
	section body link
	tip Set the styling for your email's body links. Choose a color that helps them stand out from your text.
	*/
		.bodyContainer .mcnTextContent a{
			/*editable*/color:#eebf08;
			/*editable*/font-weight:normal;
			/*editable*/text-decoration:underline;
		}
	/*
	tab Footer
	section footer style
	tip Set the background color and borders for your email's footer area.
	*/
		#templateFooter{
			/*editable*/background-color:#F2F2F2;
			/*editable*/border-top:0;
			/*editable*/border-bottom:0;
		}
	/*
	tab Footer
	section footer text
	tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*editable*/color:#606060;
			/*editable*/font-family:Helvetica;
			/*editable*/font-size:11px;
			/*editable*/line-height:125%;
			/*editable*/text-align:left;
		}
	/*
	tab Footer
	section footer link
	tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
	*/
		.footerContainer .mcnTextContent a{
			/*editable*/color:#606060;
			/*editable*/font-weight:normal;
			/*editable*/text-decoration:underline;
		}
	@media only screen and (max-width: 480px){
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:none !important;
		}

}	@media only screen and (max-width: 480px){
		body{
			width:100% !important;
			min-width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnRetinaImage{
			max-width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnTextContentContainer]{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnBoxedTextContentContainer{
			max-width:100% !important;
			min-width:100% !important;
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcpreview-image-uploader]{
			width:100% !important;
			display:none !important;
		}

}	@media only screen and (max-width: 480px){
		img[class=mcnImage]{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnImageGroupContentContainer]{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageGroupContent]{
			padding:9px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageGroupBlockInner]{
			padding-bottom:0 !important;
			padding-top:0 !important;
		}

}	@media only screen and (max-width: 480px){
		tbody[class=mcnImageGroupBlockOuter]{
			padding-bottom:9px !important;
			padding-top:9px !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnCaptionTopContent],table[class=mcnCaptionBottomContent]{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnCaptionLeftTextContentContainer],table[class=mcnCaptionRightTextContentContainer],table[class=mcnCaptionLeftImageContentContainer],table[class=mcnCaptionRightImageContentContainer],table[class=mcnImageCardLeftTextContentContainer],table[class=mcnImageCardRightTextContentContainer],.mcnImageCardLeftImageContentContainer,.mcnImageCardRightImageContentContainer{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageCardLeftImageContent],td[class=mcnImageCardRightImageContent]{
			padding-right:18px !important;
			padding-left:18px !important;
			padding-bottom:0 !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageCardBottomImageContent]{
			padding-bottom:9px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageCardTopImageContent]{
			padding-top:18px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageCardLeftImageContent],td[class=mcnImageCardRightImageContent]{
			padding-right:18px !important;
			padding-left:18px !important;
			padding-bottom:0 !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageCardBottomImageContent]{
			padding-bottom:9px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageCardTopImageContent]{
			padding-top:18px !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnCaptionLeftContentOuter] td[class=mcnTextContent],table[class=mcnCaptionRightContentOuter] td[class=mcnTextContent]{
			padding-top:9px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnCaptionBlockInner] table[class=mcnCaptionTopContent]:last-child td[class=mcnTextContent],.mcnImageCardTopImageContent,.mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent{
			padding-top:18px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnBoxedTextContentColumn]{
			padding-left:18px !important;
			padding-right:18px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnTextContent]{
			padding-right:18px !important;
			padding-left:18px !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	tab Mobile Styles
	section template width
	tip Make the template fluid for portrait or landscape view adaptability. If a fluid layout doesn't work for you, set the width to 300px instead.
	*/
		table[class=templateContainer]{
			/*tab Mobile Styles
section template width
tip Make the template fluid for portrait or landscape view adaptability. If a fluid layout doesn't work for you, set the width to 300px instead.*/max-width:600px !important;
			/*editable*/width:100% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	tab Mobile Styles
	section heading 1
	tip Make the first-level headings larger in size for better readability on small screens.
	*/
		h1{
			/*editable*/font-size:24px !important;
			/*editable*/line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	tab Mobile Styles
	section heading 2
	tip Make the second-level headings larger in size for better readability on small screens.
	*/
		h2{
			/*editable*/font-size:20px !important;
			/*editable*/line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	tab Mobile Styles
	section heading 3
	tip Make the third-level headings larger in size for better readability on small screens.
	*/
		h3{
			/*editable*/font-size:18px !important;
			/*editable*/line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	tab Mobile Styles
	section heading 4
	tip Make the fourth-level headings larger in size for better readability on small screens.
	*/
		h4{
			/*editable*/font-size:16px !important;
			/*editable*/line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	tab Mobile Styles
	section Boxed Text
	tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		table[class=mcnBoxedTextContentContainer] td[class=mcnTextContent],td[class=mcnBoxedTextContentContainer] td[class=mcnTextContent] p{
			/*editable*/font-size:18px !important;
			/*editable*/line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	tab Mobile Styles
	section Preheader Visibility
	tip Set the visibility of the email's preheader on small screens. You can hide it to save space.
	*/
		table[id=templatePreheader]{
			/*editable*/display:block !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	tab Mobile Styles
	section Preheader Text
	tip Make the preheader text larger in size for better readability on small screens.
	*/
		td[class=preheaderContainer] td[class=mcnTextContent],td[class=preheaderContainer] td[class=mcnTextContent] p{
			/*editable*/font-size:14px !important;
			/*editable*/line-height:115% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	tab Mobile Styles
	section Header Text
	tip Make the header text larger in size for better readability on small screens.
	*/
		td[class=headerContainer] td[class=mcnTextContent],td[class=headerContainer] td[class=mcnTextContent] p{
			/*editable*/font-size:18px !important;
			/*editable*/line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	tab Mobile Styles
	section Body Text
	tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		td[class=bodyContainer] td[class=mcnTextContent],td[class=bodyContainer] td[class=mcnTextContent] p{
			/*editable*/font-size:18px !important;
			/*editable*/line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	tab Mobile Styles
	section footer text
	tip Make the body content text larger in size for better readability on small screens.
	*/
		td[class=footerContainer] td[class=mcnTextContent],td[class=footerContainer] td[class=mcnTextContent] p{
			/*editable*/font-size:14px !important;
			/*editable*/line-height:115% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=footerContainer] a[class=utilityLink]{
			display:block !important;
		}

}</style></head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
      <!--*|IF:MC_PREVIEW_TEXT|*-->
      <!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">*|MC_PREVIEW_TEXT|*</span><!--<![endif]-->
      <!--*|END:IF|*-->
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN PREHEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader">
                                        <tr>
                                        	<td align="center" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="600" class="templateContainer">
                                                    <tr>
                                                        <td valign="top" class="preheaderContainer" style="padding-top:9px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="top" width="390" style="width:390px;">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:390px;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                        
                            <span style="font-size:11px"><span style="font-family:verdana,geneva,sans-serif">Email Notification From One Degree Network</span></span>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				<td valign="top" width="210" style="width:210px;">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:210px;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                        
                            
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
                                                    </tr>
                                                </table>
                                            </td>                                            
                                        </tr>
                                    </table>
                                    <!-- // END PREHEADER -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="600" class="templateContainer">
                                                    <tr>
                                                        <td valign="top" class="headerContainer" style="padding-top:10px; padding-bottom:10px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                        <tbody><tr>
                            <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0;">
                                
                                    
                                        <img align="left" alt="" src="https://gallery.mailchimp.com/0e1403a6107d387dd1e68705c/images/4de160fe-098b-418b-8c7c-b8e44e2ea06f.png" width="250" style="max-width:250px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="600" class="templateContainer">
                                                    <tr>
                                                        <td valign="top" class="bodyContainer" style="padding-top:10px; padding-bottom:10px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td valign="top" class="mcnBoxedTextBlockInner">
                
				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                    <tbody><tr>
                        
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                        
                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #FFFFFF;border: 1px solid #999999;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="padding: 18px;color: #666666;font-family: Verdana, Geneva, sans-serif;font-size: 16px;font-weight: normal;line-height: 200%;text-align: left;">
                                        <font color="#696969" face="verdana, geneva, sans-serif"><span style="font-size:28px">Password Reset Requested</span></font><br>
<br>
<span style="color:#696969"><span style="font-size:18px"><span style="font-family:verdana,geneva,sans-serif">Hi&nbsp;{{ $user->first_name }},</span></span></span><br>
<br>
<span style="font-size:16px; line-height:20.7999992370605px"><span style="font-family:verdana,geneva,sans-serif"><span style="color: #696969;line-height: 27.2000007629395px;">You recently made a request to&nbsp;reset your password on One Degree Network. &nbsp;Please follow this link to make the change:</span></span></span><br>
<br>
<span style="font-size:16px; line-height:20.7999992370605px"><span style="font-family:verdana,geneva,sans-serif"><span style="color: #696969;line-height: 27.2000007629395px;"><strong><a href="{{ 'https://www.thenetworkingeffect.com/forgot_password.php?vendor_id=5&token='.$token }}" target="_blank">Change password NOW</a></strong></span></span></span><br style="line-height: 20.7999992370605px;">
<br style="line-height: 20.7999992370605px;">
<span style="color: #696969;font-family: verdana,geneva,sans-serif;font-size: 15.5555562973022px;line-height: 27.2000007629395px;">If you did not request a password change, please ignore this message.</span><br>
<br>
<span style="font-size:16px; line-height:20.7999992370605px">If you have any questions, please contact Member Care at <a href="mailto:support@1dn.ca?subject=Help%20with%20Password%20Reset" target="_blank">support@1dn.ca</a></span>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->
                
				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCodeBlock">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                <br>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCodeBlock">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                <br>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="center" class="mcnButtonBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border: 2px none #707070;border-radius: 5px;background-color: #EEBF08;">
                    <tbody>
                        <tr>
                            <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Verdana, Geneva, sans-serif; font-size: 18px; padding: 16px;">
                                <a class="mcnButton " title="Read Message" href="{{ 'https://www.thenetworkingeffect.com/forgot_password.php?vendor_id=5&token='.$token }}" target="_blank" style="font-weight: normal;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #222222;">Reset Your Password</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
                        
                            <div style="text-align: center;"><img align="none" height="32" src="https://gallery.mailchimp.com/0e1403a6107d387dd1e68705c/images/777eb77f-f9ce-4f67-b065-e18b8d3d9103.png" style="width: 100px; height: 32px; margin: 0px;" width="100">&nbsp;<img align="none" height="32" src="https://gallery.mailchimp.com/0e1403a6107d387dd1e68705c/images/4f5ed7f8-225c-4fcf-97ba-cc5fa8421dc8.png" style="width: 100px; height: 32px; margin: 0px;" width="100">&nbsp;<img align="none" height="32" src="https://gallery.mailchimp.com/0e1403a6107d387dd1e68705c/images/5a7f8e60-62c4-44da-b96a-5503ff6beaee.png" style="width: 98px; height: 32px; margin: 0px;" width="98"></div>

                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCodeBlock">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                <br>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width:100%;">
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td align="center" valign="top" style="padding:9px" class="mcnFollowBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width:100%;">
    <tbody><tr>
        <td align="center" style="padding-left:9px;padding-right:9px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;background-color: #FFFFFF;border: 1px none #EEEEEE;" class="mcnFollowContent">
                <tbody><tr>
                    <td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td align="center" valign="top">
                                    <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnFollowStacked" style="display:inline;">
                                                 
                                                <tbody><tr>
                                                    <td align="center" valign="top" class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:5px;">
                                                        <a href="" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/gray-link-96.png" alt="Website" class="mcnFollowBlockIcon" width="48" style="width:48px; max-width:48px; display:block;"></a>
                                                    </td>
                                                </tr>
                                                
                                                
                                                <tr>
                                                    <td align="center" valign="top" class="mcnFollowTextContent" style="padding-right:10px; padding-bottom:9px;">
                                                        <a href="" target="_blank" style="color: #606060;font-family: Verdana, Geneva, sans-serif;font-size: 12px;font-weight: normal;text-decoration: none;line-height: 100%;text-align: center;">Website</a>
                                                    </td>
                                                </tr>
                                                
                                            </tbody></table>
                                        
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnFollowStacked" style="display:inline;">
                                                 
                                                <tbody><tr>
                                                    <td align="center" valign="top" class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:5px;">
                                                        <a href="" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/gray-facebook-96.png" alt="Facebook" class="mcnFollowBlockIcon" width="48" style="width:48px; max-width:48px; display:block;"></a>
                                                    </td>
                                                </tr>
                                                
                                                
                                                <tr>
                                                    <td align="center" valign="top" class="mcnFollowTextContent" style="padding-right:10px; padding-bottom:9px;">
                                                        <a href="" target="_blank" style="color: #606060;font-family: Verdana, Geneva, sans-serif;font-size: 12px;font-weight: normal;text-decoration: none;line-height: 100%;text-align: center;">Facebook</a>
                                                    </td>
                                                </tr>
                                                
                                            </tbody></table>
                                        
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnFollowStacked" style="display:inline;">
                                                 
                                                <tbody><tr>
                                                    <td align="center" valign="top" class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:5px;">
                                                        <a href="http://" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/gray-twitter-96.png" alt="Twitter" class="mcnFollowBlockIcon" width="48" style="width:48px; max-width:48px; display:block;"></a>
                                                    </td>
                                                </tr>
                                                
                                                
                                                <tr>
                                                    <td align="center" valign="top" class="mcnFollowTextContent" style="padding-right:10px; padding-bottom:9px;">
                                                        <a href="http://" target="_blank" style="color: #606060;font-family: Verdana, Geneva, sans-serif;font-size: 12px;font-weight: normal;text-decoration: none;line-height: 100%;text-align: center;">Twitter</a>
                                                    </td>
                                                </tr>
                                                
                                            </tbody></table>
                                        
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnFollowStacked" style="display:inline;">
                                                 
                                                <tbody><tr>
                                                    <td align="center" valign="top" class="mcnFollowIconContent" style="padding-right:0; padding-bottom:5px;">
                                                        <a href="" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/gray-linkedin-96.png" alt="LinkedIn" class="mcnFollowBlockIcon" width="48" style="width:48px; max-width:48px; display:block;"></a>
                                                    </td>
                                                </tr>
                                                
                                                
                                                <tr>
                                                    <td align="center" valign="top" class="mcnFollowTextContent" style="padding-right:0; padding-bottom:9px;">
                                                        <a href="" target="_blank" style="color: #606060;font-family: Verdana, Geneva, sans-serif;font-size: 12px;font-weight: normal;text-decoration: none;line-height: 100%;text-align: center;">LinkedIn</a>
                                                    </td>
                                                </tr>
                                                
                                            </tbody></table>
                                        
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>

            </td>
        </tr>
    </tbody>
</table></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="600" class="templateContainer">
                                                    <tr>
                                                        <td valign="top" class="footerContainer" style="padding-top:10px; padding-bottom:10px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
                        
                            <div style="text-align: center;"><span style="font-family:verdana,geneva,sans-serif"><em>Copyright © 2018 One Degree Network, All rights reserved.</em><br>
&nbsp;You are receiving this email because you are a member of&nbsp;<br>
One Degree Network.<br>
<strong>Our mailing address is:</strong><br>
92 Caplan Avenue, Suite 236<br>
Barrie, ON L4N 9J2&nbsp; CA<br>
<br>
<strong>Member Care:</strong> 855-654-1306</span><br>
<br>
<span style="font-family:verdana,geneva,sans-serif">Update Your Subscription Preferences&nbsp;</span></div>

                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>