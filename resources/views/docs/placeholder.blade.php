<section id="placeholder">
              <h2 class="sub-header">Placeholders</h2>
            
              <ul>
                
                <li>
                  <h3><strong>1. Add placeholder</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>placeholder</div>
                    <div class="panel-body">
                      <p>Creates a placeholder and adds them to the users network. If the email exists in the system and is owned by an active member, they will simply be added to the users network</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>first_name - (required)</td>
                            <td>Placeholders first name</td>
                          </tr>
                          <tr>
                            <td>last_name - (required)</td>
                            <td>Placeholders last name</td>
                          </tr>
                          <tr>
                            <td>email - (required)</td>
                            <td>Placeholders email</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Created",
                          "data": {
                            "email": "myplaceholder@gmail.com",
                            "first_name": "John",
                            "last_name": "Smoth",
                            "updated_at": "2016-01-25 19:42:33",
                            "created_at": "2016-01-25 19:42:33",
                            "id": 71,
                            "permalink": "john-smith",
                            "latitude": "",
                            "longitude": "",
                            "is_placeholder": true
                          }
                        }

                        STATUS_CODE: 201 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>2. Invite Placeholder</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>placeholder/invite</div>
                    <div class="panel-body">
                      <p>Sends and email invitation to the placeholder.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>placeholder_id - (required)</td>
                            <td>Placeholders user id</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                            "Invitation sent"
                          
                        }

                        STATUS_CODE: 201 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>3. Business Card Scanner</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>cardOcr</div>
                    <div class="panel-body">
                      <p>Scans and parses an image of business card.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>upfile - (required)</td>
                            <td>Business card image</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                            "first_name": "John",
                            "last_name": "Smoth",
                            ...
                          
                        }

                        STATUS_CODE: 201 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>1. Update placeholder</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>placeholder/{id}</div>
                    <div class="panel-body">
                      <p>Updates a placeholder.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>first_name</td>
                            <td>Placeholders first name</td>
                          </tr>
                          <tr>
                            <td>last_name</td>
                            <td>Placeholders last name</td>
                          </tr>
                          <tr>
                            <td>email</td>
                            <td>Placeholders email</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Placehodler Updated"
                        }

                        STATUS_CODE: 201 OK
                      </pre>
                    </div>
                  </div>
                </li>

                
              </ul>
            </section>
            <hr />