<section id="mailsub">
              <h2 class="sub-header">Mail Subscription</h2>
            
              <ul>
                <li>
                  <h3><strong>1. Get all Mail templates</strong> - Auth required</h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>mail/templates</div>
                    <div class="panel-body">
                      <p>Get all available mail templates</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "id": 1,
                            "name": "NEW_MESSAGE",
                            "description": "A member sent you a message"
                          },
                          {
                            "id": 2,
                            "name": "NEW_FOLLOWER",
                            "description": "A member has invited you to join their network"
                          },
                          {
                            "id": 3,
                            "name": "NEW_CARD_SHARE_RECIPIENT",
                            "description": "A member has sent you a referral"
                          },
                          [],
                          {
                            "id": 5,
                            "name": "NEW_TESTIMONIAL",
                            "description": "A member has left you a testimonial and it needs to be approved."
                          },
                          {
                            "id": 6,
                            "name": "EVENT_UPDATED",
                            "description": "An event you are attending has been updated"
                          },
                          {
                            "id": 7,
                            "name": "EVENT_DELETED",
                            "description": "An event you are attending has been cancelled"
                          },
                          [],
                          [],
                          {
                            "id": 10,
                            "name": "EVENT_JOIN",
                            "description": "A member has joined an event you created"
                          },
                          []
                        ]

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>2. Get users unsubbed templates</strong> - Auth Required</h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>mail/user/unsubscribed</div>
                    <div class="panel-body">
                      <p>Get templates user is unsubscribed from</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "id": 1,
                            "name": "NEW_MESSAGE",
                            "description": "A member sent you a message"
                          }
                        ]

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>3. Update users subscription</strong> - Auth Required</h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>mail/user/unsubscribe</div>
                    <div class="panel-body">
                      <p>Unscubscribe user from templates</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                          <tr>
                            <th>template_ids (required)</th>
                            <th>An array of template ids that user wants to unsubscribe from.</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Subscription updated",
                          "data": []
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>
              </ul>
            </section>