<section id="search">
              <h2 class="sub-header">Search</h2>
              <p class="alert alert-warning">Clients my also search 'users_api' index from Algolia</p>
              <ul>
                <li>
                  <h3><strong>1. Search Users</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>search/users?query=</div>
                    <div class="panel-body">
                      <p>Get users search results</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>query</td>
                            <td>Search query</td>
                          </tr>
                          <tr>
                            <td>hitsPerPage (default 20)</td>
                            <td>number of results for each page</td>
                          </tr>
                          <tr>
                            <td>page</td>
                            <td>Page number</td>
                          </tr>
                          <tr>
                            <td>geo</td>
                            <td>set true for geo search ( will use users coordinates from records )</td>
                          </tr>
                          <tr>
                            <td>lat</td>
                            <td>latitude of users current position. (DO NOT PASS "geo" if using current position)</td>
                          </tr>
                          <tr>
                            <td>lng</td>
                            <td>longitude of users current position. (DO NOT PASS "geo" if using current position)</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                        "hits": [
                          {
                            "first_name": "John",
                            "last_name": "Smith",
                            "avatar": "https:\\/\\/lh4.googleusercontent.com\\/-sauE29K0kFk\\/AAAAAAAAAAI\\/AAAAAAAAAFM\\/0YhYRJjsPDo\\/photo.jpg?sz=50",
                            "objectID": "56",
                            "_highlightResult": {
                              "first_name": {
                                "value": "<em>John<\\/em>",
                                "matchLevel": "full",
                                "matchedWords": [
                                  "John"
                                ]
                              },
                              "last_name": {
                                "value": "Smith",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "avatar": {
                                "value": "https:\\/\\/lh4.googleusercontent.com\\/-sauE29K0kFk\\/AAAAAAAAAAI\\/AAAAAAAAAFM\\/0YhYRJjsPDo\\/photo.jpg?sz=50",
                                "matchLevel": "none",
                                "matchedWords": []
                              }
                            }
                          },
                          {
                            "id": 51,
                            "first_name": "John",
                            "last_name": "Smith",
                            "email": "John_smith@hotmail.com",
                            "provider": null,
                            "avatar": null,
                            "logo": null,
                            "job_title": "CEO",
                            "business_name": "CIM",
                            "phone_1": null,
                            "phone_2": null,
                            "fax": null,
                            "about_me": null,
                            "about_business": null,
                            "website_url": null,
                            "facebook_url": null,
                            "twitter_url": null,
                            "googleplus_url": null,
                            "linkedin_url": null,
                            "youtube_url": null,
                            "blog_url": null,
                            "street": null,
                            "city": "Barrie",
                            "state": "ON",
                            "zip_code": null,
                            "country": null,
                            "latitude": "test",
                            "longitude": "test",
                            "is_admin": null,
                            "business_category": "web",
                            "deleted_at": null,
                            "created_at": "-0001-11-30 00:00:00",
                            "updated_at": "2015-07-07 15:28:59",
                            "objectID": "51",
                            "_highlightResult": {
                              "first_name": {
                                "value": "<em>John<\\/em>",
                                "matchLevel": "full",
                                "matchedWords": [
                                  "John"
                                ]
                              },
                              "last_name": {
                                "value": "Smith",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "email": {
                                "value": "<em>John<\\/em>_smith@hotmail.com",
                                "matchLevel": "full",
                                "matchedWords": [
                                  "John"
                                ]
                              },
                              "job_title": {
                                "value": "CEO",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "business_name": {
                                "value": "CIM",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "city": {
                                "value": "Barrie",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "state": {
                                "value": "ON",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "latitude": {
                                "value": "test",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "longitude": {
                                "value": "test",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "business_category": {
                                "value": "web",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "created_at": {
                                "value": "-0001-11-30 00:00:00",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "updated_at": {
                                "value": "2015-07-07 15:28:59",
                                "matchLevel": "none",
                                "matchedWords": []
                              }
                            },
                            "network": true
                          },
                          {
                            "first_name": "Johntest",
                            "last_name": "Smithytest",
                            "objectID": "57",
                            "_highlightResult": {
                              "first_name": {
                                "value": "<em>John<\\/em>test",
                                "matchLevel": "full",
                                "matchedWords": [
                                  "John"
                                ]
                              },
                              "last_name": {
                                "value": "Smithytest",
                                "matchLevel": "none",
                                "matchedWords": []
                              }
                            }
                          },
                          {
                            "first_name": "Johntest",
                            "last_name": "Smithytest",
                            "about_me": "lorem_ipsum",
                            "about_business": "lorem ipsum",
                            "business_name": "CIM",
                            "job_title": "CEO",
                            "avatar": "pic",
                            "logo": "pic",
                            "city": "barrie",
                            "state": "ON",
                            "business_category": "web",
                            "tags": [
                              "doloremque",
                              "numquam",
                              "non"
                            ],
                            "_tags": [
                              "doloremque",
                              "numquam",
                              "non"
                            ],
                            "_geo": {
                              "lat": 1.123,
                              "lng": 3.321
                            },
                            "objectID": "55",
                            "_highlightResult": {
                              "first_name": {
                                "value": "<em>John<\\/em>test",
                                "matchLevel": "full",
                                "matchedWords": [
                                  "John"
                                ]
                              },
                              "last_name": {
                                "value": "Smithytest",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "about_me": {
                                "value": "lorem_ipsum",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "about_business": {
                                "value": "lorem ipsum",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "business_name": {
                                "value": "CIM",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "job_title": {
                                "value": "CEO",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "avatar": {
                                "value": "pic",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "logo": {
                                "value": "pic",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "city": {
                                "value": "barrie",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "state": {
                                "value": "ON",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "business_category": {
                                "value": "web",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "tags": [
                                {
                                  "value": "doloremque",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                },
                                {
                                  "value": "numquam",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                },
                                {
                                  "value": "non",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                }
                              ],
                              "_tags": [
                                {
                                  "value": "doloremque",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                },
                                {
                                  "value": "numquam",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                },
                                {
                                  "value": "non",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                }
                              ]
                            },
                            "network": true,
                            "distance": 10 // Kilometers
                          }
                        ],
                        "nbHits": 4,
                        "page": 0,
                        "nbPages": 1,
                        "hitsPerPage": 20,
                        "processingTimeMS": 1,
                        "query": "John",
                        "params": "hitsPerPage=20&page=0&query=John"
                      }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>2. Search Users Public</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>search/users/public?query=</div>
                    <div class="panel-body">
                      <p>Get users search results - No auth required</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>query</td>
                            <td>Search query</td>
                          </tr>
                          <tr>
                            <td>hitsPerPage (default 20)</td>
                            <td>number of results for each page</td>
                          </tr>
                          <tr>
                            <td>page</td>
                            <td>Page number</td>
                          </tr>
                          <tr>
                            <td>geo</td>
                            <td>set true for geo search ( will use users coordinates from records )</td>
                          </tr>
                          <tr>
                            <td>lat</td>
                            <td>latitude of users current position. (DO NOT PASS "geo" if using current position)</td>
                          </tr>
                          <tr>
                            <td>lng</td>
                            <td>longitude of users current position. (DO NOT PASS "geo" if using current position)</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                        "hits": [
                          {
                            "first_name": "John",
                            "last_name": "Smith",
                            "avatar": "https:\\/\\/lh4.googleusercontent.com\\/-sauE29K0kFk\\/AAAAAAAAAAI\\/AAAAAAAAAFM\\/0YhYRJjsPDo\\/photo.jpg?sz=50",
                            "objectID": "56",
                            "_highlightResult": {
                              "first_name": {
                                "value": "<em>John<\\/em>",
                                "matchLevel": "full",
                                "matchedWords": [
                                  "John"
                                ]
                              },
                              "last_name": {
                                "value": "Smith",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "avatar": {
                                "value": "https:\\/\\/lh4.googleusercontent.com\\/-sauE29K0kFk\\/AAAAAAAAAAI\\/AAAAAAAAAFM\\/0YhYRJjsPDo\\/photo.jpg?sz=50",
                                "matchLevel": "none",
                                "matchedWords": []
                              }
                            }
                          },
                          {
                            "id": 51,
                          ...
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>


                <li>
                  <h3><strong>3. Search Events</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>search/events?query=</div>
                    <div class="panel-body">
                      <p>Get search results of events</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>query</td>
                            <td>Search query</td>
                          </tr>
                          <tr>
                            <td>hitsPerPage (default 20)</td>
                            <td>number of results for each page</td>
                          </tr>
                          <tr>
                            <td>page</td>
                            <td>Page number</td>
                          </tr>
                          <tr>
                            <td>geo</td>
                            <td>set true for geo search ( will use users coordinates from records )</td>
                          </tr>
                          <tr>
                            <td>lat</td>
                            <td>latitude of users current position. (DO NOT PASS "geo" if using current position)</td>
                          </tr>
                          <tr>
                            <td>lng</td>
                            <td>longitude of users current position. (DO NOT PASS "geo" if using current position)</td>
                          </tr>
                          <tr>
                            <td>past_events</td>
                            <td>Pass as true to get events older than current date</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                        "hits": [
                          {
                            "name": "test event",
                            "description": "test event description",
                            "event_date": "2015-08-01",
                            "banner_url": "eventbanner",
                            "start_time": "17:04:10",
                            "end_time": "17:04:10",
                            "street": "60 Molson",
                            "city": "Barrie ",
                            "state": "ON",
                            "tags": [
                              "doloremque",
                              "non",
                              "omnis"
                            ],
                            "_tags": [
                              "doloremque",
                              "non",
                              "omnis"
                            ],
                            "_geo": {
                              "lat": 43.14,
                              "lng": -13.32
                            },
                            "objectID": "51",
                            "_highlightResult": {
                              "name": {
                                "value": "<em>test<\\/em> event",
                                "matchLevel": "full",
                                "matchedWords": [
                                  "test"
                                ]
                              },
                              "description": {
                                "value": "<em>test<\\/em> event description",
                                "matchLevel": "full",
                                "matchedWords": [
                                  "test"
                                ]
                              },
                              "event_date": {
                                "value": "2015-08-01",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "banner_url": {
                                "value": "eventbanner",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "start_time": {
                                "value": "17:04:10",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "end_time": {
                                "value": "17:04:10",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "street": {
                                "value": "253 Johnson",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "city": {
                                "value": "Barrie",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "state": {
                                "value": "ON update",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              "tags": [
                                {
                                  "value": "doloremque",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                },
                                {
                                  "value": "non",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                },
                                {
                                  "value": "omnis",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                }
                              ],
                              "_tags": [
                                {
                                  "value": "doloremque",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                },
                                {
                                  "value": "non",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                },
                                {
                                  "value": "omnis",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                }
                              ]
                            }
                          }
                        ],
                        "nbHits": 1,
                        "page": 0,
                        "nbPages": 1,
                        "hitsPerPage": 20,
                        "processingTimeMS": 1,
                        "query": "test",
                        "params": "hitsPerPage=20&page=0&query=test"
                        }
                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>4. Search Events Public</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>search/events/public?query=</div>
                    <div class="panel-body">
                      <p>Get search results of events</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>query</td>
                            <td>Search query</td>
                          </tr>
                          <tr>
                            <td>hitsPerPage (default 20)</td>
                            <td>number of results for each page</td>
                          </tr>
                          <tr>
                            <td>page</td>
                            <td>Page number</td>
                          </tr>
                          <tr>
                            <td>geo</td>
                            <td>set true for geo search ( will use users coordinates from records )</td>
                          </tr>
                          <tr>
                            <td>lat</td>
                            <td>latitude of users current position. (DO NOT PASS "geo" if using current position)</td>
                          </tr>
                          <tr>
                            <td>lng</td>
                            <td>longitude of users current position. (DO NOT PASS "geo" if using current position)</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                        "hits": [
                          {
                            "name": "test event",
                            "description": "test event description",
                            "event_date": "2015-08-01",
                            "banner_url": "eventbanner",
                            "start_time": "17:04:10",
                            "end_time": "17:04:10",
                            "street": "60 Molson",
                            "city": "Barrie ",
                            "state": "ON",
                            ...
                        ],
                        "nbHits": 1,
                        "page": 0,
                        "nbPages": 1,
                        "hitsPerPage": 20,
                        "processingTimeMS": 1,
                        "query": "test",
                        "params": "hitsPerPage=20&page=0&query=test"
                        }
                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>5. Search Articles</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>search/articles?query=</div>
                    <div class="panel-body">
                      <p>Get articles search results</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>query</td>
                            <td>Search query</td>
                          </tr>
                          <tr>
                            <td>hitsPerPage (default 20)</td>
                            <td>number of results for each page</td>
                          </tr>
                          <tr>
                            <td>page</td>
                            <td>Page number</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                        "hits": [
                          {
                            "title": "awesome article update",
                            "body": "awesome article is awesome body update",
                            "teaser": "awesome article teaser update",
                            "user_id": 51,
                            "created_at": {
                              "date": "2015-07-13 15:38:11.000000",
                              "timezone_type": 3,
                              "timezone": "UTC"
                            },
                            "tags": [
                              "2"
                            ],
                            "_tags": [
                              "2"
                            ],
                            "objectID": "53",
                            "_highlightResult": {
                              "title": {
                                "value": "<em>awesome<\\/em> article update",
                                "matchLevel": "full",
                                "matchedWords": [
                                  "awesome"
                                ]
                              },
                              "body": {
                                "value": "<em>awesome<\\/em> article is <em>awesome<\\/em> body update",
                                "matchLevel": "full",
                                "matchedWords": [
                                  "awesome"
                                ]
                              },
                              "teaser": {
                                "value": "<em>awesome<\\/em> article teaser update",
                                "matchLevel": "full",
                                "matchedWords": [
                                  "awesome"
                                ]
                              },
                              "created_at": {
                                "date": {
                                  "value": "2015-07-13 15:38:11.000000",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                },
                                "timezone_type": 3,
                                "timezone": {
                                  "value": "UTC",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                }
                              },
                              "tags": [
                                {
                                  "value": "2",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                }
                              ],
                              "_tags": [
                                {
                                  "value": "2",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                }
                              ]
                            }
                          },
                          {
                            "title": "another awesome article ",
                            "body": "another awesome article is awesome body ",
                            "teaser": "another awesome article teaser ",
                            "user_id": 51,
                            "created_at": {
                              "date": "2015-07-13 15:42:10.000000",
                              "timezone_type": 3,
                              "timezone": "UTC"
                            },
                            "tags": [
                              "ut"
                            ],
                            "_tags": [
                              "ut"
                            ],
                            "objectID": "54",
                            "_highlightResult": {
                              "title": {
                                "value": "another <em>awesome<\\/em> article ",
                                "matchLevel": "full",
                                "matchedWords": [
                                  "awesome"
                                ]
                              },
                              "body": {
                                "value": "another <em>awesome<\\/em> article is <em>awesome<\\/em> body ",
                                "matchLevel": "full",
                                "matchedWords": [
                                  "awesome"
                                ]
                              },
                              "teaser": {
                                "value": "another <em>awesome<\\/em> article teaser ",
                                "matchLevel": "full",
                                "matchedWords": [
                                  "awesome"
                                ]
                              },
                              "created_at": {
                                "date": {
                                  "value": "2015-07-13 15:42:10.000000",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                },
                                "timezone_type": 3,
                                "timezone": {
                                  "value": "UTC",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                }
                              },
                              "tags": [
                                {
                                  "value": "ut",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                }
                              ],
                              "_tags": [
                                {
                                  "value": "ut",
                                  "matchLevel": "none",
                                  "matchedWords": []
                                }
                              ]
                            }
                          }
                        ],
                        "nbHits": 2,
                        "page": 0,
                        "nbPages": 1,
                        "hitsPerPage": 20,
                        "processingTimeMS": 1,
                        "query": "awesome",
                        "params": "hitsPerPage=20&page=0&query=awesome"
                      }
                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>6. Search Events Users</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>search/events/users?query=</div>
                    <div class="panel-body">
                      <p>Get articles search results</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>query</td>
                            <td>Search query</td>
                          </tr>
                          <tr>
                            <td>hitsPerPage (default 20)</td>
                            <td>number of results for each page</td>
                          </tr>
                          <tr>
                            <td>page</td>
                            <td>Page number</td>
                          </tr>
                          <tr>
                            <td>event_ids (required | array)</td>
                            <td>Array of event ids to search users from</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                        "0": {
                          "id": 7,
                          "_geoloc": {
                            "lat": -17.571352,
                            "lng": 85.066275
                          },
                          "first_name": "Karine",
                          "last_name": "Haag",
                          "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?80367",
                          "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?31954",
                          "job_title": "fugit",
                          "business_name": "Goodwin-Marks",
                          "about_me": "I think it so quickly that the pebbles were all shaped like ears and whiskers, how late it's getting!' She was close behind us, and he's treading on her spectacles, and began to tremble. Alice.",
                          "about_business": "Alice. 'Nothing,' said Alice. 'Why, there they are!' said the Dormouse, not choosing to notice this question, but hurriedly went on, 'that they'd let Dinah stop in the distance, and she tried to.",
                          "website_url": "https:\\/\\/Becker.com\\/sit-tempora-assumenda-debitis.html",
                          "facebook_url": "http:\\/\\/Ward.info\\/",
                          "twitter_url": "https:\\/\\/www.Streich.org\\/dolorum-qui-velit-vitae-et-perspiciatis-laborum-neque",
                          "googleplus_url": "http:\\/\\/www.Kovacek.com\\/voluptas-autem-animi-doloribus",
                          "linkedin_url": "https:\\/\\/Cummings.biz\\/non-qui-dolores-temporibus-et-eos-eum-sit-dolorem.html",
                          "youtube_url": "http:\\/\\/Maggio.net\\/hic-iure-rerum-vel-ut-voluptatem-velit",
                          "blog_url": "https:\\/\\/Walker.com\\/et-odit-distinctio-quia-quod-quia.html",
                          "city": "McGlynnmouth",
                          "state": "Minnesota",
                          "country": "Reunion",
                          "tags": [
                            "velit",
                            "non",
                            "corporis"
                          ],
                          "_tags": [
                            "velit",
                            "non",
                            "corporis"
                          ],
                          "objectID": "7",
                          "_highlightResult": {
                            "first_name": {
                              "value": "Karine",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "last_name": {
                              "value": "Haag",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "avatar": {
                              "value": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?80367",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "logo": {
                              "value": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?31954",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "job_title": {
                              "value": "<em>fugit<\\/em>",
                              "matchLevel": "full",
                              "matchedWords": [
                                "fugit"
                              ]
                            },
                            "business_name": {
                              "value": "Goodwin-Marks",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "about_me": {
                              "value": "I think it so quickly that the pebbles were all shaped like ears and whiskers, how late it's getting!' She was close behind us, and he's treading on her spectacles, and began to tremble. Alice.",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "about_business": {
                              "value": "Alice. 'Nothing,' said Alice. 'Why, there they are!' said the Dormouse, not choosing to notice this question, but hurriedly went on, 'that they'd let Dinah stop in the distance, and she tried to.",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "website_url": {
                              "value": "https:\\/\\/Becker.com\\/sit-tempora-assumenda-debitis.html",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "facebook_url": {
                              "value": "http:\\/\\/Ward.info\\/",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "twitter_url": {
                              "value": "https:\\/\\/www.Streich.org\\/dolorum-qui-velit-vitae-et-perspiciatis-laborum-neque",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "googleplus_url": {
                              "value": "http:\\/\\/www.Kovacek.com\\/voluptas-autem-animi-doloribus",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "linkedin_url": {
                              "value": "https:\\/\\/Cummings.biz\\/non-qui-dolores-temporibus-et-eos-eum-sit-dolorem.html",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "youtube_url": {
                              "value": "http:\\/\\/Maggio.net\\/hic-iure-rerum-vel-ut-voluptatem-velit",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "blog_url": {
                              "value": "https:\\/\\/Walker.com\\/et-odit-distinctio-quia-quod-quia.html",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "city": {
                              "value": "McGlynnmouth",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "state": {
                              "value": "Minnesota",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "country": {
                              "value": "Reunion",
                              "matchLevel": "none",
                              "matchedWords": []
                            },
                            "tags": [
                              {
                                "value": "velit",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              {
                                "value": "non",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              {
                                "value": "corporis",
                                "matchLevel": "none",
                                "matchedWords": []
                              }
                            ],
                            "_tags": [
                              {
                                "value": "velit",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              {
                                "value": "non",
                                "matchLevel": "none",
                                "matchedWords": []
                              },
                              {
                                "value": "corporis",
                                "matchLevel": "none",
                                "matchedWords": []
                              }
                            ]
                          },
                          "network": false,
                          "distance": 10 // Kilometers
                        },
                        "nbHits": 1,
                        "page": 0,
                        "nbPages": 1,
                        "hitsPerPage": 20,
                        "processingTimeMS": 1,
                        "query": "fugit",
                        "params": "numericFilters=%28id%3D7%2C+id%3D23%2C+id%3D46%29&aroundLatLng=44.4101662%2C-79.6539037&aroundRadius=100000000&hitsPerPage=20&page=0&query=fugit"
                      }
                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>
              </ul>
            </section>
            <hr />