<section id="network">
              <h2 class="sub-header">Network</h2>
            
              <ul>
                <li>
                  <h3><strong>1. Get network</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>users/network/following</div>
                    <div class="panel-body">
                      <p>List of users the authenticated user is following</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "user_id": 1,
                            "first_name": "Domenick",
                            "last_name": "Walter",
                            "job_title": "nihil",
                            "business_name": "Ankunding Group",
                            "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?22187",
                            "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?68707",
                            "network": true
                          },
                          {
                            "user_id": 2,
                            "first_name": "Nicole",
                            "last_name": "Emmerich",
                            "job_title": "et",
                            "business_name": "Daniel PLC",
                            "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?97106",
                            "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?88492",
                            "network": true
                          },
                          {
                            "user_id": 3,
                            "first_name": "Nigel",
                            "last_name": "Beatty",
                            "job_title": "nostrum",
                            "business_name": "Blick LLC",
                            "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?14016",
                            "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?84917",
                            "network": true
                          }
                        ]

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>2. Get users network</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>users/{id}/network/following</div>
                    <div class="panel-body">
                      <p>List of users the requested user is following</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>User id</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "user_id": 37,
                            "first_name": "Lucas",
                            "last_name": "Legros",
                            "job_title": "quos",
                            "business_name": "Adams-Parker",
                            "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?90420",
                            "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?11268",
                            "network": false
                          },
                          {
                            "user_id": 22,
                            "first_name": "Tyshawn",
                            "last_name": "Tremblay",
                            "job_title": "qui",
                            "business_name": "Crooks, Turcotte and Schmitt",
                            "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?47622",
                            "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?49999",
                            "network": false
                          },
                          {
                            "user_id": 50,
                            "first_name": "Jermaine",
                            "last_name": "Senger",
                            "job_title": "in",
                            "business_name": "Hagenes-Schmeler",
                            "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?76296",
                            "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?88902",
                            "network": false
                          }
                        ]

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>3. Follow user</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>users/{id}/follow</div>
                    <div class="panel-body">
                      <p>Follow another user. Previous invitations and notifications for the same will be overwritten.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>User id</td>
                          </tr>
                          <tr>
                            <td>message</td>
                            <td>A message to send along with the invite</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "User followed",
                          "data": []
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>4. Unfollow user</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>users/{id}/unfollow</div>
                    <div class="panel-body">
                      <p>unfollow another user</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>User id</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "User unfollowed"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>5. Get followers</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>users/network/followers</div>
                    <div class="panel-body">
                      <p>Get authenticated users followers</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "id": 44,
                            "follower_id": 5,
                            "subject_id": 51,
                            "created_at": "2015-07-03 19:55:42",
                            "updated_at": "2015-07-03 19:55:42"
                          },
                          ...
                        ]

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>6. Get followers</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>users/{id}/network/followers</div>
                    <div class="panel-body">
                      <p>Get users followers</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>User id</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "id": 44,
                            "follower_id": 5,
                            "subject_id": 51,
                            "created_at": "2015-07-03 19:55:42",
                            "updated_at": "2015-07-03 19:55:42"
                          },
                          ...
                        ]

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>7. Sort contact</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>network/sort</div>
                    <div class="panel-body">
                      <p>Sort a connection</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>subject_id (required)</td>
                            <td>contacts user id</td>
                          </tr>

                          <tr>
                            <td>sorting_category_id (required)</td>
                            <td>category id</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          {
                            "message": "Sorted successfully"
                          }
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>8. Park contact</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>network/park</div>
                    <div class="panel-body">
                      <p>Park/unsort a connection</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>subject_id (required)</td>
                            <td>contacts user id</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          {
                            "message": "Parked successfully"
                          }
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>9. Get Sorting categories</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>network/sort/categories</div>
                    <div class="panel-body">
                      <p>Get sorting categories</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "categories": [
                            {
                              "id": 1,
                              "name": "Follow-up tomorrow",
                              "reminder_delay_days": 1,
                              "reminder_time": "09:00:00",
                              "reminder_timezone": "America/Toronto",
                              "created_at": "2015-12-23 06:02:47",
                              "updated_at": "2015-12-23 06:02:47"
                            }
                          ]
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>10. Mark sorted connection active</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>network/sort/active</div>
                    <div class="panel-body">
                      <p>Marks a connection active</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                          <tr>
                            <td>subject_id (required)</td>
                            <td>id of the connection to mark active</td>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message" => "Marked active"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>11. Block user from sending network requests</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>users/{id}/block</div>
                    <div class="panel-body">
                      <p>Blocks a users</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                          <tr>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message" => "Blocked user"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>12. Unblock user from sending network requests</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>users/{id}/unblock</div>
                    <div class="panel-body">
                      <p>unblocks a user</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                          <tr>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message" => "unblocked user"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>13. Get new invites</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>network/invites</div>
                    <div class="panel-body">
                      <p>unblocks a user</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                          <tr>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          {
                            "user_id": 37,
                            "first_name": "Lucas",
                            "last_name": "Legros",
                            "job_title": "quos",
                            "business_name": "Adams-Parker",
                            "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?90420",
                            "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?11268",
                            "network": false
                          },
                          {
                            "user_id": 22,
                            "first_name": "Tyshawn",
                            "last_name": "Tremblay",
                            "job_title": "qui",
                            "business_name": "Crooks, Turcotte and Schmitt",
                            "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?47622",
                            "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?49999",
                            "network": false
                          },
                          {
                            "user_id": 50,
                            "first_name": "Jermaine",
                            "last_name": "Senger",
                            "job_title": "in",
                            "business_name": "Hagenes-Schmeler",
                            "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?76296",
                            "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?88902",
                            "network": false
                          }
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>13. Get invite by id</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>network/invites/{id}</div>
                    <div class="panel-body">
                      <p>unblocks a user</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                          <tr>
                            <td>id {required}</td>
                            <td>Resource id</td>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          {
                            "user_id": 37,
                            "first_name": "Lucas",
                            "last_name": "Legros",
                            "job_title": "quos",
                            "business_name": "Adams-Parker",
                            "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?90420",
                            "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?11268",
                            "network": false
                          }
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

              </ul>
            </section>
            <hr />