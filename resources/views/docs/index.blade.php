<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
     
      <meta name="description" content="">
      <meta name="author" content="Angad Dubey">
      <link rel="icon" href="favicon.ico">

      <title>The Networking Effect</title>

      <!-- Bootstrap core CSS -->
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
      <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>    

      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style>
        .list-group.panel > .list-group-item {
          border-bottom-right-radius: 4px;
          border-bottom-left-radius: 4px
        }
        .list-group-submenu {
          margin-left:20px;
        } 
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #B0BEC5;
            font-weight: 600;
            /*font-family: 'Lato';*/
        }
        .navbar.navbar-inverse {
            background: #009EC0;
            border-bottom: 0px solid white;
            color: white;
            border: none;
            border-radius: 0px;
        }
        .navbar-inverse .navbar-brand {
          font-weight: 200;
          color: white;
        }
        * ul {
          padding-left: 0px;

        }

        * ul li {
          list-style-type: none;
        }
        @media (min-width: 979px) {
          #sidebar.affix-top {
            position: static;
            margin-top:30px;
            width:228px;
          }
          
          #sidebar.affix {
            position: fixed;
            top:70px;
            width:228px;
          }
        }
      </style>
      <?php $base_url = 'https://thenetworkingeffect.herokuapp.com/api/v1/'; ?>
    </head>

    <body>

        <nav class="navbar navbar-inverse">
          <div class="container-fluid">

          <div class="navbar-header">
            <a class="navbar-brand" href="#">The Networking Effect API</a>
          </div>

          </div>
        </nav>

        <div class="container-fluid">
        <div class="row">

          <div id="leftCol" class="col-sm-3 col-md-3 sidebar">
            <div id="sidebar">
              <div class="list-group panel">
                <a href="#authentication" class="list-group-item">Authentication</a>
                <a href="#users" class="list-group-item">Users</a>
                <a href="#events" class="list-group-item">Events</a>
                <a href="#articles" class="list-group-item">Articles</a>
                <a href="#messaging" class="list-group-item">Messaging</a>
                <a href="#tags" class="list-group-item">Tags</a>
                <a href="#card_shares" class="list-group-item">Share Card</a>
                <a href="#testimonials" class="list-group-item">Testimonials</a>
                <a href="#network" class="list-group-item">Network</a>
                <a href="#opportunity" class="list-group-item">Opportunities</a>
                <a href="#placeholder" class="list-group-item">Placeholder</a>
                <a href="#notes" class="list-group-item">Notes</a>
                <a href="#search" class="list-group-item">Search</a>
                <a href="#notifications" class="list-group-item">Notifications</a>
                <a href="#mailsub" class="list-group-item">Mail Subscription</a>
                <a href="#legal" class="list-group-item">Legal</a>
                <a href="#utilities" class="list-group-item">Utilities</a>
                <a href="#channels" class="list-group-item">Channels</a>
              </div>
            </div>
          </div>

          <div class="col-md-6 main">
            
            <h1 class="page-header" style="margin-top: 0px;">REST API</h1>
            
            @include('docs.authentication')
  
            @include('docs.users')

            @include('docs.events')

            @include('docs.articles')

            @include('docs.messaging')

            @include('docs.tags')

            @include('docs.sharecard')

            @include('docs.testimonials')

            @include('docs.network')

            @include('docs.opportunities')

            @include('docs.placeholder')

            @include('docs.notes')

            @include('docs.search')

            @include('docs.notifications')

            @include('docs.mailsub')

            @include('docs.legal')
           
            @include('docs.utilities')

            @include('docs.channels');
            
          </div> <!-- /.main -->
        </div> <!-- /.row -->
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $('#sidebar').affix({
                  offset: {
                    top: 245
                  }
            });

            var $body   = $(document.body);
            var navHeight = 50;

            $body.scrollspy({
              target: '#leftCol',
              offset: navHeight
            });
        </script>
    </body>
</html>
