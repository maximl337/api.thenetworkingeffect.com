<section id="utilities">
  <h2 class="sub-header">Misc Utilities</h2>

  <ul>
    <li>
      <p class="alert alert-warning">
          Misc Utilities
      </p>
    </li>
    <li>
      <h3><strong>1. Get Timezones</strong></h3>
      <div class="panel panel-primary">
        <div class="panel-heading">GET  <?php echo $base_url; ?>timezones</div>
        <div class="panel-body">
          <p>Returns an array of timezones with GMT offsets</p>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Parameter (required/optional)</th>
                <th>Summary</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <div class="panel-footer">
          Example JSON Response
          <pre>
            {
              "timezones": {
                "Pacific/Midway": "(GMT-11:00) Midway Island",
                "US/Samoa": "(GMT-11:00) Samoa",
                "US/Hawaii": "(GMT-10:00) Hawaii",
                "US/Alaska": "(GMT-09:00) Alaska",
                "US/Pacific": "(GMT-08:00) Pacific Time (US &amp; Canada)",
                "America/Tijuana": "(GMT-08:00) Tijuana",
                ...
              }
            }

            STATUS_CODE: 200 OK
          </pre>
        </div>
      </div>
    </li>

    <li>
      <h3><strong>2. Get help - <em>Auth required</em></strong></h3>
      <div class="panel panel-primary">
        <div class="panel-heading">POST  <?php echo $base_url; ?>support/help</div>
        <div class="panel-body">
          <p>Submit a support ticket</p>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Parameter (required/optional)</th>
                <th>Summary</th>
              </tr>
              <tr>
                <td>body</td>
                <th>ticket body. required, max: 500 characters</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <div class="panel-footer">
          Example JSON Response
          <pre>
            { "message":"Help ticket submitted" }

            STATUS_CODE: 200 OK
          </pre>
        </div>
      </div>
    </li>

    
  </ul>
</section>
<hr />