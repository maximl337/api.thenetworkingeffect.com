<section id="testimonials">
              <h2 class="sub-header">Testimonials</h2>
            
              <ul>
                <li>
                  <h3><strong>1. Get testimonial</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>testimonials/{id}</div>
                    <div class="panel-body">
                      <p>Get a single testimonial</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id (required)</td>
                            <td>ID of the testimonial</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "id": 1,
                          "author_id": 30,
                          "subject_id": 34,
                          "body": "Fuga aut corrupti officiis rerum inventore ut veniam pariatur. Aut quaerat aliquam est reprehenderit. Natus reprehenderit eos minima ratione illum provident et. Voluptas aut in nihil et.",
                          "approved": 1,
                          "created_at": "2015-07-06 20:12:13",
                          "updated_at": "2015-07-06 20:12:13",
                          "author": {
                            "user_id": 30,
                            "first_name": "Jarrod",
                            "last_name": "Mills",
                            "job_title": "laborum",
                            "business_name": "Gleichner Inc",
                            "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?77438",
                            "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?55077",
                            "network": false
                          }
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>
                <li>
                  <h3><strong>1. Add testimonial</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>testimonials</div>
                    <div class="panel-body">
                      <p>Add a testimonial</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>subject_id (required)</td>
                            <td>ID of user that will receive the testimonial</td>
                          </tr>
                          <tr>
                            <td>body (required)</td>
                            <td>body of testimonial</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Testimonial Created",
                          "data": {
                            "testimonial": {
                              "subject_id": "1",
                              "body": "testimonial body",
                              "author_id": 51,
                              "updated_at": "2015-07-06 15:16:34",
                              "created_at": "2015-07-06 15:16:34",
                              "id": 257
                            }
                          }
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>2.Get testimonials</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>user/testimonials</div>
                    <div class="panel-body">
                      <p>Get all testimonials received by authenticated user</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "id": 3,
                            "author_id": 14,
                            "subject_id": 51,
                            "body": "Et et debitis voluptas ad. Deserunt vel et velit tenetur modi aliquam voluptas nemo. Laudantium vel temporibus minima nobis officia adipisci at.",
                            "approved": 0,
                            "created_at": "2015-07-06 20:12:13",
                            "updated_at": "2015-07-06 20:12:13",
                            "author": {
                              "user_id": 14,
                              "first_name": "Guadalupe",
                              "last_name": "Mraz",
                              "job_title": "saepe",
                              "business_name": "Heller LLC",
                              "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?21198",
                              "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?76346",
                              "network": false
                            }
                          },
                          {
                            "id": 41,
                            "author_id": 44,
                            "subject_id": 51,
                            "body": "Odit deleniti non mollitia consectetur quia aliquam excepturi. Ullam enim cupiditate explicabo nesciunt id accusamus dolores. Commodi maxime labore architecto voluptas dolor.",
                            "approved": 1,
                            "created_at": "2015-07-06 20:12:13",
                            "updated_at": "2015-07-06 20:12:13",
                            "author": {
                              "user_id": 44,
                              "first_name": "Lindsey",
                              "last_name": "Hoeger",
                              "job_title": "laborum",
                              "business_name": "Hyatt, Satterfield and Dietrich",
                              "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?60729",
                              "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?61087",
                              "network": false
                            }
                          }
                        ]

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>3.Get approved testimonials</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>user/testimonials/approved</div>
                    <div class="panel-body">
                      <p>Get all testimonials received by authenticated user that are approved</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "id": 41,
                            "author_id": 44,
                            "subject_id": 51,
                            "body": "Odit deleniti non mollitia consectetur quia aliquam excepturi. Ullam enim cupiditate explicabo nesciunt id accusamus dolores. Commodi maxime labore architecto voluptas dolor.",
                            "approved": 1,
                            "created_at": "2015-07-06 20:12:13",
                            "updated_at": "2015-07-06 20:12:13",
                            "author": {
                              "user_id": 44,
                              "first_name": "Lindsey",
                              "last_name": "Hoeger",
                              "job_title": "laborum",
                              "business_name": "Hyatt, Satterfield and Dietrich",
                              "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?60729",
                              "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?61087",
                              "network": false
                            }
                          }
                        ]

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>4.Get unapproved testimonials</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>user/testimonials/unapproved</div>
                    <div class="panel-body">
                      <p>Get all testimonials received by authenticated user that are not approved</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "id": 3,
                            "author_id": 14,
                            "subject_id": 51,
                            "body": "Et et debitis voluptas ad. Deserunt vel et velit tenetur modi aliquam voluptas nemo. Laudantium vel temporibus minima nobis officia adipisci at.",
                            "approved": 0,
                            "created_at": "2015-07-06 20:12:13",
                            "updated_at": "2015-07-06 20:12:13",
                            "author": {
                              "user_id": 14,
                              "first_name": "Guadalupe",
                              "last_name": "Mraz",
                              "job_title": "saepe",
                              "business_name": "Heller LLC",
                              "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?21198",
                              "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?76346",
                              "network": false
                            }
                          }
                        ]

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>5.Get another users testimonials</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>users/{id}/testimonials</div>
                    <div class="panel-body">
                      <p>Get all approved testimonials of user</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>Id of user</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "id": 30,
                            "author_id": 2,
                            "subject_id": 4,
                            "body": "Nesciunt dolore nostrum nemo culpa et deleniti quisquam velit. Quidem vel aut est assumenda earum blanditiis voluptas. Repudiandae excepturi rem error enim eos. Debitis non tempora incidunt.",
                            "approved": 1,
                            "created_at": "2015-07-06 20:12:13",
                            "updated_at": "2015-07-06 20:12:13",
                            "author": {
                              "user_id": 2,
                              "first_name": "Nicole",
                              "last_name": "Emmerich",
                              "job_title": "et",
                              "business_name": "Daniel PLC",
                              "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?97106",
                              "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?88492",
                              "network": true
                            }
                          }
                        ]

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>
                
                <li>
                  <h3><strong>6.Unapprove a testimonial</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>testimonials/{id}/unapprove</div>
                    <div class="panel-body">
                      <p>Unapprove a testimonial. Will return 403 if authenticated user is not the subject of a testimonial</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>Id of testimonial</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Testimonial unapproved"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>7. Update Testimonial</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>testimonials/{id}</div>
                    <div class="panel-body">
                      <p>Update a testimonial. Will return 403 if user tries to update an "approved" testimonial</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>Id of testimonial</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Testimonial updated",
                          "testimonial": {
                            "id": 257,
                            "author_id": 51,
                            "subject_id": 1,
                            "body": "testimonial body update",
                            "approved": 0,
                            "created_at": "2015-07-06 15:16:34",
                            "updated_at": "2015-07-06 15:32:34"
                          }
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>8. Delete Testimonial</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">DELETE  <?php echo $base_url; ?>testimonials/{id}</div>
                    <div class="panel-body">
                      <p>Delete a testimonial</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>Id of testimonial</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Testimonial Deleted"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>8. Mark testimonial as seen</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>testimonials/{id}/seen</div>
                    <div class="panel-body">
                      <p>Marks a testimonial as seen</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>Id of testimonial</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Testimonial updated"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>
              </ul>
            </section>
            <hr />