<section id="authentication">
              <h2 class="sub-header">Authentication</h2>

              <ul>
                
                <li>
                  <p class="alert alert-warning">
                    Important: The Networking Effect API uses <a href="http://jwt.io/">JWT</a> to authenticate.
                  </p>
                  
                  <p class="alert alert-warning">
                    All Resouces that require an authentication token are marked as <em>Auth Required</em>.
                    You can pass the token as a parameter in the url: eg: <em><?php echo $base_url; ?>user/update?<strong>token=&ltJWT_TOKEN&gt</strong></em>
                  </p>

                  <p class="alert alert-warning">
                    You can request a token with the <a href="">login</a> route. The registration route also returns a token for the newly created user. (see example json response).

                  </p>
                </li>

                <li>
                  <h3><strong>1. Registration</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>auth/register</div>
                    <div class="panel-body">
                      <p>Register a new user. If the given address does not yield geo coordinates and 'lat', 'lng' are given, these will be used as users primary and roaming coordinates.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>email (required)</td>
                            <td>User email. Must be unique</td>
                          </tr>
                          <tr>
                            <td>password (required)</td>
                            <td>User password</td>
                          </tr>
                          <tr>
                            <td>first_name (required)</td>
                            <td>Users first name</td>
                          </tr>
                          <tr>
                            <td>last_name (required)</td>
                            <td>Users last_name</td>
                          </tr>
                          <tr>
                            <td>job_title (required)</td>
                            <td>Users Job title</td>
                          </tr>
                           <tr>
                            <td>business_category (required)</td>
                            <td>users business category</td>
                          </tr>
                          <tr>
                            <td>business_name (required)</td>
                            <td>users business name</td>
                          </tr>     
                           <tr>
                            <td>street</td>
                            <td>users street address</td>
                          </tr>
                          <tr>
                            <td>city</td>
                            <td>users city</td>
                          </tr>
                          <tr>
                            <td>state</td>
                            <td>users state</td>
                          </tr>
                          <tr>
                            <td>device_token</td>
                            <td>Devices registration id (ANDROID/IOS)</td>
                          </tr>
                          <tr>
                            <td>lat</td>
                            <td>users current latitude</td>
                          </tr>
                          <tr>
                            <td>lng</td>
                            <td>users current longitude</td>
                          </tr>
                          <tr>
                            <td>avatar</td>
                            <td>url to users picture</td>
                          </tr>
                          <tr>
                            <td>logo</td>
                            <td>url to users business logo</td>
                          </tr>
                          
                          <tr>
                            <td>phone_1</td>
                            <td>Users phone #</td>
                          </tr>
                          <tr>
                            <td>phone_1</td>
                            <td>Users phone #</td>
                          </tr>
                          <tr>
                            <td>fax</td>
                            <td>Users fax #</td>
                          </tr>
                          <tr>
                            <td>about_me</td>
                            <td>Users bio</td>
                          </tr>
                          <tr>
                            <td>about_business</td>
                            <td>Users company bio</td>
                          </tr>
                          <tr>
                            <td>website url</td>
                            <td>url to users picture</td>
                          </tr>
                          <tr>
                            <td>facebook_url</td>
                            <td>url to users facebook profile</td>
                          </tr>
                          <tr>
                            <td>twitter_url</td>
                            <td>url to users twitter profile</td>
                          </tr>
                          <tr>
                            <td>googleplus_url</td>
                            <td>url to users google+ profile</td>
                          </tr>
                          <tr>
                            <td>linkedin_url</td>
                            <td>url to users linkedin profile</td>
                          </tr>
                          <tr>
                            <td>youtube_url</td>
                            <td>url to users youtube channel</td>
                          </tr>
                          <tr>
                            <td>blog_url</td>
                            <td>url to users blog</td>
                          </tr>
                         
                          <tr>
                            <td>zip_code</td>
                            <td>users zip code</td>
                          </tr>
                          <tr>
                            <td>country</td>
                            <td>users country</td>
                          </tr>
                      
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          'message': 'User Created'

                          'data': {
                              'user': {
                                'id': 1,
                                'first_name': 'test test',
                                ...
                              },
                              'token': 'JWT_TOKEN'
                          }
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>2. Login</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>auth/login</div>
                    <div class="panel-body">
                      <p>Login to get auth token</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>email (required)</td>
                            <td>User email</td>
                          </tr>
                          <tr>
                            <td>password (required)</td>
                            <td>User password</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          
                        'user': {
                                'id': 1,
                                'first_name': 'test test',
                                ...
                              },

                        'token': 'JWT_TOKEN'
                          
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>3. Social login/registration</strong></h3>
                  <p class="alert alert-warning">
                    This resource will create a user if the email does not exists, or simply return a token if one does. 
                  </p>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>auth/social</div>
                    <div class="panel-body">
                      <p>Login to get auth token</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>email (required)</td>
                            <td>User email. Must be unique</td>
                          </tr>
                          <tr>
                            <td>first_name (required)</td>
                            <td>Users first name</td>
                          </tr>
                          <tr>
                            <td>last_name (required)</td>
                            <td>Users last_name</td>
                          </tr>
                          <tr>
                            <td>provider (required)</td>
                            <td>social provider ('facebook', 'google', 'linkedin')</td>
                          </tr>
                          <tr>
                            <td>device_token</td>
                            <td>Devices registration id (ANDROID/IOS)</td>
                          </tr>
                          <tr>
                            <td>lat</td>
                            <td>Users current latitude</td>
                          </tr>
                          <tr>
                            <td>lng</td>
                            <td>Users current longitude</td>
                          </tr>
                          <tr>
                            <td>avatar</td>
                            <td>url to users picture</td>
                          </tr>
                          <tr>
                            <td>logo</td>
                            <td>url to users busines logo</td>
                          </tr>
                          <tr>
                            <td>job_title</td>
                            <td>Users Job title</td>
                          </tr>
                          <tr>
                            <td>phone_1</td>
                            <td>Users phone #</td>
                          </tr>
                          <tr>
                            <td>phone_1</td>
                            <td>Users phone #</td>
                          </tr>
                          <tr>
                            <td>fax</td>
                            <td>Users fax #</td>
                          </tr>
                          <tr>
                            <td>about_me</td>
                            <td>Users bio</td>
                          </tr>
                          <tr>
                            <td>about_business</td>
                            <td>Users company bio</td>
                          </tr>
                          <tr>
                            <td>website url</td>
                            <td>url to users picture</td>
                          </tr>
                          <tr>
                            <td>facebook_url</td>
                            <td>url to users facebook profile</td>
                          </tr>
                          <tr>
                            <td>twitter_url</td>
                            <td>url to users twitter profile</td>
                          </tr>
                          <tr>
                            <td>googleplus_url</td>
                            <td>url to users google+ profile</td>
                          </tr>
                          <tr>
                            <td>linkedin_url</td>
                            <td>url to users linkedin profile</td>
                          </tr>
                          <tr>
                            <td>youtube_url</td>
                            <td>url to users youtube channel</td>
                          </tr>
                          <tr>
                            <td>blog_url</td>
                            <td>url to users blog</td>
                          </tr>
                          <tr>
                            <td>street</td>
                            <td>users street address</td>
                          </tr>
                          <tr>
                            <td>city</td>
                            <td>users city</td>
                          </tr>
                          <tr>
                            <td>state</td>
                            <td>users state</td>
                          </tr>
                          <tr>
                            <td>zip_code</td>
                            <td>users zip code</td>
                          </tr>
                          <tr>
                            <td>country</td>
                            <td>users country</td>
                          </tr>
                          <tr>
                            <td>business_category</td>
                            <td>users business category</td>
                          </tr>
                          <tr>
                            <td>business_name</td>
                            <td>users business name</td>
                          </tr>        
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          
                        'user': {
                                'id': 1,
                                'first_name': 'test test',
                                ...
                              },

                        'token': 'JWT_TOKEN'
                          
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>4. Refresh Token</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>auth/refreshToken</div>
                    <div class="panel-body">
                      <p>Refresh an expired token</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>token (required)</td>
                            <td>expired token</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "token": "eyJhbGciOiJIUzI1NiJ9.ey..."
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>5. Check if email exists</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>auth/emailExists</div>
                    <div class="panel-body">
                      <p>Check if an email exists</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>email (required)</td>
                            <td>email to be validated</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "email_exists": true
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>6. Password Reset Request</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>password/email?json=true</div>
                    <div class="panel-body">
                      <p>Request a password reset</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>email (required)</td>
                            <td>Email of user requesting password reset</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        { "message":"Password reset email sent" }

                        <p class="alert alert-warning">
                          The Link Sent in email:
                          https://www.thenetworkingeffect.com/forgot_password.php?token=TOKEN_HERE
                        </p>
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>7. Password Reset</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>password/reset?json=true</div>
                    <div class="panel-body">
                      <p>Reset password with token</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>email (required)</td>
                            <td>Email of user requesting password reset</td>
                          </tr>
                          <tr>
                            <td>password (required)</td>
                            <td>New Password</td>
                          </tr>
                          <tr>
                            <td>password_confirmation (required)</td>
                            <td>New Password repeated</td>
                          </tr>
                          <tr>
                            <td>token</td>
                            <td>Token sent in mail as part of link</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Password reset successfully"
                        }
                      </pre>
                    </div>
                  </div>
                </li>

                

              </ul>
            </section>
            <hr />