<section  id="notifications">
              <h2 class="sub-header">Notifications</h2>
            
              <ul>
                <li>
                  <h3><strong>1. Get notifications</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>notifications</div>
                    <div class="panel-body">
                      <p>1. Gets users notifications - sorted by Unseen-latest before seen-latest</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>limit</td>
                            <td>limit the number of results. Default is 50</td>
                          </tr>
                          <tr>
                            <td>offset</td>
                            <td>Offset number of results</td>
                          </tr>
                          <tr>
                            <td>filter</td>
                            <td>Notification category. Available filters:
                            <br />
                            <ul>
                              <li>messages</li>
                              <li>testimonials</li>
                              <li>invites</li>
                              <li>referred</li>
                              <li>reminders</li>
                            </ul></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "notifications": [
                            {
                              "id": 2,
                              "actor_id": 31,
                              "subject_id": 52,
                              "object_id": 2,
                              "notification_type_id": 7,
                              "seen": 0,
                              "created_at": "2015-10-30 14:15:36",
                              "updated_at": "2015-10-30 14:15:36",
                              "sender": {
                                "id": 31,
                                "first_name": "Jovani",
                                "last_name": "Howe",
                                "avatar": "http://lorempixel.com/500/500/people/?76987",
                                "network": false
                              },
                              "object": {
                                "id": 2,
                                "name": "Rerum exercitationem aut quia deleniti officiis velit ea.",
                                "description": "Ut officiis cumque eius numquam. Repellat delectus aut earum dolor eum mollitia. Ut deserunt dicta sequi necessitatibus id.",
                                "banner_url": null,
                                "event_date": "2016-02-23",
                                "start_time": "16:49:59",
                                "end_time": "13:18:20",
                                "timezone": "Asia/Ho_Chi_Minh",
                                "street": "75888 Herman Pines",
                                "city": "Murphyborough",
                                "state": "Texas",
                                "zip_code": null,
                                "country": null,
                                "latitude": "-42.734514",
                                "longitude": "-147.961751",
                                "venue": null,
                                "url": null,
                                "user_id": 2,
                                "permalink": null,
                                "constant_contact_event_id": null,
                                "deleted_at": null,
                                "created_at": "2015-10-30 14:15:05",
                                "updated_at": "2015-10-30 14:15:05",
                                "original_id": null
                              },
                              "message": "Rerum exercitationem aut quia deleniti officiis velit ea. has been canceled"
                            },
                            ...
                            ]
                          }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>2. Update notifications</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>notifications/{id}</div>
                    <div class="panel-body">
                      <p>Mark a notification as seen</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id (required)</td>
                            <td>id of the notification to update</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                         "message" => "notification upated" 
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>3. Mark all as seen</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>notifications/markAllAsSeen</div>
                    <div class="panel-body">
                      <p>Mark all user notifications as seen</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message":"30 notifications updated"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>4. Get count of unseen notifications</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>notifications/getCountOfUnseen</div>
                    <div class="panel-body">
                      <p>Get a count of unseen user notificaitons</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                        
                          "total": 12

                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>4. Get count of unseen notifications</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">DELETE  <?php echo $base_url; ?>notifications/{id}</div>
                    <div class="panel-body">
                      <p>Delete a notificaiton by id</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>'id'</td>
                            <td>Id of notification to delete.</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Notification deleted"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

              </ul>
            </section>
            <hr />