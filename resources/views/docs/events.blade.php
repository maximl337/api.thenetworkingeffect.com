<section id="events">
              <h2 class="sub-header">Events</h2>
            
              <ul>
                <li>
                  <p class="alert alert-warning">
                      Client should fetch from algolia for "searching" and performance.
                  </p>
                </li>
                <li>
                  <h3><strong>1. Get events</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>events</div>
                    <div class="panel-body">
                      <p>Returns paginated result of events.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>limit</td>
                            <td>pagination limit. (default 20)</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "total": 51,
                          "per_page": 20,
                          "current_page": 1,
                          "last_page": 3,
                          "next_page_url": "http:\\/\\/localhost:8888\\/api\\/v1\\/events\\/?page=2",
                          "prev_page_url": null,
                          "from": 1,
                          "to": 20,
                          "data": [
                            {
                              "id": 1,
                              "name": "abc",
                              ...
                            },
                            {
                              "id": 2,
                              "name": "xyz"
                            }
                          ]
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>2. Event</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>events/{id}</div>
                    <div class="panel-body">
                      <p>Get a single event record</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id (required)</td>
                            <td>event id</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "data": {
                            "id": 7,
                            "name": "Ipsam est et rerum nisi et omnis.",
                            "description": "Beatae deserunt dolores ipsum ut veritatis sint excepturi. Veritatis eum ut aliquid id non et et qui. Sit dolor numquam modi eveniet velit.",
                            "banner_url": null,
                            "event_date": "2016-01-15",
                            "start_time": "18:19:46",
                            "end_time": "22:57:34",
                            "timezone": "America\\/Anchorage",
                            "street": "4679 Lula View",
                            "city": "South Elnoraport",
                            "state": "Tennessee",
                            "zip_code": null,
                            "country": null,
                            "latitude": "-11.407429",
                            "longitude": "4.992901",
                            "venue": null,
                            "url": null,
                            "user_id": 7,
                            "deleted_at": null,
                            "created_at": "2015-07-17 19:58:19",
                            "updated_at": "2015-07-17 19:58:19",
                            "attending": true,
                            "users": [
                              {
                                "first_name": "Gunner",
                                "last_name": "Weissnat",
                                "business_name": "Altenwerth-Rohan",
                                "job_title": "assumenda",
                                "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?25859",
                                "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?70789",
                                "tags": [
                                  "ut",
                                  "sint",
                                  "asperiores"
                                ],
                                "city": "Dooleyberg",
                                "state": "Massachusetts",
                                "country": "Honduras",
                                "id": 7,
                                "website_url": "http:\\/\\/www.Farrell.com\\/voluptatem-magnam-et-eos",
                                "facebook_url": "http:\\/\\/Ortiz.info\\/",
                                "twitter_url": "http:\\/\\/www.Towne.net\\/error-minima-necessitatibus-enim-occaecati",
                                "googleplus_url": "http:\\/\\/Rowe.org\\/voluptate-minus-non-qui-sint-est-dolores",
                                "linkedin_url": "http:\\/\\/www.Orn.biz\\/",
                                "youtube_url": "http:\\/\\/Crooks.com\\/",
                                "blog_url": "http:\\/\\/Kessler.com\\/",
                                "about_me": "Dormouse's place, and Alice looked all round her at the Mouse's tail; 'but why do you know what to beautify is, I can't be Mabel, for I know is, something comes at me like a snout than a pig, and.",
                                "about_business": "Alice. 'I'm glad I've seen that done,' thought Alice. The poor little feet, I wonder if I might venture to go after that savage Queen: so she began nibbling at the Cat's head with great curiosity,.",
                                "phone_1": "+46(9)1105782167",
                                "phone_2": "+61(8)0568807552",
                                "fax": "1-188-505-1100",
                                "street": "604 Meghan Cove Suite 635",
                                "zip_code": "Massachusetts",
                                "business_category": "maxime",
                                "distance": 10773.76,
                                "network": false
                              },
                              {
                                "first_name": "Bertha",
                                "last_name": "Gulgowski",
                                "business_name": "Herzog Inc",
                                "job_title": "quo",
                                "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?74305",
                                "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?30681",
                                "tags": [
                                  "quasi",
                                  "veritatis",
                                  "accusamus"
                                ],
                                "city": "South Delilah",
                                "state": "New Jersey",
                                "country": "Macedonia",
                                "id": 35,
                                "website_url": "https:\\/\\/www.Kuhlman.com\\/odio-ipsum-sapiente-amet",
                                "facebook_url": "http:\\/\\/www.Hane.com\\/",
                                "twitter_url": "http:\\/\\/www.Mertz.com\\/quo-suscipit-quidem-mollitia-facere-culpa-blanditiis",
                                "googleplus_url": "http:\\/\\/Hayes.com\\/doloremque-voluptatem-voluptatem-dignissimos-aut-aliquam-sunt-animi",
                                "linkedin_url": "http:\\/\\/www.Cronin.biz\\/",
                                "youtube_url": "http:\\/\\/Herzog.com\\/",
                                "blog_url": "http:\\/\\/www.Raynor.com\\/corrupti-qui-ut-fuga-quidem-vero-totam-necessitatibus.html",
                                "about_me": "I can find them.' As she said this, she came suddenly upon an open place, with a round face, and large eyes full of the earth. At last the Mouse, frowning, but very politely: 'Did you say pig, or.",
                                "about_business": "Alice added as an explanation; 'I've none of YOUR adventures.' 'I could tell you my history, and you'll understand why it is right?' 'In my youth,' said the Mouse, turning to the general conclusion,.",
                                "phone_1": "148-800-1933x529",
                                "phone_2": "(587)262-7179x256",
                                "fax": "(836)388-8451x40619",
                                "street": "4948 Caesar Pine",
                                "zip_code": "New Jersey",
                                "business_category": "inventore",
                                "distance": 6959.71,
                                "network": false
                              },
                              {
                                "first_name": "Angad",
                                "last_name": "Dubey",
                                "business_name": null,
                                "job_title": null,
                                "avatar": null,
                                "logo": null,
                                "tags": [
                                  "dolor",
                                  "commodi",
                                  "et"
                                ],
                                "city": null,
                                "state": null,
                                "country": null,
                                "id": 51,
                                "website_url": null,
                                "facebook_url": null,
                                "twitter_url": null,
                                "googleplus_url": null,
                                "linkedin_url": null,
                                "youtube_url": null,
                                "blog_url": null,
                                "about_me": null,
                                "about_business": null,
                                "phone_1": null,
                                "phone_2": null,
                                "fax": null,
                                "street": null,
                                "zip_code": null,
                                "business_category": null,
                                "distance": "",
                                "network": false
                              }
                            ],
                            "tags": [
                              {
                                "id": 43,
                                "name": "nam",
                                "created_at": "2015-07-17 19:58:21",
                                "updated_at": "2015-07-17 19:58:21",
                                "pivot": {
                                  "event_id": 7,
                                  "tag_id": 43,
                                  "created_at": "2015-07-17 19:58:22",
                                  "updated_at": "2015-07-17 19:58:22"
                                }
                              },
                              {
                                "id": 20,
                                "name": "est",
                                "created_at": "2015-07-17 19:58:21",
                                "updated_at": "2015-07-17 19:58:21",
                                "pivot": {
                                  "event_id": 7,
                                  "tag_id": 20,
                                  "created_at": "2015-07-17 19:58:22",
                                  "updated_at": "2015-07-17 19:58:22"
                                }
                              },
                              {
                                "id": 62,
                                "name": "laudantium",
                                "created_at": "2015-07-17 19:58:21",
                                "updated_at": "2015-07-17 19:58:21",
                                "pivot": {
                                  "event_id": 7,
                                  "tag_id": 62,
                                  "created_at": "2015-07-17 19:58:22",
                                  "updated_at": "2015-07-17 19:58:22"
                                }
                              }
                            ]
                          }
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>3. Event</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>events/{id}/public</div>
                    <div class="panel-body">
                      <p>Get a single event record. No auth required</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id (required)</td>
                            <td>event id</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "data": {
                            "id": 7,
                            "name": "Ipsam est et rerum nisi et omnis.",
                            "description": "Beatae deserunt dolores ipsum ut veritatis sint excepturi. Veritatis eum ut aliquid id non et et qui. Sit dolor numquam modi eveniet velit.",
                            "banner_url": null,
                            "event_date": "2016-01-15",
                            "start_time": "18:19:46",
                            "end_time": "22:57:34",
                            "timezone": "America\\/Anchorage",
                            "street": "4679 Lula View",
                            "city": "South Elnoraport",
                            "state": "Tennessee",
                            "zip_code": null,
                            "country": null,
                            "latitude": "-11.407429",
                            "longitude": "4.992901",
                            ...
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>4. Event by Permalink</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>events/name/{permalink}</div>
                    <div class="panel-body">
                      <p>Get a single event record by permalink. No auth required</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>permalink (required)</td>
                            <td>event permalink</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "data": {
                            "id": 7,
                            "name": "Ipsam est et rerum nisi et omnis.",
                            "description": "Beatae deserunt dolores ipsum ut veritatis sint excepturi. Veritatis eum ut aliquid id non et et qui. Sit dolor numquam modi eveniet velit.",
                            "banner_url": null,
                            "event_date": "2016-01-15",
                            "start_time": "18:19:46",
                            "end_time": "22:57:34",
                            "timezone": "America\\/Anchorage",
                            "street": "4679 Lula View",
                            "city": "South Elnoraport",
                            "state": "Tennessee",
                            "zip_code": null,
                            "country": null,
                            "latitude": "-11.407429",
                            "longitude": "4.992901",
                            ...
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>5. Create event</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>events</div>
                    <div class="panel-body">
                      <p>Create an event</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>name (required)</td>
                            <td>Name of the event.</td>
                          </tr>
                          <tr>
                            <td>description (required)</td>
                            <td>Event information.</td>
                          </tr>
                          <tr>
                            <td>event_date (required)</td>
                            <td>Date of the event. (Type: DATE)</td>
                          </tr>
                          <tr>
                            <td>start_time (required)</td>
                            <td>Time the event starts. (Type: TIME)</td>
                          </tr>
                          <tr>
                            <td>end_time (required)</td>
                            <td>Time the event ends. (Type: TIME)</td>
                          </tr>
                          <tr>
                            <td>timezone (required)</td>
                            <td>Timezone of the event.</td>
                          </tr>
                          <tr>
                            <td>street (required)</td>
                            <td>Events street address</td>
                          </tr>
                          <tr>
                            <td>city (required)</td>
                            <td>Events city</td>
                          </tr>
                          <tr>
                            <td>state (required)</td>
                            <td>Events state</td>
                          </tr>
                          <tr>
                            <td>zip_code</td>
                            <td>users zip code</td>
                          </tr>
                          <tr>
                            <td>country</td>
                            <td>Events country</td>
                          </tr>
                          <tr>
                            <td>venue</td>
                            <td>Venue name of Event</td>
                          </tr>
                          <tr>
                            <td>banner_image</td>
                            <td>Event banner image. Image file < 20 MB</td>
                          </tr>   
                          <tr>
                            <td>url</td>
                            <td>Event url</td>
                          </tr>   
                          <tr>
                            <td>tags (required)</td>
                            <td>array of tag <strong>strings</strong> (API will handle sanitization and saving)</td>
                          </tr>   
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                         {
                          'message': 'Event Created'

                          'data': {
                              'event': {
                                'id': 1,
                                'name': 'Awesome Event',
                                ...
                              }
                          }
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>6. Update event</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>events/{id}</div>
                    <div class="panel-body">
                      <p>Update an event. Will return 403 if accessed by non-owner of event.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>name (required)</td>
                            <td>Name of the event.</td>
                          </tr>
                          <tr>
                            <td>description (required)</td>
                            <td>Event information.</td>
                          </tr>
                          <tr>
                            <td>event_date (required)</td>
                            <td>Date of the event. (Type: DATE)</td>
                          </tr>
                          <tr>
                            <td>start_time (required)</td>
                            <td>Time the event starts. (Type: TIME)</td>
                          </tr>
                          <tr>
                            <td>end_time (required)</td>
                            <td>Time the event ends. (Type: TIME)</td>
                          </tr>
                          <tr>
                            <td>timezone (required)</td>
                            <td>Timezone of the event.</td>
                          </tr>
                          <tr>
                            <td>street (required without venue)</td>
                            <td>Events street address</td>
                          </tr>
                          <tr>
                            <td>city (required)</td>
                            <td>Events city</td>
                          </tr>
                          <tr>
                            <td>state (required)</td>
                            <td>Events state</td>
                          </tr>
                          <tr>
                            <td>zip_code</td>
                            <td>users zip code</td>
                          </tr>
                          <tr>
                            <td>country</td>
                            <td>Events country</td>
                          </tr>
                          <tr>
                            <td>venue (required without street)</td>
                            <td>Venue name of Event</td>
                          </tr>
                          <tr>
                            <td>banner_image (required if Banner url does not exist)</td>
                            <td>Event banner image. Image file < 20 MB</td>
                          </tr>   
                          <tr>
                            <td>url</td>
                            <td>Event url</td>
                          </tr>     
                          <tr>
                            <td>tags (required)</td>
                            <td>array of tag <strong>strings</strong> (API will handle sanitization and saving)</td>
                          </tr> 
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response:
                      <pre>
                        {
                          
                          'message': 'Event updated',
                          'data': {
                            'id': '1',
                            'name': 'Awesome event',
                            ...
                          }
                      
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>7. Delete event</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">DELETE  <?php echo $base_url; ?>events/{id}/delete</div>
                    <div class="panel-body">
                      <p>Delete an event. Will return 403 if accessed by non-owner of event.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>Id of the event</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          
                          'message': 'Event deleted',
                      
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>8. User events</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>user/events</div>
                    <div class="panel-body">
                      <p>Get paginated result of authenticated users events</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "total": 0,
                          "per_page": 20,
                          "current_page": 1,
                          "last_page": 0,
                          "next_page_url": null,
                          "prev_page_url": null,
                          "from": 1,
                          "to": 0,
                          "data": [
                            {
                              "id": 1,
                              "name": "abc",
                              ...
                            },
                            {
                              "id": 2,
                              "name": "xyz"
                            }
                          ]
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>
            
                <li>
                  <h3><strong>9. User events attending</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>user/events/attending</div>
                    <div class="panel-body">
                      <p>Get paginated result of events authenticated users is attending</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>'past_events'</td>
                            <td>Pass as true to get past events</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "total": 0,
                          "per_page": 20,
                          "current_page": 1,
                          "last_page": 0,
                          "next_page_url": null,
                          "prev_page_url": null,
                          "from": 1,
                          "to": 0,
                          "data": [
                            {
                              "id": 1,
                              "name": "abc",
                              ...
                            },
                            {
                              "id": 2,
                              "name": "xyz"
                            }
                          ]
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>10. Event users</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>events/{id}/users</div>
                    <div class="panel-body">
                      <p>Get paginated result of users attending an event</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>Id of event</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "total": 0,
                          "per_page": 20,
                          "current_page": 1,
                          "last_page": 0,
                          "next_page_url": null,
                          "prev_page_url": null,
                          "from": 1,
                          "to": 0,
                          "data": [
                            {
                              "id": 1,
                              "first_name": "abc",
                              "tags": [
                                {
                                  "name": "quam",
                                  "pivot": {
                                    "user_id": 16,
                                    "tag_id": 41,
                                    "created_at": "2015-10-30 14:15:35",
                                    "updated_at": "2015-10-30 14:15:35"
                                  }
                                },
                                {
                                  "name": "dolorem",
                                  "pivot": {
                                    "user_id": 16,
                                    "tag_id": 101,
                                    "created_at": "2015-10-30 14:15:35",
                                    "updated_at": "2015-10-30 14:15:35"
                                  }
                                },
                                {
                                  "name": "nihil",
                                  "pivot": {
                                    "user_id": 16,
                                    "tag_id": 72,
                                    "created_at": "2015-10-30 14:15:35",
                                    "updated_at": "2015-10-30 14:15:35"
                                  }
                                }
                              ],
                              ...
                            },
                            {
                              "id": 2,
                              "first_name": "xyz"
                            }
                          ]
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>11. Events users</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>events/users/attending</div>
                    <div class="panel-body">
                      <p>Get users attending multiple events</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>event_ids (required)</td>
                            <td>array of event ids</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "first_name": "Karine",
                            "last_name": "Haag",
                            "business_name": "Goodwin-Marks",
                            "job_title": "fugit",
                            "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?80367",
                            "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?31954",
                            "tags": [
                              "velit",
                              "non",
                              "corporis"
                            ],
                            "city": "McGlynnmouth",
                            "state": "Minnesota",
                            "country": "Reunion",
                            "id": 7,
                            "network": false
                          },
                          {
                            "first_name": "Katlynn",
                            "last_name": "Moore",
                            "business_name": "Zboncak Group",
                            "job_title": "veniam",
                            "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?19142",
                            "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?54757",
                            "tags": [
                              "magni",
                              "vel",
                              "possimus"
                            ],
                            "city": "Lake Dorotheamouth",
                            "state": "Washington",
                            "country": "Liechtenstein",
                            "id": 23,
                            "network": false
                          },
                          {
                            "first_name": "Catalina",
                            "last_name": "Kuhlman",
                            "business_name": "Lind, Hintz and Stanton",
                            "job_title": "sed",
                            "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?51537",
                            "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?91490",
                            "tags": [
                              "nisi",
                              "saepe",
                              "voluptas"
                            ],
                            "city": "Zoiefort",
                            "state": "Kentucky",
                            "country": "Mauritius",
                            "id": 46,
                            "network": false
                          }
                        ]

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>12. Join Event</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>events/{id}/join</div>
                    <div class="panel-body">
                      <p>Adds the authenticated user to an event</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>Id of event</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message" => "Joined event"
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>13. Leave Event</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>events/{id}/leave</div>
                    <div class="panel-body">
                      <p>Removes the authenticated user from an event</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>Id of event</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message" => "Left event"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>14. Add User to Event</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>events/{id}/addUser</div>
                    <div class="panel-body">
                      <p>Adds user to an event. Will return 403 if accessed by non-owner of event.Will return 500 (23000 UNIQUE KEY CONSTRAIN) if user_id/event_id combination record already exists</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>user_id</td>
                            <td>Id of user to add to an event</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message" => "User added to event"
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>15. Remove User from Event</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>events/{id}/removeUser</div>
                    <div class="panel-body">
                      <p>Adds user to an event. Will return 403 if accessed by non-owner of event.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>user_id</td>
                            <td>Id of user to remove from an event</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message" => "User removed"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

              </ul>
            </section>
            <hr />