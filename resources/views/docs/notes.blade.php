<section id="notes">
              <h2 class="sub-header">Notes</h2>
            
              <ul>
                <li>
                  <h3><strong>1. Get users notes by subject id</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>users/{id}/notes</div>
                    <div class="panel-body">
                      <p>Get users notes by subject id</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "id": 51,
                            "author_id": 51,
                            "subject_id": 1,
                            "body": "test",
                            "created_at": "-0001-11-30 00:00:00",
                            "updated_at": "-0001-11-30 00:00:00",
                            "subject": {
                              "user_id": 1,
                              "first_name": "Domenick",
                              "last_name": "Walter",
                              "job_title": "nihil",
                              "business_name": "Ankunding Group",
                              "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?22187",
                              "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?68707",
                              "network": true
                            }
                          },
                          {
                            "id": 55,
                            "author_id": 51,
                            "subject_id": 1,
                            "body": "test 2",
                            "created_at": "-0001-11-30 00:00:00",
                            "updated_at": "-0001-11-30 00:00:00",
                            "subject": {
                              "user_id": 1,
                              "first_name": "Domenick",
                              "last_name": "Walter",
                              "job_title": "nihil",
                              "business_name": "Ankunding Group",
                              "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?22187",
                              "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?68707",
                              "network": true
                            }
                          }
                        ]

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>2. Get notes</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>notes</div>
                    <div class="panel-body">
                      <p>Get users notes</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "id": 51,
                            "author_id": 51,
                            "subject_id": 1,
                            "body": "test",
                            "created_at": "-0001-11-30 00:00:00",
                            "updated_at": "-0001-11-30 00:00:00",
                            "subject": {
                              "user_id": 1,
                              "first_name": "Domenick",
                              "last_name": "Walter",
                              "job_title": "nihil",
                              "business_name": "Ankunding Group",
                              "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?22187",
                              "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?68707",
                              "network": true
                            }
                          },
                          {
                            "id": 52,
                            "author_id": 51,
                            "subject_id": 2,
                            "body": "test",
                            "created_at": "-0001-11-30 00:00:00",
                            "updated_at": "-0001-11-30 00:00:00",
                            "subject": {
                              "user_id": 2,
                              "first_name": "Nicole",
                              "last_name": "Emmerich",
                              "job_title": "et",
                              "business_name": "Daniel PLC",
                              "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?97106",
                              "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?88492",
                              "network": true
                            }
                          },
                          {
                            "id": 53,
                            "author_id": 51,
                            "subject_id": 3,
                            "body": "test",
                            "created_at": "-0001-11-30 00:00:00",
                            "updated_at": "-0001-11-30 00:00:00",
                            "subject": {
                              "user_id": 3,
                              "first_name": "Nigel",
                              "last_name": "Beatty",
                              "job_title": "nostrum",
                              "business_name": "Blick LLC",
                              "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?14016",
                              "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?84917",
                              "network": true
                            }
                          },
                          {
                            "id": 54,
                            "author_id": 51,
                            "subject_id": 15,
                            "body": "test",
                            "created_at": "-0001-11-30 00:00:00",
                            "updated_at": "-0001-11-30 00:00:00",
                            "subject": {
                              "user_id": 15,
                              "first_name": "Nikki",
                              "last_name": "Jones",
                              "job_title": "in",
                              "business_name": "Gusikowski, Bosco and Schowalter",
                              "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?61687",
                              "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?53641",
                              "network": false
                            }
                          }
                        ]

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>3. Create a note</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>notes</div>
                    <div class="panel-body">
                      <p>Create a new note</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>title (required)</td>
                            <td>title of note</td>
                          </tr>
                          <tr>
                            <td>body (required)</td>
                            <td>body of note</td>
                          </tr>
                          <tr>
                            <td>remind_at</td>
                            <td>reminder timestamp if given. must be in "Y-m-d H:i:s" format</td>
                          </tr>
                          <tr>
                            <td>remind_place</td>
                            <td>reminder place.</td>
                          </tr>
                          <tr>
                            <td>subject_id (required)</td>
                            <td>id of user subject</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Note created",
                          "data": [
                              "note": {
                                "id": 54,
                                "author_id": 1,
                                "subject_id": 51,
                                "body": "Sint veritatis quis delectus.",
                                "created_at": "2015-07-06 18:44:03",
                                "updated_at": "2015-07-06 18:44:03"

                              }
                          ]
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>4. Update a note</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>notes/{id}</div>
                    <div class="panel-body">
                      <p>update a note</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>title (required)</td>
                            <td>title of note</td>
                          </tr>
                          <tr>
                            <td>body (required)</td>
                            <td>body of note</td>
                          </tr>
                          <tr>
                            <td>remind_at</td>
                            <td>reminder timestamp if given. must be in "Y-m-d H:i:s" format</td>
                          </tr>
                          <tr>
                            <td>remind_place</td>
                            <td>reminder place.</td>
                          </tr>
                          <tr>
                            <td>subject_id (required)</td>
                            <td>id of user subject</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Note updated",
                          "data": [
                              "note": {
                                "id": 54,
                                "author_id": 1,
                                "subject_id": 51,
                                "body": "Sint veritatis quis delectus.",
                                "created_at": "2015-07-06 18:44:03",
                                "updated_at": "2015-07-06 18:44:03"

                              }
                          ]
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>
  
                <li>
                  <h3><strong>5. Delete a note</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">DELETE  <?php echo $base_url; ?>notes/{id}</div>
                    <div class="panel-body">
                      <p>delete a note</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>id of note to update</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Note deleted",
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>6. Get a note</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>notes/{id}</div>
                    <div class="panel-body">
                      <p>Gets a single note</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>id of note to update</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                         
                              "note": {
                                "id": 54,
                                "author_id": 1,
                                "subject_id": 51,
                                "body": "Sint veritatis quis delectus.",
                                "created_at": "2015-07-06 18:44:03",
                                "updated_at": "2015-07-06 18:44:03"

                              }
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>7. Mark Note as seen</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>notes/{id}/seen</div>
                    <div class="panel-body">
                      <p>Mark a note as seen</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>id of note to update</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                         
                          "message" => "Note updated"
                          
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>
              </ul>
            </section>
            <hr />