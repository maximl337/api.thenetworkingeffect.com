<section id="opportunity">
              <h2 class="sub-header">Opportunity</h2>
            
              <ul>
                <li>
                  <h3><strong>1. Get opportunities</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>opportunities</div>
                    <div class="panel-body">
                      <p>List of opportunities the authenticated user has collected</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "data": [
                            {
                              "id": 61,
                              "follower_id": 52,
                              "subject_id": 2,
                              "created_at": "2016-04-08 14:56:58",
                              "updated_at": "2016-04-08 14:56:58",
                              "subject": {
                                "id": 2,
                                "first_name": "Rolando",
                                "last_name": "Hegmann",
                                "email": "green.jaeden@cole.info",
                                "provider": null,
                                "avatar": "http://lorempixel.com/500/500/people/?73392",
                                "logo": "http://lorempixel.com/900/300/business/?88376",
                                "job_title": "nulla",
                                "business_name": "Kris, Bergstrom and Trantow",
                                "phone_1": "235.200.5743 x6995",
                                "phone_2": "1-893-805-4494 x991",
                                "fax": "1-437-998-5374",
                                "about_me": "King, and he called the Queen, the royal children, and everybody laughed, 'Let the jury wrote it down 'important,' and some 'unimportant.' Alice could not be denied, so she went back for a minute or.",
                                "about_business": "Just as she could remember about ravens and writing-desks, which wasn't much. The Hatter was out of court! Suppress him! Pinch him! Off with his tea spoon at the corners: next the ten courtiers;.",
                                "website_url": "http://lockman.com/quo-est-velit-officia-esse-ad-earum-ut",
                                "facebook_url": "http://www.stokes.biz/",
                                "twitter_url": "http://www.sanford.com/hic-est-necessitatibus-et-ea-inventore",
                                "googleplus_url": "http://oreilly.com/voluptas-reiciendis-sunt-earum-sit-quae-error-numquam.html",
                                "linkedin_url": "http://kub.com/dolorem-minus-omnis-quas-neque-ut-voluptatibus",
                                "youtube_url": "http://www.koch.com/voluptatem-qui-maiores-sint-rem-voluptas-quasi-deleniti.html",
                                "blog_url": "http://www.christiansen.com/in-sint-earum-omnis-voluptas-rerum-consequuntur-dolore-tempora.html",
                                "street": "81764 Brittany Mills Apt. 764",
                                "city": "West Estevan",
                                "state": "Mississippi",
                                "zip_code": "28554-4797",
                                "country": "Eritrea",
                                "hide_address": 0,
                                "latitude": "32.3546679",
                                "longitude": "-89.3985283",
                                "permalink": null,
                                "is_admin": null,
                                "is_placeholder": 0,
                                "business_category": "et",
                                "association": null,
                                "deleted_at": null,
                                "last_activity": null,
                                "timezone": null,
                                "reminder_notification_time": null,
                                "created_at": "2016-04-08 14:06:46",
                                "updated_at": "2016-04-08 14:06:50",
                                "original_id": null
                              }
                            }
                          ]
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>1.Add an opportunity</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>opportunities</div>
                    <div class="panel-body">
                      <p>Add user as an opportunity</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>subject_id (required)</td>
                            <td>Id of the user to add as an opportunity</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Opportunity added",
                          "data": []
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>1.Remove an opportunity</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">DELETE  <?php echo $base_url; ?>opportunities/{id}</div>
                    <div class="panel-body">
                      <p>Remove user from opportunities</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id (required)</td>
                            <td>Id of the user to remove</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        "Opportunity removed"

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

              </ul>
            </section>
            <hr />