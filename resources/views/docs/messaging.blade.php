<section id="messaging">
              <h2 class="sub-header">Messaging</h2>
            
              <ul>
                <li>
                  <h3><strong>1. Send message</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>messages</div>
                    <div class="panel-body">
                      <p>Send a message to another user</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>to (required)</td>
                            <td>Id of recipient user</td>
                          </tr>
                          <tr>
                            <td>body (required)</td>
                            <td>Message body</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Message sent",
                          "data": {
                            "message": {
                              "to": "2",
                              "body": "this is a test message",
                              "from": 1,
                              "updated_at": "2015-07-06 13:49:42",
                              "created_at": "2015-07-06 13:49:42",
                              "id": 119
                            }
                          }
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>2. Reply to a message</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>messages/{id}/reply</div>
                    <div class="panel-body">
                      <p>Reply to a message</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id (required)</td>
                            <td>id of parent message</td>
                          </tr>
                          <tr>
                            <td>to (required)</td>
                            <td>Id of recipient user</td>
                          </tr>
                          <tr>
                            <td>body (required)</td>
                            <td>Message body</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Reply sent",
                          "data": {
                            "message": {
                              "to": "2",
                              "body": "this is a test message",
                              "from": 1,
                              "updated_at": "2015-07-06 13:49:42",
                              "created_at": "2015-07-06 13:49:42",
                              "id": 119
                            }
                          }
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>3. Mark as seen</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>messages/{id}/seen</div>
                    <div class="panel-body">
                      <p>Mark a message as seen</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id (required)</td>
                            <td>id of message</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message" => "Message updated"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>4. Mark all messages as seen</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>messages/all/seen</div>
                    <div class="panel-body">
                      <p>Mark all authenticated users messages as seen</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message" => "Message updated"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>5. Get inbox</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>user/inbox</div>
                    <div class="panel-body">
                      <p>Get users inbox (summary of all messages).</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "id": 32,
                            "connection_id": 18,
                            "user_id": 51,
                            "last_date": "2015-07-06 20:12:13",
                            "message": "Placeat provident voluptatum tempora perspiciatis aliquam corporis. Recusandae iusto voluptatem dolorum qui. Quo et iusto dolor doloremque.",
                            "connection": {
                              "user_id": 18,
                              "first_name": "Maude",
                              "last_name": "Herman",
                              "job_title": "eligendi",
                              "business_name": "Mante, Gleason and Shields",
                              "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?25285",
                              "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?62033",
                              "network": false
                            }
                          },
                          {
                            "id": 35,
                            "connection_id": 29,
                            "user_id": 51,
                            "last_date": "2015-07-06 20:12:13",
                            "message": "Minus dicta quis magni. Enim voluptas mollitia velit animi. Molestiae iste incidunt necessitatibus error minus blanditiis officia.",
                            "connection": {
                              "user_id": 29,
                              "first_name": "Joan",
                              "last_name": "Powlowski",
                              "job_title": "minima",
                              "business_name": "Kuvalis, Runte and Metz",
                              "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?44132",
                              "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?17055",
                              "network": false
                            }
                          }
                        ]
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>6. Get Conversation/Message thread</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>messages/{connectionId}/conversation</div>
                    <div class="panel-body">
                      <p>Get Conversation/Message thread.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>connectionId (required)</td>
                            <td>Id of the other user in the conversation</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "id": 32,
                            "to": 51,
                            "from": 18,
                            "parent_id": null,
                            "body": "Placeat provident voluptatum tempora perspiciatis aliquam corporis. Recusandae iusto voluptatem dolorum qui. Quo et iusto dolor doloremque.",
                            "seen": 0,
                            "created_at": "2015-07-06 20:12:13",
                            "updated_at": "2015-07-06 20:12:13",
                            "from_user": {
                              "user_id": 18,
                              "first_name": "Maude",
                              "last_name": "Herman",
                              "job_title": "eligendi",
                              "business_name": "Mante, Gleason and Shields",
                              "avatar": "http:\\/\\/lorempixel.com\\/500\\/500\\/people\\/?25285",
                              "logo": "http:\\/\\/lorempixel.com\\/900\\/300\\/business\\/?62033",
                              "network": false
                            }
                          }
                        ]
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>7. Delete message</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">DELETE  <?php echo $base_url; ?>messages/{id}</div>
                    <div class="panel-body">
                      <p>Delete a message.</p>
                      <p class="alert alert-warning">This will hide the message for the user deleting but will still be visible to the other user in the conversation</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>connectionId (required)</td>
                            <td>Id of the message to delete</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message" => "Message deleted"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>8. Delete conversation</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">DELETE  <?php echo $base_url; ?>messages/conversation/{id}</div>
                    <div class="panel-body">
                      <p>Delete a a conversation</p>
                      <p class="alert alert-warning">Delete all messages with the given parent id</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id (required)</td>
                            <td>parent id of the messages to delete</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message" => "Conversation deleted"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>
              </ul>
            </section>
            <hr />
            
            