<section id="tags">
              <h2 class="sub-header">Tags</h2>
            
              <ul>
                <li>
                  <p class="alert alert-warning">
                      Client should fetch from algolia for "searching" and performance.
                  </p>
                </li>
                <li>
                  <h3><strong>1. Get tags</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>tags</div>
                    <div class="panel-body">
                      <p>Returns paginated result of tags.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "total": 51,
                          "per_page": 20,
                          "current_page": 1,
                          "last_page": 3,
                          "next_page_url": "http:\\/\\/localhost:8888\\/api\\/v1\\/tags\\/?page=2",
                          "prev_page_url": null,
                          "from": 1,
                          "to": 20,
                          "data": [
                            {
                              "id": 1,
                              "name": "abc",
                              ...
                            },
                            {
                              "id": 2,
                              "name": "xyz"
                            }
                          ]
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>2. Get Business Categories</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>business_categories</div>
                    <div class="panel-body">
                      <p>Returns paginated result of business categories.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "data": [
                            {
                              "name": "Advertising & Marketing"
                            },
                            ...
                          ]
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>
                <li>
                  <h3><strong>3. Popular tags - <em>Auth Required</em></strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>tags/popular</div>
                    <div class="panel-body">
                      <p>Get Tags by popularity</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "tags": [
                            {
                              "id": 15,
                              "name": "et",
                              "created_at": "2015-09-18 14:25:09",
                              "tag_count": 13
                            },
                            {
                              "id": 26,
                              "name": "dolorem",
                              "created_at": "2015-09-18 14:25:09",
                              "tag_count": 11
                            },
                            ...
                          ]
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>
                <li>
                  <h3><strong>4. Association</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>tags/association</div>
                    <div class="panel-body">
                      <p>Get Tags flagged as association</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "tags": [
                            {
                              "id": 122,
                              "name": "Lorem Ipsum"
                            }
                          ]
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>5. Search Assist</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>tags/searchAssist</div>
                    <div class="panel-body">
                      <p>Get Tags flagged as search assist</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "tags": [
                            {
                              "id": 122,
                              "name": "Lorem Ipsum"
                            }
                          ]
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>6. Note Assist</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>tags/noteAssist</div>
                    <div class="panel-body">
                      <p>Get Tags flagged as note assist</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "tags": [
                            {
                              "id": 122,
                              "name": "Lorem Ipsum"
                            }
                          ]
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

              </ul>
            </section>
            <hr />