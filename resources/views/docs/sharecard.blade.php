<section id="card_shares">
              <h2 class="sub-header">Share Card</h2>
            
              <ul>
                <li>
                  <h3><strong>1. Share card to ids</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>cardShare</div>
                    <div class="panel-body">
                      <p>Share the a users card</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>recipient_ids (required | array)</td>
                            <td>Array of user ids to send card to</td>
                          </tr>
                          <tr>
                            <th>subject_id</th>
                            <th>Id of user whose card needs to be shared. If this parameter is not sent, the authenticated users card will be shared</th>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Shared card with 2people",
                          "data": []
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>2. Share card to emails</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>cardShareEmail</div>
                    <div class="panel-body">
                      <p>Share card</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                          <tr>
                            <th>subject_id</th>
                            <th>Id of card owner. If this parameter is not sent, the authenticated users card will be shared</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>recipient_emails (required | array)</td>
                            <td>Array of emails of the recipient to send card to</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": [
                            "1 is not a valid email",
                            "2 is not a valid email",
                            "Card Shared with a@a.com"
                          ],
                          "data": []
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>3. Refer a user to ids</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>refer/ids</div>
                    <div class="panel-body">
                      <p>Share the a users card with other users</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>recipient_ids (required | array)</td>
                            <td>Array of user ids to send card to</td>
                          </tr>
                          <tr>
                            <th>subject_id (required)</th>
                            <th>Id of user whose card needs to be shared.</th>
                          </tr>
                          <tr>
                            <th>message</th>
                            <th>Message to be sent in the email.</th>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Shared card with 2 people",
                          "data": []
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>4. Refer a user to emails</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>refer/emails</div>
                    <div class="panel-body">
                      <p>Share the a users card with other users</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>recipient_emails (required | array)</td>
                            <td>Array of emails to refer user with</td>
                          </tr>
                          <tr>
                            <th>subject_id (required)</th>
                            <th>Id of user whose card needs to be shared.</th>
                          </tr>
                          <tr>
                            <th>message</th>
                            <th>Message to be sent in the email.</th>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "message": "Shared card with joe@example.com",
                          "data": []
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>5. Get sent card shares</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>user/cardShare/sent</div>
                    <div class="panel-body">
                      <p>Get list of card shares sent by authenticated user</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "id": 80,
                            "sender_id": 51,
                            "recipient_id": 1,
                            "recipient_email": null,
                            "subject_id": 51,
                            "created_at": "-0001-11-30 00:00:00",
                            "updated_at": "-0001-11-30 00:00:00"
                          }
                        ]

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>6. Get received card shares</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>user/cardShare/received</div>
                    <div class="panel-body">
                      <p>Get list of card shares received by authenticated user</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        [
                          {
                            "id": 81,
                            "sender_id": 1,
                            "recipient_id": 51,
                            "recipient_email": null,
                            "subject_id": 1,
                            "created_at": "-0001-11-30 00:00:00",
                            "updated_at": "-0001-11-30 00:00:00"
                          }
                        ]

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>
              </ul>
            </section>
            <hr />