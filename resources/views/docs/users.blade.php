<section id="users">
              <h2 class="sub-header">Users</h2>
            
              <ul>
                <li>
                  <p class="alert alert-warning">
                      Client should fetch from algolia for "searching" and performance.
                  </p>
                </li>
                <li>
                  <h3><strong>1. Users</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>users</div>
                    <div class="panel-body">
                      <p>Returns paginated result of users.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>limit</td>
                            <td>pagination limit. (default 20)</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "total": 51,
                          "per_page": 20,
                          "current_page": 1,
                          "last_page": 3,
                          "next_page_url": "http:\\/\\/localhost:8888\\/api\\/v1\\/users\\/?page=2",
                          "prev_page_url": null,
                          "from": 1,
                          "to": 20,
                          "data": [
                            {
                              "id": 1,
                              "first_name": "abc",
                              ...
                            },
                            {
                              "id": 2,
                              "first_name": "xyz"
                            }
                          ]
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>2. User</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>users/{id}</div>
                    <div class="panel-body">
                      <p>Get a single user record</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id (required)</td>
                            <td>user id</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          
                          'data': 
                          {
                            'id': 1,
                            'first_name': 'test test',
                            ...
                          }
                          
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>
                
                <li>
                  <h3><strong>3. User by Permalink</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>users/name/{permalink}</div>
                    <div class="panel-body">
                      <p>Get a single user record</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id (required)</td>
                            <td>user id</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          
                          'data': 
                          {
                            'id': 1,
                            'first_name': 'test test',
                            ...
                          }
                          
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>4. Update user</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>user/update</div>
                    <div class="panel-body">
                      <p>Updates the users information. If the given address does not yield geo coordinates and 'lat', 'lng' are given, these will be used as users primary and roaming coordinates.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>email</td>
                            <td>User email. Must be unique</td>
                          </tr>
                          <tr>
                            <td>password</td>
                            <td>User password</td>
                          </tr>
                          <tr>
                            <td>first_name</td>
                            <td>Users first name | min. 1 character long</td>
                          </tr>
                          <tr>
                            <td>last_name</td>
                            <td>Users last_name | min. 1 character long</td>
                          </tr>
                          <tr>
                            <td>job_title</td>
                            <td>Users Job title | min. 1 character long</td>
                          </tr>
                          <tr>
                            <td>hide_address - Boolean</td>
                            <td>Hide/Unhide users address</td>
                          </tr>
                          <tr>
                            <td>street</td>
                            <td>users street address</td>
                          </tr>
                          <tr>
                            <td>city</td>
                            <td>users city</td>
                          </tr>
                          <tr>
                            <td>state</td>
                            <td>users state</td>
                          </tr>
                          <tr>
                            <td>tags</td>
                            <td>array of tag <strong>strings</strong> (API will handle sanitization and saving)</td>
                          </tr>  
                          <tr>
                            <td>business_category</td>
                            <td>users business category</td>
                          </tr>
                          <tr>
                            <td>business_name</td>
                            <td>users business name | min. 1 character long</td>
                          </tr>
                          <tr>
                            <td>device_token</td>
                            <td>Devices registration id (ANDROID/IOS)</td>
                          </tr>
                          <tr>
                            <td>lat</td>
                            <td>users current latitude</td>
                          </tr>
                          <tr>
                            <td>lng</td>
                            <td>users current longitude</td>
                          </tr>

                          <tr>
                            <td>avatar</td>
                            <td>Image file. Must be filetype Image. < 20MB</td>
                          </tr>
                          <tr>
                            <td>logo</td>
                            <td>Image file. Must be filetype Image. < 20MB</td>
                          </tr>
                          
                          <tr>
                            <td>phone_1</td>
                            <td>Users phone #</td>
                          </tr>
                          <tr>
                            <td>phone_1</td>
                            <td>Users phone #</td>
                          </tr>
                          <tr>
                            <td>fax</td>
                            <td>Users fax #</td>
                          </tr>
                          <tr>
                            <td>about_me</td>
                            <td>Users bio</td>
                          </tr>
                          <tr>
                            <td>about_business</td>
                            <td>Users company bio</td>
                          </tr>
                          <tr>
                            <td>website url</td>
                            <td>url to users picture</td>
                          </tr>
                          <tr>
                            <td>facebook_url</td>
                            <td>url to users facebook profile</td>
                          </tr>
                          <tr>
                            <td>twitter_url</td>
                            <td>url to users twitter profile</td>
                          </tr>
                          <tr>
                            <td>googleplus_url</td>
                            <td>url to users google+ profile</td>
                          </tr>
                          <tr>
                            <td>linkedin_url</td>
                            <td>url to users linkedin profile</td>
                          </tr>
                          <tr>
                            <td>youtube_url</td>
                            <td>url to users youtube channel</td>
                          </tr>
                          <tr>
                            <td>blog_url</td>
                            <td>url to users blog</td>
                          </tr>
                          
                          <tr>
                            <td>zip_code</td>
                            <td>users zip code</td>
                          </tr>
                          <tr>
                            <td>country</td>
                            <td>users country</td>
                          </tr>
                          <tr>
                            <td>partner</td>
                            <td>Parter to attach</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          
                          'data' => {

                              'message' => 'User Updated',
                              'user': {
                                  'id': 1,
                                  'first_name': 'test test',
                                  'distance': 19 // Kilometers
                                  ...
                                }
                          }
                          
                          
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>5. Delete User</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">DELETE  <?php echo $base_url; ?>user/delete</div>
                    <div class="panel-body">
                      <p>Delete the authenticated user. This is a soft-delete.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          
                          'message': 'User deleted'
                      
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                 <li>
                  <h3><strong>6. Roaming Coordinates</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>user/roaming</div>
                    <div class="panel-body">
                      <p>Add Users Roaming lat/long coordinates</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>latitude</td>
                            <td>Latitude of current location</td>
                          </tr>
                          <tr>
                            <td>longitude</td>
                            <td>Longitude of current location</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          
                          'message': 'Roaming data added'
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>7. User Device Token</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>user/device</div>
                    <div class="panel-body">
                      <p>Send a device registration ID to enable push notification for user</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                            <tr>
                              <td>device_token</td>
                              <td>User device register id</td>
                            </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          
                          'message': 'User deleted'
                      
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>8. Update Email</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>user/email</div>
                    <div class="panel-body">
                      <p>Update Users email.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>email</td>
                            <td>required, unique</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        { "Email updated" }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>9. Update user password</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>user/password</div>
                    <div class="panel-body">
                      <p>Update password</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>password</td>
                            <td>min 6 characters</td>
                          </tr>
                          <tr>
                            <td>password_confirmation</td>
                            <td>min 6 characters. Must be same as 'password'</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "Password updated"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>10. Update user timezone</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>user/timezone</div>
                    <div class="panel-body">
                      <p>Update password</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>timezone</td>
                            <td>Must be a valid timezone see: http://php.net/manual/en/timezones.php</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "User timezone updated"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>11. Update user reminder notification time</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>user/reminderNotificationTime</div>
                    <div class="panel-body">
                      <p>Update password</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>reminder_notification_time</td>
                            <td>time of notificaiton. Format must be "00:00:00" mysql TIME equivalent</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "Sorting reminder notifcation time updated"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>


              </ul>
            </section>

             <hr />