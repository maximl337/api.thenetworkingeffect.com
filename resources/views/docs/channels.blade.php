<section id="channels">
  <h2 class="sub-header">Channels</h2>

  <ul>
    <li>
      <h3><strong>1. Get Channels</strong> - <em>Auth required</em></h3>
      <div class="panel panel-primary">
        <div class="panel-heading">GET  <?php echo $base_url; ?>channels</div>
        <div class="panel-body">
          <p>Returns paginated result of articles.</p>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Parameter (required/optional)</th>
                <th>Summary</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>limit</td>
                <td>pagination limit. (default 20)</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="panel-footer">
          Example JSON Response
          <pre>
            {
              "total": 51,
              "per_page": 20,
              "current_page": 1,
              "last_page": 3,
              "next_page_url": "http:\\/\\/localhost:8888\\/api\\/v1\\/articles\\/?page=2",
              "prev_page_url": null,
              "from": 1,
              "to": 20,
              "data": [
                {
                  "id": 1,
                  "title": "abc",
                  ...
                },
                {
                  "id": 2,
                  "title": "xyz"
                }
              ]
            }

            STATUS_CODE: 200 OK
          </pre>
        </div>
      </div>
    </li>
  </ul>
</section>
<hr />