<section id="legal">
              <h2 class="sub-header">Legal</h2>
            
              <ul>
                <li>
                  <h3><strong>1. Get Terms and conditions</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>legal/termsAndConditions</div>
                    <div class="panel-body">
                      <p>Get terms and conditions</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "id": 2,
                          "body": "Terms and conditions",
                          "created_at": "2015-07-06 20:12:14",
                          "updated_at": "-0001-11-30 00:00:00"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>1. Get Privacy Policy</strong></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>legal/privacyPolicy</div>
                    <div class="panel-body">
                      <p>Get privacy policy</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "id": 1,
                          "body": "Privacy Policy",
                          "created_at": "-0001-11-30 00:00:00",
                          "updated_at": "-0001-11-30 00:00:00"
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>
              </ul>
            </section>