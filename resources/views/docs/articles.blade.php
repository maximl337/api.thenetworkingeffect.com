<section id="articles">
              <h2 class="sub-header">Article</h2>
            
              <ul>
                <li>
                  <p class="alert alert-warning">
                      Client should fetch from algolia for "searching" and performance.
                  </p>
                </li>
                <li>
                  <h3><strong>1. Get articles</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>articles</div>
                    <div class="panel-body">
                      <p>Returns paginated result of articles.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>limit</td>
                            <td>pagination limit. (default 20)</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "total": 51,
                          "per_page": 20,
                          "current_page": 1,
                          "last_page": 3,
                          "next_page_url": "http:\\/\\/localhost:8888\\/api\\/v1\\/articles\\/?page=2",
                          "prev_page_url": null,
                          "from": 1,
                          "to": 20,
                          "data": [
                            {
                              "id": 1,
                              "title": "abc",
                              ...
                            },
                            {
                              "id": 2,
                              "title": "xyz"
                            }
                          ]
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>2. Get article</strong></em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>articles/{id}</div>
                    <div class="panel-body">
                      <p>Returns a single article.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id</td>
                            <td>id of article to get</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "data": {
                            "id": 2,
                            "title": "Ut sint iusto doloremque qui.",
                            "body": "Voluptatem dolor molestiae maiores minima ipsam. Ut debitis doloribus esse odit. Omnis consequatur consequuntur similique. Quisquam quo molestiae dolor rerum ab rem.",
                            "teaser": "Perferendis quo sunt voluptas aut facere molestias provident.",
                            "user_id": 2,
                            "created_at": "2015-07-03 19:55:36",
                            "updated_at": "2015-07-03 19:55:36"
                          }
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>3. Get article</strong></em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>articles/title/{permalink}</div>
                    <div class="panel-body">
                      <p>Returns a single article.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>permalink</td>
                            <td>permalink of article</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          "data": {
                            "id": 2,
                            "title": "Ut sint iusto doloremque qui.",
                            "body": "Voluptatem dolor molestiae maiores minima ipsam. Ut debitis doloribus esse odit. Omnis consequatur consequuntur similique. Quisquam quo molestiae dolor rerum ab rem.",
                            "teaser": "Perferendis quo sunt voluptas aut facere molestias provident.",
                            "user_id": 2,
                            "created_at": "2015-07-03 19:55:36",
                            "updated_at": "2015-07-03 19:55:36"
                          }
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>


                <li>
                  <h3><strong>4. Create article</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>articles</div>
                    <div class="panel-body">
                      <p>Returns a single article.</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>title (required)</td>
                            <td>title of the article</td>
                          </tr>
                          <tr>
                            <td>body (required)</td>
                            <td>body of the article</td>
                          </tr>
                          <tr>
                            <td>teaser (required)</td>
                            <td>teaser for the article</td>
                          </tr>
                          <tr>
                            <td>tags (required)</td>
                            <td>array of tag <strong>strings</strong> (API will handle sanitization and saving)</td>
                          </tr>   
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          'message': 'Article Created'

                          'data': {
                              'article': {
                                'id': 1,
                                'title': 'Awesome article',
                                ...
                              }
                          }
                        }

                        STATUS_CODE: 201 CREATED
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>5. Update article</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>articles/{id}</div>
                    <div class="panel-body">
                      <p>Update article. will return 403 if user is not the owner of the article</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>title (required)</td>
                            <td>title of the article</td>
                          </tr>
                          <tr>
                            <td>body (required)</td>
                            <td>body of the article</td>
                          </tr>
                          <tr>
                            <td>teaser (required)</td>
                            <td>teaser for the article</td>
                          </tr>
                          <tr>
                            <td>tags (required)</td>
                            <td>array of tag <strong>strings</strong> (API will handle sanitization and saving)</td>
                          </tr>   
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          
                          'message': 'Article updated',
                          'data': {
                            'id': '1',
                            'title': 'Awesome article',
                            ...
                          }
                      
                        }

                        STATUS_CODE: 200 OK
                       
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>6. Delete article</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">DELETE  <?php echo $base_url; ?>articles/{id}</div>
                    <div class="panel-body">
                      <p>Delete article. will return 403 if user is not the owner of the article</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                      
                        {
                          
                          'message': 'Article deleted'
                      
                        }

                        STATUS_CODE: 200 OK
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>7. Update article</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">POST  <?php echo $base_url; ?>articles/{id}</div>
                    <div class="panel-body">
                      <p>Update article. will return 403 if user is not the owner of the article</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>title (required)</td>
                            <td>title of the article</td>
                          </tr>
                          <tr>
                            <td>body (required)</td>
                            <td>body of the article</td>
                          </tr>
                          <tr>
                            <td>teaser (required)</td>
                            <td>teaser for the article</td>
                          </tr>
                          <tr>
                            <td>tags (required)</td>
                            <td>array of tag ids</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                        {
                          
                          'message': 'Article updated',
                          'data': {
                            'id': '1',
                            'title': 'Awesome article',
                            ...
                          }
                      
                        }

                        STATUS_CODE: 200 OK
                       
                      </pre>
                    </div>
                  </div>
                </li>

                <li>
                  <h3><strong>8. Get users articles</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>users/{id}/articles</div>
                    <div class="panel-body">
                      <p>Gets a paginated result of articles of users by id</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>id (required)</td>
                            <td>Id of the user</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                         {
                          "total": 51,
                          "per_page": 20,
                          "current_page": 1,
                          "last_page": 3,
                          "next_page_url": "http:\\/\\/localhost:8888\\/api\\/v1\\/articles\\/?page=2",
                          "prev_page_url": null,
                          "from": 1,
                          "to": 20,
                          "data": [
                            {
                              "id": 1,
                              "title": "abc",
                              ...
                            },
                            {
                              "id": 2,
                              "title": "xyz"
                            }
                          ]
                        }

                        STATUS_CODE: 200 OK
                       
                      </pre>
                    </div>
                  </div>
                </li>
                
                <li>
                  <h3><strong>9. Get logged in users articles</strong> - <em>Auth required</em></h3>
                  <div class="panel panel-primary">
                    <div class="panel-heading">GET  <?php echo $base_url; ?>users/articles</div>
                    <div class="panel-body">
                      <p>Gets a paginated result of articles of authenticated users</p>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Parameter (required/optional)</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table>
                    </div>
                    <div class="panel-footer">
                      Example JSON Response
                      <pre>
                         {
                          "total": 51,
                          "per_page": 20,
                          "current_page": 1,
                          "last_page": 3,
                          "next_page_url": "http:\\/\\/localhost:8888\\/api\\/v1\\/articles\\/?page=2",
                          "prev_page_url": null,
                          "from": 1,
                          "to": 20,
                          "data": [
                            {
                              "id": 1,
                              "title": "abc",
                              ...
                            },
                            {
                              "id": 2,
                              "title": "xyz"
                            }
                          ]
                        }

                        STATUS_CODE: 200 OK
                       
                      </pre>
                    </div>
                  </div>
                </li>
              </ul>
            </section>
            <hr />