@extends('_templates/oauth')

@section('content')

	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			@include('_partials.oauth.forms.login')
		</div>
	</div>

	@include('errors.list')
@endsection