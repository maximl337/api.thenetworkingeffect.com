@extends('_templates.oauth')

@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-primary">
				<div class="panel-heading">
					OAuth access details
				</div>
				<div class="panel-body">
					
					Client ID:
					<p class="well">{{ $user->id }}</p>
					
					Client Secret:
					<p class="well">{{ $user->secret }}</p>

					Grant Type:
					<p class="well">password</p>
				</div>
			</div> <!-- .panel -->

			<div class="panel panel-primary">
				<div class="panel-heading">
					How it works
				</div>
				<div class="panel-body">
					<p>The "<em>password</em>" grant allows you to authenticate users.</p>

					<p>To Authenticate:</p>
					<p class="well">
						POST<br />
						<em>https://thenetworkingeffect.herokuapp.com/api/v1/oauth/access_token</em><br />

						Request: <br />
						{ <br />
							'grant_type' 	=> 'password' <br />
							'client_id' 	=> 'YOUR_CLIENT_ID', <br />
							'client_secret' => 'YOUR_CLIENT_SECRET', <br />
							'username' 		=> 'USER_EMAIL', <br />
							'password'		=> 'USER_PASSWORD' <br />

						} <br />

						Response example: <br />
						{ <br />
						  "access_token": "SAohW2kEVVTNjnHsmi5IUsjgWf8Ndh0wm0XAiIM8", <br />
						  "token_type": "Bearer", <br />
						  "expires_in": 3600, <br />
						  "refresh_token": "2CgpecxxU6Q04Y3p01qZ7290knHpiCSe8UAaYCay" <br />
						}
					</p>

					<p>To Refresh Token:</p>
					<p class="well">
						POST<br />
						<em>https://thenetworkingeffect.herokuapp.com/api/v1/oauth/access_token</em><br />

						Request: <br />
						{ <br />
							'grant_type'	=> 'refresh_token' <br />
							'client_id' 	=> 'YOUR_CLIENT_ID', <br />
							'client_secret' => 'YOUR_CLIENT_SECRET', <br />
							'refresh_token' => 'YOUR_REFRESH_TOKEN' <br />
						} <br />

						Response example: <br />
						{
						  "access_token": "SAohW2kEVVTNjnHsmi5IUsjgWf8Ndh0wm0XAiIM8", <br />
						  "token_type": "Bearer", <br />
						  "expires_in": 3600, <br />
						  "refresh_token": "2CgpecxxU6Q04Y3p01qZ7290knHpiCSe8UAaYCay" <br />
						}
					</p>
				</div>
			</div> <!-- .panel -->

			
		</div>
	</div>
@endsection