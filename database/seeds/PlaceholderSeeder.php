<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class PlaceholderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	DB::table('placeholder_users')->truncate();

    	$faker = Faker::create();

    	$user = App\User::latest()->first();

    	for($i=0;$i<20;$i++) {

    		$data = [
    			'first_name' 	=> $faker->firstName,
    			'last_name'		=> $faker->lastName,
    			'email'			=> $faker->email
    		];

    		$placeholder = App\User::create($data);

    		$placeholder->is_placeholder = true;

    		$placeholder->save();

    		$transformedData = (new App\Transformers\MakePlaceholderTransformer)->transform($data);

            $transformedData['user_id'] = $placeholder->id;

    		$user->ownerPlaceholders()->save(new App\PlaceholderUser($transformedData));
    	}
        
    }
}
