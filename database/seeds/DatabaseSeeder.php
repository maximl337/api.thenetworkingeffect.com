<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // disable foreign key constraints
        $this->call('UserTableSeeder');
        $this->call('TagTableSeeder');
        $this->call('TagUserTableSeeder');
        $this->call('ArticleTagTableSeeder');
        $this->call('EventTagTableSeeder');
        $this->call('EventUserTableSeeder');
        $this->call('MessageTableSeeder');
        $this->call('TestimonialTableSeeder');
        $this->call('NetworkTableSeeder');
        $this->call('NoteTableSeeder');
        //$this->call('RoamingUserTableSeeder');
        $this->call('CardShareTableSeeder');
        $this->call('NotificationTypeTableSeeder');
        $this->call('NotificationTableSeeder');
        $this->call('MandrillTemplateSeeder');
        $this->call('MailUnsubSeeder');
        $this->call('LegalSeeder');
        $this->call('SortingCategoriesTableSeeder');
        $this->call('PlaceholderSeeder');
        $this->call('OpportunitiesTableSeeder');
        $this->call('OAuthClientSeeder');
        $this->call('PartnerTableSeeder');
        $this->call('AssociationKeywordsSeeder');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // enable foreign key constraints

        Model::reguard();
    }
}
