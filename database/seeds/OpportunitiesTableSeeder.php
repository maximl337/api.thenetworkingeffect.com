<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class OpportunitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        DB::table('opportunities')->truncate();

        $userIds = DB::table('users')->lists('id');
        $data = [];

        for($i=0; $i<50; $i++) {

            $randomizedUserIds = $userIds;

            shuffle($randomizedUserIds);

            $data[] = [
                    
                    'follower_id' => array_shift($randomizedUserIds), 
                    'subject_id' => $randomizedUserIds[ count($randomizedUserIds) - 1],
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()
                ];
            
        }

        DB::table('opportunities')->insert($data); 
    }
}
