<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        DB::table('messages')->truncate();

        $userIds = DB::table('users')->lists('id');
        $data = [];

        for($i=0; $i<50; $i++) {

            $randomizedUserIds = $userIds;

            shuffle($randomizedUserIds);

            $data[] = [
                    
                    'from' => array_shift($randomizedUserIds), 
                    'to' => $randomizedUserIds[ count($randomizedUserIds) - 1],
                    'body' => $faker->text,
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now(),
                    'seen' => $faker->boolean
                ];
            
        }

        DB::table('messages')->insert($data); 

    }
}
