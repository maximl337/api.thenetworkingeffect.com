<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MailUnsubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mail_unsubs')->truncate();

        /* SEED PIVOT */
        $templateIds = DB::table('mandrill_templates')->lists('id');
        $userIds = DB::table('users')->lists('id');

        $pivots = [];
        foreach($templateIds as $templateId)
        {
            //necessary since shuffle() and array_shift() take an array by reference
            $randomizedUserIds = $userIds;

            shuffle($randomizedUserIds);
            for($index = 0; $index < 3; $index++) {
                $pivots[] = [
                    
                    'user_id' => array_shift($randomizedUserIds), 
                    'template_id' => $templateId,
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now(),
                ];
            }
        }

        DB::table('mail_unsubs')->insert($pivots); 
        /* EO SEED PIVOT */
    }
}
