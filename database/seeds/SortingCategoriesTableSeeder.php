<?php

use Illuminate\Database\Seeder;

class SortingCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // disable foreign key constraints

        DB::table('sorting_categories')->truncate();

        $data = [];

        $data[] = [
        	'name' => 'Follow-up tomorrow',
        	'reminder_delay_days' => 1,
        	'reminder_time'  => '09:00:00',
        	'reminder_timezone'	=> 'America/Toronto'
        ];

        $data[] = [
        	'name' => 'Follow-up in one week',
        	'reminder_delay_days' => 7,
        	'reminder_time'  => '09:00:00',
        	'reminder_timezone'	=> 'America/Toronto'
        ];

        $data[] = [
        	'name' => 'Research within one week',	
        	'reminder_delay_days' => 7,
        	'reminder_time'  => '09:00:00',
        	'reminder_timezone'	=> 'America/Toronto'
        ];

        DB::table('sorting_categories')->insert($data);

        DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // enable foreign key constraints
    }
}
