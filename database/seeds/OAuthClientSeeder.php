<?php

use Illuminate\Database\Seeder;

class OAuthClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::statement('SET FOREIGN_KEY_CHECKS = 0');

    	DB::table('oauth_clients')->truncate();
    	
      $data = [];

   		for ($i=0; $i < 10; $i++) { 
   			
   			$data[] = [
   					'id' => "id$i",
   					'secret' => "secret$i",
   					'name' => "client$i",
            'email' => "email$i@test.com",
            'password' => bcrypt("password$i")
   				];
   		}

      DB::table('oauth_clients')->insert($data);

      DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // enable foreign key constraints
    }
}
