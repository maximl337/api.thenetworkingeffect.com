<?php

use Illuminate\Database\Seeder;

class MandrillTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // disable foreign key constraints

        DB::table('mandrill_templates')->truncate();

        $data = [
            [
                    
                'label' => 'NEW_MESSAGE', 
                'name'  => '1DNTM1 New Message'
            
            ],

            [
                    
                'label' => 'NEW_FOLLOWER', 
                'name'  => '1DNTN1 Network Add'
            
            ],

            [
                    
                'label' => 'FOLLOWED_BACK', 
                'name'  => '1DNTN2 Network Follow Back'
            
            ],
            
            [
                    
                'label' => 'NEW_CARD_SHARE_RECIPIENT', 
                'name'  => 'TC1 CC Share'

            ],

            [
                    
                'label' => 'NEW_CARD_SHARE_SUBJECT', 
                'name'  => 'MANDRILL NEW CARD SHARE SUBJECT TEMPLATE'

            ],

            [
                    
                'label' => 'NEW_TESTIMONIAL', 
                'name'  => '1DNTT1 New Testimonial'

            ],

            [
                    
                'label' => 'EVENT_UPDATED', 
                'name'  => 'TE1 Event Edit'

            ],

            [

                'label' => 'EVENT_DELETED',
                'name'  => 'TE2 Event Cancel'

            ],

            [
                'label' => 'TNE_MESSAGE',
                'name'  => 'TU1 TNE Update Message'
            ],

            [
                'label' => 'NEW_USER',
                'name'  => 'TS1 Member Signup'
            ],

            [
                'label' => 'EVENT_JOIN',
                'name'  => 'TE3 Event Join'
            ],

            [
                'label' => 'PASSWORD-RESET',
                'name'  => '1DNTP1 Password Reset'
            ],

            [
                'label' => 'FOLLOW-UP_TOMORROW',
                'name'  => 'Sorting Reminder'
            ],

            [
                'label' => 'FOLLOW-UP_IN_ONE_WEEK',
                'name'  => 'Sorting Reminder'
            ],

            [
                'label' => 'RESEARCH_WITHIN_ONE_WEEK',
                'name'  => 'Sorting Reminder'
            ],

            [
                'label' => 'PLACEHOLDER_INVITATION',
                'name'  => '1DNTV1 Placeholder Invite'
            ],

            [
                'label' => 'REFER_A_USER',
                'name'  => '1DNTR1 Referral Sent'
            ],

            [
                'label' => 'YOU_WERE_REFERRED',
                'name'  => 'TR2 Referred by Member'
            ],

            [
                'label' => 'NEW_MENTION',
                'name'  => '1DNT@1 Mentioned'
            ]

        ];

        DB::table('mandrill_templates')->insert($data); 

        DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // enable foreign key constraints

    }
}






