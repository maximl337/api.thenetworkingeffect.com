<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class NotificationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        DB::table('notifications')->truncate();

        $userIds = DB::table('users')->lists('id');

        $notificationTypeIds = DB::table('notification_types')->lists('id');

        $data = [];

        for($i=1; $i<51; $i++) {

            $randomizedUserIds = $userIds;
            
            $randomizedNotificationTypeIds = $notificationTypeIds;

            shuffle($randomizedUserIds);

            shuffle($randomizedNotificationTypeIds);

            $data[] = [

                'actor_id' => array_shift($userIds),
                'subject_id' => $randomizedUserIds[ count($randomizedUserIds) - 1],
                'object_id' => $i,
                'notification_type_id' => array_shift($randomizedNotificationTypeIds),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];

        }

        DB::table('notifications')->insert($data);
        
    }
}
