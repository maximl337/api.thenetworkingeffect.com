<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class RoamingUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        DB::table('roaming_users')->truncate();

        $userIds = DB::table('users')->lists('id');
        $data = [];

        for($i=0; $i<50; $i++) {

            $randomizedUserIds = $userIds;

            shuffle($randomizedUserIds);

            $data[] = [
                    
                    'user_id' => array_shift($randomizedUserIds), 
                    'latitude' => $faker->latitude,
                    'longitude' => $faker->longitude,
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()
                ];
            
        }

        DB::table('roaming_users')->insert($data);
    }
}
