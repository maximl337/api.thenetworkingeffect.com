<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // disable foreign key constraints

        DB::table('users')->truncate();

        // Init Account
        $data[] = [
            'email'             => 'system@thenetworkingeffect.com',
            'password'          => bcrypt('n3tw0rk1ng'),
            'first_name'        => 'System',
            'last_name'         => 'TNE',
            'avatar'            => 'http://i.imgur.com/n8hcJj2.jpg',
            'logo'              => 'http://i.imgur.com/916ucEd.jpg',
            'business_name'     => 'The Networking Effect',
            'job_title'         => 'Administrator',
            'about_me'          => 'TNE system admin',
            'about_business'    => 'TNE system admin',
            'business_category' => 'admin',
            'remember_token'    => str_random(10),
            'street'            => '92 caplan ave',
            'city'              => 'barrie',
            'zip_code'          => '',
            'state'             => 'ON'
        ];

        DB::table('users')->insert($data); 

        DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // enable foreign key constraints
    }
}
