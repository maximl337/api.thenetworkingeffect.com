<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CardShareTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('card_shares')->truncate();

        $userIds = DB::table('users')->lists('id');
        $data = [];

        for($i=0; $i<50; $i++) {

            $randomizedUserIds = $userIds;

            shuffle($randomizedUserIds);

            $data[] = [
                    
                    'sender_id' => array_shift($randomizedUserIds), 
                    'recipient_id' => $randomizedUserIds[ count($randomizedUserIds) - 1],
                    'subject_id' => $randomizedUserIds[ count($randomizedUserIds) - 4],
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()
                ];
            
        }

        DB::table('card_shares')->insert($data);
    }
}
