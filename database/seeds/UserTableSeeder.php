<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Services\GeoService;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        $geoService = new GeoService;

        DB::table('users')->truncate();
        DB::table('articles')->truncate();
        DB::table('events')->truncate();
        
        factory('App\User', 50)
                ->create()
                ->each(function($u) {

                    $u->articles()->save(factory('App\Article')->make());
                    
                    $u->my_events()->save(factory('App\Event')->make());

                });

        // Init Account
        $data[] = [
            'email'             => 'system@thenetworkingeffect.com',
            'password'          => bcrypt('n3tw0rk1ng2015#'),
            'first_name'        => 'System',
            'last_name'         => 'TNE',
            'avatar'            => $faker->imageUrl(500, 500, 'people'),
            'logo'              => $faker->imageUrl(900, 300, 'business'),
            'business_name'     => $faker->company,
            'job_title'         => $faker->word,
            'phone_1'           => $faker->phoneNumber,
            'phone_2'           => $faker->phoneNumber,
            'fax'               => $faker->phoneNumber,
            'about_me'          => $faker->realText,
            'about_business'    => $faker->realText,
            'website_url'       => $faker->url,
            'facebook_url'      => $faker->url,
            'twitter_url'       => $faker->url,
            'googleplus_url'    => $faker->url,
            'linkedin_url'      => $faker->url,
            'youtube_url'       => $faker->url,
            'blog_url'          => $faker->url,
            'business_category' => $faker->word,
            'remember_token'    => str_random(10),
            'street'            => '92 caplan ave',
            'city'              => 'barrie',
            'zip_code'          => '',
            'state'             => 'ON'
        ];

        $data[] = [
            'email'             => 'angad_dubey@hotmail.com',
            'password'          => bcrypt('test123'),
            'first_name'        => 'Angad',
            'last_name'         => 'Dubey',
            'avatar'            => $faker->imageUrl(500, 500, 'people'),
            'logo'              => $faker->imageUrl(900, 300, 'business'),
            'business_name'     => $faker->company,
            'job_title'         => $faker->word,
            'phone_1'           => $faker->phoneNumber,
            'phone_2'           => $faker->phoneNumber,
            'fax'               => $faker->phoneNumber,
            'about_me'          => $faker->realText,
            'about_business'    => $faker->realText,
            'website_url'       => $faker->url,
            'facebook_url'      => $faker->url,
            'twitter_url'       => $faker->url,
            'googleplus_url'    => $faker->url,
            'linkedin_url'      => $faker->url,
            'youtube_url'       => $faker->url,
            'blog_url'          => $faker->url,
            'business_category' => $faker->word,
            'remember_token'    => str_random(10),
            'street'            => '92 caplan ave',
            'city'              => 'barrie',
            'zip_code'          => '',
            'state'             => 'ON'
        ];

        $data[] = [
            'email'             => 'gary@linkweb.ca',
            'password'          => bcrypt('squirrel'),
            'first_name'        => 'Gary',
            'last_name'         => 'Oosterhuis',
            'avatar'            => $faker->imageUrl(500, 500, 'people'),
            'logo'              => $faker->imageUrl(900, 300, 'business'),
            'business_name'     => $faker->company,
            'job_title'         => $faker->word,
            'phone_1'           => $faker->phoneNumber,
            'phone_2'           => $faker->phoneNumber,
            'fax'               => $faker->phoneNumber,
            'about_me'          => $faker->realText,
            'about_business'    => $faker->realText,
            'website_url'       => $faker->url,
            'facebook_url'      => $faker->url,
            'twitter_url'       => $faker->url,
            'googleplus_url'    => $faker->url,
            'linkedin_url'      => $faker->url,
            'youtube_url'       => $faker->url,
            'blog_url'          => $faker->url,
            'business_category' => $faker->word,
            'remember_token'    => str_random(10),
            'street'            => '92 caplan ave',
            'city'              => 'barrie',
            'zip_code'          => '',
            'state'             => 'ON'
        ];

        $data[] = [
            'email'             => 'claudiatest@thenetworkingeffect.com',
            'password'          => bcrypt('test'),
            'first_name'        => 'Claudia',
            'last_name'         => 'Test',
            'avatar'            => $faker->imageUrl(500, 500, 'people'),
            'logo'              => $faker->imageUrl(900, 300, 'business'),
            'business_name'     => $faker->company,
            'job_title'         => $faker->word,
            'phone_1'           => $faker->phoneNumber,
            'phone_2'           => $faker->phoneNumber,
            'fax'               => $faker->phoneNumber,
            'about_me'          => $faker->realText,
            'about_business'    => $faker->realText,
            'website_url'       => $faker->url,
            'facebook_url'      => $faker->url,
            'twitter_url'       => $faker->url,
            'googleplus_url'    => $faker->url,
            'linkedin_url'      => $faker->url,
            'youtube_url'       => $faker->url,
            'blog_url'          => $faker->url,
            'business_category' => $faker->word,
            'remember_token'    => str_random(10),
            'street'            => '92 caplan ave',
            'city'              => 'barrie',
            'zip_code'          => '',
            'state'             => 'ON'
        ];

        $data[] = [
            'email'             => $faker->email,
            'password'          => bcrypt('test'),
            'first_name'        => $faker->firstName,
            'last_name'         => $faker->word,
            'avatar'            => $faker->imageUrl(500, 500, 'people'),
            'logo'              => $faker->imageUrl(900, 300, 'business'),
            'business_name'     => $faker->company,
            'job_title'         => $faker->word,
            'phone_1'           => $faker->phoneNumber,
            'phone_2'           => $faker->phoneNumber,
            'fax'               => $faker->phoneNumber,
            'about_me'          => $faker->realText,
            'about_business'    => $faker->realText,
            'website_url'       => $faker->url,
            'facebook_url'      => $faker->url,
            'twitter_url'       => $faker->url,
            'googleplus_url'    => $faker->url,
            'linkedin_url'      => $faker->url,
            'youtube_url'       => $faker->url,
            'blog_url'          => $faker->url,
            'business_category' => $faker->word,
            'remember_token'    => str_random(10),
            'street'            => 'Atlantis Pavillions - 955 Lake Shore Blvd W',
            'city'              => 'Toronto',
            'zip_code'          => 'M6K 3B9',
            'state'             => 'ON'
        
        ];

        $data[] = [
            'email'             => $faker->email,
            'password'          => bcrypt('test'),
            'first_name'        => $faker->firstName,
            'last_name'         => $faker->word,
            'avatar'            => $faker->imageUrl(500, 500, 'people'),
            'logo'              => $faker->imageUrl(900, 300, 'business'),
            'business_name'     => $faker->company,
            'job_title'         => $faker->word,
            'phone_1'           => $faker->phoneNumber,
            'phone_2'           => $faker->phoneNumber,
            'fax'               => $faker->phoneNumber,
            'about_me'          => $faker->realText,
            'about_business'    => $faker->realText,
            'website_url'       => $faker->url,
            'facebook_url'      => $faker->url,
            'twitter_url'       => $faker->url,
            'googleplus_url'    => $faker->url,
            'linkedin_url'      => $faker->url,
            'youtube_url'       => $faker->url,
            'blog_url'          => $faker->url,
            'business_category' => $faker->word,
            'remember_token'    => str_random(10),
            'street'            => 'ACC Toronto - 40 Bay St',
            'city'              => 'Toronto',
            'zip_code'          => 'M5J 2X2',
            'state'             => 'ON'
        
        ];

        $data[] = [
            'email'             => $faker->email,
            'password'          => bcrypt('test'),
            'first_name'        => $faker->firstName,
            'last_name'         => $faker->word,
            'avatar'            => $faker->imageUrl(500, 500, 'people'),
            'logo'              => $faker->imageUrl(900, 300, 'business'),
            'business_name'     => $faker->company,
            'job_title'         => $faker->word,
            'phone_1'           => $faker->phoneNumber,
            'phone_2'           => $faker->phoneNumber,
            'fax'               => $faker->phoneNumber,
            'about_me'          => $faker->realText,
            'about_business'    => $faker->realText,
            'website_url'       => $faker->url,
            'facebook_url'      => $faker->url,
            'twitter_url'       => $faker->url,
            'googleplus_url'    => $faker->url,
            'linkedin_url'      => $faker->url,
            'youtube_url'       => $faker->url,
            'blog_url'          => $faker->url,
            'business_category' => $faker->word,
            'remember_token'    => str_random(10),
            'street'            => 'Sheraton Toronto Airport -  801 Dixon Rd',
            'city'              => 'Toronto',
            'zip_code'          => 'M9W 1J5',
            'state'             => 'ON'
        
        ];

        $data[] = [
            'email'             => $faker->email,
            'password'          => bcrypt('test'),
            'first_name'        => $faker->firstName,
            'last_name'         => $faker->word,
            'avatar'            => $faker->imageUrl(500, 500, 'people'),
            'logo'              => $faker->imageUrl(900, 300, 'business'),
            'business_name'     => $faker->company,
            'job_title'         => $faker->word,
            'phone_1'           => $faker->phoneNumber,
            'phone_2'           => $faker->phoneNumber,
            'fax'               => $faker->phoneNumber,
            'about_me'          => $faker->realText,
            'about_business'    => $faker->realText,
            'website_url'       => $faker->url,
            'facebook_url'      => $faker->url,
            'twitter_url'       => $faker->url,
            'googleplus_url'    => $faker->url,
            'linkedin_url'      => $faker->url,
            'youtube_url'       => $faker->url,
            'blog_url'          => $faker->url,
            'business_category' => $faker->word,
            'remember_token'    => str_random(10),
            'street'            => 'Scarborough Town Centre - 300 Borough Drive',
            'city'              => 'Scarborough',
            'zip_code'          => 'M1P 4P5',
            'state'             => 'ON'
        
        ];

        $data[] = [
            'email'             => $faker->email,
            'password'          => bcrypt('test'),
            'first_name'        => $faker->firstName,
            'last_name'         => $faker->word,
            'avatar'            => $faker->imageUrl(500, 500, 'people'),
            'logo'              => $faker->imageUrl(900, 300, 'business'),
            'business_name'     => $faker->company,
            'job_title'         => $faker->word,
            'phone_1'           => $faker->phoneNumber,
            'phone_2'           => $faker->phoneNumber,
            'fax'               => $faker->phoneNumber,
            'about_me'          => $faker->realText,
            'about_business'    => $faker->realText,
            'website_url'       => $faker->url,
            'facebook_url'      => $faker->url,
            'twitter_url'       => $faker->url,
            'googleplus_url'    => $faker->url,
            'linkedin_url'      => $faker->url,
            'youtube_url'       => $faker->url,
            'blog_url'          => $faker->url,
            'business_category' => $faker->word,
            'remember_token'    => str_random(10),
            'street'            => 'Mississauga Convention Centre - 75 Derry Rd W',
            'city'              => 'Mississauga',
            'zip_code'          => 'L5W 1G3',
            'state'             => 'ON'
        
        ];

        $data[] = [
            'email'             => $faker->email,
            'password'          => bcrypt('test'),
            'first_name'        => $faker->firstName,
            'last_name'         => $faker->word,
            'avatar'            => $faker->imageUrl(500, 500, 'people'),
            'logo'              => $faker->imageUrl(900, 300, 'business'),
            'business_name'     => $faker->company,
            'job_title'         => $faker->word,
            'phone_1'           => $faker->phoneNumber,
            'phone_2'           => $faker->phoneNumber,
            'fax'               => $faker->phoneNumber,
            'about_me'          => $faker->realText,
            'about_business'    => $faker->realText,
            'website_url'       => $faker->url,
            'facebook_url'      => $faker->url,
            'twitter_url'       => $faker->url,
            'googleplus_url'    => $faker->url,
            'linkedin_url'      => $faker->url,
            'youtube_url'       => $faker->url,
            'blog_url'          => $faker->url,
            'business_category' => $faker->word,
            'remember_token'    => str_random(10),
            'street'            => 'Rogers Centre - 1 Blue Jays Way',
            'city'              => 'Toronto',
            'zip_code'          => 'M5V 1J1',
            'state'             => 'ON'
        
        ];

        $data[] = [
            'email'             => $faker->email,
            'password'          => bcrypt('test'),
            'first_name'        => $faker->firstName,
            'last_name'         => $faker->word,
            'avatar'            => $faker->imageUrl(500, 500, 'people'),
            'logo'              => $faker->imageUrl(900, 300, 'business'),
            'business_name'     => $faker->company,
            'job_title'         => $faker->word,
            'phone_1'           => $faker->phoneNumber,
            'phone_2'           => $faker->phoneNumber,
            'fax'               => $faker->phoneNumber,
            'about_me'          => $faker->realText,
            'about_business'    => $faker->realText,
            'website_url'       => $faker->url,
            'facebook_url'      => $faker->url,
            'twitter_url'       => $faker->url,
            'googleplus_url'    => $faker->url,
            'linkedin_url'      => $faker->url,
            'youtube_url'       => $faker->url,
            'blog_url'          => $faker->url,
            'business_category' => $faker->word,
            'remember_token'    => str_random(10),
            'street'            => 'ROM - 100 Queens Park',
            'city'              => 'Toronto',
            'zip_code'          => 'M5S 2C6',
            'state'             => 'ON'
        
        ];

        $data[] = [
            'email'             => $faker->email,
            'password'          => bcrypt('test'),
            'first_name'        => $faker->firstName,
            'last_name'         => $faker->word,
            'avatar'            => $faker->imageUrl(500, 500, 'people'),
            'logo'              => $faker->imageUrl(900, 300, 'business'),
            'business_name'     => $faker->company,
            'job_title'         => $faker->word,
            'phone_1'           => $faker->phoneNumber,
            'phone_2'           => $faker->phoneNumber,
            'fax'               => $faker->phoneNumber,
            'about_me'          => $faker->realText,
            'about_business'    => $faker->realText,
            'website_url'       => $faker->url,
            'facebook_url'      => $faker->url,
            'twitter_url'       => $faker->url,
            'googleplus_url'    => $faker->url,
            'linkedin_url'      => $faker->url,
            'youtube_url'       => $faker->url,
            'blog_url'          => $faker->url,
            'business_category' => $faker->word,
            'remember_token'    => str_random(10),
            'street'            => 'Metro Toronto Convention Centre - 222 Bremner Blvd',
            'city'              => 'Toronto',
            'zip_code'          => 'M5V 3L9',
            'state'             => 'ON'
        
        ];

        $data[] = [
            'email'             => $faker->email,
            'password'          => bcrypt('test'),
            'first_name'        => $faker->firstName,
            'last_name'         => $faker->word,
            'avatar'            => $faker->imageUrl(500, 500, 'people'),
            'logo'              => $faker->imageUrl(900, 300, 'business'),
            'business_name'     => $faker->company,
            'job_title'         => $faker->word,
            'phone_1'           => $faker->phoneNumber,
            'phone_2'           => $faker->phoneNumber,
            'fax'               => $faker->phoneNumber,
            'about_me'          => $faker->realText,
            'about_business'    => $faker->realText,
            'website_url'       => $faker->url,
            'facebook_url'      => $faker->url,
            'twitter_url'       => $faker->url,
            'googleplus_url'    => $faker->url,
            'linkedin_url'      => $faker->url,
            'youtube_url'       => $faker->url,
            'blog_url'          => $faker->url,
            'business_category' => $faker->word,
            'remember_token'    => str_random(10),
            'street'            => 'Delta Toronto East - 2035 Kennedy Road',
            'city'              => 'Toronto',
            'zip_code'          => 'M1T 3G2',
            'state'             => 'ON'
        
        ];

        $data[] = [
            'email'             => $faker->email,
            'password'          => bcrypt('test'),
            'first_name'        => $faker->firstName,
            'last_name'         => $faker->word,
            'avatar'            => $faker->imageUrl(500, 500, 'people'),
            'logo'              => $faker->imageUrl(900, 300, 'business'),
            'business_name'     => $faker->company,
            'job_title'         => $faker->word,
            'phone_1'           => $faker->phoneNumber,
            'phone_2'           => $faker->phoneNumber,
            'fax'               => $faker->phoneNumber,
            'about_me'          => $faker->realText,
            'about_business'    => $faker->realText,
            'website_url'       => $faker->url,
            'facebook_url'      => $faker->url,
            'twitter_url'       => $faker->url,
            'googleplus_url'    => $faker->url,
            'linkedin_url'      => $faker->url,
            'youtube_url'       => $faker->url,
            'blog_url'          => $faker->url,
            'business_category' => $faker->word,
            'remember_token'    => str_random(10),
            'street'            => 'Westin Prince Hotel -900 York Mills Rd',
            'city'              => 'Toronto',
            'zip_code'          => 'M3B 3H2',
            'state'             => 'ON'
        
        ];


        DB::table('users')->insert($data); 

        $users = App\User::all();

        DB::table('roaming_users')->truncate();

        foreach($users as $user) {

            // $geoCode = $geoService->getLatLang($user->street, $user->city, $user->state);

            // $user->latitude = $geoCode['latitude'];
            // $user->longitude = $geoCode['longitude'];

            // $user->update();

            // App\Roaming_user::create([
            //         'latitude' => $geoCode['latitude'],
            //         'longitude' => $geoCode['longitude'],
            //         'user_id'   => $user->id
            //     ]);

        }

        // DB::table('users')->truncate();

        // // Init Account
        // $data[] = [
        //     'email'             => 'system@thenetworkingeffect.com',
        //     'password'          => bcrypt('n3tw0rk1ng2015#'),
        //     'first_name'        => 'System',
        //     'last_name'         => 'TNE',
        //     'avatar'            => 'http://i.imgur.com/n8hcJj2.jpg',
        //     'logo'              => 'http://i.imgur.com/916ucEd.jpg',
        //     'business_name'     => 'The Networking Effect',
        //     'job_title'         => 'Administrator',
        //     'about_me'          => 'TNE system admin',
        //     'about_business'    => 'TNE system admin',
        //     'business_category' => 'admin',
        //     'remember_token'    => str_random(10),
        //     'street'            => '92 caplan ave',
        //     'city'              => 'barrie',
        //     'zip_code'          => '',
        //     'state'             => 'ON'
        // ];

        // DB::table('users')->insert($data); 
    }
}
