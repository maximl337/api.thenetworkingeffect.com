<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class NoteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        DB::table('notes')->truncate();

        $userIds = DB::table('users')->lists('id');
        $data = [];

        for($i=0; $i<50; $i++) {

            $randomizedUserIds = $userIds;

            shuffle($randomizedUserIds);

            $data[] = [
                    
                    'author_id' => array_shift($randomizedUserIds), 
                    'subject_id' => $randomizedUserIds[ count($randomizedUserIds) - 1],
                    'title' => $faker->text,
                    'body' => $faker->text,
                    'remind_at' => Carbon::now()->addDay(),
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()
                ];
            
        }

        DB::table('notes')->insert($data);
    }
}
