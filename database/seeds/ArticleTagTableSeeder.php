<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ArticleTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('article_tag')->truncate();

        /* SEED PIVOT */
        $articleIds = DB::table('articles')->lists('id');
        $tagIds = DB::table('tags')->lists('id');

        $pivots = [];
        foreach($articleIds as $articleId)
        {
            //necessary since shuffle() and array_shift() take an array by reference
            $randomizedTagIds = $tagIds;

            shuffle($randomizedTagIds);
            for($index = 0; $index < 3; $index++) {
                $pivots[] = [
                    
                    'tag_id' => array_shift($randomizedTagIds), 
                    'article_id' => $articleId
                ];
            }
        }

        DB::table('article_tag')->insert($pivots); 
        /* EO SEED PIVOT */
    }
}
