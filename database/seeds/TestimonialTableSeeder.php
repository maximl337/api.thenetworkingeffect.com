<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class TestimonialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        DB::table('testimonials')->truncate();

        $userIds = DB::table('users')->lists('id');
        $data = [];

        for($i=0; $i<50; $i++) {

            $randomizedUserIds = $userIds;

            shuffle($randomizedUserIds);

            $data[] = [
                    
                    'author_id' => array_shift($randomizedUserIds), 
                    'subject_id' => $randomizedUserIds[ count($randomizedUserIds) - 1],
                    'body' => $faker->text,
                    'approved' => $faker->boolean,
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()
                ];
            
        }

        DB::table('testimonials')->insert($data); 
    }
}
