<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;

class EventUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        DB::table('event_user')->truncate();

        /* SEED PIVOT */
        $eventIds = DB::table('events')->lists('id');
        $userIds = DB::table('users')->lists('id');

        $pivots = [];
        foreach($eventIds as $eventId)
        {
            //necessary since shuffle() and array_shift() take an array by reference
            $randomizedUserIds = $userIds;

            shuffle($randomizedUserIds);
            for($index = 0; $index < 3; $index++) {
                $pivots[] = [
                    
                    'user_id' => array_shift($randomizedUserIds), 
                    'event_id' => $eventId,
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now(),
                ];
            }
        }




        DB::table('event_user')->insert($pivots); 

        $unixTimestap = '1717952535';

        $claudia = App\User::where('email', 'claudiatest@thenetworkingeffect.com')->first();

        $ny_event = App\Event::create([
                'name'              => 'Upcoming Test Event in NYC',
                'description'       => $faker->text,
                'event_date'        => $faker->dateTimeBetween('now', $unixTimestap),
                'start_time'        => $faker->time,
                'end_time'          => $faker->time,
                'timezone'          => $faker->timezone,
                'street'            => '92 caplan ave',
                'city'              => 'barrie',
                'state'             => 'ON',
                'latitude'          => '44.4101662',
                'longitude'         => '-79.6970678',
                'user_id'           => array_shift($userIds),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now()
            ]);

        $data = [
            [
                'user_id' => $claudia->id,
                'event_id' => $ny_event->id,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now()
            ]
        ];
        DB::table('event_user')->insert($data); 

        /* EO SEED PIVOT */
    }
}
