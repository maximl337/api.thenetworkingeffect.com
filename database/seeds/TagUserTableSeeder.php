<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TagUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tag_user')->truncate();

        /* SEED PIVOT */
        $userIds = DB::table('users')->lists('id');
        $tagIds = DB::table('tags')->lists('id');

        $pivots = [];
        foreach($userIds as $userId)
        {
            //necessary since shuffle() and array_shift() take an array by reference
            $randomizedTagIds = $tagIds;

            shuffle($randomizedTagIds);
            for($index = 0; $index < 3; $index++) {
                $pivots[] = [
                    
                    'tag_id' => array_shift($randomizedTagIds), 
                    'user_id' => $userId,
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now(),
                ];
            }
        }

        DB::table('tag_user')->insert($pivots); 
        /* EO SEED PIVOT */
    }
}
