<?php

use Illuminate\Database\Seeder;

class NotificationTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('notification_types')->truncate();

        $data = [
            [
                    
                'name' => 'NEW_MESSAGE', 
                'message' => ' has sent you a message'
            
            ],

            [
                    
                'name' => 'NEW_FOLLOWER', 
                'message' => ' has collected your calling card'
            
            ],

            [
                    
                'name' => 'FOLLOWED_BACK', 
                'message' => ' has accepted your invitation to connect'
            
            ],
            
            [
                    
                'name' => 'NEW_CARD_SHARE_RECIPIENT', 
                'message' => ' shared a card with you'

            ],

            [
                    
                'name' => 'NEW_CARD_SHARE_SUBJECT', 
                'message' => ' shared your card'

            ],

            [
                    
                'name' => 'NEW_TESTIMONIAL', 
                'message' => ' left you a testimonial!'

            ],

            [
                    
                'name' => 'EVENT_UPDATED', 
                'message' => ' has been changed'

            ],

            [

                'name' => 'EVENT_DELETED',
                'message' => ' has been canceled'

            ],

            [

                'name' => 'FOLLOW-UP_TOMORROW',
                'message' => 'This is a reminder to follow up with '

            ],

            [

                'name' => 'FOLLOW-UP_IN_ONE_WEEK',
                'message' => 'This is a reminder to follow up with '

            ],

            [

                'name' => 'RESEARCH_WITHIN_ONE_WEEK',
                'message' => 'This is a reminder to research '

            ],

            [

                'name' => 'NEW_REFERRAL',
                'message' => ' sent you a referral'

            ],

            [

                'name' => 'REFERRED',
                'message' => ' referred you'

            ],

            [

                'name' => 'NOTE_REMINDER',
                'message' => 'Note reminder'

            ],
            [
                'name' => 'NEW_CHANNEL_CHAT_MENTION',
                'message' => ' mentioned you in '
            ]
        ];
        DB::table('notification_types')->insert($data);
    }
}
