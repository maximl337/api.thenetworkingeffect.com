<?php

use Illuminate\Database\Seeder;

class AssociationKeywordsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\AssociationKeyword::class, 20)->create();
    }
}
