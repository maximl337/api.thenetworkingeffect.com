<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvitationCountColumnToPlaceholderUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('placeholder_users', function (Blueprint $table) {
            $table->integer('invitation_count')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('placeholder_users', function (Blueprint $table) {
            $table->dropColumn('invitation_count');
        });
    }
}
