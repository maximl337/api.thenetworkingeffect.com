<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_shares', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sender_id')->unsigned();
            $table->integer('recipient_id')->unsigned()->nullable();
            $table->string('recipient_email')->nullable();
            $table->integer('subject_id')->unsigned();

            $table->timestamps();
        });

        Schema::table('card_shares', function (Blueprint $table) {
            $table->foreign('sender_id')
                    ->references('id')->on('users');
            $table->foreign('recipient_id')
                    ->references('id')->on('users');
            $table->foreign('subject_id')
                    ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('card_shares');
    }
}
