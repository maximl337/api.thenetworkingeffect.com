<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSocialUrlDefault extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            
            $table->string('website_url')->default("")->change();
            $table->string('facebook_url')->default("")->change();
            $table->string('twitter_url')->default("")->change();
            $table->string('googleplus_url')->default("")->change();
            $table->string('linkedin_url')->default("")->change();
            $table->string('youtube_url')->default("")->change();
            $table->string('blog_url')->default("")->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
