<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToPlaceholderUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('placeholder_users', function (Blueprint $table) {
            $table->boolean('invitation_sent')->after('business_category')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('placeholder_users', function (Blueprint $table) {
            $table->dropColumn('invitation_sent');
        });
    }
}
