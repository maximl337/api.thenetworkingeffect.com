<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailUnsubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_unsubs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('template_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('mail_unsubs', function (Blueprint $table) {
            $table->foreign('user_id')
                    ->references('id')->on('users')->onDelete('cascade');
            $table->foreign('template_id')
                    ->references('id')->on('mandrill_templates')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mail_unsubs');
    }
}
