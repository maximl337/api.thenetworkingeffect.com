<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTitleReminderColumnsToNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notes', function (Blueprint $table) {
            $table->string('title')->after('subject_id');
            $table->string('remind_place')->nullable()->after('body');
            $table->boolean('sent')->default(false)->after('body');
            $table->dateTime('remind_at')->nullable()->after('remind_place');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notes', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('remind_place');
            $table->dropColumn('remind_at');
        });
    }
}
