<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('actor_id')->unsigned();
            $table->integer('subject_id')->unsigned();
            $table->integer('object_id')->unsigned();
            $table->integer('notification_type_id')->unsigned();
            $table->boolean('seen')->default(false);
            $table->timestamps();
        });
        
        Schema::table('notifications', function (Blueprint $table) {

            $table->foreign('actor_id')
                    ->references('id')->on('users');
            $table->foreign('subject_id')
                    ->references('id')->on('users');
            $table->foreign('notification_type_id')
                    ->references('id')->on('notification_types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications');
    }
}
