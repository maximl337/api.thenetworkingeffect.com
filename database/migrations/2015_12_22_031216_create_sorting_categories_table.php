<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSortingCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sorting_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('reminder_delay_days')->unsigned();
            $table->time('reminder_time')->default('09:00:00');
            $table->string('reminder_timezone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sorting_categories');
    }
}
