<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('banner_url')->nullable();
            $table->date('event_date');
            $table->time('start_time');
            $table->time('end_time');
            $table->string('timezone');
            $table->string('street');
            $table->string('city');
            $table->string('state');
            $table->string('zip_code')->nullable(); 
            $table->string('country')->nullable();
            $table->string('latitude');
            $table->string('longitude');
            $table->string('venue')->nullable();
            $table->string('url')->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('permalink')->nullable();
            $table->softDeletes();
            $table->timestamps(); 
            $table->integer('original_id')->unsigned()->nullable();
        });

        Schema::table('events', function (Blueprint $table) {

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
        Schema::dropIfExists('event_user');
        Schema::dropIfExists('event_tag');
    }
}
