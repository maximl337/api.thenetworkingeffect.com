<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssociationKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('association_keywords', function (Blueprint $table) {
            $table->increments('id');
            $table->string('keyword', 3000);
            $table->integer('community_id')->unsigned()->nullable()
                ->comment("To accomodate for future requests of association keywords belonging to communities");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('association_keywords');
    }
}
