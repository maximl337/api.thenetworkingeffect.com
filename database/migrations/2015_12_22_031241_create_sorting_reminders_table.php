<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSortingRemindersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sorting_reminders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('network_id')->unsigned();
            $table->integer('sorting_category_id')->unsigned();
            $table->datetime('remind_at');
            $table->boolean('sent')->default(false);
            $table->boolean('active')->default(false);
            $table->timestamps();
        });

        Schema::table('sorting_reminders', function (Blueprint $table) {
            $table->foreign('network_id')
                    ->references('id')
                    ->on('networks');

            $table->foreign('sorting_category_id')
                    ->references('id')
                    ->on('sorting_categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sorting_reminders');
    }
}
