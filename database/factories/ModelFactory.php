<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'first_name'        => $faker->firstName,
        'last_name'         => $faker->lastName,
        'email'             => $faker->email,
        'password'          => bcrypt(str_random(10)),
        'avatar'            => $faker->imageUrl(500, 500, 'people'),
        'logo'              => $faker->imageUrl(900, 300, 'business'),
        'business_name'     => $faker->company,
        'job_title'         => $faker->word,
        'phone_1'           => $faker->phoneNumber,
        'phone_2'           => $faker->phoneNumber,
        'fax'               => $faker->phoneNumber,
        'about_me'          => $faker->realText,
        'about_business'    => $faker->realText,
        'website_url'       => $faker->url,
        'facebook_url'      => $faker->url,
        'twitter_url'       => $faker->url,
        'googleplus_url'    => $faker->url,
        'linkedin_url'      => $faker->url,
        'youtube_url'       => $faker->url,
        'blog_url'          => $faker->url,
        'street'            => $faker->streetAddress,
        'city'              => $faker->city,
        'state'             => $faker->state,
        'zip_code'          => $faker->postcode,
        'country'           => $faker->country,
        'latitude'          => $faker->latitude,
        'longitude'         => $faker->longitude,
        'business_category' => $faker->word,
        'remember_token'    => str_random(10),
    ];
});

$factory->define(App\Tag::class, function($faker) {
    return [
        'name' => $faker->word
    ];
});

$factory->define(App\Article::class, function($faker) {
    return [
        'title'  => $faker->sentence,
        'body'   => $faker->text,
        'teaser' => $faker->sentence
    ];
});

$factory->define(App\Event::class, function($faker) {
    $unixTimestap = '1717952535';
    return [
        'name'              => $faker->sentence,
        'description'       => $faker->text,
        'event_date'        => $faker->dateTimeBetween('now', $unixTimestap),
        'start_time'        => $faker->time,
        'end_time'          => $faker->time,
        'timezone'          => $faker->timezone,
        'street'            => $faker->streetAddress,
        'city'              => $faker->city,
        'state'             => $faker->state,
        'latitude'          => $faker->latitude,
        'longitude'         => $faker->longitude

    ];

});

$factory->define(App\Message::class, function($faker) {

    return [
        'body' => $faker->text
    ];

});

$factory->define(App\Partner::class, function($faker) {
    return [
        'name' => $faker->word(6)
    ];
});

$factory->define(App\Channel::class, function($faker) {
    return [
        'name' => $faker->word(6),
        'description' => $faker->word(6),
        'logo' => $faker->imageUrl(900, 300, 'business'),
        'private' => false
    ];
});

$factory->define(App\Role::class, function($faker) {
    return [
        'name' => $faker->word(6)
    ];
});

$factory->define(App\ChatMessage::class, function($faker) {
    return [
        'message' => $faker->text
    ];
});

$factory->define(App\AdminNote::class, function($faker) {

    return [
        'body' => $faker->text
    ];

});

$factory->define(App\AssociationKeyword::class, function($faker) {
    return [
        'keyword' => $faker->word(6)
    ];
});

