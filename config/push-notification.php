<?php

return array(

    'COBIos'     => array(
        'environment' => 'production',
        'certificate' => storage_path(getenv('APPLE_PUSH_CERTIFICATE_NAME')),
        'passPhrase'  => getenv('APPLE_PUSH_PASSPHRASE'),
        'service'     => 'apns'
    ),
    'COBAndroid' => array(
        'environment' => getenv('ANDROID_PUSH_ENVIRONMENT'),
        'apiKey'      => getenv('ANDROID_PUSH_API_KEY'),
        'service'     => 'gcm'
    )

);
