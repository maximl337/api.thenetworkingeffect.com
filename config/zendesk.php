<?php

return [
	'username' => env('ZENDESK_USERNAME'),
	'password' => env('ZENDESK_PASSWORD'),
	'subdomain' => env('ZENDESK_SUBDOMAIN')
];