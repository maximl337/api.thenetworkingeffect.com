## The Networking Effect - API

[![TNE: ](http://i.imgur.com/kawjKnb.png)](https://packagist.org/packages/laravel/framework)

The Networking Effect is a rapidly growing small business platform that helps businesses grow by connecting them with other local small businesses. The heart of The Networking Effect is that members can easily find new prospects, suppliers and get advice quickly, but also small businesses can get found by posting blog and profile content. Small businesses can also build a personal, trusted network, using a wide variety of connection tools. Founded in late 2013, The Networking Effect is a Canadian technology start-up headquartered in Barrie Ontario, with offices in Toronto and Calgary

The Networking Effect will help you grow your small business by connecting you with local prospects and suppliers. We make looking for what you want, and getting you found, really easy.
Now you can connect with other businesses, make new sales, network online, and create long-lasting business relationships all in one place ... for free!


