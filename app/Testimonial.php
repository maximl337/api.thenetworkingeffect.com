<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $table = 'testimonials';

    protected $fillable = [
        'author_id',
        'subject_id',
        'body',
        'approved',
        'seen'
    ];

    public function subject() {

        return $this->belongsTo('App\User', 'subject_id');

    }

    public function author() {

        return $this->belongsTo('App\User', 'author_id');
        
    }

}
