<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'banner_url',
        'event_date',
        'start_time',
        'end_time',
        'timezone',
        'street',
        'city',
        'state',
        'zip_code',
        'country',
        'latitude',
        'longitude',
        'venue',
        'user_id',
        'url',
        'permalink',
        'original_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * Get the attendees of Event
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany 
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    /**
     * Get the owner of the event
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo 
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get Tags associated with the Event
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo 
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }
}
