<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Privacy_policy extends Model
{
    protected $table = 'privacy_policies';

    protected $fillable = [
        'body'
    ];
}
