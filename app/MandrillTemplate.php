<?php

namespace App;

use Illuminate\Database\Eloquent\Model; 

class MandrillTemplate extends Model
{
    protected $table = 'mandrill_templates';

    protected $fillable = [
        'label',
        'name'
    ];

    /**
     * Get users that have unsubbed from a template
     *         
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function unsubs()
    {
        return $this->belongsToMany('App\User', 'mail_unsubs', 'template_id', 'user_id');
    }
}
