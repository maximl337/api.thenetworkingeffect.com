<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

            'author_id', 
            'subject_id', 
            'title',
            'body',
            'remind_place',
            'remind_at',
            'sent'

            ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'remind_at'];

    /**
     * Get the user of the note
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'author_id');
    }

    public function subject()
    {
        return $this->belongsTo('App\User', 'subject_id');
    }
}
