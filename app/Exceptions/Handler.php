<?php

namespace App\Exceptions;

use Bugsnag;
use Exception;
//use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Bugsnag\BugsnagLaravel\BugsnagExceptionHandler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        //Bugsnag::notifyError(get_class($e), $e->getMessage());

        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        // Eloquent model not found
        if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {

            return response()->json(['error' => [
                    'message' => 'Resource not found',
                    'status_code' => '404'
                ]], 404);
        }

        // URI route not found
        if($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException)
        {
            return response()->json(['error' => [
                    'message' => 'URI not found',
                    'status_code' => '404',
                    'details' => $e->getMessage()
                ]], 404);
        }
        
        // Method not allowed exception
        if($e instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException) {
            return response()->json(['error' => [
                    'message' => 'The resource was found but the request method is not allowed. Try using a different method (eg: POST)',
                    'status_code' => '405'
                ]], 405);
        }

        // JWT token invalid 
        if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException)
        {
            return response()->json([
                    'error' => [
                        'message' => 'Token is invalid',
                        'status_code' => '401'
                    ]
                ], 401);
        }

        // JWT token expired
        if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException)
        {
            return response()->json([
                    'error' => [
                        'message' => 'Token has expired',
                        'status_code' => '401'
                    ]
                ], 401);
        
            
        }

        //DB query exception
        if($e instanceof \Illuminate\Database\QueryException)
        {
            if($e->getCode() == 23000):
                return response()->json([
                    'error' => [
                        'message' => 'DB Error [23000]',
                        'status_code' => '500',
                        'details'   => $e->getMessage()
                    ]
                ], 500);
            endif;
            
            return response()->json([
                    'error' => [
                        'message' => 'There was an error with querying the database',
                        'status_code' => '500',
                        'error_message' => $e->getMessage()
                    ]
                ], 500);
        }

        

        return parent::render($request, $e);
    }
}
