<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification_type extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notification_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'name',
        'message'
    ];

    public function notifications()
    {
        return $this->hasMany('App\Notification', 'notification_type_id');
    }
}
