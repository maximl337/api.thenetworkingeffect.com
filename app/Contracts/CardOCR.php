<?php

namespace App\Contracts;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface CardOCR {

	public function parse(UploadedFile $file);
}