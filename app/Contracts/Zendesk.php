<?php

namespace App\Contracts;

use App\User;

interface Zendesk
{
	public function createOrUpdateUser(User $user);
}