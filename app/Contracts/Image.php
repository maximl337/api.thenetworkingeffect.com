<?php

namespace App\Contracts;

interface Image {

    public function upload($imagePath, $type = 'file');
    
}