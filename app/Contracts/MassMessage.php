<?php

namespace App\Contracts;

use Illuminate\Http\Request;

interface MassMessage 
{
    public function send(array $input);
}