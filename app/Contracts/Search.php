<?php

namespace App\Contracts;


interface Search {

    public function index($index);

    public function search($query, $args);

    public function delete($id);

    public function clearIndex();

    public function save($data);

    public function update($data);
}