<?php

namespace App\Contracts;

use App\User;

interface PushNotification {

    public function title($title);
    
    public function message($message);

    public function custom($custom);

    public function send(User $recipient);
}