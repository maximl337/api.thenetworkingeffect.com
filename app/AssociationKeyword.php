<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssociationKeyword extends Model
{
    protected $fillable = [
    	'keyword',
    	'community_id'
    ];

    /**
     * [users description]
     * @return [type] [description]
     */
    public function users()
    {
    	return $this->belongsToMany('App\User')->withTimestamps();
    }
}
