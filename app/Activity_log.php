<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity_log extends Model
{
    
    protected $table = 'activity_log';

    protected $fillable = [
        'user_id',
        'path',
        'data',
        'method',
        'ip'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
