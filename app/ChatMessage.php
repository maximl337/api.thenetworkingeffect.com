<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    protected $table = 'chat_messages';

    protected $fillable = [
    	'user_id',
    	'channel_id',
    	'message'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function channel()
    {
    	return $this->belongsTo(Channel::class);
    }

    public function deleted_messages()
    {
    	return $this->hasMany(DeletedChatMessage::class);
    }

    public function reported_messages()
    {
    	return $this->hasMany(ReportedChatMessage::class);
    }
}
