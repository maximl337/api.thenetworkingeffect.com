<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SortingReminder extends Model
{
    protected $table = 'sorting_reminders';

    protected $fillable = [
    	'network_id',
    	'sorting_category_id',
    	'remind_at',
        'sent'
    ];

    public function sortingCategory()
    {
    	return $this->belongsTo('App\SortingCategory', 'sorting_category_id');
    }

    public function network()
    {
    	return $this->belongsTo('App\Network', 'network_id');
    }
}
