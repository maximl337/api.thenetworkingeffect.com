<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SortingCategory extends Model
{
    protected $table = 'sorting_categories';

    protected $fillable = [
    	'name',
    	'reminder_delay_days',
    	'reminder_time',
    	'reminder_timezone'
    ];

    public function networks()
    {
    	return $this->belongsToMany('App\Network', 'sorting_reminders', 'sorting_category_id', 'network_id')
    				->withTimestamps();
    }
}
