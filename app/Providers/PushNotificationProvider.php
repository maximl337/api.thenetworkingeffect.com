<?php

namespace App\Providers;

use App\Contracts\PushNotification;
use App\Services\PushNotificationService;
use Illuminate\Support\ServiceProvider;

class PushNotificationProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PushNotification::class, function ($app) {

            return new PushNotificationService;
            
        });
    }
}
