<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'auth.attempt' => [
            'App\Listeners\LegacyPasswordUpgrader',
            'App\Listeners\UserLoginActivity',
        ],
        'tymon.jwt.valid' => [
            'App\Listeners\UpdateActivityLog',
        ],
        'App\Events\NewMessageSent' => [
            'App\Listeners\NewMessageNotification',
        ],
        'App\Events\NewNetworkFollower' => [
            'App\Listeners\NewNetworkFollowerNotification',
        ],
        'App\Events\NetworkUnfollowed' => [
            'App\Listeners\NetworkUnfollowedListener',
        ],
        'App\Events\NewCardShare' => [
            'App\Listeners\NewCardShareRecipientNotification',
        ],
        'App\Events\NewTestimonial' => [
            'App\Listeners\NewTestimonialNotification',
        ],
        'App\Events\EventCreated' => [
            'App\Listeners\EventCreatedListener',
        ],
        'App\Events\EventJoin' => [
            'App\Listeners\EventJoinListener'
        ],
        'App\Events\AdminEventAddUser' => [
            'App\Listeners\App\Events\AdminEventAddUserListener'
        ],
        'App\Events\EventUserLeft' => [
            'App\Listeners\EventUserLeftListener'
        ],
        'App\Events\EventUpdate' => [
            'App\Listeners\EventUpdateNotification',
        ],
        'App\Events\EventDelete' => [
            'App\Listeners\EventDeleteNotification',
        ],
        'App\Events\NewUserJoined' => [
            'App\Listeners\UserCreated',
        ],
        'App\Events\UserUpdated' => [
            'App\Listeners\UserUpdatedListener',
        ],
        'App\Events\UserDeleted' => [
            'App\Listeners\UserDeletedListener',
        ],
        'App\Events\ArticleCreated' => [
            'App\Listeners\ArticleCreatedListener',
        ],
        'App\Events\ArticleUpdated' => [
            'App\Listeners\ArticleUpdatedListener',
        ],
        'App\Events\ArticleDeleted' => [
            'App\Listeners\ArticleDeletedListener',
        ],
        'App\Events\SortingReminderEvent' => [
            'App\Listeners\SortingReminderListener',
        ],
        'App\Events\MakePlaceholderEvent' => [
            'App\Listeners\MakePlaceholderListener',
        ],
        'App\Events\ActivatePlaceholderEvent' => [
            'App\Listeners\ActivatePlaceholderListener',
        ],
        'App\Events\InvitePlaceholderEvent' => [
            'App\Listeners\InvitePlaceholderListener',
        ],
        'App\Events\ReferUserEvent' => [
            'App\Listeners\ReferUserListener',
        ],
        'App\Events\NoteReminderEvent' => [
            'App\Listeners\NoteReminderListener',
        ],
        'App\Events\NetworkFollowedBack' => [
            'App\Listeners\NetworkFollowedBackListener'
        ],
        'App\Events\UserUpdatedEmailEvent' => [
            'App\Listeners\UserUpdatedEmailListener'
        ],
        'App\Events\MassMessageSentEvent' => [
            'App\Listeners\MassMessageSentListener'
        ],
        'App\Events\InitiateMassMessageSentEvent' => [
            'App\Listeners\InitiateMassMessageSentListener'
        ],
        'App\Events\ChatMessageCreatedEvent' => [
            'App\Listeners\ChatMessageCreatedListener'
        ],
        'App\Events\ChatMessageMentionEvent' => [
            'App\Listeners\ChatMessageMentionListener'
        ],
        'App\Events\ChannelMemberJoinEvent' => [
            'App\Listeners\ChannelMemberJoinEventListener'
        ],

    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
