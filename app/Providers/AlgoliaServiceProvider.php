<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Contracts\Search;
use AlgoliaSearch\Client;
use App\Services\AlgoliaService;

class AlgoliaServiceProvider extends ServiceProvider
{



    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Search::class, function() {

            return new AlgoliaService (
                    new Client(getenv('ALGOLIA_APP_ID'), getenv('ALGOLIA_SECRET_KEY'))
                ); 
        
        });
    }

    public function provides()
    {
        return [ Search::class ];
    }

}
