<?php

namespace App\Providers;

use App\Contracts\MassMessage;
use App\Services\MassMessageService;
use Illuminate\Support\ServiceProvider;

class MassMessageProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MassMessage::class, function() {

            return new MassMessageService;
        });
    }
}
