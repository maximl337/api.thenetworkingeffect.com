<?php

namespace App\Providers;

use App\Contracts\CardOCR;
use App\Services\CamCardOCR;
use Illuminate\Support\ServiceProvider;

class CamCardServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CardOCR::class, function ($app) {

            return new CamCardOCR;
        });
    }
}
