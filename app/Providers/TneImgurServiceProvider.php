<?php

namespace App\Providers;

use App\Contracts\Image;
use App\Services\ImgurService;
use Illuminate\Support\ServiceProvider;

class TneImgurServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Image::class, function() {

            return new ImgurService(env('IMGUR_MASHAPE_CLIENT_ID'), env('IMGUR_MASHAPE_CLIENT_KEY'));
            
        });
    }
}
