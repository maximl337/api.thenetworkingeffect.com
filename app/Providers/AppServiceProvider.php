<?php

namespace App\Providers;


use App\Contracts\Zendesk;
use App\Services\ZendeskService;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Zendesk::class, function() {
            return new ZendeskService(
                config('zendesk.username'),
                config('zendesk.password'),
                config('zendesk.subdomain')
            );
        });
    }
}
