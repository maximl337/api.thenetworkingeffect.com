<?php

namespace App\Services;

use App\Contracts\Image;
use Log;

class ImgurService implements Image
{
    
    protected $clientId;

    protected $clientKey;

    public function __construct($clientId, $clientKey)
    {
        $this->clientId = $clientId;

        $this->clientKey = $clientKey;
    }

    public function upload($imagePath, $type = 'file')
    {

        $image = $imagePath;
        
        if($type == 'file') {

            $handle = fopen($imagePath, "r");
  
            //read uploaded file 
            $data = fread($handle, filesize($imagePath));

            $image = base64_encode($data);

        } 
        
        try {

            $data   = array('image' => $image, 'type' => $type);

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($curl, CURLOPT_URL, 'https://imgur-apiv3.p.mashape.com/3/image.json');

            curl_setopt($curl, CURLOPT_TIMEOUT, 30);

            curl_setopt($curl, CURLOPT_HTTPHEADER, array('X-Mashape-Key: ' . $this->clientKey, 'Authorization: Client-ID ' . $this->clientId));

            curl_setopt($curl, CURLOPT_POST, 1);

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

            $out = curl_exec($curl);

            curl_close($curl);

            $pms = json_decode($out,true);

            Log::info('IMGUR MASHAPE RETURN DATA', ['data' => $pms]);

            $url=!empty($pms['data']['link']) ? $pms['data']['link'] : "";

            return $url;

        } catch(\Exception $e) {

            Log::error('IMAGE SERVICE EXCEPTION', ['message' => $e->getMessage()]);

        }
        

        
    }
    
}