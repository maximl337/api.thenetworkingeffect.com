<?php

namespace App\Services;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Network;
use App\Opportunity;
use App\Forms\NewUser;
use App\PlaceholderUser;
use App\Forms\UpdateUser;
use App\Services\ImageService;
use App\Events\NewNetworkFollower;
use App\Events\MakePlaceholderEvent;
use App\Events\ActivatePlaceholderEvent;
use App\Transformers\MakePlaceholderTransformer;

class PlaceholderService
{
	
	/**
	 * Make a placeholder
	 * @param  array  $input
	 * @return App\User
	 */
	public function makePlaceholder(Request $request)
	{

		$user = Auth::user();

		$input = $request->input();

		try {

			// get placeholder
			$placeholder = User::where('email', $input['email'])->first();

			// create placeholder if does not exist
			if(is_null($placeholder)) {

				// Use Form object to add user
				$placeholder = (new NewUser)->add($input, $request->file('avatar'), $request->file('logo'));

				$placeholder->is_placeholder = true;

				$placeholder->save();

			} // user does not exist

			// if user exists and avatar is submitted
			// then upload image and save avatar to placeholder account
			if($request->hasFile('avatar')) {

				$input['avatar'] = (new ImageService)->upload( $request->file('avatar')->getRealPath() );

			}

			// check if avatar url was passed
			if(!empty($input['avatar_url'])) {

				$input['avatar'] = $input['avatar_url'];
			}

			// check if is placeholder 
			if($placeholder->is_placeholder) {

				// check if placeholder user for this owner already exists
				$placeholder_user_exists = $user->ownerPlaceholders()->where('user_id', $placeholder->id)->exists();

				if(!$placeholder_user_exists) {

					// transform data
					$transformedInput = (new MakePlaceholderTransformer)->transform($input);

					$transformedInput['user_id'] = $placeholder->id;

					// add to placeholder user
					$user->ownerPlaceholders()->save(new PlaceholderUser($transformedInput));
				}

				// Invitation email event
				$data = [
					'user' => $user,
					'placeholder' => $placeholder
				];

				//event(new MakePlaceholderEvent($data));
				
			} // check if placeholder

			$user->opportunities()->save(new Opportunity(['subject_id' => $placeholder->id]));

			return $placeholder;

		} catch(\Exception $e) {

			throw $e;
		}
		
	}

	/**
	 * Activates Placeholder accounts
	 * 
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function activatePlaceholder(Request $request)
	{
		try {

			// get input
			$input = $request->input();

			// get user
			$user = User::where('email', $input['email'])->first();

			if(is_null($user)) {

				throw new Exception("User with the given email does not exist");
			}

			// update user with new data
			$user = (new UpdateUser)->update($user, $input, $request->file('avatar'), $request->file('logo'));

			// call event
			event(new ActivatePlaceholderEvent($user));

			return $user;

		} catch(\Exception $e) {

			throw $e;
		}

	}
	
}