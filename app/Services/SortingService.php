<?php

namespace App\Services;

use Timezone;
use Carbon\Carbon;
use App\Notification;
use App\SortingCategory;
use App\SortingReminder;
use App\Events\SortingReminderEvent;

class SortingService
{
	
	/**
	 * Make the reminder date format
	 * 
	 * @param  SortingCategory $sortingCategory [description]
	 * @param  [type]          $timezone        [description]
	 * @return [type]                           [description]
	 */
	public function formatReminderDate(SortingCategory $sortingCategory, $time = null, $timezone = null)
	{
		
		// check reminder days delay for sorting cat
		$days = $sortingCategory->reminder_delay_days ?: 1;

		$time = $time ?: '09:00:00';
		
		// get the timezone
		$timezone = $timezone ?: env('APP_TIMEZONE');

		// get now in users timezone
		$now = Carbon::now($timezone);

		// add days
		$now->addDays($days);

		// get the timestamp in original timezone
		$timestamp = $now->toDateString() . ' ' . $time;

		// make the timestamp in utc
		$reminder_timestamp = Carbon::createFromFormat('Y-m-d H:i:s', $timestamp, $timezone);

		$reminder_timestamp->setTimezone('UTC');

		return $reminder_timestamp->format('Y-m-d H:i:s');
		
	}

	/**
	 * Send reminders
	 * 
	 * @return [type] [description]
	 */
	public function sendReminders()
	{

		try {

			// get todays date string
			$date = Carbon::now()->toDateTimeString();

			// get reminders to be sent
			$reminders = SortingReminder::where('sent', false)
											->where('remind_at', '<', $date)
											->get();

			
			foreach($reminders as $reminder) {

				event(new SortingReminderEvent($reminder));

			}

		} catch(\Exception $e) {

			return;
			
		}	
		
	}

	/**
	 * Get notification type ( Notification type must match sorting category name in upper case - change: seeder / transformer)
	 * @param  SortingCategory $sortingCategory [description]
	 * @return [type]                           [description]
	 */
    public function getNotificationType(SortingCategory $sortingCategory)
    {

    	try {

    		return $this->parseSortingCategoryName($sortingCategory->name);

    	} catch(\Exception $e) {

    		return false;

    	}

    }

    /**
     * Get Mandrill template (Mail template must match sorting category name in upper case - change in DB too)
     * @param  SortingCategory $sortingCategory [description]
     * @return [type]                           [description]
     */
    public function getMailTemplateType(SortingCategory $sortingCategory)
    {

    	try {

    		return $this->parseSortingCategoryName($sortingCategory->name);

    	} catch(\Exception $e) {

    		return false;

    	}

    }

    /**
     * [getPushNotificationMessage description]
     * @param  SortingCategory $sortingCategory [description]
     * @return [type]                           [description]
     */
    public function getPushNotificationMessage(SortingCategory $sortingCategory)
    {

    	if(strpos($sortingCategory->name, 'Follow-up') !== false) {

    		return 'Reminder to follow up with ';

    	}
    	else {

    		return 'Reminder to research ';
    	}

    	return false;

    }

    protected function parseSortingCategoryName($name) {

    	return str_replace(" ", "_", strtoupper($name));
    }
}