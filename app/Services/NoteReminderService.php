<?php

namespace App\Services;

use Carbon\Carbon;
use App\Note;
use App\Events\NoteReminderEvent;

class NoteReminderService
{

	/**
	 * [normalizeTimezone description]
	 * @param  [type] $dateTime [description]
	 * @param  [type] $timezone [description]
	 * @return [type]           [description]
	 */
	public function normalizeTimezone($dateTime, $timezone = "")
	{
		
		// Use the app timezone if timezone not given
		$timezone = $timezone ?: env('APP_TIMEZONE');

		// make the timestamp in utc
		$reminder_timestamp = Carbon::createFromFormat('Y-m-d H:i:s', $dateTime, $timezone);

		$reminder_timestamp->setTimezone('UTC');

		return $reminder_timestamp->format('Y-m-d H:i:s');
	}

	/**
	 * Send reminders
	 * 
	 * @return [type] [description]
	 */
	public function sendReminders()
	{

		try {

			// get todays date string
			$date = Carbon::now();

			// get reminders to be sent
			$reminders = Note::where('sent', false)
								->whereNotNull('remind_at')
								->where('remind_at', '<', $date)
								->get();

			
			foreach($reminders as $reminder) {

				event(new NoteReminderEvent($reminder));
			}

		} catch(\Exception $e) {

			return;
			
		}	
		
	}

}