<?php

namespace App\Services;

use DB;
use App\Notification;

class NotificationService
{

	/**
	 * [delete description]
	 * @param  [type] $object_id   [description]
	 * @param  [type] $object_type [description]
	 * @return [type]              [description]
	 */
	public function delete($object_id, $object_type)
	{
		try {

			if(is_null($object_id)) throw new \Exception("Object id cannot be empty");

			if(is_null($object_type)) throw new \Exception("Object type cannot be empty");
			
			return DB::table('notifications')
						->where('object_id', $object_id)
						->where('notification_type_id', function($query) use ($object_type) {
							$query->select('id')
									->from('notification_types')
									->where('name', $object_type)
									->get();
						})->delete();

		} catch (Exception $e) {

			throw $e;
														
		}
	}

	/**
	 * [markAsSeen description]
	 * @param  [type] $object_id   [description]
	 * @param  [type] $object_type [description]
	 * @return [type]              [description]
	 */
	public function markAsSeen($object_id, $object_type)
	{
		try {

			if(is_null($object_id)) throw new \Exception("Object id cannot be empty");

			if(is_null($object_type)) throw new \Exception("Object type cannot be empty");
			
			return DB::table('notifications')
						->where('object_id', $object_id)
						->where('notification_type_id', function($query) use ($object_type) {
							$query->select('id')
									->from('notification_types')
									->where('name', $object_type)
									->get();
						})->update(['seen' => 1]);

		} catch (Exception $e) {

			throw $e;
														
		}
	}

	/**
	 * Create a new Notification
	 * 	
	 * @param  [type] $actor_id             [description]
	 * @param  [type] $subject_id           [description]
	 * @param  [type] $object_id            [description]
	 * @param  [type] $notification_type_id [description]
	 * @return [type]                       [description]
	 */
	public function store($actor_id, $subject_id, $object_id, $notification_type_id)
	{
		try {
			
			// delete any existing Notifications
			Notification::where('actor_id', $actor_id)
							->where('subject_id', $subject_id)
							->where('object_id', $object_id)
							->where('notification_type_id', $notification_type_id)
							->delete();

			// create a new notification
			$notification = Notification::create([
                'actor_id' => $actor_id,
                'subject_id' => $subject_id,
                'object_id' => $object_id,
                'notification_type_id' => $notification_type_id
            ]);

			return $notification;

		} catch (Exception $e) {

			throw $e;
			
		}
	}
}