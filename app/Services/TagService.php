<?php

namespace App\Services;


use App\Tag;


class TagService
{
    
    /**
     * Sanitize tag 
     * @param  str $tag dirty tag
     * @return str      sanitized
     */
    public function sanitizeTag($tag)
    {
            $tag = str_slug($tag);

            // // Preserve escaped octets.
            // $tag = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $tag);

            // // Remove percent signs that are not part of an octet.
            // $tag = str_replace('%', '', $tag);

            // // Restore octets.
            // $tag = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $tag);

            // $tag = strtolower($tag);

            // $tag = preg_replace('/&.+?;/', '', $tag); // kill entities

            // $tag = str_replace('.', '-', $tag);

            // // Convert nbsp, ndash and mdash to hyphens
            // $tag = str_replace( array( '%c2%a0', '%e2%80%93', '%e2%80%94' ), '-', $tag );
               
            // // Strip these characters entirely
            // $tag = str_replace( array(
            //     // iexcl and iquest
            //     '%c2%a1', '%c2%bf',
            //     // angle quotes
            //     '%c2%ab', '%c2%bb', '%e2%80%b9', '%e2%80%ba',
            //     // curly quotes
            //     '%e2%80%98', '%e2%80%99', '%e2%80%9c', '%e2%80%9d',
            //     '%e2%80%9a', '%e2%80%9b', '%e2%80%9e', '%e2%80%9f',
            //     // copy, reg, deg, hellip and trade
            //     '%c2%a9', '%c2%ae', '%c2%b0', '%e2%80%a6', '%e2%84%a2',
            //     // acute accents
            //     '%c2%b4', '%cb%8a', '%cc%81', '%cd%81',
            //     // grave accent, macron, caron
            //     '%cc%80', '%cc%84', '%cc%8c',
            // ), '', $tag );

            // // Convert times to x
            // $tag = str_replace( '%c3%97', 'x', $tag );
            
            // $tag = preg_replace('/[^%a-z0-9 _-]/', '', $tag);

            // $tag = preg_replace('/\s+/', '-', $tag);

            // $tag = preg_replace('|-+|', '-', $tag);

            // $tag = trim($tag, '-');

            return $tag;

    }

    /**
     * Saves new tags
     * @param  array  $tags array of tags
     * @return array       array of tag ids
     */
    public function getTagIds(array $tags)
    {
        $arr = array_map([$this, 'sanitizeTag'], $tags);

        $arr = array_unique($arr); 

        return $this->save($arr);

    }

    /**
     * Save tags
     * @param  array  $cleanTags sanitized array of tags
     * @return array            array of tag ids
     */
    protected function save(array $cleanTags)
    {
        $r = [];

        foreach($cleanTags as $t) {

            $tag = Tag::where('name', '=', $t)->first();

            if($tag) {

                $r[] = $tag->id;
            
            } else {

                $tag = Tag::create(['name' => $t]);

                $r[] = $tag->id;
            }

        }

        return $r;
        
    }

    public function create($tag)
    {
        $tag = str_slug($tag);

        $exists = Tag::where('name', '=', $tag)->exists();

        if($exists) return $tag;

        $newTag = Tag::create([
                        'name' => $tag
                    ]);
        
        return $newTag->name;
    }

}
