<?php

namespace App\Services;

use App\User;
use Illuminate\Http\Request;
use App\Contracts\MassMessage;
use App\Events\MassMessageSentEvent;

class MassMessageService implements MassMessage
{
    
    public function send(array $input)   
    {

        try {

            $recipients = !empty($input['recipients']) 
                            ? $input['recipients']
                            : "";

            $from = $input['sender_id'];

            if(empty($recipients)) {

                $user_ids = User::whereNotIn('id', [$from])
                                    ->get(['id'])
                                    ->toArray();

                $recipients = array_flatten($user_ids);
            }

            foreach($recipients as $recipient) {

                $data = [

                        'from'  => $from,
                        'to'    => $recipient,
                        'body'  => $input['body']
                    ];

                event(new MassMessageSentEvent($data));
            }

        } catch (\Exception $e) {
            throw $e;
        }

    }
}