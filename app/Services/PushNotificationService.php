<?php

namespace App\Services;

use App\User;
use PushNotification as Push;
use App\Contracts\PushNotification;

class PushNotificationService implements PushNotification
{
    
    /**
     * Title of the push notification
     * 
     * @var str
     */
    protected $title;

    /**
     * Custom message 
     * 
     * @var array
     */
    protected $custom;

    /**
     * Main message
     * 
     * @var str
     */
    protected $message;

    /**
     * Notification badge
     * 
     * @var str
     */
    protected $badge;


    /**
     * Set title
     * 
     * @param  str $title Title of the message
     * @return self
     */
    public function title($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Set Custom message
     * 
     * @param  array $custom array of custom message
     * @return self
     */
    public function custom($custom)
    {
        $this->custom = $custom;

        return $this;
    }

    /**
     * Set badge
     * 
     * @param  array $badge badge of notifications
     * @return self
     */
    public function badge($badge)
    {
        $this->badge = $badge;

        return $this;
    }

    /**
     * Build message
     * 
     * @param  str $message main message
     * @return self
     */
    public function message($message)
    {
        $this->message = Push::Message(
                            $message,
                            [
                            'title' => $this->title,
                            'badge' => $this->badge,
                            'custom' => $this->custom
                            ]
                        );

        return $this;
    }

    /**
     * Send push notification
     * 
     * @param  App\User   $recipient
     * @return void
     */
    public function send(User $recipient)
    {
        
        if(!$recipient->devices()->exists()) {
            \Log::info('PushNotification@send', [
                'message' => $recipient->email . ' has no devices'
            ]);
            return;
        }

        foreach($recipient->devices as $device) {
            \Log::info('PushNotification@send', [
                'device' => $device,
                'recipient' => $recipient->email,
                'message' => 'Device found. Attempting to send push notification'
            ]);
            $appName = strtolower($device->platform) == 'android'
                        ? getenv('ANDROID_APP_NAME')
                        : getenv('APPLE_APP_NAME');

            $message = $this->message ? $this->message : [];
            try {
                Push::app($appName)
                    ->to($device->device_token)
                    ->send($this->message);
                \Log::info('PushNotification@send', [
                    'message' => 'Push notification sent to: ' . $recipient->email,
                    'device_token' => $device->device_token,
                    'push_message' => $this->message
                ]);
            } catch(\Exception $e) {
                \Log::info('PushNotification@send', [                    
                    'message' => $e->getMessage()
                ]);
            }
            

        } // eo foreach
    }
}