<?php

namespace App\Services;

use App\Contracts\Search;
use AlgoliaSearch\Client;


class AlgoliaService implements Search
{
    
    protected $client;

    protected $index;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function index($index)
    {
        $this->index = $this->client->initIndex($index);

        return $this;
    }

    public function search($query, $args = [])
    {
        return $this->index->search($query, $args);
    }

    public function delete($id)
    {
        return $this->index->deleteObject($id);
    }

    public function clearIndex()
    {
        return $this->index->clearIndex();
    }

    public function update($data)
    {
        return $this->index->partialUpdateObject($data);
    }

    public function save($data)
    {
        return $this->index->saveObject($data);
    }
    
}