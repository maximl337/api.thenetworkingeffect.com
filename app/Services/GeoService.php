<?php

namespace App\Services;

use Log;
use App\User;

class GeoService
{

    /**
     * Google Server API Key
     * @var str
     */
    protected $apiKey;

    /**
     * init operations
     */
    public function __construct()
    {
        $this->apiKey = getenv('GOOGLE_API_SERVER_KEY');
    }

    /**
     * Get lat/long coordinates 
     * @param  str $street  street name
     * @param  str $city    city name
     * @param  str $state   state name
     * @param  str $zip     postal code
     * @param  str $country country
     * @return array          lat/long coordinates
     */
    public function getLatLang($street, $city, $state, $zip='', $country='')
    {
        
        $addressString = $this->formatAddress($street, $city, $state, $zip='', $country='');

        return $this->geoCode($addressString);
    }

    /**
     * Concatenate the components of the address
     * @param  str $street  street name
     * @param  str $city    city name
     * @param  str $state   state name
     * @param  str $zip     postal code
     * @param  str $country country name
     * @return str          cocatenated string adress
     */
    protected function formatAddress($street, $city, $state, $zip='', $country='')
    {
        try {
            
            return $street . ' ' . $city . ' ' . $state . ' ' . $zip . ' ' . $country;    
        
        } catch(Exception $e) {

            return $e->getMessage();
        }
        
    }

    /**
     * Query Google Map API to get lat long coordinates
     * @param  str $address full address string
     * @return array          lat/long coordinates
     */
    protected function geoCode($address)
    {
        try {

            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address) . "&key=" . $this->apiKey;

            $resp = json_decode(file_get_contents($url), true);

            if(!$resp['results']) {

                return [
                    
                    'latitude' => "",
                
                    'longitude' => ""
                
                ];

            } 

            return [
                    
                'latitude' => $resp['results'][0]['geometry']['location']['lat'],
                
                'longitude' => $resp['results'][0]['geometry']['location']['lng']
                
                ];
            
        
        } catch(Exception $e) {

            return response()->json([
                        'error' => [
                            'message' => $e->getMessage(), 
                            'status' => '500'
                        ]
                    ], 500);
        }
    }

   /**
    * Get distance from Google
    * @param  float $lat1 origin latitude
    * @param  float $lon1 originlongitude
    * @param  float $lat2 destination latitude
    * @param  float $lon2 destination longitude
    * @param  string $unit unit tyoe
    * @return string       distance
    */
    public function getDistanceBetweenTwoCoordinates($lat1, $lon1, $lat2, $lon2, $unit = 'K') {

        return $this->distance($lat1, $lon1, $lat2, $lon2);

        try {

            $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" 
                . urlencode($lat1) . "," . urlencode($lon1)
                ."&destinations="
                . urlencode($lat2) . "," . urlencode($lon2)
                . "&key=" . $this->apiKey;

            $resp = json_decode(file_get_contents($url), true);

            //dd($resp);

            if( isset($resp['rows'][0]['elements'][0]['status']) && $resp['rows'][0]['elements'][0]['status'] == "OK" ) {

                $meters =  $resp['rows'][0]['elements'][0]['distance']['value'];

                if($unit == 'M') {

                    return round(($meters * 0.00062137), 2);

                } 

                return round(($meters / 1000), 2);

            }

        } catch(\Exception $e) {

            return 0;

        }
        
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit = 'K') {

        try {

            $lat1 = (float) $lat1;
            
            $lat2 = (float) $lat2;
            
            $lon1 = (float) $lon1;
            
            $lon2 = (float) $lon2;

            $theta = $lon1 - $lon2;

            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));

            $dist = acos($dist);

            $dist = rad2deg($dist);

            $miles = $dist * 60 * 1.1515;

            $unit = strtoupper($unit);

            if ($unit == "K") {

                return round(($miles * 1.609344), 2);

            } else if ($unit == "N") {

                return round(($miles * 0.8684), 2);

            } else {

                return round($miles, 2);

            }

        } catch(\Exception $e) {

            return 0;   
        }
    }

    public function getDistaneBetweenTwoUsers($userOneId, $userTwoId)
    {
        $userOneCoordinates = $this->getUserCoordinates($userOneId);

        $userTwoCoordinates = $this->getUserCoordinates($userTwoId);

        if($userOneCoordinates && $userTwoCoordinates) {

            return $this->getDistanceBetweenTwoCoordinates($userOneCoordinates['lat'], $userOneCoordinates['lng'], $userTwoCoordinates['lat'], $userTwoCoordinates['lng']);
        }

        return false;
    }

    public function getUserCoordinates($userId)
    {
        $user = User::find($userId);

        if(!$user) return;
        
        $roamingCoordinates = $user->roaming_coordinates()->latest()->first();

        //$latlng = $roamingCoordinates->first();

        if($roamingCoordinates) {
            
            $lat = $roamingCoordinates->latitude;

            $lng = $roamingCoordinates->longitude;    
        
        } else {

            $lat = $user->latitude;

            $lng = $user->longitude;

        }

        if(!$lat || !$lng) return false;

        return [
            'lat' => (float) $lat,
            'lng' => (float) $lng
        ];
        
    }

    protected function reverseGeocode($lat, $lng) {

        $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" 
                . urlencode($lat) . "," . urlencode($lng)
                . "&key=" . $this->apiKey;

        return json_decode(file_get_contents($url), true);

    }

    public function getUserCityByCoordinates($userId)
    {
        
        $city = "";

        $coordinates = $this->getUserCoordinates($userId);

        if(!$coordinates) return $city;

        $googleData = $this->reverseGeocode($coordinates['lat'], $coordinates['lng']);

        if(empty($googleData['results']['0']['address_components'])) return $city;

        foreach($googleData['results']['0']['address_components'] as $addressComponent) {

            if ($addressComponent['types'][0] == "locality") {
                
                $city = $addressComponent['long_name'];

            } // locality type
            
        }

        return $city;
    }

}