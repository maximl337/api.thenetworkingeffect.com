<?php

namespace App\Services;


use App\User;
use App\Event;
use App\Article;

class SanitizeUrlService
{

    protected $stringToSanitize;

    protected $model;

    public function makePrettyUrl($model)
    {

        $this->model = $model;

        if($model instanceof User) {

            $this->stringToSanitize = $model->first_name . ' ' . $model->last_name;

        }
        elseif($model instanceof Event) {

            $this->stringToSanitize = $model->name;
        }
        elseif($model instanceof Article) {

            $this->stringToSanitize = $model->title;
        }
        else {

            throw new \Exception("Model given is not instance of User or Event");
        }

        return $this->getPermalink();
    }
    
    protected function sanitize($string)
    {

        if(empty($string)) throw new \Exception("String not given");

        $string = strip_tags($string);

        // // Preserve escaped octets.
        // $string = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $string);

        // // Remove percent signs that are not part of an octet.
        // $string = str_replace('%', '', $string);

        // // Restore octets.
        // $string = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $string);

        // $string = strtolower($string);

        // $string = preg_replace('/&.+?;/', '', $string); // kill entities

        // $string = str_replace('.', '-', $string);


        // $string = str_replace( array( '%c2%a0', '%e2%80%93', '%e2%80%94' ), '-', $string );

        // // Strip these characters entirely
        // $string = str_replace( array(
        //     // iexcl and iquest
        //     '%c2%a1', '%c2%bf',
        //     // angle quotes
        //     '%c2%ab', '%c2%bb', '%e2%80%b9', '%e2%80%ba',
        //     // curly quotes
        //     '%e2%80%98', '%e2%80%99', '%e2%80%9c', '%e2%80%9d',
        //     '%e2%80%9a', '%e2%80%9b', '%e2%80%9e', '%e2%80%9f',
        //     // copy, reg, deg, hellip and trade
        //     '%c2%a9', '%c2%ae', '%c2%b0', '%e2%80%a6', '%e2%84%a2',
        //     // acute accents
        //     '%c2%b4', '%cb%8a', '%cc%81', '%cd%81',
        //     // grave accent, macron, caron
        //     '%cc%80', '%cc%84', '%cc%8c',
        // ), '', $string );

        // // Convert times to x
        // $string = str_replace( '%c3%97', 'x', $string );
        
        // $string = preg_replace('/[^%a-z0-9 _-]/', '', $string);

        // $string = preg_replace('/\s+/', '-', $string);

        // $string = preg_replace('|-+|', '-', $string);

        // $string = trim($string, '');

        // return $string;
        return str_slug($string, "-");
    }


    protected function getCountOfExistingPermalink($permalink)
    {
        if($this->model instanceof User) {

            return User::where('permalink', 'LIKE', $permalink.'%')
                        ->count();
        }
        elseif($this->model instanceof Event) {

            return Event::where('permalink', 'LIKE', $permalink.'%')
                         ->count();
        }
        elseif($this->model instanceof Article) {

            return Article::where('permalink', 'LIKE', $permalink.'%')
                         ->count();
        }
    }

    protected function getPermalink() {

        $sanitizedString = $this->sanitize($this->stringToSanitize);

        $existingPermalinkCount = $this->getCountOfExistingPermalink($sanitizedString);

        return  $existingPermalinkCount > 0
                ? $sanitizedString . "-" . $existingPermalinkCount
                : $sanitizedString;

    }


}