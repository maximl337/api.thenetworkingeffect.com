<?php

namespace App\Services;

use Log;
use Image as InterventionImage;
use App\Contracts\CardOCR;
use App\Services\ImageService;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CamCardOCR implements CardOCR 
{

	protected $file;

	public function parse(UploadedFile $file)
	{
		Log::info('CardOCR', ['status' => 'started']);

		try {

			$this->file = $file;

			$fileName = $this->makeFileName();

			$filePath = public_path() . "/images/". $fileName;

			$file->move("./images/", $fileName);
			
			// send to CamCard
			$res = $this->sendToCamCard($filePath, basename($filePath));

			\File::delete([$filePath]);

		} catch(\Exception $e) {

			Log::error('CardOCR', ['message' => $e->getMessage()]);

			throw $e;
		}

		return $res;
		
	}

	protected function sendToCamCard($image, $imageName) {

		$target_url = "https://bcr1.intsig.net/BCRService/BCR_VCF2";

		$post = array(
			'uploaded_file' => '@'.$image.';filename='.$imageName
		);

		$get_string = "?" . http_build_query(array(

				'user' => 'system@thenetworkingeffect.com',
				'pass' => 'QBDFQ4WD3Q9L7F7A',
				'lang' => '1',
				'json' => 1

			)
		);

		try {

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $target_url.$get_string);

			curl_setopt($ch, CURLOPT_HTTPGET, 1);

			curl_setopt($ch, CURLOPT_POST, 1);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

			$result = curl_exec($ch);

			curl_close($ch);

			return $result;

		} catch(\Exception $e) {

			throw $e;
		}

		
	}

	protected function makeFileName()
    {
        $name = sha1(
                    time() . $this->file->getClientOriginalName()
                );

        $extension = $this->file->getClientOriginalExtension();

        return "{$name}.{$extension}";
    }


}