<?php

namespace App\Services;

use DB;
use App\User;
use App\Network;
use App\Notification;
use App\Notification_type;
use App\Opportunity;
use App\Events\NewNetworkFollower;
use App\Events\NetworkFollowedBack;

class NetworkService
{
	/**
	 * Get count of mutual contacts between two users
	 * 
	 * @param  User   $userOne  
	 * @param  User   $userTwo
	 * @return int 	 
	 */
	public function getCountOfMutualConnectionsBetweenTwoUsers(User $userOne, User $userTwo)
	{

		if(is_null($userOne) || is_null($userTwo)) return 0;

		$userOneContactsArray = $this->getUserContacts($userOne, 'ids');

		$userTwoContactsArray = $this->getUserContacts($userTwo, 'ids');

		return count(array_intersect($userOneContactsArray, $userTwoContactsArray));

	}

	/**
	 * Check if given subject is a contact of follower
	 * 
	 * @param  User    $follower [description]
	 * @param  User    $subject  [description]
	 * @return boolean           [description]
	 */
	public function isContact(User $follower, User $subject)
	{

		$subject_id = $subject->id;

		$userContacts = $this->getUserContacts($follower, 'ids');

		return in_array($subject_id, $userContacts);
	}

	/**
	 * get contacts of a given user
	 * 
	 * @param  User   $user [description]
	 * @param  string $type [description]
	 * @return [type]       [description]
	 */
	public function getUserContacts(User $user, $type = 'users', $query = '')
	{
		if(is_null($user)) return null;
		//contacts

		//get people I follow
		$contacts = [];

		$subjects = [];

		$query = '%' . $query . '%';

		$results = DB::select("SELECT 
								`subject_id` 
								FROM 
								`networks` 
								WHERE
								`follower_id` = ?
								AND
								`subject_id` 
								IN
								(SELECT `follower_id`
								FROM
								`networks`
								WHERE
								`subject_id` = ?)
								AND
								`subject_id`
								IN
								(SELECT `id`
								FROM
								`users`
								WHERE
								(
                                `first_name` LIKE ?
                                OR
                                `last_name` LIKE ?
                                OR
                                CONCAT(`first_name`, ' ', `last_name`) LIKE ?
                                OR
                                `job_title` LIKE ?
                                OR
                                `business_name` LIKE ?
                                OR
                                `business_category` LIKE ?
                                ))
								GROUP BY `subject_id`", [$user->id, $user->id, $query, $query, $query,$query, $query, $query]);	
		
		$users = [];

		$ids = [];

		if($type == 'users') {
			foreach($results as $result) {
				$users[] = User::find($result->subject_id);
			}
			return $users;
		}

		foreach($results as $result) {
			$ids[] = $result->subject_id;
		}

		return $ids;
	}

	/**
	 * Delete Network
	 */
	public function delete(User $follower, User $subject)
	{
		if(is_null($follower) || is_null($subject)) throw new \Exception("Follower or subject not found");

		$notificationsToRemove = [];

		// remove network of follower
		$networkOne = Network::where('follower_id', $follower->id)
								->where('subject_id', $subject->id)
								->first();

		if(!is_null($networkOne)) {

			array_push($notificationsToRemove, $networkOne->id);

			$networkOne->delete();
		}

		// remove network of subject
		$networkTwo = Network::where('follower_id', $subject->id)
								->where('subject_id', $follower->id)
								->first();		

		if(!is_null($networkTwo)) {

			array_push($notificationsToRemove, $networkTwo->id);
			
			$networkTwo->delete();
		}
					
		// remove notifications
		Notification::whereIn('object_id', $notificationsToRemove)->delete();
	}

	/**
	 * [migrateOpportunities description]
	 * @return [type] [description]
	 */
	public function migrateOpportunities()
	{
		// get all network
		$network = Network::all();

		// loop through network
		foreach($network as $conn) {

			$follower = $conn->follower()->first();

			$subject = $conn->subject()->first();

			// check if mutual contact

			if((empty($follower) || is_null($follower)) ||
				(empty($subject) || is_null($subject))) {
				continue;
			}
			
			$isContact = $this->isContact($follower, $subject);

			if($isContact) continue;

			// if not mutual contact 
			// add to opportunities
			Opportunity::create([
					'follower_id' => $follower->id,
					'subject_id' => $subject->id,
					'created_at' => $conn->created_at,
				]);
			

		} // loop

	}

	public function addToNetwork($follower_id, $subject_id, $message = '')
	{

		try {
			
			$existing_connections = Network::where('follower_id', $follower_id)
											->where('subject_id', $subject_id)
											->get();

			/**
			 * Remove duplicate Connections
			 */
			foreach($existing_connections as $network) {

				$notification_type = Notification_type::where('name', 'NEW_FOLLOWER')->first()->toArray();

				// Remove notifications
				Notification::where('object_id', $network->id)
								->where('notification_type_id', $notification_type['id'])
								->delete();

				Notification::where('actor_id', $follower_id)
	                         ->where('subject_id', $subject_id)
	                         ->where('notification_type_id', $notification_type['id'])
	                         ->delete();

				// also delete the network
				// Dont delete duplicate networks coz 
				// user should be able to send multiple network requests
				//$network->delete();
			}

			// if duplicate opportunities exist 
			// then delete all but one
			$opportunities = Opportunity::where('follower_id', $follower_id)
											->where('subject_id', $subject_id)
											->get();

			if(count($opportunities) > 1) {

				for ($i=1; $i < count($opportunities); $i++) { 
					$opportunities[$i]->delete();
				}
			}

			// check if connecting back
			$check = Network::where('follower_id', $subject_id)
							   ->where('subject_id', $follower_id)
							   ->exists();

			// create the network
			$network = Network::create([
                            'follower_id' => $follower_id,
                            'subject_id' => $subject_id,
                            'message' => $message
                        ]);

			/**
			 * if user is responding to a network
			 * request ( i.e. adding back )
			 * remove both opportunity records
			 */
			if($check) {

				Opportunity::where(function($q) use($follower_id, $subject_id) {
					$q->where('follower_id', $follower_id)
						->where('subject_id', $subject_id);
				})->orWhere(function($q) use($follower_id, $subject_id) {
					$q->where('follower_id', $subject_id)
						->where('subject_id', $follower_id);
				})->delete();

				$notification_type = Notification_type::where('name', 'FOLLOWED_BACK')->first()->toArray();

				Notification::where('actor_id', $follower_id)
				            ->where('subject_id', $subject_id)
				            ->where('notification_type_id', $notification_type['id'])
				            ->delete();
				
				// Responding back: 
				// send a different notification
				event(new NetworkFollowedBack($network));

			} else {

				$notification_type = Notification_type::where('name', 'NEW_FOLLOWER')->first()->toArray();

               Notification::where('actor_id', $follower_id)
                            ->where('subject_id', $subject_id)
                            ->where('notification_type_id', $notification_type['id'])
                            ->delete();

				// New network send invite
				event(new NewNetworkFollower($network));

			}

            return $network;

		} catch (Exception $e) {

			throw $e;
							
		}
	}
}