<?php 

namespace App\Services;

use App\Notification;

class NotificationCountService
{

	public function categorize($notifications)
	{

		$categories = [
			'messages' => 0,
			'testimonials' => 0,
			'invites' => 0,
			'referred' => 0,
			'reminders' => 0,
			'channel' => 0,
		];
		$total = 0;
		foreach ($notifications as $notification) {
			if(!$notification instanceof Notification) continue;
			$type = $notification->type()->first();
			if(is_null($type)) continue;
			switch ($type->name) {
				case 'NEW_MESSAGE':
					$categories['messages']++;
					$total++;
					break;
				case 'NEW_TESTIMONIAL':
					$categories['testimonials']++;
					$total++;
					break;
				case 'NEW_FOLLOWER':
					$categories['invites']++;
					$total++;
					break;
				case 'NEW_CHANNEL_CHAT_MENTION':
					$categories['channel']++;
					$total++;
					break;
				case 'NEW_REFERRAL':
					$categories['referred']++;
					$total++;
					break;
				case 'NOTE_REMINDER':
					$categories['reminders']++;
					$total++;
					break;
				default:
					# code...
					break;
			}
		}

		$categories['total'] = $total;

		return $categories;
	}
}