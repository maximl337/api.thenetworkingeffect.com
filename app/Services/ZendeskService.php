<?php

namespace App\Services;

use App\User;
use App\Contracts\Zendesk as ZendeskContract;

class ZendeskService implements ZendeskContract
{
	protected $username;
	protected $password;
	protected $subdomain;

	public function __construct($username, $password, $subdomain)
	{
		$this->username = $username;
		$this->password = $password;
		$this->subdomain = $subdomain;
	}

	/**
	 * [createOrUpdateUser description]
	 * @param  User   $user [description]
	 * @return [type]       [description]
	 */
	public function createOrUpdateUser(User $user)
	{
		$payload = [
			"user" => [
				"verified" => true,
				"name" => $user->first_name . ' ' . $user->last_name,
				"email" => $user->email,
				"tags" => ["1dn"]
			]
		];
		$zendeskUser = $this->post('api/v2/users/create_or_update.json', $payload);
	}

	protected function post($endpoint, $payload)
	{
		$payload = json_encode($payload);
		$url = $this->subdomain . $endpoint;
        $ch = curl_init( $url );
        curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $payload); 
        curl_setopt($ch, CURLOPT_USERPWD, $this->username.":".$this->password);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
		    'Content-Length: ' . strlen($payload)
		]);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = json_decode(curl_exec($ch));
        if(curl_errno($ch)){
            \Log::error('ZendeskService@post', [
                    'curl_error' => curl_error($ch),
                    'endpoint' => $endpoint,
                    'payload' => $payload
                ]);
            curl_close($ch);
            return [];
        }
        curl_close($ch);
        return $result;
	}
}