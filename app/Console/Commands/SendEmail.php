<?php

namespace App\Console\Commands;

use Mail;
use Illuminate\Console\Command;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an test email mandrill';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $token = 'test';

        Mail::send('emails.password', compact('token'), function ($message) {

            $message->from('us@example.com', 'Laravel');

            $message->to('foo@example.com')->cc('bar@example.com');
        });
    }
}
