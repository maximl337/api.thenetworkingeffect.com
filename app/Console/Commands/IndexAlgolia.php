<?php 

namespace App\Console\Commands;

use Log;
use App\User;
use App\Event;
use App\Article;
use AlgoliaSearch\Client;
use App\Utilities\Timezone;
use Illuminate\Console\Command;

class IndexAlgolia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'algolia:index {model=users}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Index Algolia';

    protected $algolia;

    protected $model;

    protected $index;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->algolia = new Client(getenv('ALGOLIA_APP_ID'), getenv('ALGOLIA_SECRET_KEY'));

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');
        
        $this->model = $this->argument('model');

        if($this->model == 'users') {
            $this->index = getenv('ALGOLIA_USERS_INDEX');
        }
        elseif($this->model == 'events') {
            $this->index = getenv('ALGOLIA_EVENTS_INDEX');
        }
        elseif($this->model == 'tags') {
            $this->index = 'tags_api';
        }
        elseif($this->model == 'articles') {
            $this->index = getenv('ALGOLIA_ARTICLES_INDEX');
        }

        $modelClient = $this->algolia->initIndex($this->index);

        $modelClient->clearIndex();

        if($this->model == 'users'):

            $batch = [];

            $users = User::where('is_placeholder', false)->with('tags')->with('partners')->get();

            $this->output->progressStart(count($users));

            foreach($users as $user):

                $final = [
                    'objectID' => $user->id,
                    'id'       => $user->id,
                    '_geoloc' => [
                        "lat" => (float) $user->latitude,
                        "lng" => (float) $user->longitude
                        ],
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'avatar' => $user->avatar,
                    'logo'  => $user->logo,
                    'job_title' => $user->job_title,
                    'business_name' => $user->business_name,
                    'about_me' => $user->about_me,
                    'about_business' => $user->about_business,
                    'website_url'   => $user->website_url,
                    'facebook_url' => $user->facebook_url,
                    'twitter_url' => $user->twitter_url,
                    'googleplus_url' => $user->googleplus_url,
                    'linkedin_url' => $user->linkedin_url,
                    'youtube_url' => $user->youtube_url,
                    'blog_url' => $user->blog_url,
                    'city'  => $user->city,
                    'state' => $user->state,
                    'country' => $user->country,
                    'business_category' => $user->business_category,
                    'association'       => $user->association
                ];

                // add partners
                foreach($user->partners as $partner) {
                    $final['partners'][] = $partner->name;
                }

                // add channels
                foreach($user->channels as $channel) {
                    $final['channels'][] = $channel->id;
                }

                // add association keywords
                foreach($user->association_keywords as $association_keyword) {
                    $final['association_keywords'][] = [ 
                        "id" => $association_keyword->id,
                        "keyword" => $association_keyword->keyword,
                    ];
                }

                foreach($user->tags as $tag):
                    $final['tags'][] = $tag->name;
                    $final['_tags'][] = $tag->name;
                endforeach;

                array_push($batch, $final);

                if(count($batch) == 10000) {
                    $modelClient->saveObjects($batch);
                    $batch=[];
                }
                $this->output->progressAdvance();
            endforeach;

            $modelClient->saveObjects($batch);

            $this->output->progressFinish();

            $this->info('Algolia indexing complete');

        endif; //EO if users

        if($this->model == 'events'):

            $batch = [];

            $events = Event::with('tags')->get();

            $this->output->progressStart(count($events));

            $timezone = new Timezone;

            foreach($events as $event):

                $event_timestamp = $timezone->get_normalized_unix_timestamp($event['event_date'] . ' ' . $event['end_time'], $event['timezone']);

                $isTimestampValid = $timezone->isValidTimeStamp((string) $event_timestamp);

                if($isTimestampValid) $event_timestamp += 3600;

                $final = [
                    'objectID' => $event->id,
                    'id'       => $event->id,
                    '_geoloc' => [
                        "lat" => (float) $event->latitude,
                        "lng" => (float) $event->longitude
                        ],
                    'name'              => $event->name,
                    'description'       => $event->description,
                    'event_date'        => $event_timestamp,
                    'banner_url'        => $event->banner_url, 
                    'start_time'        => $event->start_time,
                    'end_time'          => $event->end_time,
                    'street'            => $event->street,
                    'city'              => $event->city,
                    'state'             => $event->state,
                ];

                foreach($event->tags as $tag):
                    $final['tags'][] = $tag->name;
                    $final['_tags'][] = $tag->name;
                endforeach;

                array_push($batch, $final);

                if(count($batch) == 10000) {
                    $modelClient->saveObjects($batch);
                    $batch=[];
                }
                $this->output->progressAdvance();
            endforeach;

            $modelClient->saveObjects($batch);

            $this->output->progressFinish();

            $this->info('Algolia indexing complete');

        endif; //EO if events

        if($this->model == 'articles'):

            $batch = [];

            $articles = Article::with('tags')->get();

            $this->output->progressStart(count($articles));

            foreach($articles as $article):

                $final = [
                    'title'         => $article->title,
                    'body'          => $article->body,
                    'teaser'        => $article->teaser,
                    'user_id'       => $article->user_id,
                    'created_at'    => strtotime($article->created_at),
                    'objectID'      => $article->id
                ];

                foreach($article->tags as $tag):
                    $final['tags'][] = $tag->name;
                    $final['_tags'][] = $tag->name;
                endforeach;

                array_push($batch, $final);

                if(count($batch) == 10000) {
                    $modelClient->saveObjects($batch);
                    $batch=[];
                }
                $this->output->progressAdvance();
            endforeach;

            $modelClient->saveObjects($batch);

            $this->output->progressFinish();

            $this->info('Algolia indexing complete');

        endif; //EO if articles

        
    }
}
