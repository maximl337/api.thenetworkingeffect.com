<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\SortingService;

class SortingNotification extends Command
{

    protected $sortingService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sorting:notifications {send}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send the sorting notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(SortingService $sortingService)
    {
        
        $this->sortingService = $sortingService;

        parent::__construct();
        
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $this->sortingService->sendReminders();
        
    }
}
