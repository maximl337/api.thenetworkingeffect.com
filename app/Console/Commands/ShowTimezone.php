<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ShowTimezone extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'time:show {type=unix}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show current unix time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');

        $today = date('Y-m-d H:i:s');

        if($type == 'unix') {

            $this->info(strtotime($today));

        } elseif ($type == 'normal') {

            $this->info($today);
            
        } elseif($type == 'now') {

            $this->info(strtotime("now"));

        } elseif($type == 'EST') {

            $date = new \DateTime($today, new \DateTimeZone('America/New_York'));

            $timestamp = $date->format('U') - 28800;

            $this->info($timestamp);
        }
    }
}
