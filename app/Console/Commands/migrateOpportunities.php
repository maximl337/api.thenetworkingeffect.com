<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\NetworkService;

class migrateOpportunities extends Command
{

    protected $networkService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'opportunities:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrates networks to opportunities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(NetworkService $networkService)
    {
        parent::__construct();

        $this->networkService = $networkService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->networkService->migrateOpportunities();
    }
}
