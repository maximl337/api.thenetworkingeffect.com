<?php

namespace App\Console\Commands;

use App\Services\NoteReminderService;
use Illuminate\Console\Command;

class NoteReminder extends Command
{

    protected $noteReminderService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminders:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send note reminders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(NoteReminderService $service)
    {
        parent::__construct();

        $this->noteReminderService = $service;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->noteReminderService->sendReminders();
    }
}
