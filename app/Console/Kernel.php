<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\IndexAlgolia::class,
        \App\Console\Commands\MakeAdmin::class,
        \App\Console\Commands\RemoveAdmin::class,
        \App\Console\Commands\SendEmail::class,
        \App\Console\Commands\ShowTimezone::class,
        \App\Console\Commands\SortingNotification::class,
        \App\Console\Commands\NoteReminder::class,
        \App\Console\Commands\migrateOpportunities::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }
}
