<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $table='channels';

    protected $fillable = [
        'name',
        'description',
        'logo',
        'user_id',
        'private'
    ];

    public function scopeAvailable($query, $userId)
    {
        return $query->where('private', false)
                ->orWhereIn('user_id',[$userId]);
    }
    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)
                ->withPivot(['role_id'])
                ->withTimestamps();
    }

    public function roles()
    {
        return $this->hasMany(Role::class);
    }

    public function chat()
    {
        return $this->hasMany(ChatMessage::class);
    }
}
