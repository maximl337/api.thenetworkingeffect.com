<?php

namespace App\Forms;

use Illuminate\Http\Request;

use App\User;
use App\Device;
use App\Roaming_user;
use App\Http\Requests;
use App\Services\TagService;
use App\Services\GeoService;
use App\Services\ImageService;
use App\Services\SanitizeUrlService;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class NewUser
{

	public function add($input, UploadedFile $avatar = null, UploadedFile $logo = null) {

        try {

            $user = new User;

            $user->password = bcrypt(!empty($input['password']) ? $input['password'] : "");

            $user->email = $input['email'];

            $user->first_name = $input['first_name'];

            $user->last_name = $input['last_name'];

            $user->save();

            // Avatar
	        if(!empty($input['avatar_url'])) {

	            $input['avatar'] = $input['avatar_url'];
	        
	        }
	        elseif(!is_null($avatar)) {

	            $input['avatar'] = (new ImageService)->upload( $avatar->getRealPath() );
	        }  

	        // Logo
	        if(!empty($input['logo_url'])) {

	            $input['logo'] = $input['logo_url'];

	        }
	        elseif(!is_null($logo)) {

	            $input['logo'] = (new ImageService)->upload( $logo->getRealPath() );
	            
	        }

            $input['permalink'] = (new SanitizeUrlService)->makePrettyUrl($user);


            // Lat Long
            $coordinates = $this->makeLocation($user, $input);

            $input['latitude'] = $coordinates['latitude'];

            $input['longitude'] = $coordinates['longitude'];

            // create user
            $user->update($input);

            // check for tags
            if(!empty($input['tags']))

                $user->tags()->attach( (new TagService)->getTagIds($input['tags']) );

            if(!empty($input['business_category']))

                $input['business_category'] = (new TagService)->create($input['business_category']);


            if(!empty($input['device_token']))

                $user->devices()->save(new Device(["token" => $input['device_token']]));

            return $user;

        } catch(Exception $e) {

            throw $e;
        }
	}

	/**
	 * [makeLocation description]
	 * @param  [type] $input [description]
	 * @param  User   $user  [description]
	 * @return [type]        [description]
	 */
	protected function makeLocation(User $user, $input)
	{
		$roamingCoordinatesGiven = (!empty($input['lng']) && !empty($input['lat']));

		$address_exists = (!empty($input['street']) && !empty($input['city']) && !empty($input['state']));

		$r = [
          		'latitude' => "",
          		'longitude' => ""
          	];  

        if($address_exists) {

          // try to get coordinates from given address
          $geoCode = (new GeoService)
          				->getLatLang($input['street'], $input['city'], $input['state']);

          $r = [
          	'latitude' => $geoCode['latitude'],
          	'longitude' => $geoCode['longitude']
          ];

        } // check if address exists

		if($roamingCoordinatesGiven) {

			Roaming_user::create([
				'latitude'  => $input['lat'],
				'longitude' => $input['lng'],
				'user_id'   => $user->id
			]); 

		// check if address return coordinates
		} elseif(!empty($geoCode['latitude']) && !empty($geoCode['longitude'])) {

			Roaming_user::create([
				'latitude'  => $geoCode['latitude'],
				'longitude' => $geoCode['longitude'],
				'user_id'   => $user->id
			]);

		}                

		// if original address did not yield coordinates and roaming coordinates were given
		// update user coordinates with roaming coordinates
		if( $roamingCoordinatesGiven && (empty($input['latitude']) && empty($input['longitude'])) )  {

			$r = [
          		'latitude' => $input['lat'],
          		'longitude' => $input['lng']
          	];  
		}


		return $r;
		
	}

}