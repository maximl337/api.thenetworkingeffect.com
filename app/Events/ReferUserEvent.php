<?php

namespace App\Events;

use App\Card_shares;
use Log;
use App\User;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ReferUserEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $cardShare;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Card_shares $cardShare)
    {
        $this->cardShare = $cardShare;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {

        if(!$this->cardShare->recipient_id) return ['user.'.$this->cardShare->subject_id];

        return [
            'user.'.$this->cardShare->recipient_id, 
            'user.'.$this->cardShare->subject_id
            ];
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        
        try {

            $notification_subject_count = User::find($this->cardShare->subject_id)
                                            ->notifications()
                                            ->where('seen', false)
                                            ->count();

            if(!$this->cardShare->recipient_id) {

                return [

                    'data' => [
                            
                        'type' => 'referral',
                        'data' => $this->cardShare,
                        'sender' => $this->cardShare->sender()->get(),
                        'subject'   => $this->cardShare->subject()->get(),
                        'notification_count' => (int) $notification_subject_count + 1
                    ]
                ];

            } 

            $notification_recipient_count = User::find($this->cardShare->recipient_id)
                                                ->notifications()
                                                ->where('seen', false)
                                                ->count();
            return [

                'data' => [
                        
                    'type' => 'referral',
                    'data' => $this->cardShare,
                    'sender' => $this->cardShare->sender()->get(),
                    'subject' => $this->cardShare->subject()->get(),
                    'notification_count' => (int) $notification_recipient_count + 1
                ]
            ];

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            Log::error('ReferUserEvent', [$e->getMessage()]);

            return [];


        } catch(\Exception $e) {

            Log::error('ReferUserEvent', [$e->getMessage()]);

            return [];
        }
        
    }
}
