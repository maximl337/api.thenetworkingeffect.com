<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChatMessageMentionEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        $subject = $this->data['subject'];
        if(!$subject) return [];
        return ['user.'.$subject->id];
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        $subject = $this->data['subject'];
        if(!$subject) return [];
        $notification_count = $subject->notifications()
                                        ->where('seen', false)
                                        ->count();
        return [
            'data' => [
                'type' => 'chat_mention',
                'data' => $this->data['chatMessage'],
                'sender' => $this->data['actor'],
                'notification_count' => (int) $notification_count + 1
            ]
        ];
    }
}
