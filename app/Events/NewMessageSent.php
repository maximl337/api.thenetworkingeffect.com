<?php

namespace App\Events;

use App\User;
use App\Message;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewMessageSent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['user.'.$this->message->to];
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        $notification_count = User::find($this->message->to)
                                        ->notifications()
                                        ->where('seen', false)
                                        ->count();
        return [

            'data' => [
                    
                'type' => 'message',
                'data' => $this->message,
                'from' => $this->message->from()->get(),
                'notification_count' => (int) $notification_count + 1
            ]
        ];
    }
}
