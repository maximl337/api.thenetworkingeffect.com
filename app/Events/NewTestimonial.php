<?php

namespace App\Events;

use App\User;
use App\Testimonial;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewTestimonial extends Event implements ShouldBroadcast
{
    use SerializesModels;


    public $testimonial;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Testimonial $testimonial)
    {
        $this->testimonial = $testimonial;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['user.'.$this->testimonial->subject_id];
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        $notification_count = User::find($this->testimonial->subject_id)
                                        ->notifications()
                                        ->where('seen', false)
                                        ->count();
        return [

            'data' => [
                    
                'type' => 'testimonial',
                'data' => $this->testimonial,
                'author' => $this->testimonial->author()->get(),
                'notification_count' => (int) $notification_count + 1
            ]
        ];
    }
}
