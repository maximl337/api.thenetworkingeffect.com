<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChatMessageCreatedEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [    
            'channel.'.$this->data['channel']->id
        ];
    }

    public function broadcastWith()
    {
        return [
            'data' => [
                'type' => 'chat',
                'channel_id' => $this->data['channel']->id,
                'chat_message' => $this->data['chat_message']
            ]
        ];
    }
}
