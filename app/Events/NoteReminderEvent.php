<?php

namespace App\Events;

use App\Note;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NoteReminderEvent extends Event
{
    use SerializesModels;

    public $note;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Note $note)
    {
        $this->note = $note;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        try {

            $recipient_id = $this->note->author_id;

            return ['user.'.$recipient_id];

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return [];

        } catch(Exception $e) {

            return [];
        }
        
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        try {

            return [

                'data' => [
                        
                    'type' => 'Note reminder',
                    'message' => $this->note->title,
                    'note' => $this->note,
                    'notification_count' => (int) $notification_count + 1
                ]
            ];

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return [];

        } catch(Exception $e) {

            return [];
        }
        
    }
}
