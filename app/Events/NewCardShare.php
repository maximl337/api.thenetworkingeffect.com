<?php

namespace App\Events;

use App\User;
use App\Card_shares;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewCardShare extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $cardShare;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Card_shares $cardShare)
    {
        $this->cardShare = $cardShare;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        if(!$this->cardShare->recipient_id) return [];

        return ['user.'.$this->cardShare->recipient_id];
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {

        if(!$this->cardShare->recipient_id) return [];
        
        $notification_count = User::find($this->cardShare->recipient_id)
                                        ->notifications()
                                        ->where('seen', false)
                                        ->count();
        return [

            'data' => [
                    
                'type' => 'card_share',
                'data' => $this->cardShare,
                'sender' => $this->cardShare->sender()->get(),
                'notification_count' => (int) $notification_count + 1
            ]
        ];
    }
}
