<?php

namespace App\Events;

use App\User;
use App\Event as TneEvent;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EventUpdate extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $event;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(TneEvent $event)
    {
        $this->event = $event;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {

        $channels = [];

        $users = $this->event->users()->get();

        foreach($users as $user) {
            $channels[] = 'user.'.$user->id;
        }

        return $channels;
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {

        return [

            'data' => [
                    
                'type' => 'event_update',
                'data' => $this->event,
                'notification_count' => false
            ]
        ];
    }
}
