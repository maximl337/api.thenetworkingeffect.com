<?php

namespace App\Events;

use App\User;
use App\Network;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NetworkFollowedBack extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $network;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Network $network)
    {
        $this->network = $network;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['user.'.$this->network->subject_id, 'live_event'];
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        $notification_count = User::find($this->network->subject_id)
                                        ->notifications()
                                        ->where('seen', false)
                                        ->count();
        return [

            'data' => [
                    
                'type' => 'network',
                'data' => $this->network,
                'follower' => $this->network->follower()->get(),
                'notification_count' => (int) $notification_count + 1
            ]
        ];
    }
}
