<?php

namespace App\Events;

use App\Events\Event;
use App\SortingReminder;
use App\Services\SortingService;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SortingReminderEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $sortingReminder;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(SortingReminder $sortingReminder)
    {
        $this->sortingReminder = $sortingReminder;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        try {

            $recipient_id = $this->sortingReminder->network()->firstOrFail()->follower_id;

            return ['user.'.$recipient_id];

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return [];

        } catch(Exception $e) {

            return [];
        }
        
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        try {

            // get push notification message
            $network = $this->sortingReminder->network()->firstOrFail();

            $subject = $network->subject()->firstOrFail();

            // get the category
            $sortingCategory = $this->sortingReminder->sortingCategory()->firstOrFail();

            // get push notification message
            $pushNotificationMessage = (new SortingService)->getPushNotificationMessage($sortingCategory) . $subject->first_name;

            $notification_count = $network->follower()
                                        ->firstOrFail()
                                        ->notifications()
                                        ->where('seen', false)
                                        ->count();


            return [

                'data' => [
                        
                    'type' => 'Sorting reminder',
                    'message' => $pushNotificationMessage,
                    'sorting_reminder_id' => $this->sortingReminder->id,
                    'network' => $network,
                    'subject'    => $subject,
                    'notification_count' => (int) $notification_count + 1
                ]
            ];

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return [];

        } catch(Exception $e) {

            return [];
        }
        
    }
}
