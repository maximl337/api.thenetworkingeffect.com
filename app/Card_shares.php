<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card_shares extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'card_shares';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sender_id',
        'recipient_id',
        'recipient_email',
        'subject_id',
        'message'
    ];

    /**
     * Get sender of card share
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id');
    }

    /**
     * Get recipient of card share
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recipient()
    {
        return $this->belongsTo('App\User', 'recipient_id');
    }

    /**
     * Get subject of card share
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subject()
    {
        return $this->belongsTo('App\User', 'subject_id');
    }
}
