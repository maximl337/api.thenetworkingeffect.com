<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from',
        'to',
        'parent_id',
        'body',
        'status',
        'original_id'
    ];

    /**
     * Get the Sender of the Message
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo 
     */
    public function from() {

        return $this->belongsTo('App\User', 'from');

    }

    /**
     * Get the recipient of the Message
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo 
     */
    public function to() {

        return $this->belongsTo('App\User', 'to');
        
    }

    public function deleted_messages()
    {
        return $this->belongsToMany('App\User', 'deleted_messages')->withTimestamps();
    }
}
