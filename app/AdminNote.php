<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminNote extends Model
{
    protected $table = 'admin_notes';

    protected $fillable = [
    	'user_id',
    	'admin_id',
    	'body'
    ];
}
