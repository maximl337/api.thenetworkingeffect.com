<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tags';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'original_id',
        'business_category',
        'association',
        'search_assist',
        'note_assist'
    ];

    /**
     * Get the Articles associated with the Tag
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany 
     */
    public function articles()
    {
        return $this->belongsToMany('App\Article')->withTimestamps();
    }

    /**
     * Get the Users associated with the Tag
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany 
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    /**
     * Get the Events associated with the Tag
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany 
     */
    public function events()
    {
        return $this->belongsToMany('App\Event')->withTimestamps();
    }

    /**
     * [scopeSearchAssist description]
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeSearchAssist($query)
    {
        return $query->where('search_assist', true)->orderBy('updated_at', 'DESC');
    }

    /**
     * [scopeNoteAssist description]
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeNoteAssist($query)
    {
        return $query->where('note_assist', true)->orderBy('updated_at', 'DESC');
    }
}
