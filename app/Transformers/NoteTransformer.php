<?php

namespace App\Transformers;


use Auth;
use App\User;

class NoteTransformer extends Transformer
{
    
    public function transform($note)
    {

        try {

            $user = User::findOrFail($note['subject_id']);

            $in_network = Auth::user()
                                ->network_following()
                                ->where('subject_id', $note['subject_id'])
                                ->exists();
                                
            $note['subject'] = [

                'user_id'       => $user->id,
                'first_name'    => $user->first_name,
                'last_name'     => $user->last_name,
                'job_title'     => $user->job_title,
                'business_name' => $user->business_name,
                'avatar'        => $user->avatar,
                'logo'          => $user->logo,
                'network'       => $in_network ? true : false

            ];

        } catch(\Exception $e) {

            return;

        }
        

        return $note;
    }
}
