<?php

namespace App\Transformers;

class ChannelTransformer extends Transformer
{
	public function transform($channel)
	{
		return [
			'name'		=> $channel['name'],
			'body'		=> $channel['description'],
			'logo'		=> $channel['logo'],
        	'user_id'	=> $channel['user_id'],
        	'id'		=> $channel['id'],
        	'private'	=> $channel['private']
		];
	}
}