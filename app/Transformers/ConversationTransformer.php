<?php

namespace App\Transformers;

use Auth;
use App\User;


class ConversationTransformer extends Transformer
{
    
    public function transform($message)
    {
        $user = User::find($message->from);

        $message->from_user = [];

        if($user) {

            $is_blocked = Auth::user()->isBlocked($user->id);

            $in_network = Auth::user()
                            ->network_following()
                            ->where('subject_id', $message->from)
                            ->exists();

            $message->from_user = [

                'user_id'       => $user->id,
                'first_name'    => $user->first_name,
                'last_name'     => $user->last_name,
                'job_title'     => $user->job_title,
                'business_name' => $user->business_name,
                'avatar'        => $user->avatar,
                'logo'          => $user->logo,
                'is_blocked'    => $is_blocked,
                'network'       => $in_network ? true : false

            ];

        }

        return $message;
    }
}