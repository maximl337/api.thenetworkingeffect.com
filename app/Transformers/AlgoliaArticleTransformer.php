<?php

namespace App\Transformers;

use App\Article;

class AlgoliaArticleTransformer extends Transformer
{

    public function transform($article)
    {
    
        $articleTags = Article::find($article['id'])->tags;

        $tags = [];

        foreach($articleTags as $articleTag) {

            $tags[] = $articleTag->name;
        }

        return [

            'title'         => $article['title'],
            'body'          => $article['body'],
            'teaser'        => $article['teaser'],
            'user_id'       => $article['user_id'],
            'created_at'    => strtotime($article['created_at']),
            'tags'          => $tags,
            '_tags'         => $tags,
            'objectID'      => $article['id']
        ]; 
    }
}