<?php

namespace App\Transformers;

use Auth;
use App\Note;
use App\User;
use App\Network;
use App\Services\NetworkService;

class NetworkTransformerWithoutPlaceholders extends Transformer
{
    
    public function transform($network)
    {

        try {

            $user = User::findOrFail($network['subject_id']);

            // dont return placeholders when viewing others network
            if($user->is_placeholder) return;

            $in_network = Auth::user()
                            ->network_following()
                            ->where('subject_id', $user['id'])
                            ->exists();

            $mutualContactCount = (new NetworkService)->getCountOfMutualConnectionsBetweenTwoUsers(Auth::user(), $user);

            $mutualContact = (new NetworkService)->isContact(Auth::user(), $user);

            // get sorting category
            $sortingCategory = Network::findOrFail($network['id'])->sorting()->latest()->first();

            //$notes = Auth::user()->notes()->where('subject_id', $user->id)->latest()->get();

            //$tags = $user->tags()->get();

            $is_blocked = Auth::user()->isBlocked($user->id);

            $resp = [

                'user_id'                   => $user->id,
                'first_name'                => $user->first_name,
                'last_name'                 => $user->last_name,
                'job_title'                 => $user->job_title,
                'business_name'             => $user->business_name,
                'avatar'                    => $user->avatar,
                'logo'                      => $user->logo,
                'network'                   => $in_network ? true : false,
                'connection'                => $network,
                'mutual_connections_count'  => $mutualContactCount,  
                'testimonials'              => [], //$user->testimonials()->where('approved', true)->get(),
                'articles'                  => [], //$user->articles()->get(),
                'notes'                     => [], //$notes,
                'tags'                      => [], //$tags,
                'is_placeholder'            => $user->is_placeholder ? true : false,
                'sorting_category'          => !is_null($sortingCategory) ? $sortingCategory->name : 'Park',
                'seen'                      => $network['seen'] ? true : false,
                'message'                   => $network['message'],
                'new'                       => $network['seen'] ? true : false,
                'network_id'                => $network['id'],
                'is_blocked'                => $is_blocked,
                'mutual_contact'            => $mutualContact,
            ];
            
            return $resp;

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $network;

        } catch(\Exception $e) {

            return $network;
        }
        
    }
}