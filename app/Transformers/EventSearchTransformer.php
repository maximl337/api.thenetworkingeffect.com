<?php

namespace App\Transformers;

use Auth;
use App\User;
use App\Event;
use App\Services\GeoService;

class EventSearchTransformer extends SearchTransformer
{
    
    public function transform($hit)
    {

        if(Auth::check()) {

            $event = Event::find($hit['objectID']);

            if($event) {

                $geoService = new GeoService;

                $hit['attending'] = $event->users()->where('user_id', Auth::id())->exists();

                // GET DISTANCE
                $userCoordinates = Auth::user()->roaming_coordinates()->latest()->first();

                if($userCoordinates && ($event->latitude && $event->longitude)) {

                    $hit['distance'] = $geoService->getDistanceBetweenTwoCoordinates($userCoordinates->latitude, $userCoordinates->longitude, $event->latitude, $event->longitude) ?: "0";
                }

            } //EO event found
            
        } // EO Auth check
        
        return $hit;
    }
}