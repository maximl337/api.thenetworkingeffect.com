<?php

namespace App\Transformers;

use Auth;
use Log;
use App\Note;
use App\User;
use App\Network;
use App\Services\NetworkService;

class NetworkTransformer extends Transformer
{
    
    public function transform($network)
    {
        
        try {

            $user = User::findOrFail($network['subject_id']);

            if($user) {

                $in_network = Auth::user()
                                ->network_following()
                                ->where('subject_id', $user['id'])
                                ->exists();

                //$mutualContactCount = (new NetworkService)->getCountOfMutualConnectionsBetweenTwoUsers(Auth::user(), $user);
                
                // $mutualContact = (new NetworkService)->isContact(Auth::user(), $user);   

                // if(!$mutualContact) {
                //     Log::info("myFollowing:notAContact", [ $user->id ]);
                //     return null;
                // }             
                // Log::info("myFollowing:isContact", [ $user->id ]);

                //$notes = Auth::user()->notes()->where('subject_id', $user->id)->latest()->get();

                //$tags = $user->tags()->get();

                $resp = [

                    'user_id'                   => $user->id,
                    'first_name'                => $user->first_name,
                    'last_name'                 => $user->last_name,
                    'job_title'                 => $user->job_title,
                    'business_name'             => $user->business_name,
                    'avatar'                    => $user->avatar,
                    'logo'                      => $user->logo,
                    'network'                   => $in_network ? true : false,
                    'connection'                => $network,
                    'mutual_connections_count'  => 0,        
                    'testimonials'              => [], //$user->testimonials()->where('approved', true)->get(),
                    'articles'                  => [], //$user->articles()->get(),
                    'notes'                     => [], //$notes,
                    'tags'                      => [], //$tags,
                    'is_placeholder'            => $user->is_placeholder ? true : false,
                    'seen'                      => $network['seen'] ? true : false,
                    'new'                       => $network['seen'] ? true : false,
                    'message'                   => $network['message'],
                    'network_id'                => $network['id'],
                    'mutual_contact'            => true,
                ];

                // if user is placeholder
                // return data from placeholder_user table
                if($user->is_placeholder) {

                    // get the placeholder record
                    $placeholder = $user->userPlaceholders()->where('owner_id', Auth::id())->first();

                    if(!is_null($placeholder)) {

                        return [

                            'user_id'                   => $user->id,
                            'first_name'                => $placeholder->first_name,
                            'last_name'                 => $placeholder->last_name,
                            'avatar'                    => $placeholder->avatar,
                            'job_title'                 => $placeholder->job_title,
                            'business_name'             => $placeholder->business_name,
                            'avatar'                    => $placeholder->avatar,
                            'network'                   => $in_network ? true : false,
                            'connection'                => $network,
                            'mutual_connections_count'  => 0,  
                            'testimonials'              => [], //$user->testimonials()->where('approved', true)->get(),
                            'articles'                  => [], //$user->articles()->get(),
                            'notes'                     => [], //$notes,
                            'tags'                      => [], //$tags,
                            'is_placeholder'            => $user->is_placeholder ? true : false,
                            'message'                   => $network['message']
                        ];

                    }
                    else {

                        return $resp;
                        
                    } // eo if placeholder exists

                } // eo if placeholder

                return $resp;
            }

            Log::error('NetworkTransformer', ["No user found"]);

            return null;

        } catch(\Exception $e) {

            Log::error("NetworkTransformer", [$e->getMessage()]);
            
            return null;
        }
        
    }
}