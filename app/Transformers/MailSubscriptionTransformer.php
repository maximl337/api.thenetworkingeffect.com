<?php

namespace App\Transformers;

use App\MandrillTemplate;


class MailSubscriptionTransformer extends Transformer
{

    public function transform($template)
    {
        $description = '';

        // Add descriptions to templates
        switch($template['label']) {

            case 'NEW_MESSAGE':
                $description = 'A member sent you a message';
                break;
            case 'NEW_FOLLOWER':
                $description = 'A member has invited you to join their network';
                break;
            case 'NEW_CARD_SHARE_RECIPIENT':
                $description = 'A member has sent you a referral';
                break;
            case 'NEW_TESTIMONIAL':
                $description = 'A member has left you a testimonial and it needs to be approved.';
                break;
            // case 'EVENT_UPDATED':
            //     $description = 'An event you are attending has been updated';
            //     break;
            // case 'EVENT_DELETED':
            //     $description = 'An event you are attending has been cancelled';
            //     break;
            // case 'EVENT_JOIN':
            //     $description = 'A member has joined an event you created';
            //     break;

        } // eo switch

        // return empty for some templates
        switch($template['label']) {
            case 'TNE_MESSAGE':
            case 'NEW_USER':
            case 'NEW_CARD_SHARE_SUBJECT':
            case 'PASSWORD-RESET':
                return [];

        } // eo switch

        return [

            'id' => $template['id'],
            'name' => $template['label'],
            'description' => $description
        ];
    }
}