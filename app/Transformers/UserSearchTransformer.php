<?php

namespace App\Transformers;

use Log;
use Auth;
use App\Services\NetworkService;
use App\User;
use App\Services\GeoService;

class UserSearchTransformer extends SearchTransformer
{
    
    public function transform($hit)
    {

        try {

            $user = User::findOrFail($hit['objectID']);

            $hit['testimonials'] = []; //$user->testimonials()->where('approved', true)->get();

            $hit['articles'] = []; //$user->articles()->get();

            $hit['partners'] = $user->partners()->get();

            if(Auth::user()) {

                $in_network = Auth::user()->network_following()
                                ->where('subject_id', $hit['objectID'])
                                ->exists();

                $mutualContactCount = (new NetworkService)->getCountOfMutualConnectionsBetweenTwoUsers(Auth::user(), $user);

                $hit['network'] = $in_network ? true : false;

                $hit['mutual_connection'] = false;

                $hit['mutual_connections_count'] = $mutualContactCount; 

                if($hit['network']) {

                    $mutual_connection = Auth::user()
                                        ->network_subject()
                                        ->where('follower_id', $hit['objectID'])
                                        ->exists();

                    $hit['mutual_connection'] = $mutual_connection ? true : false;
                }

                $geoService = new GeoService;

                if(!empty($hit['_rankingInfo']['matchedGeoLocation']['distance'])) {

                    $hit['distance'] = ($hit['_rankingInfo']['matchedGeoLocation']['distance'] / 1000);

                } else {

                    $hit['distance'] = $geoService->getDistaneBetweenTwoUsers(Auth::id(), $hit['objectID']) ?: "0";
                }

            } // EO Auth check

        } catch(\Exception $e) {
            // do something
        }
        
        

        // return distance based on users current coordinates
        return $hit;
    }
}