<?php

namespace App\Transformers;

use Auth;
use App\Event;
use App\User;
use App\Services\GeoService;

class EventTransformer extends Transformer
{

    public function transform($event)
    {
        $event = Event::find($event['id']);

        $eventTags = $event->tags->toArray();

        $event['attending'] = $event->users()->where('user_id', Auth::id())->exists() ? true : false;

        $event['user_count'] = $event->users()->count();

        $event['tags'] = $event->tags->toArray();

        return $event;

        
    }

}