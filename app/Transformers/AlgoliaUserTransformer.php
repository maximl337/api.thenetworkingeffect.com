<?php

namespace App\Transformers;

use Auth;
use App\User;
use App\Roaming_user;

class AlgoliaUserTransformer extends Transformer
{

    public function transform($user)
    {
    
        $userTags = User::find($user['id'])->tags;

        $tags = [];

        foreach($userTags as $userTag) {

            $tags[] = $userTag->name;
        }

        $userPartners = User::find($user['id'])->partners;

        $partners = [];

        foreach($userPartners as $partner) {
            $partners[] = $partner->name;
        }

        $roamingData = Roaming_user::where('user_id', $user['id'])->where('latitude', '!=', 0)->where('longitude', '!=', 0)->latest()->first();

        if($roamingData) {

            $user['longitude']  = $roamingData->longitude;

            $user['latitude']   = $roamingData->latitude;
        } 

        $return = [

            'objectID'          => $user['id'],
            'id'                => $user['id'],
            'first_name'        => $user['first_name'],
            'last_name'         => $user['last_name'],
            'about_me'          => isset($user['about_me']) ? $user['about_me'] : "",
            'about_business'    => isset($user['about_business']) ? $user['about_business'] : "",
            'business_name'     => isset($user['business_name']) ? $user['business_name'] : "",
            'job_title'         => isset($user['job_title']) ? $user['job_title'] : "",
            'avatar'            => isset($user['avatar']) ? $user['avatar'] : "",
            'logo'              => isset($user['logo']) ? $user['logo'] : "",
            'street'            => isset($user['street']) ? $user['street'] : "",
            'city'              => isset($user['city']) ? $user['city'] : "",
            'state'             => isset($user['state']) ? $user['state'] : "",
            'country'           => isset($user['country']) ? $user['country'] : "",
            'business_category' => isset($user['business_category']) ? $user['business_category'] : "",
            'association'       => isset($user['association']) ? $user['association'] : "",
            'website_url'       => isset($user['website_url']) ? $user['website_url'] : "",
            'facebook_url'      => isset($user['facebook_url']) ? $user['facebook_url'] : "",
            'twitter_url'       => isset($user['twitter_url']) ? $user['twitter_url'] : "",
            'googleplus_url'    => isset($user['googleplus_url']) ? $user['googleplus_url'] : "",
            'linkedin_url'      => isset($user['linkedin_url']) ? $user['linkedin_url'] : "",
            'youtube_url'       => isset($user['youtube_url']) ? $user['youtube_url'] : "",
            'blog_url'          => isset($user['blog_url']) ? $user['blog_url'] : "",
            'tags'              => $tags,
            '_tags'             => $tags,
            'partners'          => $partners
        ]; 

        if(!empty($user['latitude']) && !empty($user['longitude'])) {

            $return['_geoloc'] = [
                "lat" => (float) $user['latitude'], 
                "lng" => (float) $user['longitude']
            ];
        }

        return $return;
    }
}