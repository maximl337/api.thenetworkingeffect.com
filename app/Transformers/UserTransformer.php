<?php

namespace App\Transformers;

use Auth;
use App\User;
use App\Network;
use App\PlaceholderUser;
use App\Services\GeoService;
use App\Services\NetworkService;

class UserTransformer extends Transformer
{
    
    public function transform($user)
    {
        $_user = User::find($user['id']);
        // add tags
        $userTags = $_user->tags;

        $in_network = false;

        $mutual_connection = false;

        $invitation_count = false;

        $distance = "0";

        $user['tags'] = [];

        foreach($userTags as $userTag) {

            array_push($user['tags'], $userTag->name);
        }

        $association_keywords = $_user->association_keywords()->get();

        $partners = $_user->partners()->get();

        $channels = $_user->channels()->get();

        if(Auth::check()) {

            $_authenticated = Auth::user();

            $geoService = new GeoService;

            $distance = $geoService->getDistaneBetweenTwoUsers($_authenticated->id, $user['id']);

            $in_network = $_authenticated
                            ->network_following()
                            ->where('subject_id', $user['id'])
                            ->exists();

            if($in_network) {

                $mutual_connection = $_authenticated
                                    ->network_subject()
                                    ->where('follower_id', $user['id'])
                                    ->exists();
            }

            $network_invite = $_authenticated
                                    ->network_subject()
                                    ->where('follower_id', $user['id'])
                                    ->latest()->first();

            $last_invite_sent = $_authenticated
                                    ->network_following()
                                    ->where('subject_id', $user['id'])
                                    ->latest()->first();

            if($user['is_placeholder'] != 0) {

                $placeholder = PlaceholderUser::where('user_id', $user['id'])->where('owner_id', $_authenticated->id)->first();
 
                if(!is_null($placeholder)) {
                    $invitation_count = $placeholder->invitation_count;
                } 
            }

            $mutualContactCount = (new NetworkService)
                                    ->getCountOfMutualConnectionsBetweenTwoUsers($_authenticated, $_user);

            $network_count = Network::where('follower_id', $_authenticated->id)
                                      ->where('subject_id', $user['id'])
                                      ->count();

            $is_opportunity = $_authenticated->opportunities()->where('subject_id', $user['id'])->exists();

            $is_blocked = $_authenticated->blocked()->where('blocked_id', $user['id'])->exists();
                            
        } // check Auth

        
        $response = [
            'first_name'                    => $user['first_name'],
            'last_name'                     => $user['last_name'],
            'business_name'                 => $user['business_name'],
            'email'                         => $user['email'],
            'job_title'                     => $user['job_title'],
            'avatar'                        => $user['avatar'],
            'logo'                          => $user['logo'],
            'tags'                          => $user['tags'],
            'id'                            => $user['id'],
            'website_url'                   => $user['website_url'],
            'facebook_url'                  => $user['facebook_url'],
            'twitter_url'                   => $user['twitter_url'],
            'googleplus_url'                => $user['googleplus_url'],
            'linkedin_url'                  => $user['linkedin_url'],
            'youtube_url'                   => $user['youtube_url'],
            'blog_url'                      => $user['blog_url'],
            'about_me'                      => $user['about_me'],
            'about_business'                => $user['about_business'],
            'phone_1'                       => $user['phone_1'],
            'phone_2'                       => $user['phone_2'],
            'fax'                           => $user['fax'],
            'business_category'             => $user['business_category'],
            'permalink'                     => $user['permalink'],
            'timezone'                      => $user['timezone'],
            'reminder_notification_time'    => $user['reminder_notification_time'],
            'association'                   => $user['association'],
            'distance'                      => $distance ?: "",
            'network'                       => $in_network ? true : false,
            'mutual_connection'             => $mutual_connection ? true : false,
            'mutual_connections_count'      => $mutualContactCount,
            'is_placeholder'                => $user['is_placeholder'] ? true : false,
            'placeholder_invitation_count'  => $invitation_count,
            'hide_address'                  => $user['hide_address'],
            'hide_email'                    => $user['hide_email'],
            'opportunity'                   => $is_opportunity,
            'network_count'                 => $network_count,
            'partners'                      => $partners,
            'channels'                      => $channels,
            'network_invite'                => $network_invite,
            'last_invite_sent'              => $last_invite_sent,
            'is_blocked'                    => $is_blocked,
            'association_keywords'          => $association_keywords
        ];

        if(!$user['hide_address'] || ( $user['id'] == Auth::id() ) ) {

            $response['street']     = $user['street'];
            $response['city']       = $user['city'];
            $response['state']      = $user['state'];
            $response['country']    = $user['country'];
            $response['zip_code']   = $user['zip_code'];

        }
    
        return $response;
    }
}