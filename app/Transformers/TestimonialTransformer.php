<?php

namespace App\Transformers;


use Auth;
use App\User;

class TestimonialTransformer extends Transformer
{
    
    public function transform($testimonial)
    {

        try {

            $user = User::findOrFail($testimonial['author_id']);

            $in_network = Auth::user()
                                ->network_following()
                                ->where('subject_id', $testimonial['author_id'])
                                ->exists();

            $mutual_testimonial = Auth::user()->sent_testimonials()->where('subject_id', $user->id)->first();

            $testimonial['author'] = [

                'user_id'               => $user->id,
                'first_name'            => $user->first_name,
                'last_name'             => $user->last_name,
                'job_title'             => $user->job_title,
                'business_name'         => $user->business_name,
                'avatar'                => $user->avatar,
                'logo'                  => $user->logo,
                'network'               => $in_network ? true : false,
                'mutual_testimonial'    => $mutual_testimonial ? true : false

            ];

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            $testimonial['author'] = ['error' => $e->getMessage()];

        }
        

        return $testimonial;
    }
}