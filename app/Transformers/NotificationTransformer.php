<?php

namespace App\Transformers;

use DB;
use App\User;
use App\Event;
use App\Message;
use App\Network;
use App\Testimonial;
use App\Card_shares;
use App\ChatMessage;
use App\Notification;
use App\Notification_type;

class NotificationTransformer extends Transformer
{
    
    public function transform($notification)
    {
        $sender = User::find($notification['actor_id'], ['id', 'first_name', 'last_name', 'avatar']);

        if(!is_null($sender)) {
            $sender_full_name = $sender['first_name'] . ' ' . $sender['last_name'];
            $sender['network'] = Network::where('follower_id', $notification['subject_id'])->where('subject_id', $notification['actor_id'])->exists();
            $notification['sender'] = $sender;
            $notificationType = Notification_type::find($notification['notification_type_id']);
            switch ($notificationType->name) {
                case 'EVENT_UPDATED':
                case 'EVENT_DELETED':
                    $notification['object'] = Event::withTrashed()
                        ->where('id', $notification['object_id'])
                        ->first();    
                    $notification['message'] = $notification['object'] 
                        ? $notification['object']['name'] . $notificationType->message 
                        : "";
                    break;
                case 'NEW_MESSAGE':
                    $notification['object'] = Message::find($notification['object_id']);
                    $notification['message'] = $sender_full_name . $notificationType->message;
                    break;
                case 'FOLLOWED_BACK':
                    $notification['object'] = Network::find($notification['object_id']);
                    $notification['message'] = $sender_full_name . $notificationType->message;
                    break;
                case 'NEW_FOLLOWER':
                    $notification['object'] = Network::find($notification['object_id']);
                    $notification['message'] = $sender_full_name . $notificationType->message;
                    break;
                case 'NEW_TESTIMONIAL':
                    $notification['object'] = Testimonial::find($notification['object_id']);
                    $notification['message'] = $sender_full_name . $notificationType->message;
                    break;
                case 'NEW_CARD_SHARE_RECIPIENT':
                    $notification['object'] = Card_shares::find($notification['object_id']);
                    $notification['message'] = $sender_full_name . $notificationType->message;
                    break;
                case 'NEW_CARD_SHARE_SUBJECT':
                    $notification['object'] = Card_shares::find($notification['object_id']);
                    $notification['message'] = $sender_full_name . $notificationType->message;
                    break;
                case 'FOLLOW-UP_TOMORROW':
                    $notification['object'] = Network::find($notification['object_id']);
                    $notification['message'] = $notificationType->message . $sender_full_name;
                    break;
                case 'FOLLOW-UP_IN_ONE_WEEK':
                    $notification['object'] = Network::find($notification['object_id']);
                    $notification['message'] = $notificationType->message . $sender_full_name;
                    break;
                case 'RESEARCH_WITHIN_ONE_WEEK':
                    $notification['object'] = Network::find($notification['object_id']);
                    $notification['message'] = $notificationType->message . $sender_full_name;
                    break;
                case 'NEW_REFERRAL':
                    $referral = Card_shares::find($notification['object_id']);
                    if($referral):
                        $notification['referred_user'] = User::find($referral->subject_id, ['id', 'first_name', 'last_name', 'avatar', 'business_name', 'job_title']);
                        $notification['object'] = $referral;
                        $notification['message'] = $sender_full_name . $notificationType->message;     
                    endif;
                    break;
                case 'REFERRED':
                    $notification['object'] = Card_shares::find($notification['object_id']);
                    $notification['message'] = $sender_full_name . $notificationType->message;
                    break;
                case 'NEW_CHANNEL_CHAT_MENTION':
                    $notification['object'] = $chatMessage = ChatMessage::find($notification['object_id']);
                    $notification['message'] = $sender_full_name . $notificationType->message . $chatMessage->channel->name;
                    break;
                default:
                    # code...
                    break;
            }
        }
        return $notification;
    }

}