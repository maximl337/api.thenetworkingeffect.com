<?php

namespace App\Transformers;

use Log;

abstract class SearchTransformer
{
    
    public function transformCollection(array $results)
    {
        $t['hits'] = array_map([$this, 'transform'], $results['hits']);

        $t['nbHits']            = $results['nbHits'];
        $t['page']              = $results['page'];
        $t['nbPages']           = $results['nbPages'];
        $t['hitsPerPage']       = $results['hitsPerPage'];
        $t['processingTimeMS']  = $results['processingTimeMS'];
        $t['query']             = $results['query'];
        $t['params']            = $results['params'];

        return $t; 
    }

    public abstract function transform($result);
}