<?php

namespace App\Transformers;

use Auth;
use App\Note;
use App\User;
use App\Network;
use App\Services\NetworkService;

class UsersNetworkTransformer extends Transformer
{
    
    public function transform($network)
    {

        try {

            $user = User::findOrFail($network['subject_id']);

            $follower = User::findOrFail($network['follower_id']);

            // dont return placeholders when viewing others network
            if($user->is_placeholder) return;

            $in_network = Auth::user()
                            ->network_following()
                            ->where('subject_id', $user['id'])
                            ->exists();

            $mutualContactCount = (new NetworkService)->getCountOfMutualConnectionsBetweenTwoUsers(Auth::user(), $user);

            $followersMutualContact = (new NetworkService)->isContact($follower, $user);

            $mutualContact = (new NetworkService)->isContact(Auth::user(), $user);

            if(!$followersMutualContact) {
                return [];
            }
            
            //$notes = Auth::user()->notes()->where('subject_id', $user->id)->latest()->get();

            //$tags = $user->tags()->get();

            $is_blocked = Auth::user()->isBlocked($user->id);

            $resp = [

                'user_id'                   => $user->id,
                'first_name'                => $user->first_name,
                'last_name'                 => $user->last_name,
                'job_title'                 => $user->job_title,
                'business_name'             => $user->business_name,
                'avatar'                    => $user->avatar,
                'logo'                      => $user->logo,
                'network'                   => $in_network ? true : false,
                'connection'                => $network,
                'mutual_connections_count'  => $mutualContactCount,  
                'testimonials'              => [], //$user->testimonials()->where('approved', true)->get(),
                'articles'                  => [], //$user->articles()->get(),
                'notes'                     => [], //$notes,
                'tags'                      => [], //$tags,
                'is_placeholder'            => $user->is_placeholder ? true : false,
                'seen'                      => $network['seen'] ? true : false,
                'message'                   => $network['message'],
                'new'                       => $network['seen'] ? true : false,
                'network_id'                => $network['id'],
                'is_blocked'                => $is_blocked,
                'mutual_contact'            => $mutualContact,
            ];
            
            return $resp;

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return null;
        } catch(\Exception $e) {

            return null;  
        }
        
    }
}