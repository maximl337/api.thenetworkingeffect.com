<?php

namespace App\Transformers;

use Auth;
use App\User;

class InboxTransformer extends Transformer
{
    
    public function transform($message)
    {
        $user = User::withTrashed()->where('id', $message->connection_id)->first();

        if($user) {
            $_authenticated = Auth::user();
            
            $in_network = $_authenticated
                            ->network_following()
                            ->where('subject_id', $message->connection_id)
                            ->exists();

            $is_blocked = $_authenticated->blocked()->where('blocked_id', $message->connection_id)->exists();                            
            $message->connection = [

                'user_id'       => $user->id,
                'first_name'    => $user->first_name,
                'last_name'     => $user->last_name,
                'job_title'     => $user->job_title,
                'business_name' => $user->business_name,
                'avatar'        => $user->avatar,
                'logo'          => $user->logo,
                'network'       => $in_network ? true : false,
                'is_blocked'    => $is_blocked
            ];
        }
        return $message;
    }
}