<?php

namespace App\Transformers;

use App\User;

class MakePlaceholderTransformer extends Transformer
{

	public function transform($placeholder)
	{

		$r = [

			'first_name' 				    => !empty($placeholder['first_name'])     ? $placeholder['first_name']     : "",
			'last_name' 				    => !empty($placeholder['last_name'])      ? $placeholder['last_name']      : "",
			'avatar' 				        => !empty($placeholder['avatar'])         ? $placeholder['avatar']         : "",
			'logo' 				            => !empty($placeholder['logo'])           ? $placeholder['logo']           : "",
			'job_title' 				    => !empty($placeholder['job_title'])      ? $placeholder['job_title']      : "",
			'business_name' 				=> !empty($placeholder['business_name'])  ? $placeholder['business_name']  : "",
			'phone_1' 				        => !empty($placeholder['phone_1'])        ? $placeholder['phone_1']        : "",
			'phone_2' 				        => !empty($placeholder['phone_2'])        ? $placeholder['phone_2']        : "",
			'fax' 				            => !empty($placeholder['fax'])            ? $placeholder['fax']            : "",
			'website_url' 				    => !empty($placeholder['website_url'])    ? $placeholder['website_url']    : "",
			'facebook_url' 				    => !empty($placeholder['facebook_url'])   ? $placeholder['facebook_url']   : "",
			'twitter_url' 				    => !empty($placeholder['twitter_url'])    ? $placeholder['twitter_url']    : "",
			'googleplus_url' 				=> !empty($placeholder['googleplus_url']) ? $placeholder['googleplus_url'] : "",
			'linkedin_url' 				    => !empty($placeholder['linkedin_url'])   ? $placeholder['linkedin_url']   : "",
			'youtube_url' 				    => !empty($placeholder['youtube_url'])    ? $placeholder['youtube_url']    : "",
			'blog_url' 				        => !empty($placeholder['blog_url'])       ? $placeholder['blog_url']       : "",
			'street' 				        => !empty($placeholder['street'])         ? $placeholder['street']         : "",
			'city' 				            => !empty($placeholder['city'])           ? $placeholder['city']           : "",
			'state' 				        => !empty($placeholder['state'])          ? $placeholder['state']          : "",
			'zip_code' 				        => !empty($placeholder['zip_code'])       ? $placeholder['zip_code']       : "",
			'country' 				        => !empty($placeholder['country'])        ? $placeholder['country']        : "",

		];

		return $r;
	}
}