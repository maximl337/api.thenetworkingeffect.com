<?php

namespace App\Transformers;

use Auth;
use App\Event;
use App\Utilities\Timezone;

class AlgoliaEventTransformer extends Transformer
{
    
    public function transform($event)
    {
    
        $eventTags = Event::findOrFail($event['id'])->tags;

        $tags = [];

        foreach($eventTags as $eventTag) {

            $tags[] = $eventTag->name;
        }

        $timezone = new Timezone;

        $event_timestamp = $timezone->get_normalized_unix_timestamp($event['event_date'] . ' ' . $event['end_time'], $event['timezone']);

        $isTimestampValid = $timezone->isValidTimeStamp((string) $event_timestamp);

        if($isTimestampValid) $event_timestamp += 3600;
       
        return [

            'objectID'          => $event['id'],
            'id'                => $event['id'],
            'name'              => $event['name'],
            'description'       => $event['description'],
            'event_date'        => $event_timestamp,
            'banner_url'        => isset($event['banner_url']) ? $event['banner_url'] : "",
            'start_time'        => $event['start_time'],
            'end_time'          => $event['end_time'],
            'street'            => $event['street'],
            'city'              => $event['city'],
            'state'             => $event['state'],
            'tags'              => $tags,
            '_tags'             => $tags,
            '_geoloc'           => [
                                    "lat" => (float) $event['latitude'], 
                                    "lng" => (float) $event['longitude']
                                    ]
        ];
    }
    
}