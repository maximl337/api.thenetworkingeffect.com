<?php

namespace App\Transformers;

use App\Tag;

class AssociationTagTransformer extends Transformer
{

	public function transform($tag)
	{

		$r['id'] = $tag['id'];

		$r['name'] = ucfirst(str_replace("-", " ", $tag['name']));

		return $r;
	}
}