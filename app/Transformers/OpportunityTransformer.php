<?php

namespace App\Transformers;

use Auth;
use App\User;
use App\Article;
use App\Services\NetworkService;

class OpportunityTransformer extends Transformer {

	public function transform($opportunity) {

		try {

			$subject = User::findOrFail($opportunity->subject_id);

			$opportunity->subject = $subject;

			// $mutualContactCount = (new NetworkService)->getCountOfMutualConnectionsBetweenTwoUsers(Auth::user(), $subject);

			// $opportunity->mutual_connections_count = $mutualContactCount;


		} catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

			// log it

		} catch(\Exception $e) {

			// log it
		}
		
		return $opportunity;

	}
}