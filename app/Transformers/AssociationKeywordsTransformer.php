<?php

namespace App\Transformers;

class AssociationKeywordsTransformer extends Transformer
{
	public function transform($associationKeyword)
	{
		return [
			'id' => $associationKeyword['id'],
			'keyword' => $associationKeyword['keyword']
		];
	}
}