<?php

namespace App\Transformers;

use Auth;
use App\Note;
use App\User;
use App\Network;
use App\Notification;
use App\Notification_type;
use App\Services\NetworkService;

class InvitesTransformer extends Transformer
{
    
    public function transform($network)
    {

        try {

            $user = User::findOrFail($network['follower_id']);

            $is_blocked = Auth::user()->isBlocked($user->id);

            $in_network = Auth::user()
                            ->network_following()
                            ->where('subject_id', $user['id'])
                            ->exists();

            $mutualContact = (new NetworkService)->isContact(Auth::user(), $user);

            if($mutualContact) {
                return null;    
            }


            $notification_type_id = Notification_type::where("name", "NEW_FOLLOWER")->first()->id;

            // check if notification exists for this invite
            // since it may have been deleted
            // to prevent duplication
            $notificationExists = Notification::where('object_id', $network['id'])
                                                ->where("notification_type_id", $notification_type_id)
                                                ->exists();

            // If notification does not exists then dont
            // add in payload
            if(!$notificationExists) {
                return null;
            }

            $network['follower'] = [

            	'first_name' 	=> $user->first_name,
            	'last_name'	 	=> $user->last_name,
            	'avatar'	 	=> $user->avatar,
            	'job_title'	 	=> $user->job_title,
  	          	'business_name' => $user->business_name,
  	          	'is_blocked' 	=> $is_blocked,
                'network'       => $in_network
            ];

            $network['new'] = $network['seen'] ? true : false;
            
            return $network;

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

        	//$network['error'] = $e->getMessage();
            return null;

        } catch(\Exception $e) {

        	//$network['error'] = $e->getMessage();
            return null;
        }
        
    }
}