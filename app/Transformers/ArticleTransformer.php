<?php

namespace App\Transformers;

use App\User;
use App\Article;

class ArticleTransformer extends Transformer {

	public function transform($article) {

		try {

			$article['author'] = User::findOrFail($article['user_id']);

		} catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

			// log it

		}
		
		return $article;

	}
}