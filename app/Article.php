<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'articles';

    /**
     * Mass assignment attrbutes
     * @var arr
     */
    protected $fillable = [
        'title',
        'body',
        'teaser',
        'user_id',
        'original_id',
        'permalink'
    ];

    /**
     * Get the Author of the Article
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the Tags of the Article
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }
}
