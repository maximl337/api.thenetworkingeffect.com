<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailUnsub extends Model
{
    protected $table = 'mail_unsubs';

    protected $fillable = [
        'user_id',
        'template_id'
    ];
}
