<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roaming_user extends Model
{
    protected $table = 'roaming_users';

    protected $fillable = [
        'latitude',
        'longitude',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
