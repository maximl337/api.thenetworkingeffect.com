<?php

namespace App;


use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\Services\NotificationCountService;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    public static $autoIndex = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

            'first_name', 
            'last_name', 
            // 'email', 
            // 'password',
            'provider',
            'avatar',
            'logo',
            'job_title',
            'phone_1',
            'phone_2',
            'fax',
            'about_me',
            'about_business',
            'website_url',
            'facebook_url',
            'twitter_url',
            'googleplus_url',
            'linkedin_url',
            'youtube_url',
            'blog_url',
            'street',
            'city',
            'state',
            'zip_code',
            'country',
            'latitude',
            'longitude',
            'business_category',
            'association',
            'business_name',
            'permalink',
            'original_id',
            'timezone',
            'reminder_notification_time',
            'hide_address',
            'hide_email'
            ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Get User Articles
     * 
     */
    
    /**
     * Get User Articles
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany 
     */
    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    /**
     * Get User Tags
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }

    /**
     * Get Events User attends
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */ 
    public function events()
    {
        return $this->belongsToMany('App\Event');
    }

    /**
     * Get User events
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function my_events()
    {
        return $this->hasMany('App\Event');
    }

    /**
     * Get Messages received by user
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages() {

        return $this->hasMany('App\Message', 'to');
    }

    /**
     * Get Messages sent by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sent_messages() {

        return $this->hasMany('App\Message', 'from');
        
    }

    public function deleted_messages()
    {
        return $this->belongsToMany('App\Message', 'deleted_messages')->withTimestamps();
    }

    /**
     * Get Testimonials received by User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function testimonials()
    {
        return $this->hasMany('App\Testimonial', 'subject_id');
    }

    /**
     * Get Testimonials sent by User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sent_testimonials()
    {
        return $this->hasMany('App\Testimonial', 'author_id');
    }

    /**
     * Get Notes Written by User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany('App\Note', 'author_id');
    }

    /**
     * Get people User follows
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function network_following()
    {
        return $this->hasMany('App\Network', 'follower_id');
    }

    /**
     * Get people that follow User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function network_subject()
    {
        return $this->hasMany('App\Network', 'subject_id');
    }

    /**
     * Get Users roaming coordinates
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roaming_coordinates()
    {
        return $this->hasMany('App\Roaming_user');
    }

    /**
     * Get Card Shares sent by User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sent_card_shares()
    {
        return $this->hasMany('App\Card_shares', 'sender_id');
    }

    /**
     * Get Card shares received by User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function received_card_shares()
    {
        return $this->hasMany('App\Card_shares',  'recipient_id');
    }

    /**
     * Get Card shares where User is the subject
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subject_card_shares()
    {
        return $this->hasMany('App\Card_shares', 'subject_id');
    }

    /**
     * Get user notifications
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->hasMany('App\Notification', 'subject_id');
    }

    /**
     * Get Templates User is unsubbed from
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function unsubbed_mail()
    {
        return $this->belongsToMany('App\MandrillTemplate', 'mail_unsubs', 'user_id', 'template_id')->withTimestamps();
    }

    /**
     * Get users device tokens
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function devices()
    {
        return $this->hasMany('App\Device');
    }

    /**
     * Get User complaints
     *     
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function complaints()
    {
        return $this->hasMany('App\Complaint');
    }

    /**
     * Get User activity
     *     
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activity()
    {
        return $this->hasMany('App\Activity_log');
    }

    /**
     * Get placeholder accounts of user
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userPlaceholders()
    {
        return $this->hasMany('App\PlaceholderUser', 'user_id');
    }

    /**
     * Get placeholder account owner has added
     *     
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ownerPlaceholders()
    {
        return $this->hasMany('App\PlaceholderUser', 'owner_id');
    }

    /**
     * Return placeholder users
     *     
     * @param  string $query 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePlaceholders($query)
    {
        return $query->where('is_placeholder', true);
    }

    /**
     * Return active users - not placeholders
     * 
     * @param  string $query [description]
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('is_placeholder', false);
    }

    public function getCallingCardWebLink() 
    {

        return env('WEBSITE_PATH') .'cc/' . $this->permalink;
    }

    public function blocked()
    {
        return $this->hasMany('App\BlockedUser', 'user_id');
    }

    /**
     * Check if given id is blocked by user
     *
     * @param  [type]  $id [description]
     * @return boolean     [description]
     */
    public function isBlocked($id) 
    {

        return $this->blocked()->where('blocked_id', $id)->exists();
    }

    /**
     * Get users opportunities
     * 
     * @return [type] [description]
     */
    public function opportunities()
    {
        return $this->hasMany('App\Opportunity', 'follower_id');
    }

    public function unseenNotificationCount()
    {
        $unseenNotifications = $this->notifications()->where('seen', 0)->get();

        $categorized = (new NotificationCountService)->categorize($unseenNotifications);

        return $categorized['total'];
    }   

    public function partners()
    {
        return $this->belongsToMany('App\Partner');
    }

    public function channels()
    {
        return $this->belongsToMany('App\Channel')
                    ->withPivot(['role_id'])
                    ->withTimestamps();
    }

    public function admin_notes()
    {
        return $this->hasMany('App\AdminNote', 'user_id');
    }

    /**
     * [associationKeywords description]
     * @return  [description]
     */
    public function association_keywords()
    {
        return $this->belongsToMany('App\AssociationKeyword')->withTimestamps();
    }
}
