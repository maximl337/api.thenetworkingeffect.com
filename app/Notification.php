<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use App\Services\NotificationCountService;

class Notification extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'actor_id',
        'subject_id',
        'object_id',
        'notification_type_id',
        'seen'
    ];

    /**
     * Get the actor of notification
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function actor()
    {
        return $this->belongsTo('App\User', 'actor_id');
    }

    /**
     * Get the subject of notification
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo 
     */
    public function subject()
    {
        return $this->belongsTo('App\User', 'subject_id');
    }

    public function type()
    {
        return $this->belongsTo('App\Notification_type', 'notification_type_id');
    }

    public function scopeUnread($query)
    {
        return $query->where('seen', false);
    }

}
