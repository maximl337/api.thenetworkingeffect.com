<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeletedChatMessage extends Model
{
    protected $table = 'deleted_chat_messages';

    protected $fillable = [
    	'chat_message_id',
    	'user_id'
    ];

    public function chat_message()
    {
    	return $this->belongsTo(ChatMessage::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
