<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaceholderUser extends Model
{
    protected $table = 'placeholder_users';

    protected $fillable = [

    	'user_id',
    	'owner_id',
    	'first_name',
    	'last_name',
		'first_name',
		'last_name',
		'email',
		'avatar',
		'logo',
		'job_title',
		'business_name',
		'phone_1',
		'phone_2',
		'fax',
		'website_url',
		'facebook_url',
		'twitter_url',
		'googleplus_url',
		'linkedin_url',
		'youtube_url',
		'blog_url',
		'street',
		'city',
		'state',
		'zip_code',
		'country',
		'latitude',
		'longitude',
		'business_category',
		'about_me',
		'about_business',
		'invitation_sent',
		'invitation_count',
    ];

    /**
     * Owner of placeholder
     * 			
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
    	return $this->belongsTo('App\User', 'owner_id');
    }

    /**
     * Placeholder user
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

}
