<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opportunity extends Model
{
	protected $table = 'opportunities';

	protected $fillable = [
		'follower_id',
		'subject_id'
	];

	public function follower()
	{
		return $this->belongsTo('App\User', 'follower_id');
	}

	public function subject()
	{
		return $this->belongsTo('App\User', 'subject_id');
	}
}
