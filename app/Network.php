<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Network extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'networks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'follower_id',
        'subject_id',
        'seen',
        'message'
    ];

    /**
     * Get the follower of the network relation
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function follower()
    {
        return $this->belongsTo('App\User', 'follower_id');
    }

    /**
     * Get the subject of the network relation
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subject()
    {
        return $this->belongsTo('App\User', 'subject_id');
    }

    public function sorting()
    {
        return $this->belongsToMany('App\SortingCategory', 'sorting_reminders', 'network_id', 'sorting_category_id')
                    ->withPivot('remind_at', 'sent', 'active')
                    ->withTimestamps();
    }
}
