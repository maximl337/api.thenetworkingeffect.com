<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    protected $fillable = [
        'name',
        'user_id',
        'channel_id'
    ];

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }
}
