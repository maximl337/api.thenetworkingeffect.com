<?php

namespace App\Listeners;

use App\Events\ChatMessageMentionEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Contracts\Mail;
use App\Notification_type;
use App\Contracts\PushNotification;
USE App\Services\NotificationService;

class ChatMessageMentionListener implements ShouldQueue
{
    protected $mailer;

    protected $push;

    protected $notificationService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mail, PushNotification $push, NotificationService $notificationService)
    {
        $this->mailer = $mail;

        $this->push = $push;

        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  ChatMessageMentionEvent  $event
     * @return void
     */
    public function handle(ChatMessageMentionEvent $event)
    {
        $data = $event->data;
        $actor = $sender = $data['actor'];
        $subject = $recipient = $data['subject'];
        $chatMessage = $data['chatMessage'];

        $notification_type = Notification_type::where('name', 'NEW_CHANNEL_CHAT_MENTION')->firstOrFail();
        //$actor_id, $subject_id, $object_id, $notification_type_id
        $notification = $this->notificationService->store($actor->id, $subject->id, $chatMessage->id, $notification_type->id);

        $link = "http://www.1dn.ca/channel?channel_id=$chatMessage->channel_id";

        $this->mailer->sender($sender)
                        ->recipient($recipient)
                        ->setDefaultMergeVars()
                        ->addMergeVar('FNAME', $recipient->first_name)
                        ->addMergeVar('LINK', $link)
                        ->event('NEW_MENTION')
                        ->sendTemplate();

        $this->push->title(config('app.name'))
                    ->badge($recipient->unseenNotificationCount())
                    ->custom(['test' => 'test'])
                    ->message($sender->first_name . ' ' . $sender->last_name . ' mentioned you in a chat message')
                    ->send($recipient);
    }
}
