<?php

namespace App\Listeners;

use App\Events\NetworkUnfollowed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NetworkUnfollowedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NetworkUnfollowed  $event
     * @return void
     */
    public function handle(NetworkUnfollowed $event)
    {
        //
    }
}
