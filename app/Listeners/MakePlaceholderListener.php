<?php

namespace App\Listeners;

use App\User;
use App\Contracts\Mail;
use App\Events\MakePlaceholderEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakePlaceholderListener implements ShouldQueue
{

    protected $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mail)
    {
        $this->mailer = $mail;
    }

    /**
     * Handle the event.
     *
     * @param  MakePlaceholderEvent  $event
     * @return void
     */
    public function handle(MakePlaceholderEvent $event)
    {
        $data = $event->data;

        $this->mailer->sender($data['user'])
                        ->recipient($data['placeholder'])
                        ->setDefaultMergeVars()
                        ->event('PLACEHOLDER_INVITATION')
                        ->sendTemplate();
    }
}
