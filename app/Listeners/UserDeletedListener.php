<?php

namespace App\Listeners;

use App\Contracts\Search;
use App\Events\UserDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Transformers\AlgoliaUserTransformer;

class UserDeletedListener
{
    public $search;


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Search $search)
    {
        $this->search = $search;

    }

    /**
     * Handle the event.
     *
     * @param  UserDeleted  $event
     * @return void
     */
    public function handle(UserDeleted $event)
    {
        $user = $event->user;

        $this->search->index( getenv('ALGOLIA_USERS_INDEX') )->delete($user->id);
    }
}
