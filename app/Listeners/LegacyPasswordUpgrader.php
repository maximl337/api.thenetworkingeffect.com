<?php 

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class LegacyPasswordUpgrader 
{

    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Credentials given for login
     * @return void
     */
    public function handle($credentials)
    {

        $user = User::where('email', '=', $credentials['email'])->first();

        if(!$user) return false;
            
        // Check if given password matches legacy hashing
        // If it does then update the password to new hash
        if($user->password === md5($credentials['password'])) {

            $user->password = bcrypt($credentials['password']);

            $user->save();
        }
    }

}
