<?php

namespace App\Listeners;

use Log;
use Carbon\Carbon;
use App\Activity_log;
use App\Contracts\Search;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateActivityLog
{
    protected $request;

    protected $algolia;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request, Search $algolia)
    {
        $this->request = $request;

        $this->algolia = $algolia;
    }

    /**
     * Handle the event.
     *
     * @param  EventDelete  $event
     * @return void
     */
    public function handle($user)
    {

        if( $this->request->path() != 'api/v1/notifications/getCountOfUnseen' &&
            $this->request->path() !=  'api/v1/user/roaming') {
            
            $data = '';

            try {

                $data = serialize($this->request->input());
            
            } catch(\Exception $e) {

                Log::error('activity-log', ['error' => $e->getMessage()]);

            }

            // Add to Activity Log
            Activity_log::create([
                    'user_id' => $user->id, 
                    'path'  => $this->request->path(),
                    'data'  => $data,
                    'method' => $this->request->method(),
                    'ip'    => $this->request->ip()
                ]);

        } // EO if not get count of unseen notifications

        // Update user last activity
        $user->last_activity = Carbon::now();

        $user->update();

        try {

            $this->algolia->index(env('ALGOLIA_USERS_INDEX'))
                ->update([
                    'last_activity' => $user->last_activity,
                    'objectID' => $user->id
                ]);
            
        } catch (Exception $e) {
            Log::error('activity-log', ['error' => $e->getMessage()]);
        }
        
    }
}
