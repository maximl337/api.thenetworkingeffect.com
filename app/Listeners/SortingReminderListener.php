<?php

namespace App\Listeners;

use Log;
use App\User;
use App\Network;
use App\Notification;
use App\Contracts\Mail;
use App\SortingCategory;
use App\Notification_type;
use App\Services\SortingService;
use App\Contracts\PushNotification;
use App\Events\SortingReminderEvent;
use App\Services\NotificationService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SortingReminderListener
{

    protected $mailer;

    protected $push;

    protected $sortingService;

    protected $notificationService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mailer, PushNotification $push, SortingService $sortingService, NotificationService $notificationService)
    {
        $this->mailer = $mailer;

        $this->push = $push;

        $this->sortingService = $sortingService;

        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  SortingReminder  $event
     * @return void
     */
    public function handle(SortingReminderEvent $event)
    {

        try {

            $sortingReminder = $event->sortingReminder;

            // get the category
            $sortingCategory = $sortingReminder->sortingCategory()->firstOrFail();

            // get the network
            $network = $sortingReminder->network()->firstOrFail();

            // get the type of sorting reminder
            $notificationType = $this->sortingService->getNotificationType($sortingCategory);

            // get mail template
            $mailTemplateType = $this->sortingService->getMailTemplateType($sortingCategory);

            // get push notification message
            $pushNotificationMessage = $this->sortingService->getPushNotificationMessage($sortingCategory) . $network->subject()->firstOrFail()->first_name;

            $notificationType = Notification_type::where('name', $notificationType)->firstOrFail()->toArray();

            $this->notificationService->store($network->subject_id, $network->follower_id, $network->id, $notificationType['id']);

            // $this->mailer->sender($network->subject()->firstOrFail())
            //                 ->recipient($network->follower()->firstOrFail())
            //                 ->setDefaultMergeVars()
            //                 ->addMergeVar('test', 'test')
            //                 ->event($mailTemplateType)
            //                 ->sendTemplate();

            $recipient = $network->follower->firstOrFail();
            
            $this->push->title('The Networking Effect')
                        ->badge($recipient->unseenNotificationCount())
                        ->custom(['id' => $sortingReminder->id])
                        ->message($pushNotificationMessage)
                        ->send($recipient);

            $sortingReminder->update(['sent' => true]);

        } catch(\Exception $e) {

            Log::error('Sorting:error', [$e->getMessage()]);
        }
        
    }
}
