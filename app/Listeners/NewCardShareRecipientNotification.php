<?php

namespace App\Listeners;

use App\User;
use App\Notification;
use App\Services\NotificationService;
use App\Contracts\Mail;
use App\Notification_type;
use App\Events\NewCardShare;
use App\Contracts\PushNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewCardShareRecipientNotification implements ShouldQueue
{

    protected $mailer;

    protected $push;

    protected $notificationService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mail, PushNotification $push, NotificationService $notificationService)
    {
        $this->mailer = $mail;

        $this->push = $push;

        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  NewCardShare  $event
     * @return void
     */
    public function handle(NewCardShare $event)
    {
        $cardShare = $event->cardShare;

        $notification_type = Notification_type::where('name', 'NEW_CARD_SHARE_RECIPIENT')->first()->toArray();
        
        $sender = User::find($cardShare->sender_id);

        // If sharing card with a user
        if($cardShare->recipient_id) {

            $recipient = User::find($cardShare->recipient_id);

            $this->notificationService->store($cardShare->sender_id, $cardShare->recipient_id, $cardShare->id, $notification_type['id']);

            $this->mailer->sender(User::find($sender->id))
                            ->recipient(User::find($recipient->id))
                            ->setDefaultMergeVars()
                            ->addMergeVar('test', 'test')
                            ->event('NEW_CARD_SHARE_RECIPIENT')
                            ->sendTemplate();

            $this->push->title('The Networking Effect')
                    ->badge($recipient->unseenNotificationCount())
                    ->custom(['id' => $cardShare->subject_id, 'type' => 'user'])
                    ->message($sender->first_name . ' ' . $sender->last_name . ' shared a card with you')
                    ->send($recipient);

        }
        // Sharing with email no need to save notification
        elseif($cardShare->recipient_email) {

            $this->mailer->sender(User::find($cardShare->sender_id))
                        ->recipientEmail($cardShare->recipient_email)
                        ->setDefaultMergeVars()
                        ->addMergeVar('test', 'test')
                        ->event('NEW_CARD_SHARE_RECIPIENT')
                        ->sendTemplate();

        } // check if sharing with TNE user
    }
}
