<?php

namespace App\Listeners;


use App\User;
use App\Notification;
use App\Contracts\Mail;
use App\Contracts\Search;
use App\Notification_type;
use App\Events\EventDelete;
use App\Contracts\PushNotification;
use App\Services\NotificationService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventDeleteNotification implements ShouldQueue
{

    protected $mailer;

    protected $search;

    protected $push;

    protected $notificationService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mail, Search $search, PushNotification $push, NotificationService $notificationService)
    {
        $this->mailer = $mail;

        $this->search = $search;

        $this->push = $push;

        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  EventDelete  $event
     * @return void
     */
    public function handle(EventDelete $event)
    {
        $tneEvent = $event->event;

        $users = $tneEvent->users()->get();

        $eventOwner = User::find($tneEvent->user_id);

        $notification_type = Notification_type::where('name', 'EVENT_DELETED')->first()->toArray();

        foreach($users as $user) {

            $this->notificationService->store($tneEvent->user_id, $user->id, $tneEvent->id, $notification_type['id']);

            $this->mailer->sender($eventOwner)
                            ->recipient($user)
                            ->setDefaultMergeVars()
                            ->addMergeVar('EVENTNAME', $tneEvent->name)
                            ->event('EVENT_DELETED')
                            ->sendTemplate();

            $recipient = User::find($user->id);

            $this->push->title('The Networking Effect')
                    ->badge($recipient->unseenNotificationCount())
                    ->custom(['id' => $tneEvent->id, 'type' => 'event'])
                    ->message($tneEvent->name . ' has been cancelled')
                    ->send($recipient);
                    
        } //EO foreach attendee

        $this->search
                ->index( getenv('ALGOLIA_EVENTS_INDEX') )
                ->delete( $tneEvent->id );

        $tneEvent->delete();
    }
}
