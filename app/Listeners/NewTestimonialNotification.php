<?php

namespace App\Listeners;

use App\User;
use App\Notification;
use App\Contracts\Mail;
use App\Notification_type;
use App\Events\NewTestimonial;
use App\Contracts\PushNotification;
use App\Services\NotificationService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewTestimonialNotification implements ShouldQueue
{

    protected $mailer;

    protected $push;

    protected $notificationService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mail, PushNotification $push, NotificationService $notificationService)
    {
        $this->mailer = $mail;

        $this->push = $push;

        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  NewTestimonial  $event
     * @return void
     */
    public function handle(NewTestimonial $event)
    {
        $testimonial = $event->testimonial;

        $notification_type = Notification_type::where('name', 'NEW_TESTIMONIAL')->first()->toArray();

        $this->notificationService->store($testimonial->author_id, $testimonial->subject_id, $testimonial->id, $notification_type['id']);

        $this->mailer->sender(User::find($testimonial->author_id))
                        ->recipient(User::find($testimonial->subject_id))
                        ->setDefaultMergeVars()
                        ->addMergeVar('test', 'test')
                        ->event('NEW_TESTIMONIAL')
                        ->sendTemplate();

        $sender = User::find($testimonial->author_id);

        $recipient = User::find($testimonial->subject_id);
        
        $this->push->title('The Networking Effect')
                    ->badge($recipient->unseenNotificationCount())
                    ->custom(['id' => $testimonial->id, 'type' => 'testimonial'])
                    ->message($sender->first_name . ' ' . $sender->last_name . ' left you a testimonial!')
                    ->send($recipient);
    }
}
