<?php

namespace App\Listeners;

use App\Contracts\Search;
use App\Transformers\AlgoliaArticleTransformer;
use App\Events\ArticleUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ArticleUpdatedListener implements ShouldQueue
{

    protected $search;

    protected $transformer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Search $search, AlgoliaArticleTransformer $transformer)
    {
        $this->search = $search;

        $this->transformer = $transformer;
    }

    /**
     * Handle the event.
     *
     * @param  ArticleUpdated  $event
     * @return void
     */
    public function handle(ArticleUpdated $event)
    {
        $article = $event->article;

        $this->search
                ->index( getenv('ALGOLIA_ARTICLES_INDEX') )
                ->update( $this->transformer->transform( $article->toArray() ) );
    }
}
