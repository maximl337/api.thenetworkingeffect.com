<?php

namespace App\Listeners;

use Mail;
use App\Events\ChannelMemberJoinEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChannelMemberJoinEventListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChannelMemberJoinEvent  $event
     * @return void
     */
    public function handle(ChannelMemberJoinEvent $event)
    {
        $data = $event->data;

        $to = ['frannie@thenetworkingeffect.com', 'claudia@thenetworkingeffect.com'];

        try {
            Mail::send('emails.channel-member-join', ['data' => $data], function ($m) use ($to) {
                $m->to($to)->subject('1DN user joined a channel');
            }); 
        } catch(Exception $e) {
            Log::error("ChannelMemberJoinEventListener", [$e->getMessage()]);
        }
    }
}
