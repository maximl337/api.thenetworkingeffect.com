<?php

namespace App\Listeners;

use Auth;
use App\User;
use App\Contracts\Mail;
use App\Events\EventJoin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventJoinListener
{

    protected $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mail)
    {
        $this->mailer = $mail;
    }

    /**
     * Handle the event.
     *
     * @param  EventJoin  $event
     * @return void
     */
    public function handle(EventJoin $event)
    {
        $data = $event->data;

        $this->mailer->sender(User::find($data['event']->user_id))
                        ->recipient($data['user'])
                        ->setDefaultMergeVars()
                        ->addMergeVar('EVENTNAME', $data['event']->name)
                        ->event('EVENT_JOIN')
                        ->sendTemplate();
    }
}
