<?php

namespace App\Listeners;

use App\User;
use App\Notification;
use App\Contracts\Mail;
use App\Notification_type;
use App\Events\NewMessageSent;
use App\Contracts\PushNotification;
USE App\Services\NotificationService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewMessageNotification implements ShouldQueue
{

    protected $mailer;

    protected $push;

    protected $notificationService;
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mail, PushNotification $push, NotificationService $notificationService)
    {
        $this->mailer = $mail;

        $this->push = $push;

        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  NewMessageSent  $event
     * @return void
     */
    public function handle(NewMessageSent $event)
    {
        $message = $event->message;

        $notification_type = Notification_type::where('name', 'NEW_MESSAGE')->first()->toArray();

        $this->notificationService->store($message->from, $message->to, $message->id, $notification_type['id']);

        $this->mailer->sender(User::find($message->from))
                        ->recipient(User::find($message->to))
                        ->setDefaultMergeVars()
                        ->addMergeVar('MESSAGE', $message->body)
                        ->event('NEW_MESSAGE')
                        ->sendTemplate();
        
        $recipient = User::find($message->to);

        $sender = User::find($message->from);

        $this->push->title('The Networking Effect')
                    ->badge($recipient->unseenNotificationCount())
                    ->custom(['test' => 'test'])
                    ->message($sender->first_name . ' ' . $sender->last_name . ' has sent you a message')
                    ->send($recipient);
        
    }
}
