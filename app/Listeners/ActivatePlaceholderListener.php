<?php

namespace App\Listeners;

use App\Contracts\Search;
use App\Events\ActivatePlaceholderEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Transformers\AlgoliaUserTransformer;

class ActivatePlaceholderListener
{
    public $search;

    public $searchTransformer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Search $search, AlgoliaUserTransformer $searchTransformer)
    {
        $this->search = $search;

        $this->searchTransformer = $searchTransformer;
    }

    /**
     * Handle the event.
     *
     * @param  ActivatePlaceholderEvent  $event
     * @return void
     */
    public function handle(ActivatePlaceholderEvent $event)
    {
        $user = $event->user;

        $user->is_placeholder = false;

        $user->save();
        
        $this->search
                ->index(getenv('ALGOLIA_USERS_INDEX'))
                ->save( $this->searchTransformer->transform($user->toArray()) );
    }
}
