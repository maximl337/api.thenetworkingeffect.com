<?php

namespace App\Listeners;

use App\Contracts\Search;
use App\Events\ArticleCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Transformers\AlgoliaArticleTransformer;

class ArticleCreatedListener implements ShouldQueue
{

    protected $search;

    protected $transformer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Search $search, AlgoliaArticleTransformer $transformer)
    {
        $this->search = $search;

        $this->transformer = $transformer;
    }

    /**
     * Handle the event.
     *
     * @param  ArticleCreated  $event
     * @return void
     */
    public function handle(ArticleCreated $event)
    {
        $article = $event->article;

        $this->search
                ->index( getenv('ALGOLIA_ARTICLES_INDEX') )
                ->save( $this->transformer->transform( $article->toArray() ) );
    }
}
