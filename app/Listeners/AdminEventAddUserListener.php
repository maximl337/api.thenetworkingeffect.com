<?php

namespace App\Listeners;

use App\Events\AdminEventAddUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminEventAddUserListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdminEventAddUser  $event
     * @return void
     */
    public function handle(AdminEventAddUser $event)
    {
        
    }
}
