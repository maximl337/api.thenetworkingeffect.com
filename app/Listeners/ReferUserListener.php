<?php

namespace App\Listeners;

use Log;
use App\User;
use App\Notification;
use App\Contracts\Mail;
use App\Notification_type;
use App\Events\ReferUserEvent;
use App\Contracts\PushNotification;
use App\Services\NotificationService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReferUserListener implements ShouldQueue
{

    protected $mailer;

    protected $push;

    protected $notificationService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mail, PushNotification $push, NotificationService $notificationService)
    {
        $this->mailer = $mail;

        $this->push = $push;

        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  ReferUserEvent  $event
     * @return void
     */
    public function handle(ReferUserEvent $event)
    {
        $cardShare = $event->cardShare;

        Log::info('ReferUserListener', ['stage' => 'started']);

        try {

            $sender = User::findOrFail($cardShare->sender_id);

            $subject = User::findOrFail($cardShare->subject_id);

            // If sharing card with a user
            if($cardShare->recipient_id) {

                $recipient = User::findOrFail($cardShare->recipient_id);

                $notificationTypeRecipient = Notification_type::where('name', 'NEW_REFERRAL')->firstOrFail();

                $this->notificationService->store($sender->id, $cardShare->recipient_id, $cardShare->id, $notificationTypeRecipient->id);

                $this->mailer->sender($sender)
                                ->recipient($recipient)
                                ->setDefaultMergeVars()
                                ->addMergeVar('SUBJECT', $subject->first_name . ' ' . $subject->last_name)
                                ->addMergeVar('SNAME_LINK', $subject->getCallingCardWebLink())
                                ->addMergeVar('MESSAGE', $cardShare->message)
                                ->event('REFER_A_USER')
                                ->sendTemplate();

                $this->push->title('The Networking Effect')
                    ->badge($recipient->unseenNotificationCount())
                    ->custom(['id' => $cardShare->subject_id, 'type' => 'user'])
                    ->message($sender->first_name . ' ' . $sender->last_name . ' has referred someone to you')
                    ->send($recipient);

            }
            // Sharing with email no need to save notification
            elseif($cardShare->recipient_email) {

                $this->mailer->sender($sender)
                            ->recipientEmail($cardShare->recipient_email)
                            ->setDefaultMergeVars()
                            ->addMergeVar('SUBJECT', $subject->first_name . ' ' . $subject->last_name)
                            ->addMergeVar('SNAME_LINK', $subject->getCallingCardWebLink())
                            ->addMergeVar('MESSAGE', $cardShare->message)
                            ->event('REFER_A_USER')
                            ->sendTemplate();

            } // check if sharing with TNE user

            $notificationTypeSubject = Notification_type::where('name', 'REFERRED')->firstOrFail();

            $this->notificationService->store($sender->id, $subject->id, $cardShare->id, $notificationTypeSubject->id);

            // send to subject
            // $this->mailer->sender($sender)
            //                 ->recipient($subject)
            //                 ->setDefaultMergeVars()
            //                 ->addMergeVar('SCCLINK', $sender->getCallingCardWebLink())
            //                 ->event('YOU_WERE_REFERRED')
            //                 ->sendTemplate();

            $this->push->title('The Networking Effect')
                    ->badge($subject->notifications()->unseenCount())
                    ->custom(['id' => $cardShare->sender_id, 'type' => 'user'])
                    ->message($sender->first_name . ' ' . $sender->last_name . ' has referred you')
                    ->send($subject);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            // log
            Log::error('ReferUserListener', [$e->getMessage()]);

        } catch(\Exception $e) {

            // log
            Log::error('ReferUserListener', [$e->getMessage()]);
        }
    
    }
}
