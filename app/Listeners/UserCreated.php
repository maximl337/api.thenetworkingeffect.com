<?php

namespace App\Listeners;

use App\Contracts\Search;
use App\Events\NewUserJoined;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Transformers\AlgoliaUserTransformer;
use App\Contracts\Zendesk;


class UserCreated
{

    public $search;

    public $searchTransformer;

    public $zendesk;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Search $search, AlgoliaUserTransformer $searchTransformer, Zendesk $zendesk)
    {
        $this->search = $search;

        $this->searchTransformer = $searchTransformer;

        $this->zendesk = $zendesk;
    }

    /**
     * Handle the event.
     *
     * @param  NewUserJoined  $event
     * @return void
     */
    public function handle(NewUserJoined $event)
    {
        $user = $event->user;

        $this->search
                ->index(getenv('ALGOLIA_USERS_INDEX'))
                ->save( $this->searchTransformer->transform($user->toArray()) );

        try {
            $result = $this->zendesk->createOrUpdateUser($user);
            \Log::info('UserCreated@handle', ['Zendesk response' => $result]);
        } catch(\Exception $e) {
            \Log::error('UserCreated@handle', ['Zendesk Error' => $e->getMessage()]);
        }
    }
}
