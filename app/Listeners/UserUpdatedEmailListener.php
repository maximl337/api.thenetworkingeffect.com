<?php

namespace App\Listeners;

use Log;
use Mail;
use App\User;
use App\Events\UserUpdatedEmailEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserUpdatedEmailListener implements ShouldQueue
{
    protected $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  UserUpdatedEmailEvent  $event
     * @return void
     */
    public function handle(UserUpdatedEmailEvent $event)
    {
        $data = $event->data;

        $recipients = [
            'claudia@thenetworkingeffect.com',
            'frannie@thenetworkingeffect.com'
        ];

        try {

            foreach($recipients as $to) {

                Mail::send('emails.email-changed', ['data' => $data], function ($m) use ($to) {
                    
                    $m->to($to)->subject('1DN user changed their email address');
                });
            }

        } catch(Exception $e) {

            Log::error("UserUpdatedEmailListener", [$e->getMessage()]);

        }

        
    }
}
