<?php

namespace App\Listeners;

use App\Events\InitiateMassMessageSentEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Contracts\MassMessage;

class InitiateMassMessageSentListener implements ShouldQueue
{

    public $mass_message;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(MassMessage $mass_message)
    {
        $this->mass_message = $mass_message;
    }

    /**
     * Handle the event.
     *
     * @param  InitiateMassMessageSentEvent  $event
     * @return void
     */
    public function handle(InitiateMassMessageSentEvent $event)
    {
        try {
            
            $data = $event->data;

            $this->mass_message->send($data);

        } catch (Exception $e) {

            Log::error('InitiateMassMessageSentListener', [$e->getMessage()]);

        }
    }
}
