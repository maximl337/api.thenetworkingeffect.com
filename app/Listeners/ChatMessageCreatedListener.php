<?php

namespace App\Listeners;

use App\Events\ChatMessageCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChatMessageCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChatMessageCreatedEvent  $event
     * @return void
     */
    public function handle(ChatMessageCreatedEvent $event)
    {
        $data = $event->data;
        $user = $data['user'];
        $chat_message = $data['chat_message'];
        $channel = $data['channel'];

        // send phone notification
    }
}
