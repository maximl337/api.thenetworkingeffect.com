<?php

namespace App\Listeners;

use App\User;
use App\Notification;
use App\Contracts\Mail;
use App\Notification_type;
use App\Contracts\PushNotification;
use App\Events\NetworkFollowedBack;
use App\Services\NotificationService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NetworkFollowedBackListener implements ShouldQueue
{
    protected $mailer;

    protected $push;

    protected $notificationService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mail, PushNotification $push, NotificationService $notificationService)
    {
        $this->mailer = $mail;

        $this->push = $push;

        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  NetworkFollowedBack  $event
     * @return void
     */
    public function handle(NetworkFollowedBack $event)
    {
        $network = $event->network;

        $notification_type = Notification_type::where('name', 'FOLLOWED_BACK')->first()->toArray();

        $this->notificationService->store($network->follower_id, $network->subject_id, $network->id, $notification_type['id']);

        $this->mailer->sender(User::find($network->follower_id))
                        ->recipient(User::find($network->subject_id))
                        ->setDefaultMergeVars()
                        ->addMergeVar('test', 'test')
                        ->event('FOLLOWED_BACK')
                        ->sendTemplate();

        $recipient = User::find($network->subject_id);

        $sender = User::find($network->follower_id);

        $this->push->title('The Networking Effect')
                    ->badge($recipient->unseenNotificationCount())
                    ->custom(['id' => $sender->id, 'type' => 'user'])
                    ->message($sender->first_name . ' ' . $sender->last_name . ' has accepted your invitation to connect')
                    ->send($recipient);
        
    
    }
}
