<?php

namespace App\Listeners;

use App\Contracts\Search;
use App\Events\EventCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Transformers\AlgoliaEventTransformer;

class EventCreatedListener
{

    public $search;

    public $transformer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Search $search, AlgoliaEventTransformer $transformer)
    {
        $this->search = $search;

        $this->transformer = $transformer;
    }

    /**
     * Handle the event.
     *
     * @param  EventCreated  $event
     * @return void
     */
    public function handle(EventCreated $event)
    {
        $event = $event->event;

        $this->search
                ->index( getenv('ALGOLIA_EVENTS_INDEX') )
                ->save( $this->transformer->transform( $event->toArray() ) );
    }
}
