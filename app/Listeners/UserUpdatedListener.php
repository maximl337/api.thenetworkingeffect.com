<?php

namespace App\Listeners;

use App\Contracts\Search;
use App\Events\UserUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Transformers\AlgoliaUserTransformer;

class UserUpdatedListener implements ShouldQueue
{
    public $search;

    public $searchTransformer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Search $search, AlgoliaUserTransformer $searchTransformer)
    {
        $this->search = $search;

        $this->searchTransformer = $searchTransformer;
    }

    /**
     * Handle the event.
     *
     * @param  UserUpdated  $event
     * @return void
     */
    public function handle(UserUpdated $event)
    {
        $user = $event->user;

        $this->search
                ->index( getenv('ALGOLIA_USERS_INDEX') )
                ->update( $this->searchTransformer->transform( $user->toArray() ) );
    }
}
