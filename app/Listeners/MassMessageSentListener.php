<?php

namespace App\Listeners;

use Log;
use App\User;
use App\Message;
use App\Notification;
use App\Contracts\Mail;
use App\Notification_type;
use App\Contracts\PushNotification;
USE App\Services\NotificationService;

use App\Events\MassMessageSentEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MassMessageSentListener implements ShouldQueue
{

    protected $mailer;

    protected $push;

    protected $notificationService;
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mail, PushNotification $push, NotificationService $notificationService)
    {
        $this->mailer = $mail;

        $this->push = $push;

        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  MassMessageSentEvent  $event
     * @return void
     */
    public function handle(MassMessageSentEvent $event)
    {
        try {
            
            $data = $event->data;

            $message = Message::create([
                    'from' => $data['from'],
                    'to' => $data['to'],
                    'body' => $data['body']
                ]);

            $notification_type = Notification_type::where('name', 'NEW_MESSAGE')->first()->toArray();

            $this->notificationService->store($message->from, $message->to, $message->id, $notification_type['id']);

            $this->mailer->sender(User::find($message->from))
                            ->recipient(User::find($message->to))
                            ->setDefaultMergeVars()
                            ->addMergeVar('MESSAGE', $message->body)
                            ->event('NEW_MESSAGE')
                            ->sendTemplate();
            
            $recipient = User::find($message->to);

            $sender = User::find($message->from);

            $this->push->title('The Networking Effect')
                        ->badge($recipient->unseenNotificationCount())
                        ->custom(['test' => 'test'])
                        ->message($sender->first_name . ' ' . $sender->last_name . ' has sent you a message')
                        ->send($recipient);

        } catch (\Exception $e) {
            
            Log::error("MassMessageSentListener", [$e->getMessage()]);
        }
    }
}
