<?php

namespace App\Listeners;

use App\User;
use App\Notification;
use App\Contracts\Mail;
use App\Contracts\Search;
use App\Notification_type;
use App\Events\EventUpdate;
use App\Contracts\PushNotification;
use App\Services\NotificationService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Transformers\AlgoliaEventTransformer;

class EventUpdateNotification implements ShouldQueue
{

    protected $mailer;

    public $search;

    public $transformer;

    protected $push;

    protected $notificationService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mail, Search $search, AlgoliaEventTransformer $transformer, PushNotification $push, NotificationService $notificationService)
    {
        $this->mailer = $mail;

        $this->search = $search;

        $this->transformer = $transformer;

        $this->push = $push;

        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  EventUpdate  $event
     * @return void
     */
    public function handle(EventUpdate $event)
    {
        $event = $event->event;

        $users = $event->users()->get();

        $eventOwner = User::find($event->user_id);

        $notification_type = Notification_type::where('name', 'EVENT_UPDATED')->first()->toArray();


        foreach($users as $user) {

            $this->notificationService->store($event->user_id, $user->id, $event->id, $notification_type['id']);

            $this->mailer->sender($eventOwner)
                        ->recipient($user)
                        ->setDefaultMergeVars()
                        ->addMergeVar('EVENTNAME', $event->name)
                        ->event('EVENT_UPDATED')
                        ->sendTemplate();

            $recipient = User::find($user->id);  

            $this->push->title('The Networking Effect')
                    ->badge($recipient->unseenNotificationCount())
                    ->custom(['id' => $event->id, 'type' => 'event'])
                    ->message($event->name . ' has been changed')
                    ->send($recipient);

        } // EO foreach event attendee

        $this->search
                ->index( getenv('ALGOLIA_EVENTS_INDEX') )
                ->update( $this->transformer->transform( $event->toArray() ) );

        
    }
}
