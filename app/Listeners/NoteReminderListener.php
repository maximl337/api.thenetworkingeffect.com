<?php

namespace App\Listeners;

use Log;
use App\User;
use App\Note;
use App\Notification;
use App\Notification_type;
use App\Events\NoteReminderEvent;
use App\Contracts\PushNotification;
use App\Services\NotificationService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NoteReminderListener
{

    protected $push;

    protected $notificationService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PushNotification $push, NotificationService $notificationService)
    {
        $this->push = $push;

        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  NoteReminderEvent  $event
     * @return void
     */
    public function handle(NoteReminderEvent $event)
    {
        try {

            $note = $event->note;

            $notification_type_id = Notification_type::where('name', 'NOTE_REMINDER')->first()->id;

            // dont create any more if one already exists
            $exists = Notification::where('object_id', $note->id)
                                    ->where("notification_type_id", $notification_type_id)
                                    ->exists();

            if(!$exists) {

                $this->notificationService->store($note->subject_id, $note->author_id, $note->id, $notification_type_id);

            }
             

            $note->update(['sent' => 1]);

            $recipient = $note->user()->firstOrFail();

            $this->push->title('The Networking Effect')
                        ->badge($recipient->unseenNotificationCount())
                        ->custom(['id' => $recipient->id, 'type' => 'user'])
                        ->message($note->title)
                        ->send($recipient);

        } catch(\Exception $e) {

            Log::error('Note Reminder', [$e->getMessage()]);
        }
    }
}
