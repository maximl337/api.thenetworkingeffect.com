<?php

namespace App\Listeners;

use App\Events\EventUserLeft;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventUserLeftListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventUserLeft  $event
     * @return void
     */
    public function handle(EventUserLeft $event)
    {
        //
    }
}
