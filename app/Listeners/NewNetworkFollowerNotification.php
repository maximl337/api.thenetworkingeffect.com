<?php

namespace App\Listeners;


use App\User;
use App\Notification;
use App\Contracts\Mail;
use App\Notification_type;
use App\Services\NotificationService;
use App\Contracts\PushNotification;
use App\Events\NewNetworkFollower;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewNetworkFollowerNotification implements ShouldQueue
{

    protected $mailer;

    protected $push;

    protected $notificationService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mail, PushNotification $push, NotificationService $notificationService)
    {
        $this->mailer = $mail;

        $this->push = $push;

        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  NewNetworkFollower  $event
     * @return void
     */
    public function handle(NewNetworkFollower $event)
    {
        $network = $event->network;

        $notification_type = Notification_type::where('name', 'NEW_FOLLOWER')->first()->toArray();

        $this->notificationService->store($network->follower_id, $network->subject_id, $network->id, $notification_type['id']);

        $this->mailer->sender(User::find($network->follower_id))
                        ->recipient(User::find($network->subject_id))
                        ->setDefaultMergeVars()
                        ->addMergeVar('test', 'test')
                        ->event('NEW_FOLLOWER')
                        ->sendTemplate();

        $recipient = User::find($network->subject_id);

        $sender = User::find($network->follower_id);

        $this->push->title('The Networking Effect')
                    ->badge($recipient->unseenNotificationCount())
                    ->custom(['id' => $sender->id, 'type' => 'user'])
                    ->message($sender->first_name . ' ' . $sender->last_name . ' has invited you to connect')
                    ->send($recipient);
        
    }
}
