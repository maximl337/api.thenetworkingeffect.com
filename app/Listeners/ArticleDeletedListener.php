<?php

namespace App\Listeners;

use App\Contracts\Search;
use App\Events\ArticleDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ArticleDeletedListener
{

    protected $search;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Search $search)
    {
        $this->search = $search;
    }

    /**
     * Handle the event.
     *
     * @param  ArticleDeleted  $event
     * @return void
     */
    public function handle(ArticleDeleted $event)
    {
        $article = $event->article;

        $this->search
                ->index( getenv('ALGOLIA_ARTICLES_INDEX') )
                ->delete( $article->id );
    }
}
