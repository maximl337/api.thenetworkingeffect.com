<?php

namespace App\Listeners;

use App\User;
use App\PlaceholderUser;
use App\Contracts\Mail;
use App\Events\InvitePlaceholderEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvitePlaceholderListener implements ShouldQueue
{

    protected $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mail)
    {
        $this->mailer = $mail;
    }

    /**
     * Handle the event.
     *
     * @param  InvitePlaceholderEvent  $event
     * @return void
     */
    public function handle(InvitePlaceholderEvent $event)
    {
        $data = $event->data;

        $this->mailer->sender($data['user'])
                        ->recipient($data['placeholder'])
                        ->setDefaultMergeVars()
                        ->event('PLACEHOLDER_INVITATION')
                        ->sendTemplate();

        $placeholder = PlaceholderUser::where('user_id', $data['placeholder']->id)->where('owner_id', $data['user']->id)->first();

        if($placeholder) {

            $placeholder->update([
                    'invitation_sent' => true, 
                    'invitation_count' => $placeholder->invitation_count + 1
                    ]);
        } 

    }
}
