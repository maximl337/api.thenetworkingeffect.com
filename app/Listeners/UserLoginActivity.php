<?php 

namespace App\Listeners;

use Log;
use Hash;
use App\User;
use Carbon\Carbon;
use App\Activity_log;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class UserLoginActivity 
{

    protected $request;

    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  Credentials given for login
     * @return void
     */
    public function handle($credentials)
    {

        $user = User::where('email', '=', $credentials['email'])->first();

        if(!$user) return false;

        // Check if given password matches legacy hashing
        // If it does then update the password to new hash
        if (Hash::check($credentials['password'], $user->password)) {

            // Add to Activity Log
            Activity_log::create([
                    'user_id' => $user->id, 
                    'path'  => $this->request->path(),
                    'data'  => serialize($this->request->get('email')),
                    'method' => $this->request->method(),
                    'ip'    => $this->request->ip()
                ]);

            // Update user last activity
            $user->last_activity = Carbon::now();

            $user->update();
            
        }

    }

}
