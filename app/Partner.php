<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $table = 'partners';

    protected $fillable = [
    	'name'
    ];

    public function users()
    {
    	return $this->belongsToMany('App\User');
    }
}
