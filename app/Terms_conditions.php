<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terms_conditions extends Model
{
    protected $table = 'terms_conditions';

    protected $fillable = [
        'body'
    ];
}
