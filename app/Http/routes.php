<?php

/** Log to Papertrail ( Heroku ) */
if (App::environment('staging') || App::environment('production')) {
    Log::useFiles('php://stderr');
}

Route::get('/', function () { return view('welcome'); });

// Public routes
Route::get('/sample/auth', 'TNEAuthController@home');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

/**
 * JSON WEB TOKEN required to access the group below.
 * Upon Oauth2 implementation, implicitly trusted 
 * clients, i.e. TNE website and app, should use
 * oauth2 protocol instead with password grant.
 * 
 */
Route::group(['prefix' => 'api/v1'], function () {

    Route::get('docs', function () {
        return view('docs.index');
    });

    // Password reset link request routes...
    Route::get('password/email', 'Auth\PasswordController@getEmail');
    Route::post('password/email', 'Auth\PasswordController@postEmail');

    // Password reset routes...
    Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
    Route::post('password/reset', 'Auth\PasswordController@postReset');

    /* AUTHENTICATION */
    Route::post('auth/login', 'TNEAuthController@login');
    Route::post('auth/register', 'TNEAuthController@register');
    Route::post('auth/social', 'TNEAuthController@social');
    Route::get('auth/refreshToken', 'TNEAuthController@refreshToken');
    Route::get('auth/emailExists', 'TNEAuthController@emailExists');

    /* USERS */
    Route::get('users', 'UserController@getUsers');
    Route::get('users/{id}', 'UserController@getUser');
    Route::get('public/users/{id}', 'UserController@getUserPublic');
    Route::get('users/name/{permalink}', 'UserController@getUserByPermalink');
    Route::get('user/email/{email}', 'UserController@getUserByEmailPublic');
    Route::post('user/update', 'UserController@updateUser');
    Route::post('user/update/new', 'UserUpdateController@updateUser');
    Route::delete('user/delete', 'UserController@deleteUser'); 
    Route::post('user/roaming', 'UserController@addRoamingGeoCode');
    Route::post('user/device', 'UserController@userDevice');

    Route::post('user/avatar', 'UserController@userAvatar');

    Route::post('user/logo', 'UserController@userLogo');

    Route::post('user/email', 'UserController@updateEmail');

    Route::post('user/password', 'UserController@updatePassword');

    Route::post('user/timezone', 'UserController@updateTimezone');

    Route::post('user/reminderNotificationTime', 'UserController@updateReminderNotificationTime');

    /* Events */
    Route::get('events', 'EventController@getEvents');

    Route::get('events/{id}', 'EventController@getEvent');

    Route::get('events/{id}/public', 'EventController@getEventPublic');

    Route::get('events/name/{permalink}', 'EventController@getByPermalink');

    Route::get('users/{id}/events', 'EventController@userEvents');

    Route::post('events/{id}', 'EventController@updateEvent');

    Route::post('events', 'EventController@makeEvent');    

    Route::delete('events/{id}/delete', 'EventController@deleteEvent');

    Route::get('user/events', 'EventController@myEvents');

    Route::get('user/events/attending', 'EventController@eventsAuthenticatedUserIsAttending');

    Route::get('events/{id}/users', 'EventController@eventUsers');

    Route::post('events/{id}/join', 'EventController@join');

    Route::post('events/{id}/leave', 'EventController@leave');

    Route::post('events/{id}/addUser', 'EventController@addUser'); 

    Route::post('events/{id}/removeUser', 'EventController@removeUser'); 


    /* Articles */
    Route::get('articles', 'ArticleController@getArticles');

    Route::get('articles/{id}', 'ArticleController@getArticle');

    Route::get('articles/title/{permalink}', 'ArticleController@getByPermalink');

    Route::post('articles', 'ArticleController@createArticle');    

    Route::post('articles/{id}', 'ArticleController@updateArticle');

    Route::delete('articles/{id}', 'ArticleController@deleteArticle');

    Route::get('users/{id}/articles', 'ArticleController@userArticles');

    Route::get('user/articles', 'ArticleController@myArticles');      

    /* Messages */
    Route::post('messages', 'MessageController@send');

    Route::post('messages/{id}/reply', 'MessageController@reply');

    Route::post('messages/all/seen', 'MessageController@markAllAsSeen'); 

    Route::post('messages/many/seen', 'MessageController@markManyAsSeen');

    Route::post('messages/{id}/seen', 'MessageController@markAsSeen'); 

    Route::get('user/inbox', 'MessageController@inbox');

    Route::get('messages/{connectionId}/conversation', 'MessageController@getConversation');
    Route::get('messages/{connectionId}/conversation/new', 'MessageController@getConversationNew');

    Route::delete('messages/{id}', 'MessageController@deleteMessage');

    Route::delete('messages/conversation/{id}', 'MessageController@deleteConversation');

    /* Tags */
    Route::get('tags', 'TagController@index');

    Route::get('business_categories', 'TagController@getBusinessCategories');

    Route::get('tags/popular', 'TagController@getPopularTags');

    Route::get('tags/association', 'TagController@getAssociationTags');

    Route::get('tags/searchAssist', 'TagController@getSearchAssistTags');

    Route::get('tags/noteAssist', 'TagController@getNoteAssistTags');

    /* Card Share */
    Route::post('cardShare', 'CardShareController@shareCardByIds');

    Route::post('cardShareEmail', 'CardShareController@shareCardByEmails');

    Route::get('user/cardShare/sent', 'CardShareController@sentCardShares');

    Route::get('user/cardShare/received', 'CardShareController@receivedCardShares');

    Route::get('user/cardShare/subject', 'CardShareController@subjectCardShares');

    Route::post('refer/ids', 'CardShareController@referUserToIds');

    Route::post('refer/emails', 'CardShareController@referUserToEmails');
    
    /* Testimonials */
    Route::post('testimonials', 'TestimonialController@createTestimonial');

    Route::get('user/testimonials', 'TestimonialController@myTestimonials');

    Route::get('testimonials/{id}', 'TestimonialController@getTestimonial');

    Route::get('users/{id}/testimonials', 'TestimonialController@usersTestimonials');

    Route::get('user/testimonials/approved', 'TestimonialController@myApprovedTestimonials');

    Route::get('user/testimonials/unapproved', 'TestimonialController@myUnapprovedTestimonials');

    Route::post('testimonials/{id}/approve', 'TestimonialController@approveTestimonial');

    Route::post('testimonials/{id}/unapprove', 'TestimonialController@unapproveTestimonial');

    Route::post('testimonials/{id}/seen', 'TestimonialController@markAsSeen');

    Route::delete('testimonials/{id}', 'TestimonialController@deleteTestimonial');

    Route::post('testimonials/{id}', 'TestimonialController@updateTestimonial');

    /* Opportunities */
    Route::get('opportunities', 'OpportunityController@getOpportunities');

    Route::post('opportunities', 'OpportunityController@add');

    Route::delete('opportunities/{id}', 'OpportunityController@remove');

    /* Network */
    Route::get('users/network/following', 'NetworkController@myFollowing');

    Route::get('users/{id}/network/following', 'NetworkController@userFollowing');

    Route::post('users/{id}/follow', 'NetworkController@follow');

    Route::post('users/{id}/unfollow', 'NetworkController@unfollow');

    Route::get('users/network/followers', 'NetworkController@myFollowers');

    Route::get('users/{id}/network/followers', 'NetworkController@userFollowers');

    Route::get('user/network', 'NetworkController@network');

    Route::post('network/sort', 'SortingController@sort');

    Route::post('network/park', 'SortingController@unsort');
    
    Route::get('network/sort/categories', 'SortingController@getCategories');

    Route::post('network/sort/active', 'SortingController@markActive');

    Route::post('network/all/seen', 'NetworkController@markAllAsSeen');

    Route::post('network/{id}/seen', 'NetworkController@markAsSeen');

    Route::post('network/{id}/delete', 'NetworkController@deleteInvite');
    
    Route::post('users/{id}/block', 'NetworkController@block');

    Route::post('users/{id}/unblock', 'NetworkController@unblock');

    Route::get('network/invites', 'NetworkController@getNewInvites');

    Route::get('network/invites/{id}', 'NetworkController@getInvite');


    /* Placeholder Users */
    Route::post('placeholder', 'PlaceholderController@store');

    Route::post('placeholder/invite', 'PlaceholderController@sendInvitation');

    Route::post('placeholder/{id}', 'PlaceholderController@update');

    Route::post('cardOcr', 'PlaceholderController@CardOCR');

    /* Notes */
    Route::get('users/{id}/notes', 'NoteController@getSubjectsNotes');
    Route::post('notes', 'NoteController@createNote');
    Route::get('notes', 'NoteController@userNotes');
    Route::get('notes/{id}', 'NoteController@userNote');
    Route::post('notes/{id}', 'NoteController@updateNote');
    Route::post('notes/{id}/seen', 'NoteController@markAsSeen');
    Route::delete('notes/{id}', 'NoteController@deleteNote');
    Route::get('notes/reminders/send', 'NoteController@sendReminders');

    /* Notifications */
    Route::get('notifications', 'NotificationController@getNotifications');
    Route::post('notifications/markAllAsSeen', 'NotificationController@markAllAsSeen');
    Route::get('notifications/getCountOfUnseen', 'NotificationController@getCountOfUnseen');
    Route::get('notifications/{id}', 'NotificationController@show');
    Route::post('notifications/{id}', 'NotificationController@markAsSeen');
    Route::delete('notifications/{id}', 'NotificationController@deleteNotification');

    /* Search */
    Route::get('search/users', 'UserController@search');
    Route::get('search/users/public', 'UserController@searchPublic');
    Route::get('search/events', 'EventController@search');
    Route::get('search/events/public', 'EventController@searchPublic');
    Route::get('search/articles', 'ArticleController@search');
    Route::get('search/events/users', 'EventController@eventsUsersSearch');

    /* Legal */
    Route::get('legal/termsAndConditions', 'LegalController@getTermsAndConditions');
    Route::get('legal/privacyPolicy', 'LegalController@getPrivacyPolicy');

    /* Complaints */
    Route::post('report/abuse', 'ComplaintController@submitAbuse');

    /* Help */
    Route::post('support/help', 'MiscController@getHelp');

    /* Mail Subscription */
    Route::get('mail/templates', 'MailController@getTemplates');
    Route::get('mail/user/unsubscribed', 'MailController@getUnsubbed');
    Route::post('mail/user/unsubscribe', 'MailController@updateSubscription');

    /* Misc Utilities */
    Route::get('timezones', 'MiscController@getTimezones');

    /* Channels */
    Route::get('channels', 'ChannelController@getChannels');
    Route::get('channels/{id}', 'ChannelController@getChannel');
    Route::get('channels/{id}/members', 'ChannelController@getMembers');
    Route::post('channels/{id}/join', 'ChannelController@join');
    Route::post('channels/{id}/leave', 'ChannelController@leave');
    Route::get('channels/{id}/roles', 'ChannelController@getRoles');
    Route::post('channels/{id}/roles/update', 'ChannelController@updateRole');
    Route::delete('channels/{id}/notifications', 'ChannelController@clearNotifications');

    /* Channel Chat */
    Route::get('channels/{id}/chat', 'ChatMessageController@index');
    Route::post('channels/{id}/chat', 'ChatMessageController@store');
    Route::delete('chat/{id}', 'ChatMessageController@destroy');
    Route::post('chat/{id}/report', 'ChatMessageController@report');
    Route::post('chat/{id}/mention', 'ChatMessageController@mention');

    /* OAuth */
    Route::group(['prefix' => 'oauth'], function() {
        Route::get('/', 'OAuthController@login');
        Route::get('docs', function() {
            return view('docs.oauth');
        });
        Route::get('register', 'OAuthController@register');
        Route::post('register', 'OAuthController@create');
        Route::get('login', 'OAuthController@login');
        Route::post('login', 'OAuthController@authenticate');
        Route::post('access_token', function() {
            return response()->json(Authorizer::issueAccessToken());
        });
    });

    /* Export contact */
    Route::post('export/contacts', 'ExportController@export');

    /** Association Keywords */
    Route::get('associationKeywords', 'AssociationKeywordController@getAll');
    Route::post('user/associationKeywords/{id}', 'AssociationKeywordController@attachUser');
    Route::delete('user/associationKeywords/{id}', 'AssociationKeywordController@detachUser');

}); 
// EO route group api/v1

/**
 * Admin Dashboard routes
 */
Route::group(['prefix' => 'api/v1/admin'], function() {

    Route::post('auth/login', 'TNEAuthController@login');

    Route::group(['middleware' => ['jwt.auth', 'admin']], function() {

        // USERS
        Route::get('users/activity', 'UserController@getActivity');
        Route::post('users/{id}', 'UserController@update');
        Route::post('users/{user}/restore', 'UserController@restore');

        // Admin notes
        Route::post('adminNotes/{id}', 'AdminNoteController@update');
        Route::post('users/{id}/addNote', 'AdminNoteController@addNote');
        Route::get('users/{id}/adminNotes', 'AdminNoteController@getNotesByUser');
        Route::delete('adminNotes/{id}', 'AdminNoteController@destroy');
        
        Route::get('users/datatables', 'UserController@dataTables');
        Route::get('users/count', 'UserController@count');
        Route::resource('users', 'UserController');

        // EVENTS
        Route::post('events/{id}/addUser', 'EventController@addUser');
        Route::post('events/{id}/removeUser', 'EventController@removeUser');
        Route::post('events/{id}/restore', 'EventController@restore');
        Route::post('events/{id}', 'EventController@update');
        Route::get('events/{id}/users', 'EventController@attendees');
        Route::resource('events', 'EventController');

        // TAGS
        Route::post('tags/{id}', 'TagController@update');
        Route::resource('tags', 'TagController');

        // LEGAL
        Route::get('legal', 'LegalController@getLegal');
        Route::get('legal/tc/{id}', 'LegalController@showTermsAndConditions');
        Route::get('legal/pp/{id}', 'LegalController@showPrivacyPolicy');
        Route::post('legal/tc/{id}', 'LegalController@updateTermsAndConditions');
        Route::post('legal/pp/{id}', 'LegalController@updatePrivacyPolicy');
        Route::post('legal/tc', 'LegalController@createTermsAndConditions');
        Route::post('legal/pp', 'LegalController@createPrivacyPolicy');
        Route::delete('legal/tc/{id}', 'LegalController@deleteTermsAndConditions');
        Route::delete('legal/pp/{id}', 'LegalController@deletePrivacyPolicy');

        // ARTICLES
        Route::post('articles/{id}', 'ArticleController@update');
        Route::resource('articles', 'ArticleController');

        // CARD SHARES
        Route::resource('cardshares', 'CardShareController');

        // NETWORK
        Route::resource('network', 'NetworkController');

        // TESTIMONIAL
        Route::post('testimonials/{id}', 'TestimonialController@update');
        Route::resource('testimonials', 'TestimonialController');

        // NOTE
        Route::post('notes/{id}', 'NoteController@update');
        Route::resource('notes', 'NoteController');

        // MESSAGES
        Route::post('messages/mass_message', 'MessageController@mass_message');
        Route::resource('messages', 'MessageController');

        // ANALYTICS
        Route::get('users-cities', 'AnalyticsController@getUserWithCities');

        // PLACEHOLDERS
        Route::get('placeholders', 'PlaceholderController@index');

        // PARTNERS
        Route::resource('partners', 'PartnerController');

        // MIGRATIONS
        Route::group(['prefix' => 'migration'], function() {

            //Route::post('messageDate', 'LegacyDataMigrationController@messageDate');

        }); 
        // EO MIGRATIONS

        // CHANNELS
        Route::get('channels', 'ChannelController@index');
        Route::post('channels', 'ChannelController@store');
        Route::get('channels/{id}', 'ChannelController@show');
        Route::post('channels/{id}/update', 'ChannelController@update');
        Route::post('channels/{id}/delete', 'ChannelController@destroy');

        // CHANNEL MEMBER
        Route::post('channels/{id}/addMember', 'ChannelController@addMember');
        Route::post('channels/{id}/removeMember', 'ChannelController@removeMember');
        Route::post('channels/{id}/updateMemberRole', 'ChannelController@updateMemberRole');
        Route::get('channels/{id}/indexMembers', 'ChannelController@indexMembers');

        //ROLES
        Route::post('channels/{id}/roles', 'RoleController@store');
        Route::get('channels/{id}/roles', 'RoleController@index');
        Route::get('roles/{id}', 'RoleController@show');
        Route::post('roles/{id}/update', 'RoleController@update');
        Route::post('roles/{id}/delete', 'RoleController@destroy');

        //ASSOCIATION KEYWORDS
        Route::resource('associationKeywords', 'AssociationKeywordController');
    });

}); // EO route group admin api

