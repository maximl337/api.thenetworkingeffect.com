<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
Use App\User;
use Auth;

class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'email'             => 'email|unique:users,email,' . Auth::id(),
            'password'          => 'min:3',
            'first_name'        => 'min:1',
            'last_name'         => 'min:1',
            'business_name'     => 'min:1',
            'job_title'         => 'min:1',
            'business_category' => 'min:1',
            'tags'              => 'array',
            'hide_address'      => 'boolean',
            'avatar'            => 'image|max:20000',
            'logo'              => 'image|max:20000'
        ];
    }
}
