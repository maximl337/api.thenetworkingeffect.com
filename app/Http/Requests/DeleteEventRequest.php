<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Event;
use Auth;

class DeleteEventRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        $eventId = $this->route('id');

        return Event::where('id', $eventId)
                        ->where('user_id', Auth::id())->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
