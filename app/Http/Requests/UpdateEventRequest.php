<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Event;
use Auth;

class UpdateEventRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        // $eventId = $this->route('id');

        // return Event::where('id', $eventId)
        //                 ->where('user_id', Auth::id())->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required',
            'description'       => 'required',
            'event_date'        => 'required',
            'start_time'        => 'required',
            'end_time'          => 'required',
            'timezone'          => 'required|timezone',
            'street'            => 'required_without:venue',
            'venue'             => 'required_without:street',
            'city'              => 'required',
            'state'             => 'required',
            'banner_image'      => 'mimes:jpeg,bmp,png|max:20000',
            'tags'              => 'required|array'
        ];
    }
}
