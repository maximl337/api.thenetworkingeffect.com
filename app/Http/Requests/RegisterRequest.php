<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'             => 'required|email|unique:users,email',
            'password'          => 'required',
            'first_name'        => 'required',
            'last_name'         => 'required',
            'business_name'     => 'required',
            'job_title'         => 'required',
            'business_category' => 'required'
        ];
    }
}
