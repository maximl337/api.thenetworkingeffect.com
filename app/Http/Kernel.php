<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Barryvdh\Cors\Middleware\HandleCors::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \LucaDegasperi\OAuth2Server\Middleware\OAuthExceptionHandlerMiddleware::class,
        // \App\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth'                 => \App\Http\Middleware\Authenticate::class,
        'auth.basic'           => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest'                => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'admin'                => \App\Http\Middleware\AdminMiddleware::class,
        'jwt.auth'             => \Tymon\JWTAuth\Middleware\GetUserFromToken::class,
        'event.owner'          => \App\Http\Middleware\EventOwnerMiddleware::class,
        'article.owner'        => \App\Http\Middleware\ArticleOwnerMiddleware::class,
        'testimonial.subject'  => \App\Http\Middleware\TestimonialSubjectMiddleware::class,
        'testimonial.author'   => \App\Http\Middleware\TestimonialAuthorMiddleware::class,
        'message.recipient'    => \App\Http\Middleware\MessageRecipientMiddleware::class,
        'note.owner'           => \App\Http\Middleware\NoteOwnerMiddleware::class,
        'notification.subject' => \App\Http\Middleware\NotificationSubjectMiddleware::class,
        'placeholder.activate' => \App\Http\Middleware\ActivatePlaceholderMiddleware::class,
        'network.subject'      => \App\Http\Middleware\NetworkSubjectMiddleware::class,
        'oauth'                => \LucaDegasperi\OAuth2Server\Middleware\OAuthMiddleware::class,
        'csrf'                 => \App\Http\Middleware\VerifyCsrfToken::class,
    ];
}
