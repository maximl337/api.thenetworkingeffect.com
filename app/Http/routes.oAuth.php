<?php

/**
 * Oauth2 required to access routes below
 * The controllers are copied into a
 * dedicated oauth2 folder. Any change
 * must hence be implemented twice in
 * the JSON web token group above and 
 * these. 
 *
 * Ideally all clients will access the API
 * through oauth2 and the controllers above can be removed.
 */
Route::group(['prefix' => 'api/v1/oauth', 'middleware' => ['oauth']], function() {

    /* USERS */
    Route::get('users', 'OAuth\UserController@getUsers');
    
    Route::get('users/{id}', 'OAuth\UserController@getUser');

    Route::get('public/users/{id}', 'OAuth\UserController@getUserPublic');

    Route::get('users/name/{permalink}', 'OAuth\UserController@getUserByPermalink');

    Route::get('user/email/{email}', 'OAuth\UserController@getUserByEmailPublic');
    
    Route::post('user/update', 'OAuth\UserController@updateUser');
    
    Route::delete('user/delete', 'OAuth\UserController@deleteUser'); 

    Route::post('user/roaming', 'OAuth\UserController@addRoamingGeoCode');

    Route::post('user/device', 'OAuth\UserController@userDevice');

    Route::post('user/avatar', 'OAuth\UserController@userAvatar');

    Route::post('user/logo', 'OAuth\UserController@userLogo');

    Route::post('user/email', 'OAuth\UserController@updateEmail');

    Route::post('user/password', 'OAuth\UserController@updatePassword');

    Route::post('user/timezone', 'OAuth\UserController@updateTimezone');

    Route::post('user/reminderNotificationTime', 'OAuth\UserController@updateReminderNotificationTime');

    /* Events */
    Route::get('events', 'OAuth\EventController@getEvents');

    Route::get('events/{id}', 'OAuth\EventController@getEvent');

    Route::get('events/{id}/public', 'OAuth\EventController@getEventPublic');

    Route::get('events/name/{permalink}', 'OAuth\EventController@getByPermalink');

    Route::get('users/{id}/events', 'OAuth\EventController@userEvents');

    Route::post('events/{id}', 'OAuth\EventController@updateEvent');

    Route::post('events', 'OAuth\EventController@makeEvent');    

    Route::delete('events/{id}/delete', 'OAuth\EventController@deleteEvent');

    Route::get('user/events', 'OAuth\EventController@myEvents');

    Route::get('user/events/attending', 'OAuth\EventController@eventsAuthenticatedUserIsAttending');

    Route::get('events/{id}/users', 'OAuth\EventController@eventUsers');

    Route::post('events/{id}/join', 'OAuth\EventController@join');

    Route::post('events/{id}/leave', 'OAuth\EventController@leave');

    Route::post('events/{id}/addUser', 'OAuth\EventController@addUser'); 

    Route::post('events/{id}/removeUser', 'OAuth\EventController@removeUser');

    /* Articles */
    Route::get('articles', 'OAuth\ArticleController@getArticles');

    Route::get('articles/{id}', 'OAuth\ArticleController@getArticle');

    Route::get('articles/title/{permalink}', 'OAuth\ArticleController@getByPermalink');

    Route::post('articles', 'OAuth\ArticleController@createArticle');    

    Route::post('articles/{id}', 'OAuth\ArticleController@updateArticle');

    Route::delete('articles/{id}', 'OAuth\ArticleController@deleteArticle');

    Route::get('users/{id}/articles', 'OAuth\ArticleController@userArticles');

    Route::get('user/articles', 'OAuth\ArticleController@myArticles');  

    /* Messages */
    Route::post('messages', 'OAuth\MessageController@send');

    Route::post('messages/{id}/reply', 'OAuth\MessageController@reply');

    Route::post('messages/all/seen', 'OAuth\MessageController@markAllAsSeen'); 

    Route::post('messages/{id}/seen', 'OAuth\MessageController@markAsSeen'); 

    Route::get('user/inbox', 'OAuth\MessageController@inbox');

    Route::get('messages/{connectionId}/conversation', 'OAuth\MessageController@getConversation');

    Route::delete('messages/{id}', 'OAuth\MessageController@deleteMessage');

    /* Tags */
    Route::get('tags', 'OAuth\TagController@index');

    Route::get('business_categories', 'OAuth\TagController@getBusinessCategories');

    Route::get('tags/popular', 'OAuth\TagController@getPopularTags');

    Route::get('tags/association', 'OAuth\TagController@getAssociationTags');

    Route::get('tags/searchAssist', 'OAuth\TagController@getSearchAssistTags');

    Route::get('tags/noteAssist', 'OAuth\TagController@getNoteAssistTags');

    /* Card Share */
    Route::post('cardShare', 'OAuth\CardShareController@shareCardByIds');

    Route::post('cardShareEmail', 'OAuth\CardShareController@shareCardByEmails');

    Route::get('user/cardShare/sent', 'OAuth\CardShareController@sentCardShares');

    Route::get('user/cardShare/received', 'OAuth\CardShareController@receivedCardShares');

    Route::get('user/cardShare/subject', 'OAuth\CardShareController@subjectCardShares');

    Route::post('refer/ids', 'OAuth\CardShareController@referUserToIds');

    Route::post('refer/emails', 'OAuth\CardShareController@referUserToEmails');
    
    /* Testimonials */
    Route::post('testimonials', 'OAuth\TestimonialController@createTestimonial');

    Route::get('user/testimonials', 'OAuth\TestimonialController@myTestimonials');

    Route::get('testimonials/{id}', 'OAuth\TestimonialController@getTestimonial');

    Route::get('users/{id}/testimonials', 'OAuth\TestimonialController@usersTestimonials');

    Route::get('user/testimonials/approved', 'OAuth\TestimonialController@myApprovedTestimonials');

    Route::get('user/testimonials/unapproved', 'OAuth\TestimonialController@myUnapprovedTestimonials');

    Route::post('testimonials/{id}/approve', 'OAuth\TestimonialController@approveTestimonial');

    Route::post('testimonials/{id}/unapprove', 'OAuth\TestimonialController@unapproveTestimonial');

    Route::post('testimonials/{id}/seen', 'OAuth\TestimonialController@markAsSeen');

    Route::delete('testimonials/{id}', 'OAuth\TestimonialController@deleteTestimonial');

    Route::post('testimonials/{id}', 'OAuth\TestimonialController@updateTestimonial');

    /* Opportunities */
    Route::get('opportunities', 'OAuth\OpportunityController@getOpportunities');

    Route::post('opportunities', 'OAuth\OpportunityController@add');

    Route::delete('opportunities/{id}', 'OAuth\OpportunityController@remove');

    /* Network */
    Route::get('users/network/following', 'OAuth\NetworkController@myFollowing');

    Route::get('users/{id}/network/following', 'OAuth\NetworkController@userFollowing');

    Route::post('users/{id}/follow', 'OAuth\NetworkController@follow');

    Route::post('users/{id}/unfollow', 'OAuth\NetworkController@unfollow');

    Route::get('users/network/followers', 'OAuth\NetworkController@myFollowers');

    Route::get('users/{id}/network/followers', 'OAuth\NetworkController@userFollowers');

    Route::get('user/network', 'OAuth\NetworkController@network');

    // Route::post('network/sort', 'SortingController@sort');

    // Route::post('network/park', 'SortingController@unsort');
    
    // Route::get('network/sort/categories', 'SortingController@getCategories');

    // Route::post('network/sort/active', 'SortingController@markActive');

    Route::post('network/all/seen', 'OAuth\NetworkController@markAllAsSeen');

    Route::post('network/{id}/seen', 'OAuth\NetworkController@markAsSeen');

    Route::post('users/{id}/block', 'OAuth\NetworkController@block');

    Route::post('users/{id}/unblock', 'OAuth\NetworkController@unblock');

    Route::get('network/invites', 'OAuth\NetworkController@getNewInvites');

    Route::get('network/invites/{id}', 'OAuth\NetworkController@getInvite');

    /* Placeholder Users */
    Route::post('placeholder', 'OAuth\PlaceholderController@store');

    Route::post('cardOcr', 'OAuth\PlaceholderController@CardOCR');

    Route::post('placeholder/invite', 'OAuth\PlaceholderController@sendInvitation');

    /* Notes */
    Route::get('users/{id}/notes', 'OAuth\NoteController@getSubjectsNotes');

    Route::post('notes', 'OAuth\NoteController@createNote');

    Route::get('notes', 'OAuth\NoteController@userNotes');

    Route::get('notes/{id}', 'OAuth\NoteController@userNote');

    Route::post('notes/{id}', 'OAuth\NoteController@updateNote');

    Route::post('notes/{id}/seen', 'OAuth\NoteController@markAsSeen');

    Route::delete('notes/{id}', 'OAuth\NoteController@deleteNote');

    /* Notifications */
    Route::get('notifications', 'OAuth\NotificationController@getNotifications');

    Route::post('notifications/markAllAsSeen', 'OAuth\NotificationController@markAllAsSeen');

    Route::get('notifications/getCountOfUnseen', 'OAuth\NotificationController@getCountOfUnseen');

    Route::get('notifications/{id}', 'OAuth\NotificationController@show');

    Route::post('notifications/{id}', 'OAuth\NotificationController@markAsSeen');

    Route::delete('notifications/{id}', 'OAuth\NotificationController@deleteNotification');

    /* Search */
    Route::get('search/users', 'OAuth\UserController@search');

    Route::get('search/users/public', 'OAuth\UserController@searchPublic');

    Route::get('search/events', 'OAuth\EventController@search');

    Route::get('search/events/public', 'OAuth\EventController@searchPublic');

    Route::get('search/articles', 'OAuth\ArticleController@search');

    Route::get('search/events/users', 'OAuth\EventController@eventsUsersSearch');

});
/* EO OAUTH2 ROUTES */