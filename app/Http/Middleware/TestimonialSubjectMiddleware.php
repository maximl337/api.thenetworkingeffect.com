<?php

namespace App\Http\Middleware;

use Auth;
use App\Testimonial;
use Closure;

class TestimonialSubjectMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        $testimonialSubject = Testimonial::findOrFail($request->route('id'))->subject()->firstOrFail();

        if( $testimonialSubject->id != $user->id ) 
                return response()->json([
                        'error' => [
                            'message' => 'Request not authorized', 
                            'status' => '403'
                        ]
                    ], 403);

        return $next($request);
    }
}
