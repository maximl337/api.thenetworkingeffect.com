<?php

namespace App\Http\Middleware;

use Closure;
use App\Article;
use App\User;
use Auth;

class ArticleOwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        $articleOwner = Article::findOrFail($request->route('id'))->user()->firstOrFail(); //will return 404

        if( $articleOwner->id != $user->id ) 
                return response()->json([
                        'error' => [
                            'message' => 'Request not authorized', 
                            'status' => '403'
                        ]
                    ], 403);

        return $next($request);
    }
}
