<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {

            $user = Auth::user(); //JWTAuth::parseToken()->toUser();

            if( ! $user->is_admin )
                return response()->json([
                        'error' => 'Unauthorized Access'
                    ], 401);

            return $next($request);

        } catch (JWTException $e) {

            return response()->json(['error' => 'could_not_create_token', 'message' => $e->getMessage()], 500);

        } catch(Exception $e) {

            return response()->json(['error' => 'could_not_create_token', 'message' => $e->getMessage()], 500);
        }
           
    }
}
