<?php

namespace App\Http\Middleware;

use Auth;
use App\Message;
use Closure;

class MessageRecipientMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        $messageRecipient = Message::findOrFail($request->route('id'))->to()->firstOrFail();

        if( $messageRecipient->id != $user->id ) 
                return response()->json([
                        'error' => [
                            'message' => 'Request not authorized', 
                            'status' => '403'
                        ]
                    ], 403);
            
        return $next($request);
    }
}
