<?php

namespace App\Http\Middleware;

use JWTAuth;
use Closure;
use App\User;
use App\Services\PlaceholderService;

class ActivatePlaceholderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // check if user is placeholder
        $is_placeholder = User::where('email', $request->get('email'))
                                ->where('is_placeholder', true)
                                ->exists();

        if($is_placeholder) {

            try {

                $user = (new PlaceholderService)->activatePlaceholder($request);

                // token
                $token = JWTAuth::fromUser($user);

                return response()->json([
                            'message' => 'Placeholder activated',
                            'data' => [ 
                                'user'    => $user,
                                'token'   => $token
                                ]
                        ], 200);

            } catch(JWTException $e) {

                return response()->json([
                            'error' => [
                                'message' => $e->getMessage(), 
                                'status' => '500'
                            ]
                        ], 500);

            } catch(\Exception $e) {

                return response()->json([
                            'error' => [
                                'message' => $e->getMessage(), 
                                'status' => '500'
                            ]
                        ], 500);
            }
            
        }

        return $next($request);
    }
}
