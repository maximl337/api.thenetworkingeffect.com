<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Note;

class NoteOwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        try {

            $noteOwner = Note::findOrFail($request->route('id'))->user()->firstOrFail();

            if( $noteOwner->id != $user->id ) 
                    return response()->json([
                            'error' => [
                                'message' => 'Request not authorized', 
                                'status' => '403'
                            ]
                        ], 403);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return response()->json([
                    'error' => [
                            'message' => 'Note not found',
                            'status'    => '404'
                        ]
                    ], 404);

        }
        

        return $next($request);
    }
}
