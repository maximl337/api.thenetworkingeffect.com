<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use App\Network;

class NetworkSubjectMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        try {

            $networkSubject = Network::findOrFail($request->route('id'))->subject()->firstOrFail();

            if( $networkSubject->id != Auth::id() ) 
                    return response()->json([
                            'error' => [
                                'message' => 'Request not authorized', 
                                'status' => '403'
                            ]
                        ], 403);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return response()->json([
                            'error' => [
                                'message' => $e->getMessage(), 
                                'status' => '404'
                            ]
                        ], 404);

        } catch(\Exception $e) {

            return response()->json([
                            'error' => [
                                'message' => $e->getMessage(), 
                                'status' => '500'
                            ]
                        ], 500);

        }
        
            
        return $next($request);
    }
}
