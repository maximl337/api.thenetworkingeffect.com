<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Notification;

class NotificationSubjectMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        try {

            $notificationSubject = Notification::findOrFail($request->route('id'))->subject()->firstOrFail();

            if( $notificationSubject->id != $user->id ) 
                    return response()->json([
                            'error' => [
                                'message' => 'Request not authorized', 
                                'status' => '403'
                            ]
                        ], 403);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return response()->json([
                        'error' => [
                            'message' => 'Notificaton or subject not not found', 
                            'status' => '404'
                        ]
                    ], 404);

        }
        
        return $next($request);
    }
}
