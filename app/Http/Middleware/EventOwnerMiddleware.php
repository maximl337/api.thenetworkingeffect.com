<?php

namespace App\Http\Middleware;

use Closure;
use App\Event;
use Auth;

class EventOwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $user = Auth::user();

        try {

            $eventOwner = Event::findOrFail($request->route('id'))->user()->firstOrFail();

            if( ($eventOwner->id != $user->id) && !$user->is_admin ) 
                    return response()->json([
                            'error' => [
                                'message' => 'Request not authorized', 
                                'status' => '403'
                            ]
                        ], 403);

            return $next($request);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return response()->json([
                        'error' => [
                            'message' => 'Event not found', 
                            'status' => '404'
                        ]
                    ], 404);

        }

        


        
        
    }
}
