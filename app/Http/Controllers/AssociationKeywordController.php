<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;
use App\Contracts\Search;
use App\AssociationKeyword;
use App\Http\Controllers\ApiController;
use App\Transformers\AssociationKeywordsTransformer;

class AssociationKeywordController extends ApiController
{

    protected $indice;
    protected $algolia;
    protected $associationKeywordsTransformer;

    public function __construct(Search $algolia, AssociationKeywordsTransformer $associationKeywordsTransformer)
    {
        $this->middleware('jwt.auth', ['except' => ['getAll']]);
        $this->algolia = $algolia;
        $this->indice = env('ALGOLIA_USERS_INDEX');
        $this->associationKeywordsTransformer = $associationKeywordsTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $association_keywords = $this->associationKeywordsTransformer->transformCollection(AssociationKeyword::all()->toArray());

        return $this->respond(compact('association_keywords'));
    }

    /**
     * [attachUser description]
     * @param  [type]  $id      [description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function attachUser($id, Request $request)
    {
        $association_keyword = AssociationKeyword::findOrFail($id);
        $request->user()
            ->association_keywords()
            ->attach($id);
        $this->updateAlgoliaAssociationKeywords($request->user());
        return $this->respondCreated(compact('association_keyword'));
    }

    /**
     * [detachUser description]
     * @param  [type]  $id      [description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function detachUser($id, Request $request)
    {
        $association_keyword = AssociationKeyword::findOrFail($id);
        $request->user()
            ->association_keywords()
            ->detach($id);
        $this->updateAlgoliaAssociationKeywords($request->user());
        return $this->respond("Keyword removed");
    }

    /**
     * [updateAlgoliaAssociationKeywords description]
     * @param  User   $user [description]
     * @return [type]       [description]
     */
    public function updateAlgoliaAssociationKeywords(User $user)
    {
        $associationKeywordsData = [];

        foreach($user->association_keywords as $association_keyword) {
            $associationKeywordsData[] = [ 
                "id" => $association_keyword->id,
                "keyword" => $association_keyword->keyword,
            ];
        }

        // event that updates users channels attribute
        $this->algolia->index($this->indice)
                ->update([
                    'association_keywords' => $associationKeywordsData,
                    'objectID' => $user->id
                ]);
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        return $this->respond(AssociationKeyword::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'unique:association_keywords,keyword'
        ]);
        $association_keyword = AssociationKeyword::create(['keyword' => $request->keyword]);
        return $this->respondCreated(compact('association_keyword'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $association_keyword = AssociationKeyword::findOrFail($id);
        return $this->respond(compact('association_keyword'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'keyword' => 'unique:association_keywords,keyword,'.$id
        ]);
        $association_keyword = AssociationKeyword::findOrFail($id);
        $association_keyword->update(['keyword' => $request->keyword]);
        return $this->respond(compact('association_keyword'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $associationKeyword = AssociationKeyword::findOrFail($id);
        //remove user associations
        $users = $associationKeyword->users()->get();
        $associationKeyword->users()->detach();
        foreach($users as $user) {
            $this->updateAlgoliaAssociationKeywords($user);
        }
        $associationKeyword->delete();
        return $this->respond("Keyword delete");
    }
}
