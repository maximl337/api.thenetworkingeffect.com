<?php

namespace App\Http\Controllers\OAuth;

use Illuminate\Http\Request;


use DB;
use Auth;
use App\User;
use Carbon\Carbon;
use App\Message;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

use App\Http\Requests\MessageRequest;
use App\Http\Requests;

use App\Transformers\InboxTransformer;
use App\Transformers\ConversationTransformer;

use App\Events\NewMessageSent;

class MessageController extends ApiController
{

    protected $inboxTransformer;

    protected $conversationTransformer;

    /**
     * Init middlerware
     */
    public function __construct(InboxTransformer $inboxTransformer, ConversationTransformer $conversationTransformer)
    {
        // $this->middleware('jwt.auth');

        $this->middleware('message.recipient', ['only' => ['markAsSeen']]);

        $this->inboxTransformer = $inboxTransformer;

        $this->conversationTransformer = $conversationTransformer;
    }

    /**
     * Send a message
     * @param  MessageRequest $request validation
     * @return mixed                  
     */
    public function send(MessageRequest $request)
    {
        $input = $request->input();

        $input['from'] = Auth::user()->id;

        $message = Message::create($input);

        event(new NewMessageSent($message));

        return $this->respondCreated('Message sent', [
                'message' => $message
            ]);  
    }

    /**
     * reply to a message thread
     * @param  int         $id      id of the message to reply to
     * @param  MessageRequest $request [description]
     * @return [type]                  [description]
     */
    public function reply($id, MessageRequest $request)
    {

        $parentMessage = Message::findOrFail($id);

        $input = $request->input();

        $input['from'] = Auth::user()->id;

        $input['parent_id'] = $parentMessage->id;

        $message = Message::create($input);

        event(new NewMessageSent($message));

        return $this->respondCreated('Reply sent', [
                'message' => $message
            ]);
    }

    public function inbox() {

        $userId = Auth::user()->id;

        
        $conversation_list = $this->get_conversation_list();
        
        //get the latest message of from the inbox
        foreach ( $conversation_list as $item ) {

            $latestMessage = $this->get_latest_message_from_conversation($userId, $item->connection_id);

            $item->message = $latestMessage[0]->body;

            $item->status = $latestMessage[0]->seen ? true : false;

        }

        return $this->respond(
                $this->inboxTransformer->transformCollection($conversation_list)
            );

    }

    protected function get_conversation_list()
    {

        $userId = Auth::user()->id;

        $conversation_list = DB::select("SELECT `id`, connection_id, user_id, MAX(`created_at`) as last_date FROM 
                            ( 
                            SELECT `id`, `to` as 'connection_id', `from` as 'user_id', `created_at` 
                            FROM `messages` 
                            WHERE `from` = ? 
                            AND 
                            `id` NOT IN (SELECT `message_id` FROM `deleted_messages` WHERE `user_id` = ?)  
                            UNION ALL 

                            SELECT `id`, `from` as 'connection_id', `to` as 'user_id', `created_at` 
                            FROM `messages` 
                            WHERE `to` = ?
                            AND 
                            `id` NOT IN (SELECT `message_id` FROM `deleted_messages` WHERE `user_id` = ?)  
                            ) x 
                            GROUP BY connection_id 
                            ORDER BY last_date DESC", [$userId, $userId,  $userId,  $userId]);

        return $conversation_list;
    }

    protected function get_latest_message_from_conversation($userId, $connectionId)
    {
        $latestMessage = DB::select("SELECT * FROM `messages` WHERE
                (`from` = ? AND `to` = ?)
                OR
                (`from` = ? AND `to` = ?)
                AND
                `id` NOT IN (SELECT `message_id` FROM `deleted_messages` WHERE `user_id` = ?) 
                ORDER BY `created_at` DESC
                LIMIT 1", [$userId, $connectionId, $connectionId, $userId, $userId]);

        return $latestMessage;
    }

    public function getConversation($connectionId)
    {

        $userId = Auth::user()->id;

        $conversation = DB::select("SELECT * FROM (
                        
                        SELECT i.*
                        FROM `messages` i
                        WHERE
                        i.`to` = ?
                        AND
                        i.`from` = ?
                        AND
                        i.`id` NOT IN (SELECT `message_id` FROM `deleted_messages` WHERE `user_id` = ?) 
                        UNION
                        
                        SELECT i.*
                        FROM `messages` i
                        WHERE
                        i.`to` = ?
                        AND
                        i.`from` = ?
                        AND
                        i.`id` NOT IN (SELECT `message_id` FROM `deleted_messages` WHERE `user_id` = ?)                    
                    ) tmp ORDER BY tmp.created_at ASC", [$userId, $connectionId, $userId, $connectionId, $userId, $userId]);

        return $this->respond(
            
                $this->conversationTransformer->transformCollection($conversation)

            );
    }

    public function deleteMessage($id)
    {
        $userId = Auth::user()->id;

        Message::findOrFail($id)->deleted_messages()->attach($userId);

        return $this->respond([
                'message' => 'Message deleted'
            ]);
    }

    public function markAsSeen($id)
    {

        try {
            
            $message = Message::findOrFail($id);

            $message->seen = true;

            $message->save();

            $tableName = $message->getTable();

            // mark converation as seen
            if(!is_null($message->parent_id)) {

                $parentId = $message->parent_id;

            } else {
                
                $parentId = $message->id;
            }

            DB::table($tableName)
                ->where('parent_id', $parentId)
                ->update(['seen' => true]);

            return $this->respond([
                    'message' => 'Message updated'
                ]);

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("Message not found");
                        
        } catch (\Exception $e) {

            return $this->respondInternalError($e->getMessage());
        }
        
    }

    public function markAllAsSeen()
    {
        $userId = Auth::user()->id;

        User::findOrFail($userId)->messages()->update(['seen' => true]);

        return $this->respond([
                'message' => 'Message updated'
            ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        ini_set('memory_limit','256M');

        $messages = Message::with(['to' => function($q) { $q->withTrashed(); }])
                            ->with(['from' => function($q) { $q->withTrashed(); }])
                            ->get();

        return $this->respond($messages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
                'from'  => 'required|exists:users,id',
                'to'    => 'required|exists:users,id',
                'body'  => 'required'
            ]);

        $message = Message::create($request->input());

        event(new NewMessageSent($message));

        return $this->respondCreated($message);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $message = Message::findOrFail($id);

        $result = [

            'message' => $message,

            'from'    => $message->from()->get(),

            'to'      => $message->to()->get()

        ];

        return $this->respond($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $message = Message::findOrFail($id);

        $message->delete();

        return $this->respond('Message deleted');
    }
}
