<?php

namespace App\Http\Controllers\OAuth;

use Illuminate\Http\Request;

use DB;
use Auth;
use App\User;
use App\PlaceholderUser;
use App\Http\Requests;
use App\Contracts\CardOCR;
use App\Services\PlaceholderService;
use App\Events\InvitePlaceholderEvent;
use App\Http\Controllers\ApiController;

class PlaceholderController extends ApiController
{
    
    protected $cardOcr;

    public function __construct(CardOCR $cardOcr)
    {
        //$this->middleware('jwt.auth');

        $this->cardOcr = $cardOcr;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'first_name'    => 'required',
                'last_name'     => 'required',
                'email'         => 'required|email'  
            ]);

        // send data to be store
        try {

            $resp = (new PlaceholderService)->makePlaceholder($request);

            return $this->respondCreated("Created", $resp);

        } catch(\Exception $e) {

            return $this->respondInternalError(["message" => "Something went wrong", "error" => $e->getMessage()]);

        }
    }

    public function CardOCR(Request $request)
    {
       
        try {

            $res = $this->cardOcr->parse($request->file('upfile'));

            return $res;

        } catch(Exception $e) {

            return $this->respondInternalError($e->getMessage());
        }
        
    }

    public function sendInvitation(Request $request)
    {
        $this->validate($request, [
                'placeholder_id' => 'required|exists:users,id'
            ]);

        $input = $request->input();

        try {

            // get placeholder
            $placeholderExists = Auth::user()->ownerPlaceholders()->where('user_id', $input['placeholder_id'])->first();

            if(is_null($placeholderExists)) throw new Exception("User does not own this placeholder");

            if($placeholderExists->invitation_count == 3) throw new Exception("You cannot send more then 3 invitations");

            $placeholder = User::findOrFail($input['placeholder_id']);

            $data = [
                'user'          => Auth::user(),
                'placeholder'   => $placeholder
            ];

            event(new InvitePlaceholderEvent($data));

            return $this->respond("Invitation sent");

        } catch( \Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondInternalError($e->getMessage());

        } catch(Exception $e) {

            return $this->respondInternalError($e->getMessage());
        }
        
    }

    /**
     *
     *
     *  ADMIN SECTION
     *
     * 
     */
    
    public function index() {

        $placeholders = DB::select("SELECT 
                            p.`owner_id`, 
                            p.`user_id`, 
                            p.`invitation_sent`, 
                            u.`email` as 'owner_email', 
                            u.`first_name` as 'owner_first_name', 
                            u.`last_name` as 'owner_last_name', 
                            u2.`email` as 'placeholder_email', 
                            p.`first_name` as 'placeholder_first_name', 
                            p.`last_name` as 'placeholder_last_name', 
                            u2.`is_placeholder`
                            FROM
                            `placeholder_users` p
                            LEFT JOIN
                            `users` u
                            ON
                            u.`id` = p.`owner_id`
                            LEFT JOIN
                            users u2
                            ON
                            u2.`id` = p.`user_id`", []);

        return $this->respond($placeholders);
    }
}
