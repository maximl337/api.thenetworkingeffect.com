<?php

namespace App\Http\Controllers\OAuth;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Opportunity;
use App\Http\Requests;
use App\Http\Controllers\ApiController;

class OpportunityController extends ApiController
{

    public function __construct()
    {
        //$this->middleware('jwt.auth');
    }

    /**
     * Add the given id to opportunities
     * 
     * @param [type] $id [description]
     * @return 
     */
    public function add(Request $request)
    {
        try {
            
            $this->validate($request, [
                    'subject_id' => 'exists:users,id|not_in:' . Auth::id()
                ],
                [
                    'subject_id.not_id' => 'Cannot add self to opportunities'
                ]);

            Auth::user()->opportunities()->save(new Opportunity(['subject_id' => $request->get('subject_id')]));

            return $this->respondCreated("Opportunity added");

        } catch (\Exception $e) {
              
            return $this->respondInternalError($e->getMessage());
        }  
    }

    /**
     * [remove description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function remove($id)
    {
        try {
            
            Auth::user()->opportunities()->where('subject_id', $id)->firstOrFail()->delete();

            return $this->respond("Opportunity removed");

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound($e->getMessage());

        } catch (\Exception $e) {
            
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function getOpportunities()
    {
        try {
            
            $opportunities = Auth::user()->opportunities()->with('subject')->get();

            return $this->respond([
                    "data" => $opportunities
                ]);

        } catch (Exception $e) {
            
            return $this->respondInternalError($e->getMessage());
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        try {

            $opportunities = DB::table('opportunities as o')
                             ->leftJoin('users as u1', 'o.follower_id', '=', 'u1.id')
                             ->leftJoin('users as u2', 'o.subject_id', '=', 'u2.id')
                             ->select(  'o.*', 
                                        'u1.first_name as follower_fname', 
                                        'u1.last_name as follower_lname',
                                        'u2.first_name as subject_fname',
                                        'u2.last_name as subject_lname')
                             ->orderBy('o.created_at', 'desc')
                             ->get();
            
        } catch(\Illuminate\Database\QueryException $e) {

            return $this->respondInternalError($e->getMessage);

        } catch (\Exception $e) {
            
            return $this->respondInternalError($e->getMessage());
        }

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
