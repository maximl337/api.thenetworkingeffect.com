<?php

namespace App\Http\Controllers\OAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

use DB;
use Log;
use Auth;
use Imgur;
use JWTAuth;
use App\Tag;
use App\User;
use App\Device;
use App\Roaming_user;
use App\Http\Requests;
use App\Contracts\Image;
use App\Transformers\UserSearchTransformer;
use App\Transformers\UserTransformer;
use App\Transformers\AlgoliaUserTransformer;
use App\Services\ImageService;
use App\Services\GeoService;
use App\Services\TagService;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Contracts\Search;
use App\Services\SanitizeUrlService;
use App\Events\UserUpdated;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\AdminUpdateUserRequest;
use App\Events\NewUserJoined;
use App\Events\UserDeleted;
use App\MandrillTemplate;
use App\Activity_log;
use Datatables;

class UserController extends ApiController
{

    protected $indices;

    protected $imageService;

    protected $userTransformer;

    protected $searchTransformer;

    protected $geoService;

    protected $tagService;

    protected $search;

    protected $algoliaTransformer;

    /**
     * Add AUTH Middleware to UserController
     */
    public function __construct(ImageService $imageService, UserTransformer $userTransformer, UserSearchTransformer $searchTransformer, GeoService $geoService, TagService $tagService, Search $search)
    {

        // $this->middleware('jwt.auth', ['except' => ['searchPublic', 'getUserByPermalink', 'getUserPublic', 'getUserByEmailPublic', 'dt']]);

        $this->imageService = $imageService;

        $this->userTransformer = $userTransformer;

        $this->searchTransformer = $searchTransformer;

        $this->geoService = $geoService;

        $this->tagService = $tagService;

        $this->search = $search;

        $this->indices = getenv('ALGOLIA_USERS_INDEX');

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getUsers(Request $request)
    {
        
        $limit = $request->get('limit') ?: 20;

        $page = $request->get('page') ?: 0;

        $users = User::with(['tags' => function ($query) use ($limit, $page) {
                               $query->skip($limit * ($page-1))
                                    ->take($limit);
                                }])->paginate($limit);

        return $this->respond($users);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getUser($id, Request $request)
    {

        if($request->get('lat') && $request->get('lng')) {

            Roaming_user::create([
                'latitude' => $request->get('lat'),
                'longitude' => $request->get('lng'),
                'user_id' => Auth::id()
            ]);
        }
       
        try {

            $user = User::findOrFail($id);

            $user['tags'] = $user->tags;

            if( ! $user ) return $this->respondNotFound('User does not exist.');

            return $this->respond([
                    
                    'data' => $this->userTransformer->transform($user->toArray())

                ]);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound('User with that ID was not found');

        } catch(\Exception $e) {

            return $this->respondInternalError("There was an Internal error:" . $e->getMessage());

        }
        
    }

    /**
     * [getUserPublic description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getUserPublic($id)
    {
        try {

            $user = User::findOrFail($id);

            $return = [
                'user' => $user,
                'testimonials' => $user->testimonials()->with('author')->get(),
                'articles'     => $user->articles()->get(),
                'network'       => $user->network_following()->groupBy('subject_id')->with('subject')->get()
            ];


            return $this->respond($return);

        } catch(\Exception $e) {

            return $this->respondNotFound($e->getMessage());
        }

    }

    /**
     * [getUserByEmailPublic description]
     * @param  [type] $email [description]
     * @return [type]        [description]
     */
    public function getUserByEmailPublic($email)
    {
        try {

            $user = User::where('email', $email)->firstOrFail();

            $resp = [
                'user' => [
                    'first_name'        => $user->first_name,
                    'last_name'         => $user->last_name,
                    'email'             => $user->email,
                    'avatar'            => $user->avatar,
                    'logo'              => $user->logo,
                    'job_title'         => $user->job_title,
                    'business_name'     => $user->business_name,
                    'permalink'         => $user->permalink,
                    'phone'             => $user->phone_1,
                    'street'            => $user->street,
                    'city'              => $user->city,
                    'zip_code'          => $user->zip_code,
                    'about_me'          => $user->about_me,
                    'about_business'    => $user->about_business,
                    'articles'          => $user->articles()->get()->toArray(),
                    'testimonials'      => $user->testimonials()->where('approved', true)->with('author')->get()->toArray(),

                ]
            ];

            return $this->respond($resp);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound($e->getMessage());

        } catch(Exception $e) {

            return $this->respondInternalError($e->getMessage());
        }
    }

    /**
     * [getUserByPermalink description]
     * @param  [type] $permalink [description]
     * @return [type]            [description]
     */
    public function getUserByPermalink($permalink)
    {
        $user = User::where('permalink', $permalink)->first();

        return $this->respond($user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function updateUser(UpdateUserRequest $request, SanitizeUrlService $sanitizeUrl)
    {

        $user = Auth::user();

        if( ! $user ) return $this->respondNotFound('User not found');

        $input = $request->input();

        $roamingCoordinatesGiven = (isset($input['lat']) && isset($input['lng']));

        // Password
        if(isset($input['password'])) {

            $user->password = bcrypt($input['password']);

            $user->save();            
        }

        //email
        if(isset($input['email'])) {

            $user->email = $input['email'];

            $user->save();

        }

        $address_exists = (!empty($input['street']) && !empty($input['city']) && !empty($input['state']));

        if($address_exists) {

            // Geo
            $geoCode = $this->geoService
                                ->getLatLang($input['street'], $input['city'], $input['state'], $request->get('zip_code'), $request->get('country'));

            $input['latitude']  = $geoCode['latitude'];
            
            $input['longitude'] = $geoCode['longitude'];

        } //check if address exists

        // check if roaming coordinates were sent
        if($roamingCoordinatesGiven) {

            $exists = Roaming_user::where([
                            'latitude'  => $request->get('lat'),
                            'longitude' => $request->get('lng'),
                            'user_id'   => $user->id
                        ])->exists();

            if(!$exists) {

                Roaming_user::create([
                    'latitude'  => $request->get('lat'),
                    'longitude' => $request->get('lng'),
                    'user_id'   => $user->id
                ]); 

            }
            

        // check if address return coordinates
        } elseif(!empty($geoCode['latitude']) && !empty($geoCode['longitude'])) {

            Roaming_user::create([
                'latitude'  => $geoCode['latitude'],
                'longitude' => $geoCode['longitude'],
                'user_id'   => $user->id
            ]);

        }

        // if original address did not yield coordinates and roaming coordinates were given
        // update user coordinates with roaming coordinates
        if( $roamingCoordinatesGiven && (empty($input['latitude']) && empty($input['longitude'])) ) {

            $input['latitude'] = $request->get('lat');

            $input['longitude'] = $request->get('lng');
        } 

        // Avatar
        if(isset($input['avatar_url'])) {

            $input['avatar'] = $input['avatar_url'];
        
        }
        if($request->hasFile('avatar')) {

            $input['avatar'] = $this->imageService->upload( $request->file('avatar')->getRealPath() );
        }  

        // Logo
        if(isset($input['logo_url'])) {

            $input['logo'] = $input['logo_url'];

        }
        if($request->hasFile('logo')) {

            $input['logo'] = $this->imageService->upload( $request->file('logo')->getRealPath() );
            
        }

        if(isset($input['business_category'])) {

            $input['business_category'] = $this->tagService->create($input['business_category']);

        }
        
        $input['permalink'] = $sanitizeUrl->makePrettyUrl($user);

        $user->update($input);

        // Handler removing avatar
        if(isset($input['remove_avatar']) && $input['remove_avatar'] == true) 
                $user->update([
                        'avatar' => getenv('DEFAULT_AVATAR')
                    ]);
        

        if(isset($input['remove_logo']) && $input['remove_logo'] == true) 
                $user->update([
                        'logo' => ""
                    ]); 

        //Tags
        if(isset($input['tags'])) {

            $user->tags()->detach();
            
            $user->tags()->attach($this->tagService->getTagIds( $input['tags']) );

        }

        if(isset($input['device_token']))

                $user->devices()->save(new Device(["token" => $input['device_token']]));


        // Algolia
        event(new UserUpdated($user));

        return $this->respond([
                'message' => 'User Updated'
            ]);

    }

    /**
     * Update user email 
     * 
     * @param  Request $request 
     * @return Response
     */
    public function updateEmail(Request $request)
    {
        $this->validate($request, [
              'email' => 'required|email|unique:users,email,' . Auth::id()
          ]);

        $input = $request->input();

        $user = Auth::user();

        $user->email = $input['email'];

        $user->save();

        return $this->respond('Email updated');
    }

    /**
     * Update users Password
     * 
     * @param  Request $request 
     * @return Response
     */
    public function updatePassword(Request $request)
    {
        $this->validate($request, [
                'password' => 'min:6,confirmed'
            ]);

        $input = $request->input();

        $user = Auth::user();

        $user->password = bcrypt($input['password']);

        $user->save();

        return $this->respond('Password updated');
    }

    /**
     * [userAvatar description]
     * @param  Request $request      [description]
     * @param  Image   $imageService [description]
     * @return [type]                [description]
     */
    public function userAvatar(Request $request, Image $imageService)
    {
        $this->validate($request, [
                'avatar' => 'required|image|max:20000'
            ]);

        Log::info('USER AVATAR UPDATE ROUTE', $request->all());

        $user = Auth::user();

        $input = $request->input();

        if($request->hasFile('avatar')) {

            $input['avatar'] = $imageService->upload( $request->file('avatar')->getRealPath() );
        }  

        if($request->hasFile('tmp_name')) {

            $input['avatar'] = $imageService->upload( $request->file('avatar')->getRealPath() );
        }  

        $user->update($input);

        return $this->respond('Avatar added');
    }

    /**
     * [userLogo description]
     * @param  Request $request      [description]
     * @param  Image   $imageService [description]
     * @return [type]                [description]
     */
    public function userLogo(Request $request, Image $imageService)
    {
        $this->validate($request, [
                'logo' => 'required|image|max:20000'
            ]);

        Log::info('USER LOGO UPDATE ROUTE', $request->all());

        $user = Auth::user();

        $input = $request->input();

        if($request->hasFile('logo')) {

            $input['logo'] = $imageService->upload( $request->file('logo')->getRealPath() );
        } 

        if($request->hasFile('tmp_name')) {

            $input['logo'] = $imageService->upload( $request->file('logo')->getRealPath() );
        }  

        $user->update($input);

        return $this->respond('Logo added');
    }

    /**
     * Add to user devices
     *     
     * @param  Request $request [description]
     * @return JSON           [description]
     */
    public function userDevice(Request $request)
    {
        $this->validate($request, [
            'device_token' => 'required',
            'token'         => 'required'
        ]); 

        $exists = Device::where([
                    'device_token' => $request->get('device_token'),
                    'user_id' => Auth::id()
                    ])->exists();

        if($exists) return $this->respond('Device already exists');

        $device = new Device($request->input());
                
        Auth::user()->devices()->save($device);

        return $this->respondCreated("Device Token added", []);
    }

    /**
     * Update user timezone
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateTimezone(Request $request)
    {
        $input = $request->input();

        if(!empty($input['timezone'])) {

            if (!in_array($input['timezone'], \DateTimeZone::listIdentifiers())) {

                return $this->respondValidationError("Timezone given is not the right format. See: http://php.net/manual/en/timezones.php");
            }

            Auth::user()->update(['timezone' => $input['timezone']]);

            return $this->respond("User timezone updated");
        }
    }

    /**
     * Update reminder Notification time
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateReminderNotificationTime(Request $request)
    {
        $this->validate($request, ['reminder_notification_time' => 'required|date_format:H:i:s']);

        Auth::user()->update(['reminder_notification_time' => $request->get('reminder_notification_time')]);

        return $this->respond("Reminder notification time updated");

    }

    /**
     * Soft delete logged in user
     *
     * @return Response
     */
    public function deleteUser()
    {

        $user = Auth::user();

        try {

            $this->search->index($this->indices)->delete($user->id);

            $user->delete();

            return $this->respond(['message' => 'User deleted']);

        } catch(Exception $e) {

            return $this->respondInternalError('Could not delete user'); 
        }
        
    }

    /**
     * Serch users
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function search(Request $request)
    {

        $pre_range = 4000;

        $range = (int) $pre_range * 1000;

        $args = [
            "hitsPerPage" => $request->get('hitsPerPage') ?: 20, 
            "page" => $request->get('page') ?: 0,
            "getRankingInfo" => 1
        ];

        Log::info('search:user:start', ['query'     => $request->get('query'), 
                                            'latitude'  => $request->get('lat'),
                                            'longitude' => $request->get('lng'),
                                            'geo'       => $request->get('geo'),
                                            'user_id'   => Auth::id()
                                            ]);

        if($request->get('geo')) {

            if(Auth::user()) {

                $user = Auth::user();

                if(!$user->latitude || !$user->longitude) 

                    return $this->respondValidationError('User does not have coordinates attached to their profile to perform a Geo search');
                
                $args["aroundLatLng"] = $user->latitude . "," . $user->longitude;

                $args["aroundRadius"] = $range;

                //$args["getRankingInfo"] = 1;

            } // Auth::check

        }

        if($request->get('lat') && $request->get('lng')) {

            if(Auth::user()) {

                $exists = Roaming_user::where([
                            'latitude'  => $request->get('lat'),
                            'longitude' => $request->get('lng'),
                            'user_id'   => Auth::id()
                        ])->exists();

                if(!$exists) {
                    
                    Roaming_user::create([
                            'latitude'  => $request->get('lat'),
                            'longitude' => $request->get('lng'),
                            'user_id'   => Auth::id()
                        ]);

                    event(new UserUpdated(Auth::user()));
                }

            } // Authenticated user
            
            $args["aroundLatLng"] = $request->get('lat') . "," . $request->get('lng');

            $args["aroundRadius"] = $range;

            //$args["getRankingInfo"] = 1;
        
        } // EO lat/lng

        $result = $this->search->index($this->indices)->search($request->get('query'), $args);

        return $this->searchTransformer->transformCollection($result);

    }

    /**
     * Serch users
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function searchPublic(Request $request)
    {

        $pre_range = 100000;

        $range = (int) $pre_range * 1000;

        $args = [
            "hitsPerPage" => $request->get('hitsPerPage') ?: 20, 
            "page" => $request->get('page') ?: 0
        ];

        if($request->get('lat') && $request->get('lng')) {

            $args["aroundLatLng"] = $request->get('lat') . "," . $request->get('lng');

            $args["aroundRadius"] = $range;

            $args["getRankingInfo"] = 1;
        
        } // EO lat/lng

        $result = $this->search->index($this->indices)->search($request->get('query'), $args);

        return $this->searchTransformer->transformCollection($result);

    }

    /**
     * Add roamin geocode
     * 
     * @param Request $request [description]
     */
    public function addRoamingGeoCode(Request $request)
    {
        $this->validate($request, [
            'latitude' => 'required',
            'longitude' => 'required'
        ]);

        $input = $request->input();

        $exists = Roaming_user::where([
                        'latitude' => $input['latitude'],
                        'longitude' => $input['longitude'],
                        'user_id' => Auth::id()
                    ])->exists();

        if($exists)

            return $this->respond("OK");

        Roaming_user::create([
                'latitude' => $input['latitude'],
                'longitude' => $input['longitude'],
                'user_id' => Auth::id()
            ]);

        event(new UserUpdated(Auth::user()));

        return $this->respondCreated('Roaming data added');
    }


    /**
     *
     *      ADMIN SECTION
     * 
     */
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        //$users = User::withTrashed()->get();

        $users = DB::select("SELECT 
                            u.`id`, 
                            u.`first_name`, 
                            u.`last_name`, 
                            u.`business_name`, 
                            u.`created_at`,
                            u.`deleted_at`,
                            u.`email`,
                            u.`job_title`,
                            u.`street`,
                            u.`city`,
                            u.`state`,
                            u.`zip_code`,
                            u.`business_category`,
                            u.`avatar`,
                            u.`logo`,
                            u.`about_me`,
                            u.`about_business`,
                            u.`website_url`,
                            u.`facebook_url`,
                            u.`twitter_url`,
                            u.`googleplus_url`,
                            u.`linkedin_url`,
                            u.`youtube_url`,
                            u.`blog_url`,
                            u.`timezone`,
                            u.`reminder_notification_time`,
                            u.`is_placeholder`,
                            (SELECT 1 FROM `placeholder_users` p WHERE p.`user_id` = u.`id` GROUP BY p.`user_id`) as 'placeholder_user_exists',
                            COUNT(t.`id`) as 'tags'
                            FROM `users` u
                            LEFT JOIN
                            `tag_user` t ON
                            t.`user_id` = u.`id`
                            GROUP BY u.`id`", []);

        return $this->respond($users);
    }


    /**
     * Store a newly created resource in storage
     *
     * Admin only method
     *
     * @return Response
     */
    public function store(CreateUserRequest $request, SanitizeUrlService $sanitizeUrl)
    {
        
        try {

            $input = $request->input();

            $user = new User;

            $user->password = bcrypt($input['password']);

            $user->email = $input['email'];

            $user->first_name = $input['first_name'];

            $user->last_name = $input['last_name'];

            $user->save();

            $user->update($input);                    

            if(isset($input['tags']))
                $user->tags()->attach($this->tagService->getTagIds( $input['tags']) );            

            $roamingCoordinatesGiven = (isset($input['lat']) && isset($input['lng']));

            // Geo
            $geoCode = $this->geoService
                                ->getLatLang($input['street'], $input['city'], $input['state'], $request->get('zip_code'), $request->get('country'));

            $input['latitude']  = $geoCode['latitude'];
            
            $input['longitude'] = $geoCode['longitude'];

            // check if roaming coordinates were sent
            if($roamingCoordinatesGiven)

                Roaming_user::create([
                    'latitude'  => $request->get('lat'),
                    'longitude' => $request->get('lng'),
                    'user_id'   => $user->id
                ]);

            // if original address did not yield coordinates and roaming coordinates were given
            // update user coordinates with roaming coordinates
            if( $roamingCoordinatesGiven && (empty($input['latitude']) && empty($input['longitude'])) ) {

                $input['latitude']  = $request->get('lat');

                $input['longitude'] = $request->get('lng');
            } 

            // Avatar
            if(isset($input['avatar_url'])) {

                $input['avatar'] = $input['avatar_url'];
            
            }
            if($request->hasFile('avatar')) {

                $input['avatar'] = $this->imageService->upload( $request->file('avatar')->getRealPath() );
            }  

            // Logo
            if(isset($input['logo_url'])) {

                $input['logo'] = $input['logo_url'];

            }
            if($request->hasFile('logo')) {

                $input['logo'] = $this->imageService->upload( $request->file('logo')->getRealPath() );
                
            }

            $input['permalink'] = $sanitizeUrl->makePrettyUrl($user);

            if(isset($input['business_category'])) {

                $input['business_category'] = $this->tagService->create($input['business_category']);

            }

            $user->update($input);

            event(new NewUserJoined($user));

            return $this->respondCreated('User Created', [
                    'user' => $user
                ]);

        } catch(Exception $e) {

            return $this->respondInternalError('Could not store user');

        }
        
    } 

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($user, Request $request)
    {

        $filter = $request->get('filter') ?: null;

        try {

            $user = User::withTrashed()->findOrFail($user);

            switch ($filter) {
                case null:
                    $result['user'] = $user;
                    $result['allTags'] = Tag::all();
                    $result['tags'] = $user->tags()->latest()->get();
                    break;
                case 'articles':
                    $result['articles'] = $user->articles()->latest()->get();
                    break;
                case 'my_events':
                    $result['my_events'] = $user->my_events()->orderBy('event_date', 'DESC')->get();
                    break;
                case 'events':
                    $result['events'] = $user->events()->orderBy('event_date', 'DESC')->get();
                    break;
                case 'messages':
                    $result['messages'] = $user->messages()->with(['from'  => function($q) {
                                                    return $q->withTrashed();
                                                }])->latest()->get();
                    break;
                case 'sent_messages':
                    $result['sent_messages'] = $user->sent_messages()->with(['to'  => function($q) {
                                                    return $q->withTrashed();
                                                }])->latest()->get();
                    break;
                case 'deleted_messages':
                    $result['deleted_messages'] = $user->deleted_messages()->latest()->get();
                    break;
                case 'testimonials':
                    $result['testimonials'] = $user->testimonials()->with(['author'  => function($q) {
                                                    return $q->withTrashed();
                                                }])->latest()->get();
                    break;
                case 'sent_testimonials':
                    $result['sent_testimonials'] = $user->sent_testimonials()->with(['subject'  => function($q) {
                                                    return $q->withTrashed();
                                                }])->latest()->get();
                    break;
                case 'network_following':
                    $result['network_following'] = $user->network_following()->groupBy('subject_id')
                                                ->with(['subject' => function($q) {
                                                    return $q->withTrashed();
                                                }])->latest()->get();
                    break;
                case 'network_subject':
                    $result['network_subject'] = $user->network_subject()->with(['follower' => function($q) {
                                                    return $q->withTrashed();
                                                }])->latest()->get();
                    break;
                case 'roaming_coordinates':
                    $result['roaming_coordinates'] = $user->roaming_coordinates()->latest()->get();
                    break;
                case 'sent_card_shares':
                    $result['sent_card_shares'] = $user->sent_card_shares()->with(['recipient' => function($q) {
                                                    return $q->withTrashed();
                                                }])->with(['subject' => function($q) {
                                                    return $q->withTrashed();
                                                }])->latest()->get();
                    break;
                case 'received_card_shares':
                    $result['received_card_shares'] = $user->received_card_shares()->with(['sender' => function($q) {
                                                    return $q->withTrashed();
                                                }])->with(['subject' => function($q) {
                                                    return $q->withTrashed();
                                                }])->latest()->get();
                    break;
                case 'subject_card_shares':
                    $result['subject_card_shares'] = $user->subject_card_shares()->with(['sender' => function($q) {
                                                    return $q->withTrashed();
                                                }])->with(['recipient' => function($q) {
                                                    return $q->withTrashed();
                                                }])->latest()->get();
                    break;
                case 'unsubbed_mail':
                    $result['unsubbed_mail'] = $user->unsubbed_mail()->latest()->get();
                    break;
                case 'devices':
                    $result['devices'] = $user->devices()->latest()->get();
                    break;
                case 'notes':
                    $result['notes'] = $user->notes()->with(['subject' => function($q) {
                                                    return $q->withTrashed();
                                                }])->latest()->get();
                    break;
                default:
                    $result['user'] = $user;
                    $result['allTags'] = Tag::all();
                    $result['tags'] = $user->tags()->latest()->get();
                    break;
            }

            return $this->respond($result);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("User not found");

        } catch(\Exception $e) {

            return $this->respondInternalError($e->getMessage());

        }
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(AdminUpdateUserRequest $request, $id, SanitizeUrlService $sanitizeUrl)
    {
        $user = User::findOrFail($id);

        $input = $request->input();

        $roamingCoordinatesGiven = (isset($input['lat']) && isset($input['lng']));

        // Password
        if(!empty($input['password'])) {

            $user->password = bcrypt($input['password']);

            $user->save();            
        }

        //email
        if(!empty($input['email'])) {

            $user->email = $input['email'];

            $user->save();

        }

        $address_exists = (!empty($input['street']) && !empty($input['city']) && !empty($input['state']));

        if($address_exists) {

            // Geo
            $geoCode = $this->geoService
                                ->getLatLang($input['street'], $input['city'], $input['state'], $request->get('zip_code'), $request->get('country'));

            $input['latitude']  = $geoCode['latitude'];
            
            $input['longitude'] = $geoCode['longitude'];

        } // check if address exists

        

        // check if roaming coordinates were sent
        if($roamingCoordinatesGiven) {

            $exists = Roaming_user::where([
                            'latitude'  => $request->get('lat'),
                            'longitude' => $request->get('lng'),
                            'user_id'   => $user->id
                        ])->exists();

            if(!$exists) {
                
                Roaming_user::create([
                        'latitude'  => $request->get('lat'),
                        'longitude' => $request->get('lng'),
                        'user_id'   => $user->id
                    ]);

            }
        }

            

        // if original address did not yield coordinates and roaming coordinates were given
        // update user coordinates with roaming coordinates
        if( $roamingCoordinatesGiven && (empty($input['latitude']) && empty($input['longitude'])) ) {

            $input['latitude'] = $request->get('lat');

            $input['longitude'] = $request->get('lng');
        } 

        // Avatar
        if(isset($input['avatar_url']) && !empty($input['avatar_url'])) {

            $input['avatar'] = $input['avatar_url'];
        
        }
        if($request->hasFile('avatar')) {

            $input['avatar'] = $this->imageService->upload( $request->file('avatar')->getRealPath() );
        }  

        // Logo
        if(isset($input['logo_url']) && !empty($input['logo_url'])) {

            $input['logo'] = $input['logo_url'];

        }
        if($request->hasFile('logo')) {

            $input['logo'] = $this->imageService->upload( $request->file('logo')->getRealPath() );
            
        }

        if(isset($input['business_category'])) {

            $input['business_category'] = $this->tagService->create($input['business_category']);

        }


        $input['permalink'] = $sanitizeUrl->makePrettyUrl($user);

        $user->update($input);

        // Handler removing avatar
        if(isset($input['remove_avatar']) && $input['remove_avatar'] == true) 
                $user->update([
                        'avatar' => getenv('DEFAULT_AVATAR')
                    ]);
        

        if(isset($input['remove_logo']) && $input['remove_logo'] == true) 
                $user->update([
                        'logo' => ""
                    ]);

        //Tags
        if(isset($input['tags'])) {

            $user->tags()->detach();
            
            $user->tags()->attach($this->tagService->getTagIds( $input['tags']) );

        }

        if(isset($input['device_token']))

                $user->devices()->save(new Device(["token" => $input['device_token']]));

        // Algolia
        event(new UserUpdated($user));

        return $this->respond([
                'message' => 'User Updated',
                'user'  => $user
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($user)
    {
        $user = User::findOrFail($user);

        event(new UserDeleted($user));

        $user->delete();

        return $this->respond("User Soft Deleted");
    }

    /**
     * Restore a deleted user
     * @param  [type] $user [description]
     * @return [type]       [description]
     */
    public function restore($user)
    {
        $user = User::withTrashed()->findOrFail($user);

        $user->restore();

        event(new NewUserJoined($user));

        return $this->respond("User restored");
    }

    public function getActivity()
    {

        $users = DB::table('users')
                        ->select('id', 'first_name', 'last_name', 'email', 'last_activity')
                        ->get();

        foreach($users as $user) {

            $user->activity = DB::table('activity_log')
                                    ->where('user_id', $user->id)
                                    ->get();
        }

        return $this->respond([
                'users' => $users //User::with('activity')->get()
            ]);
    }

    public function dataTables()
    {
        return Datatables::of(User::query())->make(true);
    }

}
