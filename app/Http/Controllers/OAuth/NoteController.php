<?php

namespace App\Http\Controllers\OAuth;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Note;
use App\Http\Requests;
use App\Http\Requests\NoteRequest;
use App\Http\Controllers\Controller;
use App\Services\NotificationService;
use App\Services\NoteReminderService;
use App\Transformers\NoteTransformer;
use App\Http\Controllers\ApiController;
use App\Http\Requests\UpdateNoteRequest;

class NoteController extends ApiController
{

    protected $transformer;

    protected $noteReminderService;

    protected $notificationService;

    public function __construct(NoteTransformer $transformer, NoteReminderService $noteReminderService, NotificationService $notificationService)
    {
        //$this->middleware('jwt.auth');

        $this->middleware('note.owner', ['only' => ['updateNote', 'deleteNote', 'userNote']]);

        $this->transformer = $transformer;

        $this->noteReminderService = $noteReminderService;

        $this->notificationService = $notificationService;
    }

    public function createNote(NoteRequest $request)
    {

        try {

            $input = $request->input();

            $note = new Note([
                    'title' => $input['title'],
                    'body' => $input['body'],
                    'subject_id' => $input['subject_id'],
                    'remind_place' => !empty($input['remind_place']) ? $input['remind_place'] : ""

                ]); 

            if(!empty($input['remind_at'])) {

                $date = $this->noteReminderService->normalizeTimezone($input['remind_at'], Auth::user()->timezone);

                $note->remind_at = $date;
            }

            Auth::user()->notes()
                        ->save($note);

            return $this->respondCreated('Note Created', [
                    'note' => $note
                ]);

        } catch(\Exception $e) {

            return $this->respondInternalError($e->getMessage());

        }

        

    }


    public function userNotes()
    {
        $notes = User::findOrFail(Auth::id())->notes->toArray();

        return $this->respond(
                
                    $this->transformer->transformCollection($notes)

                );

    }

    public function userNote($id)
    {
        $note = Note::findOrFail($id)->toArray();

        if(!empty($input['remind_at'])) {

            $date = $this->noteReminderService->normalizeTimezone($input['remind_at'], Auth::user()->timezone);

            $note->remind_at = $date;

            $note->sent = false;

            $note->update();
        }

        $note->update([
                'title' => $input['title'],
                'body' => $input['body'],
                'subject_id' => $input['subject_id'],
                'remind_place' => !empty($input['remind_place']) ? $input['remind_place'] : ""
            ]);

        return $this->respond($note);
    }

    public function updateNote($id, UpdateNoteRequest $request)
    {

        try {

            $input = $request->input();

            $note = Note::findOrFail($id);
            //->update($input);


            return $this->respond([
                    'message' => 'Note updated'
                ]);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

        } catch(\Exception $e) {

        }
        

    }

    public function deleteNote($id)
    {
        Note::findOrFail($id)->delete();

        return $this->respond([
                'message' => 'Note deleted'
            ]);
    }

    public function getSubjectsNotes($id)
    {
        $notes = User::find(Auth::id())
                        ->notes()
                        ->where('subject_id', $id)
                        ->get()
                        ->toArray();

        return $this->respond($this->transformer->transformCollection($notes));

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $note = Note::with(['user' => function($q) { $q->withTrashed(); }])
                        ->with(['subject' => function($q) { $q->withTrashed(); }])
                        ->get();

        return $this->respond($note);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'author_id'     => 'required|exists:users,id',
                'subject_id'    => 'required|exists:users,id',
                'title'         => 'required',
                'remind_at'     => 'date_format:"Y-m-d H:i:s"',
                'body'          => 'required'
            ]);

        $note = Note::create($request->input());

        return $this->respondCreated('Note added!', $note);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $note = Note::findOrFail($id);

        $result = [
            'note'      => $note,

            'author'    => $note->user()->withTrashed()->get(),

            'subject'   => $note->subject()->withTrashed()->get(),

            'allUsers'  => User::all()
        ];

        return $this->respond($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
                'author_id'     => 'required|exists:users,id',
                'subject_id'    => 'required|exists:users,id',
                'body'          => 'required',
                'title'         => 'required',
                'remind_at'     => 'date_format:"Y-m-d H:i:s"'
            ]);

        $note = Note::findOrFail($id);

        $input = $request->input();

        $note->update($input);

        return $this->respond('Note updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $note = Note::findOrFail($id);

        $note->delete();

        return $this->respond('Note deleted');
    }

    /**
     * Mark a note as seen
     *     
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function markAsSeen($id)
    {

        try {
            
            $note = Note::findOrFail($id);

            $this->notificationService->markAsSeen($id, 'NOTE_REMINDER');   

            return $this->respond([
                    "message" => "Note reminder updated"
                ]);

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            
            return $this->respondNotFound($e->getMessage());

        } catch(\Exception $e) {

            return $this->respondInternalError($e->getMessage());

        }
    }
}
