<?php

namespace App\Http\Controllers\OAuth;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Tag;
use App\Article;
use App\Http\Requests;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use AlgoliaSearch\Client;
use App\Services\TagService;
use App\Contracts\Search;
use App\Events\ArticleCreated;
use App\Events\ArticleUpdated;
use App\Events\ArticleDeleted;
use App\Services\SanitizeUrlService;
use App\Transformers\ArticleTransformer;

class ArticleController extends ApiController
{

    protected $indices;

    protected $algolia;

    protected $tagService;

    protected $search;

    protected $sanitizeUrl;

    protected $transformer;

    public function __construct(TagService $tagService, Search $search, SanitizeUrlService $sanitizeUrl, ArticleTransformer $transformer)
    {
        // $this->middleware('jwt.auth', ['except' => ['search', 'getArticle', 'getByPermalink']]);

        $this->middleware('article.owner', ['only' => ['updateArticle', 'deleteArticle']]);

        $this->algolia = new Client(getenv('ALGOLIA_APP_ID'), getenv('ALGOLIA_SECRET_KEY'));

        $this->tagService = $tagService;

        $this->indices = getenv('ALGOLIA_ARTICLES_INDEX');

        $this->search = $search;

        $this->sanitizeUrl = $sanitizeUrl;

        $this->transformer = $transformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getArticles(Request $request)
    {

        $limit = $request->get('limit') ?: 20;

        $page = $request->get('page') ?: 0;

        $articles = Article::with('user')
                                ->with(['tags' => function ($query) use ($limit, $page) {
                               $query->skip($limit * ($page-1))
                                    ->take($limit);
                                }])->paginate($limit);

        return $this->respond($articles);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getArticle($id)
    {
        $article = Article::find($id);

        if(!$article) return $this->respondNotFound('Article not found');

        $article->tags = $article->tags()->get();

        $article->author = $article->user()->get();
        
        return $this->respond(['data' => $article]);
    }

    /**
     * Get article by permalink
     * @param  string $permalink permalink of article
     * @return App\Article
     */
    public function getByPermalink($permalink)
    {
        $article = Article::where('permalink', $permalink)->first();

        return $this->respond(['data' => $article]);
    }

    public function createArticle(ArticleRequest $request)
    {
        try {

            $input = $request->input();
            
            $input['user_id'] = Auth::user()->id;

            $article = Article::create($input);

            $article->tags()->attach($this->tagService->getTagIds($input['tags']));

            $article->update([
                'permalink' => $this->sanitizeUrl->makePrettyUrl($article)
            ]);

            event(new ArticleCreated($article));

            return $this->respondCreated('Article Created', [
                    'article' => $article
                ]);

        } catch(Exception $e) {

            return $this->respondInternalError('Could not create article');

        }
    }

    public function updateArticle($id, ArticleRequest $request)
    {
        try {

            $article = Article::findOrFail($id);

            $input = $request->input();

            $article->update($input);

            $article->tags()->detach();

            $article->tags()->attach($this->tagService->getTagIds($input['tags']));

            $article->update([
                    'permalink' => $this->sanitizeUrl->makePrettyUrl($article)
                ]);

            event(new ArticleUpdated($article));

            return $this->respond([
                    'message' => 'Article updated'
                ]);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("Article not found");

        } catch(\Exception $e) {

            return $this->respondInternalError("There was an internal error");
        }

    }

    public function deleteArticle($id)
    {

        try {

            $article = Article::findOrFail($id);

            event(new ArticleDeleted($article));

            $article->delete();

            return $this->respond([
                    'message' => 'Article deleted'
                ]);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("Article not found");

        } catch(\Exception $e) {

            return $this->respondInternalError("There was an internal error");
        }
        
    }

    public function userArticles($id, Request $request)
    {

        $limit = $request->get('limit') ?: 20;

        $page = $request->get('page') ?: 0;

        $userArticles = User::findOrFail($id)->articles()->with(['tags' => function ($query) use ($limit, $page) {
                               $query->skip($limit * ($page-1))
                                    ->take($limit);
                                }])->paginate($limit);

        

        return $this->respond($userArticles);
    }

    public function myArticles(Request $request)
    {
        $limit = $request->get('limit') ?: 20;

        $page = $request->get('page') ?: 0;

        $userId = Auth::id();

        $userArticles = User::findOrFail($userId)->articles()->with(['tags' => function ($query) use ($limit, $page) {
                               $query->skip($limit * ($page-1))
                                    ->take($limit);
                                }])->paginate($limit);

        return $this->respond($userArticles);
    }

    /**
     * Search articles algolia
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function search(Request $request)
    {
        $args["hitsPerPage"] = $request->get('hitsPerPage') ?: 20;

        $args["page"] = $request->get('page') ?: 0;

        $results = $this->search->index($this->indices)->search($request->get('query'), $args); 

        $results['hits'] = $this->transformer->transformCollection($results['hits']);

        return $results;
    }

    /**
     * 
     *      ADMIN SECTION
     *      
     */
    
    /**
     * Get all articles
     * @return App\Article 
     */
    public function index()
    {
        $articles = Article::with(['user' => function($q) { $q->withTrashed(); }])->get();

        return $this->respond($articles);
    }

    /**
     * Get article by id
     * @param  int $id id of article
     * @return App\Article   
     */
    public function show($id)
    {
        try {

            $article = Article::findOrFail($id);

            $result = [

                'article'   => $article,
                'owner'     => $article->user()->get(),
                'tags'      => $article->tags()->get(),
                'allTags'   => Tag::all(),
                'allUsers'  => User::all()

            ];

            return $this->respond($result);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("Article not found");

        } catch(\Exception $e) {

            return $this->respondInternalError("There was an internal error");
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'title'     => 'required',
            'body'      => 'required',
            'teaser'    => 'required',
            'tags'      => 'required|array',
            'user_id'   => 'required|exists:users,id'
        ]); 

        $input = $request->input();

        $article = Article::create($input);

        $article->tags()->attach($this->tagService->getTagIds($input['tags']));

        $article->update([
                'permalink' => $this->sanitizeUrl->makePrettyUrl($article)
            ]);

        event(new ArticleCreated($article));

        return $this->respondCreated('Article Created', [
                    'article' => $article
                ]);
        
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title'     => 'required',
            'body'      => 'required',
            'teaser'    => 'required',
            'tags'      => 'required',
            'user_id'   => 'required|exists:users,id'
        ]); 

        $input = $request->input();

        try {

            $article = Article::findOrFail($id);

            $input['permalink'] =  $this->sanitizeUrl->makePrettyUrl($article);

            $article->update($input);

            $article->tags()->detach();

            $article->tags()->attach($this->tagService->getTagIds($input['tags']));

            event(new ArticleUpdated($article));

            return $this->respond('Article updated');

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("Article not found");

        } catch(\Exception $e) {

            return $this->respondInternalError("There was an internal error");
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

        try {

            $article = Article::findOrFail($id);

            event(new ArticleDeleted($article));
            
            $article->delete();

            return $this->respond('Article Deleted');

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("Article not found");

        } catch(\Exception $e) {

            return $this->respondInternalError("There was an internal error");
        }

    }
}
