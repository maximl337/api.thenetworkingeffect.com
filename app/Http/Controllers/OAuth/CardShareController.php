<?php

namespace App\Http\Controllers\OAuth;

use Illuminate\Http\Request;

use Auth;
use App\User;
use Validator;
use App\Http\Requests;
use App\Card_shares as CardShare;
use App\Http\Requests\CreateCardShareRequest;
use App\Http\Requests\ShareCardByEmailsRequest;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Events\NewCardShare;
use App\Events\ReferUserEvent;

class CardShareController extends ApiController
{

    public function __construct()
    {
        // $this->middleware('jwt.auth');
    }

    /**
     * [shareCardByIds description]
     * @param  CreateCardShareRequest $request [description]
     * @return [type]                          [description]
     */
    public function shareCardByIds(CreateCardShareRequest $request)
    {

        $user = User::findOrFail(Auth::user()->id);

        $input = $request->input();

        for($i=0;$i<count($input['recipient_ids']);$i++) {

            $cardShare = new CardShare([
                    'subject_id' => isset($input['subject_id']) ? $input['subject_id'] : $user->id,
                    'recipient_id' => $input['recipient_ids'][$i]                    
                ]);    

            $user->sent_card_shares()->save($cardShare);

            event(new NewCardShare($cardShare));
            
        }

        return $this->respondCreated('Shared card with ' . $i . ' people');
        
    }

    /**
     * [shareCardByEmails description]
     * @param  ShareCardByEmailsRequest $request [description]
     * @return [type]                            [description]
     */
    public function shareCardByEmails(ShareCardByEmailsRequest $request)
    {
        $user = Auth::user();

        $input = $request->input();

        $message = [];
        
        for($i=0;$i<count($input['recipient_emails']);$i++) {

            $emailValidator = Validator::make([
                    'email' => $input['recipient_emails'][$i]
                ], [
                    'email' => 'email'
                ]);

            if($emailValidator->fails()) {

                $message[] = $input['recipient_emails'][$i] . ' is not a valid email';
                
                continue;
            }

            $cardShare = new CardShare([
                    'subject_id' => isset($input['subject_id']) ? $input['subject_id'] : $user->id,
                    'recipient_email' => $input['recipient_emails'][$i]
                ]);    

            $user->sent_card_shares()->save($cardShare);

            $message[] = 'Card Shared with ' . $input['recipient_emails'][$i];

            event(new NewCardShare($cardShare));
            
        }

        return $this->respondCreated($message);
    }

    /**
     * [referUserById description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function referUserToIds(Request $request)
    {

        $this->validate($request, [
                'subject_id' => 'required|exists:users,id',
                'recipient_ids' => 'required|array'
            ]);

        $user = Auth::user();

        $input = $request->input();

        for($i=0;$i<count($input['recipient_ids']);$i++) {

            $idValidator = Validator::make([
                    'recipient_id' => $input['recipient_ids'][$i]
                ], [
                    'recipient_id' => 'exists:users,id'
                ]);


            if($idValidator->fails()) {

                $message[] = $input['recipient_ids'][$i] . ' does not exist';
                
                continue;
            }

            $cardShare = new CardShare([
                    'subject_id'    => $input['subject_id'],
                    'recipient_id'  => $input['recipient_ids'][$i],
                    'message'       => !empty($input['message']) ? $input['message'] : ""
                ]);    

            $user->sent_card_shares()->save($cardShare);

            // event
            event(new ReferUserEvent($cardShare));

            $message[] = 'Card refered to ' . $input['recipient_ids'][$i];
            
        }

        return $this->respondCreated($message);

    }

    /**
     * [referUserToEmails description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function referUserToEmails(Request $request)
    {
        
        $this->validate($request, [
                'subject_id' => 'required|exists:users,id',
                'recipient_emails' => 'required|array'
            ]);

        $user = Auth::user();

        $input = $request->input();

        for($i=0;$i<count($input['recipient_emails']);$i++) {

            $emailValidator = Validator::make([
                    'recipient_email' => $input['recipient_emails'][$i]
                ], [
                    'recipient_email' => 'email'
                ]);


            if($emailValidator->fails()) {

                $message[] = $input['recipient_emails'][$i] . ' is not a valid email';
                
                continue;
            }

            $cardShare = new CardShare([
                    'subject_id'        => $input['subject_id'],
                    'recipient_email'   => $input['recipient_emails'][$i],
                    'message'           => !empty($input['message']) ? $input['message'] : ""
                ]);

            $user->sent_card_shares()->save($cardShare);

            event(new ReferUserEvent($cardShare));

            $message[] = 'Card refered to ' . $input['recipient_emails'][$i];
        }

        return $this->respondCreated($message);
    }

    /**
     * [sentCardShares description]
     * @return [type] [description]
     */
    public function sentCardShares()
    {
        $user = User::findOrFail(Auth::user()->id);

        $sent_card_shares = $user->sent_card_shares->toArray();

        return $this->respond($sent_card_shares);
    }

    /**
     * [receivedCardShares description]
     * @return [type] [description]
     */
    public function receivedCardShares()
    {
        $user = User::findOrFail(Auth::user()->id);

        $received_card_shares = $user->received_card_shares->toArray();

        return $this->respond($received_card_shares);
    }

    /**
     *
     *      ADMIN SECTION
     *      
     */
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $cardshares = CardShare::with(['sender' => function($q) { $q->withTrashed(); }])
                                    ->with(['recipient' => function($q) { $q->withTrashed(); }])
                                    ->with(['subject' => function($q) { $q->withTrashed(); }])
                                    ->get();

        return $this->respond($cardshares);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'sender_id'             => 'required|exists:users,id',
            'recipient_emails'      => 'array|required_without:recipient_ids',
            'recipient_ids'         => 'array|required_without:recipient_emails'
        ]); 

        $input = $request->input();

        $user = User::find($input['sender_id']);

        $message = [];

        if(isset($input['recipient_ids']) && !empty($input['recipient_ids']) ) {

            for($i=0;$i<count($input['recipient_ids']);$i++) {

                $cardShare = new CardShare([
                        'subject_id' => isset($input['subject_id']) ? $input['subject_id'] : $user->id,
                        'recipient_id' => $input['recipient_ids'][$i]                    
                    ]);    

                $user->sent_card_shares()->save($cardShare);

                $message[] = 'Card Shared with ' . $input['recipient_ids'][$i];

                event(new NewCardShare($cardShare));
                
            }

        }


        if(isset($input['recipient_emails']) && !empty($input['recipient_emails']) ) {

            for($i=0;$i<count($input['recipient_emails']);$i++) {

                $emailValidator = Validator::make([
                        'email' => $input['recipient_emails'][$i]
                    ], [
                        'email' => 'email'
                    ]);

                if($emailValidator->fails()) {

                    $message[] = $input['recipient_emails'][$i] . ' is not a valid email';
                    
                    continue;
                }

                $cardShare = new CardShare([
                        'subject_id' => isset($input['subject_id']) ? $input['subject_id'] : $user->id,
                        'recipient_email' => $input['recipient_emails'][$i]                    
                    ]);    

                $user->sent_card_shares()->save($cardShare);

                $message[] = 'Card Shared with ' . $input['recipient_emails'][$i];

                event(new NewCardShare($cardShare));
                
            }

        }

        return $this->respond('Shared Cards');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $cardshare = CardShare::findOrFail($id);

        $result = [
            'cardshare' => $cardshare,

            'recipient' =>  $cardshare->recipient()->withTrashed()->get(),

            'sender'    =>  $cardshare->sender()->withTrashed()->get(),

            'subject'   =>  $cardshare->subject()->withTrashed()->get()
        ];

        return $this->respond($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $cardshare = CardShare::findOrFail($id);

        $cardshare->delete();

        return $this->respond('Card share record deleted');
    }
}
