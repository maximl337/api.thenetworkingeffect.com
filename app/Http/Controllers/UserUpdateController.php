<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

use Auth;
use JWTAuth;
use App\Tag;
use App\User;
use App\Roaming_user;
use App\Services\TagService;
use App\Http\Controllers\Controller;
use App\Events\UserUpdatedEmailEvent;
use App\Events\UserUpdated;

class UserUpdateController extends ApiController
{

    protected $tagService;

    public function __construct(TagService $tagService)
    {
        $this->middleware('jwt.auth');
        $this->tagService = $tagService;
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function updateUser(Request $request)
    {
        $user = Auth::user();

        \Log::info("User update request", [$request->input()]);

        $this->validate($request, [
            'email'             => 'email|unique:users,email,' . $user->id,
            'password'          => 'min:3',
            'first_name'        => 'min:1',
            'last_name'         => 'min:1',
            'business_name'     => 'min:1',
            'job_title'         => 'min:1',
            'business_category' => 'min:1',
            'tags'              => 'array',
            'hide_address'      => 'boolean',
            'avatar'            => 'image|max:20000',
            'logo'              => 'image|max:20000'
        ]);

        if( ! $user ) return $this->respondNotFound('User not found');

        $input = $request->input();

        $roamingCoordinatesGiven = (isset($input['lat']) && isset($input['lng']));

        // Password
        if(isset($input['password'])) {

            $user->password = bcrypt($input['password']);
            $user->save();            
        }

        //email
        if(isset($input['email'])) {

            $data = [
                'old_email' => $user->email,
                'new_email' => $input['email'],
                'id' => $user->id
            ];

            $user->email = $input['email'];

            $user->save();

            event(new UserUpdatedEmailEvent($data));

        }

        // check if roaming coordinates were sent
        if($roamingCoordinatesGiven) {

            $exists = Roaming_user::where([
                            'latitude'  => $request->get('lat'),
                            'longitude' => $request->get('lng'),
                            'user_id'   => $user->id
                        ])->exists();

            if(!$exists) {

                Roaming_user::create([
                    'latitude'  => $request->get('lat'),
                    'longitude' => $request->get('lng'),
                    'user_id'   => $user->id
                ]); 

            }
            

        // check if address return coordinates
        }

        // if original address did not yield coordinates and roaming coordinates were given
        // update user coordinates with roaming coordinates
        if( $roamingCoordinatesGiven && (empty($input['latitude']) && empty($input['longitude'])) ) {

            $input['latitude'] = $request->get('lat');

            $input['longitude'] = $request->get('lng');
        } 

        if(isset($input['business_category'])) {
            $input['business_category'] = $this->tagService->create($input['business_category']);
        }

        $user->update($input);
        //Tags
        if(isset($input['tags'])) {

            $user->tags()->detach();
            $user->tags()->attach($this->tagService->getTagIds( $input['tags']) );

        }

        if(isset($input['device_token'])) {
            $user->devices()->save(new Device(["token" => $input['device_token']]));
        }
        // Algolia
        event(new UserUpdated($user));

        return $this->respond([
                'message' => 'User Updated'
            ]);

    }
}