<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Excel;
use Auth;
use Mail;
use Storage;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\ApiController;

class ExportController extends ApiController
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * [export description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function export(Request $request)
    {
        try {
            
            $this->validate($request, [
                    'ids' => 'required|array'
                ]);

            $ids = $request->get('ids');

            $filename = "excel_" . Auth::id() . "_" . md5(microtime());

            //$contacts_count = 0;

            // create csv
            $excel = Excel::create($filename, function($excel) use($ids) {

                $excel->sheet('Worksheet 1', function($sheet) use($ids) {

                    // add headers
                    $sheet->row(1, [
                            'Name',
                            'Email',
                            'Company',
                            'Job Title',
                            'Phone',
                            'Address'
                        ]);

                    $row = 2;

                    foreach($ids as $id) {

                        // get user
                        $user = User::find($id);

                        if(!$user) {
                            continue;
                        }

                        $phone = !empty($user->phone_1) ? $user->phone_1 : "";

                        $address = "";

                        if(!empty($user->street)) {
                            $address .= $user->street . ", ";
                        }

                        if(!empty($user->city)) {
                            $address .= $user->city . ", ";
                        }

                        if(!empty($user->state)) {
                            $address .= $user->state . ", ";
                        }

                        if(!empty($user->zip_code)) {
                            $address .= $user->zip_code . ", ";
                        }

                        if(!empty($user->country)) {
                            $address .= $user->country;
                        }

                        $sheet->row($row, [
                                $user->first_name . " " . $user->last_name,
                                $user->email,
                                $user->business_name,
                                $user->job_title,
                                $phone,
                                $address
                            ]);

                        $row++;

                        // save csv 

                        // mail with attachement

                        // delete file

                        //$contact_count++;

                    }

                });

            })->store('xlsx');

            // Storage::put(
            //             'tne/' . $filename.".xlsx",
            //             file_get_contents(storage_path('exports/' . $filename . ".xlsx"))
            //         );

            $user = Auth::user();

            Mail::send('emails.export', ['user' => $user], function ($message) use ($filename, $user) {

                $message->attach(storage_path('exports/' . $filename . ".xlsx"));

                $message->to($user->email, $user->first_name)->subject('Your Contact Export');   
            });

            //Storage::disk('local')->delete('exports/'.$filename.".xlsx");

            return $this->respond("Done");

        } catch (Exception $e) {

            Log::error("Export error", [$e->getMessage]);
            
            return $this->respondInternalError("Could not send export: " . $e->getMessage());
        }
    }
}
