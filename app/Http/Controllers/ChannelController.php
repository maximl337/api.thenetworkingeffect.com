<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;
use App\User;
use App\Channel;
use App\Http\Requests;
use App\Contracts\Search;
use App\Notification;
use App\Notification_type;
use App\Services\ImageService;
use App\Transformers\ChannelTransformer;
use App\Http\Controllers\ApiController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Events\ChannelMemberJoinEvent;

class ChannelController extends ApiController
{

    protected $algolia;
    protected $indice;
    protected $imageService;
    protected $transformer;


    public function __construct(Search $algolia, ImageService $imageService, ChannelTransformer $transformer)
    {
        $this->middleware('jwt.auth');
        $this->algolia = $algolia;
        $this->indice = env('ALGOLIA_USERS_INDEX');
        $this->imageService = $imageService;
        $this->transformer = $transformer;
    }

    /**
     * [getChannels description]
     * @return [type] [description]
     */
    public function getChannels(Request $request)
    {
        $user = $request->user();
        $channels = Channel::available($user->id)->get();
        $userChannels = $user->channels()->get();
        $mergedChannels = $channels->merge($userChannels);
        return $this->respond([
            'channels' => $this->transformer->transformCollection($mergedChannels->toArray())
        ]);
    }

    /**
     * [getRoles description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getRoles($id)
    {
        try {
            $channel = Channel::findOrFail($id);
            $roles = $channel->roles()->get();
            return $this->respond(compact('roles'));
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->respondNotFound("Channel not found");
        }
    }

    /**
     * [getChannel description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getChannel($id)
    {
        try {
            
            $channel = Channel::findOrFail($id);

            return $this->respond([
                'channel' => $this->transformer->transform($channel->toArray())
            ]);

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->respondNotFound("Channel not found");
        }
    }

    /**
     * [getMembers description]
     * @return [type] [description]
     */
    public function getMembers($id)
    {
        try {
            
            $channel = Channel::findOrFail($id);

            $members = $channel->users()->get();
            
            return $this->respond(compact('members'));

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->respondNotFound("Channel not found");
        }
    }

    /**
     * [join description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function join($id)
    {
        try {
            
            $channel = Channel::findOrFail($id);

            $user = Auth::user();

            $user_id = $user->id;
            // remove if already exists
            $channel->users()->detach([$user_id]);

            $channel->users()->attach([$user_id]);

            $this->updateAlgoliaChannels($user);

            event(new ChannelMemberJoinEvent(
                compact('user', 'channel')
            ));
            
            return $this->respondCreated("User added to channel");

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->respondNotFound("Channel not found");
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    /**
     * [join description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function leave($id)
    {
        try {
            
            $channel = Channel::findOrFail($id);

            $user = Auth::user();

            $user_id = $user->id;
            // remove if already exists
            $channel->users()->detach([$user_id]);

            $this->updateAlgoliaChannels($user);
            
            return $this->respond("User removed channel");

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->respondNotFound("Channel not found");
        }
    }

    /**
     * [updateAlgoliaChannels description]
     * @param  User   $user [description]
     * @return [type]       [description]
     */
    protected function updateAlgoliaChannels(User $user)
    {
        $channelData = [];

        foreach($user->channels()->get() as $channel) {
            $channelData[] = $channel->id;
        }

        // event that updates users channels attribute
        $this->algolia->index($this->indice)
                ->update([
                    'channels' => $channelData,
                    'objectID' => $user->id
                ]);
    }

    /**
     * [updateRole description]
     * @param  [type]  $id      [description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateRole($id, Request $request)
    {
        $this->validate($request, [
                'role_id' => 'exists:roles,id,channel_id,'.$id
            ]);

        try {
            
            $role_id = $request->get('role_id');

            $channel = Channel::findOrFail($id);

            if(empty($role_id)) {
                $role_id = NULL;
            }

            $channel->users()->updateExistingPivot(Auth::id(), ['role_id' => $role_id]);

            return $this->respond("Role updated");

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->respondNotFound("Channel not found");
        }
    }

    /**
     * [clearNotifications description]
     * @param  [type]  $id      [description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function clearNotifications($id, Request $request)
    {
        $user = $request->user();
        $channel = Channel::findOrFail($id);
        $notification_type = Notification_type::where('name', 'NEW_CHANNEL_CHAT_MENTION')->firstOrFail();
        foreach($channel->chat()->get() as $chatMessage) {
            Notification::where('notification_type_id', $notification_type->id)
                ->where('subject_id', $user->id)
                ->where('object_id', $chatMessage->id)
                ->update(['seen' => 1]);
        }
        return $this->respond("Channel notifications cleared");
    }

    /** ADMIN SECTION */
    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        return $this->respond(Channel::all());
    }

    /**
     * [indexMembers description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function indexMembers($id)
    {
        try {
            
            $channel = Channel::findOrFail($id);

            $members = $channel->users()->get();

            $pluckedIds = $members->pluck('id');

            $users = DB::table('users')
                        ->select('id', 'first_name', 'last_name', 'email')
                        ->whereNotIn('id', $pluckedIds->all())
                        ->whereNull('deleted_at')
                        ->get();

            $roles = $channel->roles()->get();
            
            return $this->respond(compact('members', 'users', 'roles'));

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->respondNotFound("Channel not found");
        }
    }

    /**
     * [store description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'name' => 'required'
            ]);

        $channel = new Channel;

        $input = $request->only($channel->getFillable());

        if($request->hasFile('logo')) {
            $input['logo'] = $this->imageService->upload($request->file('logo')->getRealPath());
        }

        $input = array_filter($input, 'strlen');

        $channel->fill($input);

        $channel->user_id = $request->user()->id;

        $channel->save();

        return $this->respondCreated("Channel created", compact('channel'));
    }

    /**
     * [update description]
     * @param  Channel $channel [description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update($id, Request $request)
    {
        if(!$request->get('name')) {
            return $this->respondValidationError("Channel name cannot be empty");
        }

        $channel = Channel::findOrFail($id);

        $channel->name = $request->name;

        $channel->description = $request->description;

        if($request->hasFile('logo')) {
            $channel->logo = $this->imageService->upload($request->file('logo')->getRealPath());
        } 

        if($request->has('remove_logo')) {
            $channel->logo = '';
        }

        if($request->has('private')) {
            $channel->private = $request->private == 1 ? true : false;
        }

        $channel->update();

        return $this->respond(compact('channel'));
    }

    /**
     * [destroy description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy($id)
    {
        $channel = Channel::findOrFail($id);

        $channel->delete();

        return $this->respond("Channel delete");
    }

    /**
     * [show description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function show($id)
    {
        $channel = Channel::findOrFail($id);

        return $this->respond(compact('channel'));
    }

    /**
     * Add member to channel
     * 
     * @param [type]  $id      [description]
     * @param Request $request [description]
     */
    public function addMember($id, Request $request)
    {
        $this->validate($request, [
                'user_id' => 'required|exists:users,id',
                'role_id' => 'exists:roles,id,channel_id,'.$id
            ]);

        try {

            $channel = Channel::findOrFail($id);

            $user = User::findOrFail($request->user_id);

            $channel->users()->detach($user->id);

            $pivotData = [];
            if($request->role_id) {
                $pivotData['role_id'] = $request->role_id;
            }

            $channel->users()->detach($user->id);

            $channel->users()->attach($user->id, $pivotData);

            $this->updateAlgoliaChannels($user);

            return $this->respondCreated("User added");
            
        } catch (ModelNotFoundException $e) {
            
            return $this->respondNotFound($e->getMessage());
        }
    }

    /**
     * [removeMember description]
     * @param  [type]  $id      [description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function removeMember($id, Request $request)
    {
        $this->validate($request, [
                'user_id' => 'required|exists:users,id'
            ]);

        try {

            $channel = Channel::findOrFail($id);

            $channel->users()->detach($request->user_id);

            $this->updateAlgoliaChannels(User::findOrFail($request->user_id));

            return $this->respond("User removed");
            
        } catch (ModelNotFoundException $e) {
            
            return $this->respondNotFound($e->getMessage());
        }
    }

    /**
     * [updateMemberRole description]
     * @param  [type]  $id      [description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateMemberRole($id, Request $request)
    {
        $this->validate($request, [
                'user_id' => 'required|exists:users,id',
                'role_id' => 'exists:roles,id,channel_id,'.$id
            ]);

        try {
            
            $role_id = $request->get('role_id');

            $channel = Channel::findOrFail($id);

            if(empty($role_id)) {
                $role_id = NULL;
            }

            $channel->users()->updateExistingPivot($request->user_id, ['role_id' => $role_id]);

            return $this->respond("Role updated");

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->respondNotFound("Channel not found");
        }
    }
}
