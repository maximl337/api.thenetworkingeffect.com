<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\AdminNote;
use App\Http\Requests;
use App\Http\Controllers\ApiController;

class AdminNoteController extends ApiController
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function getNotesByUser($id, Request $request)
    {
        $user = User::findOrFail($id);
        $admin = Auth::user();
        $admin_notes = $user->admin_notes()->get();
        return response()->json(compact('admin_notes'));
    }

    public function addNote($id, Request $request)
    {
        $this->validate($request, [
            'body' => 'required'
        ]);
        $user = User::findOrFail($id);
        $admin = Auth::user();
        $admin_note = AdminNote::create([
                'admin_id' => $admin->id,
                'user_id' => $user->id,
                'body' => $request->body
            ]);
        return response()->json(compact('admin_note'), 201);
    }

    /**
     * [destroy description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy($id)
    {
        AdminNote::find($id)->delete();
        return response()->json([]);
    }

    /**
     * Update note
     * @param  [type]  $id      [description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'body' => 'required'
        ]);

        AdminNote::findOrFail($id)->update([
            'body' => $request->body
        ]);

        return response()->json([], 200);
    }

}
