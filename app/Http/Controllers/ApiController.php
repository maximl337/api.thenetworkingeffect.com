<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Response;
use App\Http\Middleware;

class ApiController extends Controller
{
    
    protected $statusCode = 200;

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function respondNotFound($message = 'Not Found!')
    {
        return $this->setStatusCode(404)->respondWithError($message);
       
    }

    public function respondInternalError($message = 'Internal Error')
    {
        return $this->setStatusCode(500)->respondWithError($message);
       
    }

    public function respond($data, $headers = [])
    {
        return Response::json($data, $this->getStatusCode(), $headers);
    }

    public function respondWithError($message)
    {
        return $this->respond([
                'error' => [

                    'message' => $message,

                    'status_code' => $this->getStatusCode()

                ]
            ]);
    }

    public function respondCreated($message = 'Created.', $data = [])
    {
        return $this->setStatusCode(201)
                    ->respond([

                            'message' => $message,

                            'data' => $data
                            
                        ]);
    }

    public function respondValidationError($message = 'Parameters failed validation')
    {
        return $this->setStatusCode(422)->respondWithError($message);
    }

    public function respondNotAuthorized($message = 'Not authorized')
    {
        return $this->setStatusCode(401)->respondWithError($message);
    }

    public function respondForbidden($message = 'Request Forbidden')
    {
        return $this->setStatusCode(403)->respondWithError($message);
    }

    public function utf8_encode_deep(&$input) {

        if (is_string($input)) {

            $input = utf8_encode($input);

        } else if (is_array($input)) {

            foreach ($input as &$value) {

                self::utf8_encode_deep($value);

            }

            unset($value);

        } else if (is_object($input)) {

            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {

                self::utf8_encode_deep($input->$var);

            }
        }

        return $input;
    }


}