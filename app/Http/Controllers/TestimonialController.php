<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Testimonial;
use App\Notification;
use App\Notification_type;
use App\Http\Requests;
use App\Services\NotificationService;
use App\Transformers\TestimonialTransformer;
use App\Http\Requests\CreateTestimonialRequest;
use App\Http\Requests\UpdateTestimonialRequest;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Events\NewTestimonial;

class TestimonialController extends ApiController
{

    protected $transformer;

    protected $notificationService;

    public function __construct(TestimonialTransformer $transformer, NotificationService $service)
    {
        $this->middleware('jwt.auth');

        $this->middleware('testimonial.subject', ['only' => ['approveTestimonial', 'unapproveTestimonial']]);

        $this->middleware('testimonial.author', ['only' => ['updateTestimonial']]);

        $this->transformer = $transformer;

        $this->notificationService = $service;
    }

    public function createTestimonial(CreateTestimonialRequest $request)
    {
        $input = $request->input();

        $input['author_id'] = Auth::user()->id;

        $testimonial = Testimonial::create($input);

        event(new NewTestimonial($testimonial));
        
        return $this->respondCreated('Testimonial Created', [
                'testimonial' => $testimonial
            ]);


    }

    public function myTestimonials()
    {

        $testimonials = Auth::user()->testimonials()->get();

        return $this->respond(
            
                $this->transformer->transformCollection($testimonials->toArray())
            
            );
    }

    public function myApprovedTestimonials()
    {
        
        $testimonials = Auth::user()->testimonials()->where('approved', true)->get();

        return $this->respond(
            
                $this->transformer->transformCollection($testimonials->toArray())
            
            );
    }

    public function myUnapprovedTestimonials()
    {
        $user = Auth::user();

        $testimonials = $user->testimonials()->where('approved', false)->get();

        return $this->respond(
            
                $this->transformer->transformCollection($testimonials->toArray())
            
            );
    }

    public function approveTestimonial($id)
    {

        try {

            $testimonial = Testimonial::findOrFail($id);

            $testimonial->update(['approved' => true]);

            return $this->respond([
                    'message' => 'Testimonial approved'
                ]);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("Testimonial not found");

        } catch(\Exception $e) {

            return $this->respondInternalError("There was an internal error");

        }
        
    }

    public function unapproveTestimonial($id)
    {

        try {

            $testimonial = Testimonial::findOrFail($id);

            $testimonial->update(['approved' => false]);

            return $this->respond([
                    'message' => 'Testimonial unapproved'
                ]);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("Testimonial not found");

        } catch(\Exception $e) {

            return $this->respondInternalError("There was an internal error");

        }
        
    }

    public function deleteTestimonial($id)
    {

        try {

            $testimonial = Testimonial::findOrFail($id);

            $userId = Auth::user()->id;

            if(($testimonial->author_id != $userId) && ($testimonial->subject_id != $userId))
            {

                return $this->respondForbidden('Request Not authorized');
            }

            $notification_type_id = Notification_type::where("name", "NEW_TESTIMONIAL")->first()->id;

            // delete notification
            Notification::where("object_id", $testimonial->id)
                            ->where("notification_type_id", $notification_type_id)
                            ->delete();

            $testimonial->delete();

            return $this->respond([
                    'message' => 'Testimonial Deleted'
                ]);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("Testimonial not found");

        } catch(\Exception $e) {

            return $this->respondInternalError("There was an internal error");

        }
        
    }

    public function updateTestimonial($id, UpdateTestimonialRequest $request)
    {

        try {

            $testimonial = Testimonial::findOrFail($id);

            if($testimonial->approved == true) {

                return $this->respondForbidden('Cannot update an approved testimonial');
            }

            $testimonial->update($request->input());

            return $this->respond([
                    'message' => 'Testimonial updated'
                ]);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("Testimonial not found");

        } catch(\Exception $e) {

            return $this->respondInternalError("There was an internal error");

        }
        

    }

    public function usersTestimonials($id)
    {
        try {

            $user = User::findOrFail($id);

            $testimonials = $user->testimonials()->where('approved', 1)->get();

            return $this->respond(

                $this->transformer->transformCollection($testimonials->toArray())

                );

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("Testimonial not found");

        } catch(\Exception $e) {

            return $this->respondInternalError("There was an internal error");

        }
        
    }

    public function getTestimonial($id)
    {
        try {

            $testimonial = Testimonial::findOrFail($id);

            return $this->respond(
                    $this->transformer->transform($testimonial->toArray())
                );

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("Testimonial not found");

        } catch(\Exception $e) {

            return $this->respondInternalError("There was an internal error");

        }
        
    }

    /**
     * Mark testimonial as seen
     * 
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function markAsSeen($id)
    {
       try {
            
            $testimonial = Testimonial::findOrFail($id);

            $testimonial->seen = true;

            $testimonial->update();

            $this->notificationService->delete($testimonial->id, 'NEW_TESTIMONIAL');

            return $this->respond([
                    "message" => "Testimonial updated"
                ]);

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                   
            return $this->respondNotFound($e->getMessage());

        } catch(\Exception $e) {

            return $this->respondInternalError($e->getMessage());

        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $testimonials = Testimonial::with(['author' => function($q) { $q->withTrashed(); }])
                                        ->with(['subject' => function($q) { $q->withTrashed(); }])
                                        ->get();

        return $this->respond($testimonials);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'author_id' => 'required|exists:users,id',
                'subject_id' => 'required|exists:users,id',
                'body'      => 'required'
            ]);

        $input = $request->input();

        $testimonial = Testimonial::create($input);

        event(new NewTestimonial($testimonial));

        return $this->respondCreated('Testimonial created', $testimonial);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $testimonial = Testimonial::find($id);

        $result = [
            
            'testimonial' => $testimonial,

            'author'      => $testimonial->author()->get(),

            'subject'     => $testimonial->subject()->get(),

            'allUsers'    => User::all()
        ];

        return $this->respond($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
                'author_id' => 'required|exists:users,id',
                'subject_id' => 'required|exists:users,id',
                'body'      => 'required'
            ]);

        $input = $request->input();

        $testimonial = Testimonial::find($id);

        $input['approved'] = isset($input['approved'])
                                ? 1
                                : 0;

        $testimonial->update($input);

        return $this->respond('Testimonial updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $testimonial = Testimonial::findOrFail($id);

        $testimonial->delete();

        return $this->respond('Testimonial Deleted');
    }
}
