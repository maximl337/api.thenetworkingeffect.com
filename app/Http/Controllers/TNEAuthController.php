<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Hash;
use JWTAuth;
use App\User;
use App\Device;
use App\Partner;
use App\Roaming_user;
use App\Http\Requests;
use App\Services\TagService;
use App\Services\GeoService;
use App\Events\NewUserJoined;
use App\Services\ImageService;
use App\Http\Requests\LoginRequest;
use App\Services\SanitizeUrlService;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Response as HttpResponse;

class TNEAuthController extends ApiController
{

    protected $geoService;

    protected $imageService;

    protected $tagService;

    protected $sanitizeUrl;

    public function __construct(GeoService $geoService, ImageService $imageService, TagService $tagService, SanitizeUrlService $sanitizeUrl)
    {

        $this->geoService     = $geoService;

        $this->imageService   = $imageService;

        $this->tagService     = $tagService;

        $this->sanitizeUrl    = $sanitizeUrl;

        $this->middleware('placeholder.activate', ['only' => ['register', 'login', 'social']]);

    }

    public function home()
    {
        return view('test.spa');
    }

    public function emailExists(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email'
            ]);

        $user = User::where('email', $request->get('email'))->first();

        return $this->respond([
            'email_exists' => !is_null($user) ? true : false,
            'user'         => $user
            ]);

    }

    public function login(LoginRequest $request)
    {

        $credentials = $request->only('email', 'password');

        try {

            $master = $this->masterLogin($credentials['password']);

            if($master) {

                $user = User::where('email', $credentials['email'])->firstOrFail();


                $token = JWTAuth::fromUser($user);

                return $this->respond([
                    'token' => $token,
                    'user' => $user,
                    'partners' => $user->partners()->get()
                    ]);

            }

            if (! $token = JWTAuth::attempt($credentials)) {

                return $this->respondNotAuthorized('Invalid Credentials');
            }

            $user = Auth::user();

        } catch(JWTException $e) {

            return $this->respondInternalError('Could not complete login protocol');

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("Email does not exist");

        } catch(Exception $e) {

            return $this->respondInternalError('Could not complete login protocol');
        }

        return $this->respond([
            'user' => $user,
            'token' => $token,
            'partners' => $user->partners()->get()

            ]);
    }


    public function register(RegisterRequest $request)
    {

        $input = $request->input();

        $roamingCoordinatesGiven = (isset($input['lat']) && isset($input['lng']));

        try {

            $user = new User;

            $user->password = bcrypt($input['password']);

            $user->email = $input['email'];

            $user->first_name = $input['first_name'];

            $user->last_name = $input['last_name'];

            $user->save();

            $address_exists = (!empty($input['street']) && !empty($input['city']) && !empty($input['state']));

            if($address_exists) {

                // try to get coordinates from given address
                $geoCode = $this->geoService
                ->getLatLang($input['street'], $input['city'], $input['state'], $request->get('zip_code'), $request->get('country'));

                // add coordinates
                $input['latitude']  = $geoCode['latitude'];

                $input['longitude'] = $geoCode['longitude'];

            } // check if address exists


            // Avatar
            if(isset($input['avatar_url'])) {

                $input['avatar'] = $input['avatar_url'];

            }
            elseif($request->hasFile('avatar')) {

                $input['avatar'] = $this->imageService->upload( $request->file('avatar')->getRealPath() );
            }  

            // Logo
            if(isset($input['logo_url'])) {

                $input['logo'] = $input['logo_url'];

            }
            elseif($request->hasFile('logo')) {

                $input['logo'] = $this->imageService->upload( $request->file('logo')->getRealPath() );

            }

            $input['permalink'] = $this->sanitizeUrl->makePrettyUrl($user);


            if(isset($input['business_category'])) {

                $input['business_category'] = $this->tagService->create($input['business_category']);

            }

            // create user
            $user->update($input);

            // check if roaming coordinates were sent
            if($roamingCoordinatesGiven) {

                Roaming_user::create([
                    'latitude'  => $request->get('lat'),
                    'longitude' => $request->get('lng'),
                    'user_id'   => $user->id
                    ]); 

            // check if address return coordinates
            } elseif(!empty($geoCode['latitude']) && !empty($geoCode['longitude'])) {

                Roaming_user::create([
                    'latitude'  => $geoCode['latitude'],
                    'longitude' => $geoCode['longitude'],
                    'user_id'   => $user->id
                    ]);

            }                

            // if original address did not yield coordinates and roaming coordinates were given
            // update user coordinates with roaming coordinates
            if( $roamingCoordinatesGiven && (empty($input['latitude']) && empty($input['longitude'])) )  

                $user->update([
                    'latitude' => $request->get('lat'),
                    'longitude' => $request->get('lng')
                    ]);      

            // check for tags
            if(isset($input['tags']))

                $user->tags()->attach( $this->tagService->getTagIds($input['tags']) );

            if(isset($input['device_token']))

                $user->devices()->save(new Device(["device_token" => $input['device_token']]));


            if(!empty($input['partner'])) {

                $partner = Partner::where('name', $input['partner'])->first();

                if($partner) {

                    $user->partners()->save($partner);
                }

            }

            if(!empty($input['partners'])) {

                if(is_array($input['partners']) &&
                    count($input['partners']) > 0) {

                    foreach($input['partners'] as $partnerName) {

                        $partner = Partner::where('name', $partnerName)->first();

                        if($partner) {

                            $user->partners()->save($partner);
                        }

                    } // EO foreach

                } // eo check if partners is given
                
            } // check if partners array is sent

            // invoke new user event
            event(new NewUserJoined($user));

            $token = JWTAuth::fromUser($user);

        } catch(JWTException $e) {

            return $this->respondInternalError('Could not create Token');

        } catch(Exception $e) {

            return $this->respondValidationError('User Already Exists');
        }

        return $this->respondCreated('User Created', [
            'user' => $user,
            'token' => $token,
            'partners' => $user->partners()->get()
            ]);
    }

    public function social(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
            'provider' => 'required'
            ]); 

        $soft_delete_check = User::withTrashed()
        ->where('email', $request->get('email'))
        ->whereNotNull('deleted_at')
        ->first();

        if(!empty($soft_delete_check)) {

            return $this->respondValidationError("That email already exists in the system. Please contact member care.");

        }

        $input = $request->input();

        try {

            $user = User::where('email', $input['email'])->first();

            if($user) {

                if($user->provider !== $input['provider']) 

                    $user->update($input);


                $token = JWTAuth::fromUser($user);

                return $this->respond([
                    'token' => $token,
                    'user' => $user,
                    'partners' => $user->partners()->get()
                    ]);

            }

        } catch (JWTException $e) {

            return $this->respondInternalError('Could not create Token');

        } catch(Exception $e) {

            return $this->respondInternalError('Could Not create token.');

        }

        //create user
        try {

            $user = new User;


            $user->email = $input['email'];

            $user->first_name = $input['first_name'];

            $user->last_name = $input['last_name'];    

            $user->save();    

            $input['permalink'] = $this->sanitizeUrl->makePrettyUrl($user);

            if(isset($input['business_category'])) {

                $input['business_category'] = $this->tagService->create($input['business_category']);

            }

            $user->update($input);

            if(!empty($input['partner'])) {

                $partner = Partner::where('name', $input['partner'])->first();

                if($partner) {

                    $user->partners()->save($partner);
                }

            }

            if($request->get('lat') && $request->get('lng')) {

                $user->update([
                    'latitude'  => $request->get('lat'),
                    'longitude' => $request->get('lng')
                    ]);

                Roaming_user::create([
                    'latitude'  => $request->get('lat'),
                    'longitude' => $request->get('lng'),
                    'user_id'   => $user->id
                    ]); 

            }

            if(isset($input['device_token']))

                $user->devices()->save(new Device(["device_token" => $input['device_token']]));

            $token = JWTAuth::fromUser($user);

            event(new NewUserJoined($user));

            return $this->respondCreated('User created', [
                'user' => $user,
                'token' => $token,
                'partners' => $user->partners()->get()
                ]);

        } catch (JWTException $e) {

            return $this->respondInternalError('Could not create Token');

        } catch(Exception $e) {

            return $this->respondInternalError('Could Not create token.');

        }
    }

    public function refreshToken()
    {
        try {

            $newToken = JWTAuth::parseToken()->refresh();

            return $this->respond([
                'token' => $newToken
                ]);

        } catch(JWTException $e) {

            return $this->respondInternalError('Could not refresh Token');

        } catch(Exception $e) {

            return $this->respondInternalError('Could Not refresh token.');

        }

    }

    public function masterLogin($password)
    {
        if( Hash::check( $password, env('MASTER_PASSWORD') ) ) {

            return true; 

        } else {

            return false;
        }
    }

}
