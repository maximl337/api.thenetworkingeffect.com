<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use DB;
use Auth;
use App\User;
use App\Message;
use Carbon\Carbon;
use App\Http\Requests;
use App\Events\NewMessageSent;
use App\Events\InitiateMassMessageSentEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\MessageRequest;
use App\Services\NotificationService;
use App\Transformers\InboxTransformer;
use App\Http\Controllers\ApiController;
use App\Transformers\ConversationTransformer;
use App\Transformers\ConversationTransformerNoFromUserAvatar;

class MessageController extends ApiController
{

    protected $inboxTransformer;

    protected $conversationTransformer;

    protected $notificationService;

    /**
     * Init middlerware
     */
    public function __construct(InboxTransformer $inboxTransformer, ConversationTransformer $conversationTransformer, NotificationService $notificationService)
    {
        $this->middleware('jwt.auth');

        $this->middleware('message.recipient', ['only' => ['markAsSeen']]);

        $this->inboxTransformer = $inboxTransformer;

        $this->conversationTransformer = $conversationTransformer;

        $this->notificationService = $notificationService;
    }

    /**
     * Send a message
     * @param  MessageRequest $request validation
     * @return mixed                  
     */
    public function send(MessageRequest $request)
    {
        $input = $request->input();

        $subject = User::find($input['to']);

        $isBlocked = $subject->isBlocked(Auth::id());

        if($isBlocked) {
            return $this->respondForbidden("User is blocked by the subject");
        }

        $input['from'] = Auth::user()->id;

        $message = Message::create($input);

        event(new NewMessageSent($message));

        return $this->respondCreated('Message sent', [
                'message' => $message
            ]);  
    }

    /**
     * reply to a message thread
     * @param  int         $id      id of the message to reply to
     * @param  MessageRequest $request [description]
     * @return [type]                  [description]
     */
    public function reply($id, MessageRequest $request)
    {

        $parentMessage = Message::findOrFail($id);

        $input = $request->input();

        $subject = User::find($input['to']);

        $isBlocked = $subject->isBlocked(Auth::id());

        if($isBlocked) {
            return $this->respondForbidden("User is blocked by the subject");
        }

        $input['from'] = Auth::id();

        $input['parent_id'] = $parentMessage->id;

        $message = Message::create($input);

        event(new NewMessageSent($message));

        return $this->respondCreated('Reply sent', [
                'message' => $message
            ]);
    }

    /**
     * [inbox description]
     * @return [type] [description]
     */
    public function inbox() {

        $userId = Auth::user()->id;

        
        $conversation_list = $this->get_conversation_list();
        
        //get the latest message of from the inbox
        foreach ( $conversation_list as $item ) {

            $latestMessage = $this->get_latest_message_from_conversation($userId, $item->connection_id);

            $item->message = $latestMessage[0]->body;

            $item->status = $latestMessage[0]->seen ? true : false;

            // If sender was user then set seen to true
            if($userId == $latestMessage[0]->from) {
                $item->status = true;
            }

        }

        return $this->respond(
                $this->inboxTransformer->transformCollection($conversation_list)
            );

    }

    /**
     * [get_conversation_list description]
     * @return [type] [description]
     */
    protected function get_conversation_list()
    {

        $userId = Auth::user()->id;

        $conversation_list = DB::select("SELECT `id`, connection_id, user_id, MAX(`created_at`) as last_date FROM 
                            ( 
                            SELECT `id`, `to` as 'connection_id', `from` as 'user_id', `created_at` 
                            FROM `messages` 
                            WHERE `from` = ? 
                            AND 
                            `id` NOT IN (SELECT `message_id` FROM `deleted_messages` WHERE `user_id` = ?)  
                            UNION ALL 

                            SELECT `id`, `from` as 'connection_id', `to` as 'user_id', `created_at` 
                            FROM `messages` 
                            WHERE `to` = ?
                            AND 
                            `id` NOT IN (SELECT `message_id` FROM `deleted_messages` WHERE `user_id` = ?)  
                            ) x 
                            GROUP BY connection_id 
                            ORDER BY last_date DESC", [$userId, $userId,  $userId,  $userId]);

        return $conversation_list;
    }

    /**
     * [get_latest_message_from_conversation description]
     * @param  [type] $userId       [description]
     * @param  [type] $connectionId [description]
     * @return [type]               [description]
     */
    protected function get_latest_message_from_conversation($userId, $connectionId)
    {
        $latestMessage = DB::select("SELECT * FROM `messages` WHERE
                (`from` = ? AND `to` = ?)
                OR
                (`from` = ? AND `to` = ?)
                AND
                `id` NOT IN (SELECT `message_id` FROM `deleted_messages` WHERE `user_id` = ?) 
                ORDER BY `created_at` DESC
                LIMIT 1", [$userId, $connectionId, $connectionId, $userId, $userId]);

        return $latestMessage;
    }

    /**
     * [getConversation description]
     * @param  [type] $connectionId [description]
     * @return [type]               [description]
     */
    public function getConversation($connectionId, ConversationTransformerNoFromUserAvatar $transformer)
    {

        $userId = Auth::user()->id;

        $conversation = DB::select("SELECT * FROM (
                        
                        SELECT i.*
                        FROM `messages` i
                        WHERE
                        i.`to` = ?
                        AND
                        i.`from` = ?
                        AND
                        i.`id` NOT IN (SELECT `message_id` FROM `deleted_messages` WHERE `user_id` = ?) 
                        UNION
                        
                        SELECT i.*
                        FROM `messages` i
                        WHERE
                        i.`to` = ?
                        AND
                        i.`from` = ?
                        AND
                        i.`id` NOT IN (SELECT `message_id` FROM `deleted_messages` WHERE `user_id` = ?)                    
                    ) tmp ORDER BY tmp.created_at ASC", [$userId, $connectionId, $userId, $connectionId, $userId, $userId]);

        return $this->respond(
            
                $transformer->transformCollection($conversation)

            );
    }

    /**
     * [getConversation description]
     * @param  [type] $connectionId [description]
     * @return [type]               [description]
     */
    public function getConversationNew($connectionId)
    {

        $userId = Auth::user()->id;

        $conversation = DB::select("SELECT * FROM (
                        
                        SELECT i.*
                        FROM `messages` i
                        WHERE
                        i.`to` = ?
                        AND
                        i.`from` = ?
                        AND
                        i.`id` NOT IN (SELECT `message_id` FROM `deleted_messages` WHERE `user_id` = ?) 
                        UNION
                        
                        SELECT i.*
                        FROM `messages` i
                        WHERE
                        i.`to` = ?
                        AND
                        i.`from` = ?
                        AND
                        i.`id` NOT IN (SELECT `message_id` FROM `deleted_messages` WHERE `user_id` = ?)                    
                    ) tmp ORDER BY tmp.created_at ASC", [$userId, $connectionId, $userId, $connectionId, $userId, $userId]);

        return $this->respond(
            
                $this->conversationTransformer->transformCollection($conversation)

            );
    }

    /**
     * [deleteMessage description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function deleteMessage($id)
    {
        $userId = Auth::user()->id;

        Message::findOrFail($id)->deleted_messages()->attach($userId);

        return $this->respond([
                'message' => 'Message deleted'
            ]);
    }

    /**
     * Delete all messages with the parent id
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function deleteConversation($id)
    {
        try {

            $userId = Auth::id();
           
            // check if its a parent message
            $parent_message = Message::find($id);

            if($parent_message) {

                $parent_message->seen = true;

                $parent_message->save();

                $parent_message->deleted_messages()->attach($userId);
            }

            // Find all messages with this parent id
            $messages = Message::where('parent_id', $id)->get();

            foreach($messages as $message) {

                $message->seen = true;

                $message->save();

                $message->deleted_messages()->attach($userId);
            
            }

            return $this->respond([
                    "message" => "conversation deleted"
                ]);

        } catch (Exception $e) {
           
            return $this->respondInternalError($e->getMessage());
        
        }
    }

    public function markAsSeen($id)
    {

        try {
            
            $message = Message::findOrFail($id);

            $message->seen = true;

            $message->save();

            $this->notificationService->markAsSeen($id, 'NEW_MESSAGE');

            $tableName = $message->getTable();

            // mark converation as seen
            if(!is_null($message->parent_id)) {

                $parentId = $message->parent_id;

            } else {
                
                $parentId = $message->id;
            }

            DB::table($tableName)
                ->where('parent_id', $parentId)
                ->update(['seen' => true]);

            $conversation = Message::where('parent_id', $parentId)->get();

            foreach($conversation as $msg) {
                $this->notificationService->markAsSeen($msg->id, 'NEW_MESSAGE');    
            }

            return $this->respond([
                    'message' => 'Message updated'
                ]);

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("Message not found");
                        
        } catch (\Exception $e) {

            return $this->respondInternalError($e->getMessage());
        }
        
    }

    public function markAllAsSeen()
    {
        $userId = Auth::user()->id;

        User::findOrFail($userId)->messages()->update(['seen' => true]);

        return $this->respond([
                'message' => 'Message updated'
            ]);
    }

    /**
     * [markManyAsSeen description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function markManyAsSeen(Request $request)
    {
        try {

            $message_ids = $request->get('message_ids');

            if(!is_array($message_ids)) {
                throw new Exception("Given message ids must be of type array");
            }

            if(!$message_ids || count($message_ids) < 1) {
                throw new Exception("Message ids not given");
            }

            foreach($message_ids as $message_id) {

                $message = Message::find($message_id);

                if($message) {

                    $message->seen = true;

                    $message->save();

                    $this->notificationService->markAsSeen($message_id, 'NEW_MESSAGE');
                }
                
            }

            return $this->respond("Messages marked as seen");

        } catch (Exception $e) {

            return $this->respondInternalError($e->getMessage());

        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        ini_set('memory_limit','256M');

        $messages = Message::with(['to' => function($q) { $q->withTrashed(); }])
                            ->with(['from' => function($q) { $q->withTrashed(); }])
                            ->get();

        return $this->respond($messages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
                'from'  => 'required|exists:users,id',
                'to'    => 'required|exists:users,id',
                'body'  => 'required'
            ]);

        $message = Message::create($request->input());

        event(new NewMessageSent($message));

        return $this->respondCreated($message);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $message = Message::findOrFail($id);

        $result = [

            'message' => $message,

            'from'    => $message->from()->get(),

            'to'      => $message->to()->get()

        ];

        return $this->respond($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $message = Message::findOrFail($id);

        $message->delete();

        return $this->respond('Message deleted');
    }

    /**
     * [mass_message description]
     * @param  MessageRequest $request [description]
     * @return [type]                  [description]
     */
    public function mass_message(Request $request)
    {

        $this->validate($request, [
                'body' => 'required',
                'recipients' => 'array'
            ]);

        try {

            $input = $request->input();

            $input['sender_id'] = $request->user()->id;

            event(new InitiateMassMessageSentEvent($input));

            return $this->respondCreated('Mass Message sending queued!');  

        } catch (\Exception $e) {

            return $this->respondInternalError($e->getMessage());

        }

    }
}
