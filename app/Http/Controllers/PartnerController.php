<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Partner;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\ApiController;

class PartnerController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partner::all();

        return $this->respond($partners);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            
            $this->validate($request, [
                    'name' => 'required|unique:partners'
                ]);

            $partner = Partner::create([
                    'name' => $request->get('name')
                ]);

            return $this->respondCreated($partner);

        } catch (Exception $e) {
            
            return $this->respondInternalError($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            
            $partner = Partner::findOrFail($id);

            return $this->respond($partner);

        } catch (Exception $e) {
            
            return $this->respondInternalError($e->getMessage());

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $this->validate($request, [
                    'name' => 'unique:partners,name,'.$id
                ]);
            
            $partner = Partner::findOrFail($id);

            $partner->update([
                    'name' => $request->get('name')
                ]);

            return $this->respond($partner);

        } catch (Exception $e) {
            
            return $this->respondInternalError($e->getMessage());

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            Partner::findOrFail($id)->delete();

            return $this->respond("Deleted");
            
        } catch (Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }
}
