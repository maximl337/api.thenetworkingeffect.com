<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Channel;
use App\ChatMessage;
use App\DeletedChatMessage;
use App\ReportedChatMessage;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Events\ChatMessageCreatedEvent;
use App\Events\ChatMessageMentionEvent;

class ChatMessageController extends ApiController
{

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, Request $request)
    {
        $limit = $request->limit ?: 20;

        $offset = $request->offset ?: 0;

        $channel = Channel::findOrFail($id);

        $chat = $channel->chat()
                    ->with('user')
                    ->latest()
                    ->skip($offset)
                    ->take($limit)
                    ->get();

        foreach($chat as $key => $message) {
            $isDeleted = DeletedChatMessage::where('chat_message_id', $message->id)
                            ->where('user_id', $request->user()->id)
                            ->exists();
            $userIsNull = is_null($message->user);
            if($isDeleted || $userIsNull) {
                $chat->forget($key);
            }
        }
        $chat = $chat->values();

        return $this->respond(compact('chat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
                'message' => 'required'
            ]);
        $channel = Channel::findOrFail($id);
        $chat_message = new ChatMessage([
                'message' => $request->message,
                'user_id' => $request->user()->id
            ]);
        $channel->chat()->save($chat_message);
        $event_data = [
            'user' => $request->user(),
            'chat_message' => $chat_message->with('user')->get(),
            'channel' => $channel
        ];
        event(new ChatMessageCreatedEvent($event_data));

        return $this->respondCreated(compact('chat_message'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $chatMessage = ChatMessage::findOrFail($id);
        $deletedChatMessage = new DeletedChatMessage([
                'user_id' => $request->user()->id
            ]);
        $chatMessage->deleted_messages()->save($deletedChatMessage);
        $this->respond(['message' => 'Chat deleted successfully']);
    }

    /**
     * [report description]
     * @param  [type]  $id      [description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function report($id, Request $request)
    {
        $chatMessage = ChatMessage::findOrFail($id);
        $reported = new ReportedChatMessage([
                'user_id' => $request->user()->id
            ]);
        $chatMessage->reported_messages()->save($reported);
        $this->respond(['message' => 'Chat reported successfully']);
    }

    /**
     * [newMention description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function mention(Request $request, $id)
    {
        $this->validate($request, [
            // make sure user is part of channel
            'user_id' => 'exists:users,id',
            'everyone' => 'boolean'
        ]);

        \Log::info('ChatMessageController@mention', [
            'request' => $request->input(),
        ]);

        $chatMessage = ChatMessage::findOrFail($id);
        $actor = $chatMessage->user;

        if($request->user_id) {
            $subject = User::findOrFail($request->user_id);
            // send event
            $data = compact('subject', 'chatMessage', 'actor');
            event(new ChatMessageMentionEvent($data));
            $message = 'User notified';
        } 
        if($request->everyone) {
            // get all users of channel
            $channel = $chatMessage->channel;
            $channelUsers = $channel->users()->whereNotIn('users.id', [$actor->id])->get();
            foreach($channelUsers as $subject) {
                $data = compact('subject', 'chatMessage', 'actor');
                event(new ChatMessageMentionEvent($data));
            }
            $message = 'Users notified';
            \Log::info('ChatMessageController@mention', [
                'NOTIFIED_EVERYONE' => true,
            ]);
        } // check if mentioning everyone

        
        return $this->respondCreated(compact('message'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    
}
