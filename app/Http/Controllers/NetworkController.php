<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Log;
use Auth;
use App\User;
use App\Network;
use App\BlockedUser;
use App\Notification;
use App\Services\NetworkService;
use App\Http\Requests;
use App\Events\NetworkUnfollowed;
use App\Events\NewNetworkFollower;
use App\Http\Controllers\Controller;
use App\Services\NotificationService;
use App\Http\Controllers\ApiController;
use App\Transformers\NetworkTransformer;
use App\Transformers\InvitesTransformer;
use App\Transformers\UsersNetworkTransformer;
use App\Transformers\NetworkTransformerWithoutPlaceholders;

class NetworkController extends ApiController
{

    protected $networkTransformer;

    protected $notificationService;

    protected $networkService;

    public function __construct(NetworkTransformer $networkTransformer, NotificationService $notificationService, NetworkService $networkService)
    {
        $this->middleware('jwt.auth');

        $this->middleware('network.subject', ['only' => ['markAsSeen']]);

        $this->networkTransformer = $networkTransformer;

        $this->notificationService = $notificationService;

        $this->networkService = $networkService;
    }

    /**
     * [follow description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function follow($id, Request $request)
    {

        try {

            $subject = User::findOrFail($id);

            $isBlocked = $subject->isBlocked(Auth::id());

            if($isBlocked) {
                return $this->respondForbidden("User is blocked by the subject");
            }

            if($id == Auth::id()) {
                return $this->respondValidationError("Cannot follow self");
            }

            $network = $this->networkService->addToNetwork(Auth::id(), $subject->id, $request->get('message'));

            
            
            return $this->respondCreated('User followed', [$network]);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound($e->getMessage());

        } catch(\Exception $e) {

            return $this->respondInternalError($e->getMessage());
        }
    }

    /**
     * [unfollow description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function unfollow($id)
    {
        $subjectId = $id;

        $followerId = Auth::id();

        try {

            $network =  Network::where('follower_id', $followerId)
                        ->where('subject_id', $subjectId)
                        ->delete();

            //$network->delete();

            // remove from opportunity
            Auth::user()->opportunities()->where('subject_id', $subjectId)->delete();

            $data = [
                'follower' => Auth::user(),
                'subject' => User::find($subjectId)
            ];

            event(new NetworkUnfollowed($data));

            return $this->respond([
                    "message" => "User unfollowed"
                ]);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound('Network not found');

        } catch(\Exception $e) {

            return $this->respondForbidden($e->getMessage());
        }        

    }

    /**
     * [myFollowing description]
     * @return [type] [description]
     */
    public function myFollowing(Request $request)
    {
        
        $limit = $request->get('limit') ?: 15;

        $offset = $request->get('offset') ?: 0;

        $query = $request->get('query') ?: '';

        $contacts = $this->networkService->getUserContacts(Auth::user(), 'ids', $query);

        $network = Auth::user()->network_following()
                                ->whereIn("subject_id", $contacts)
                                ->groupBy('subject_id')
                                ->orderBy('created_at', 'desc')
                                ->take($limit)
                                ->skip($offset)
                                ->get()
                                ->toArray();

        return $this->respond(
                array_values(array_filter($this->networkTransformer->transformCollection($network)))
            );
    }

    /**
     * [myFollowers description]
     * @return [type] [description]
     */
    public function myFollowers()
    {
        $network = Auth::User()->network_subject->toArray();

        return $this->respond($network);
    }

    /**
     * [userFollowing description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function userFollowing($id, UsersNetworkTransformer $transformer, Request $request)
    {
        try {

        
            $limit = $request->get('limit') ?: 9;

            $offset = $request->get('offset') ?: 0;

            $query = $request->get('query') ?: '';

            $user = User::findOrFail($id);

            $contacts = $this->networkService->getUserContacts($user, 'ids', $query);

            $network = $user->network_following()
                            ->whereIn("subject_id", $contacts)
                            ->groupBy('subject_id')
                            ->orderBy('created_at', 'desc')
                            ->take($limit)
                            ->skip($offset)
                            ->get()
                            ->toArray();

            return $this->respond(
                    array_values(array_filter($transformer->transformCollection($network)))
                );

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("User not found for given ID");

        } catch(Exception $e) {

            return $this->respondInternalError($e->getMessage());
        }
        
    }

    /**
     * [userFollowers description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function userFollowers($id)
    {
        $network = User::findOrFail($id)->network_subject->toArray();

        return $this->respond($network);
    }

    /**
     * Mark a contact as seen
     *     
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function markAsSeen($id)
    {
        try {

            $network = Network::findOrFail($id);

            $network->seen = true;

            $network->save();

            $this->notificationService->markAsSeen($network->id, 'NEW_FOLLOWER');

            return $this->respond([
                    'message' => 'Network updated'
                ]);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound($e->getMessage());

        } catch(\Exception $e) {

            return $this->respondInternalError($e->getMessage());

        }
        
    }

    public function markAllAsSeen()
    {
        Auth::user()->network_subject()->update(['seen' => true]);

        return $this->respond([
                'message' => 'Network updated'
            ]);
    }

    /**
     * Block another user
     * 
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function block($id)
    {
        $blocked = Auth::user()->isBlocked($id);

        if(!$blocked) {

            $blockedUser = new BlockedUser(['blocked_id' => $id]);

            Auth::user()->blocked()->save($blockedUser);
        }

        return $this->respond([
                "message" => "Blocked user"
            ]);
    }

    /**
     * Unblock a user
     *
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function unblock($id)
    {
        $blocked = Auth::user()->isBlocked($id);

        if($blocked) {

            Auth::user()->blocked()->where('blocked_id', $id)->delete();
        }

        return $this->respond([
                "message" => "Unblocked user"
            ]);
        
    }

    /**
     * Get new invites
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getNewInvites(Request $request, InvitesTransformer $transformer)
    {
        try {
            
            $limit = $request->get('limit') ?: 9;

            $offset = $request->get('offset') ?: 0;

            $user = Auth::user();

            $invites = $user->network_subject()
                            ->orderBy('seen', 'asc')
                            ->latest()
                            ->take($limit)
                            ->skip($offset)
                            ->get()
                            ->toArray();

            return $this->respond(
                    array_values(array_filter($transformer->transformCollection($invites)))
                );

        } catch (Exception $e) {
                                        
            $this->respondInternalError($e->getMessage());
        }
    }

    /**
     * [deleteInvite description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function deleteInvite($id)
    {
        try {
            $user = Auth::user();
            $firstNetwork = $user->network_subject()->where('networks.id', $id)->first();
            if($firstNetwork) {
                // get follower
                $follower = $firstNetwork->follower()->first();
                $user->network_subject()->where('follower_id', $follower->id)->delete();
            }

            return $this->respond('Invites deleted');

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->respondNotFound('Could not find invite with the given id');
        }
    }

    /**
     * Get a single invite
     * 
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getInvite($id, InvitesTransformer $transformer)
    {
        try {
        
            $user = Auth::user();

            $invite = Network::findOrFail($id)->toArray();

            return $this->respond(
                    $transformer->transform($invite)
                );

        } catch (Exception $e) {
                                        
            $this->respondInternalError($e->getMessage());
        }
    }

    /**
     * ADMIN SECTION
     */

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        

        $network = DB::select("SELECT n.*, 
                            CONCAT(f.first_name, ' ', f.last_name) 
                            AS 'follower_name', 
                            CONCAT(s.first_name, ' ', s.last_name) 
                            AS 'subject_name'
                            FROM networks n
                            LEFT JOIN
                            users f
                            ON
                            f.id = n.follower_id
                            LEFT JOIN
                            users s
                            ON
                            s.id = n.subject_id
                            ORDER BY
                            n.created_at DESC", []);

        return $this->respond($network);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'follower_id' => 'required|exists:users,id',
                'subject_id'  => 'required|exists:users,id'
            ]);

        $input = $request->input();

        $network = Network::create($input);

        event(new NewNetworkFollower($network));

        return $this->respondCreated('Subject Followed', $network);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $network = Network::findOrFail($id);

        $data = [
            'follower' => User::find($network->follower_id),
            'subject' => User::find($network->subject_id)
        ];

        event(new NetworkUnfollowed($data));

        $network->sorting()->detach();

        $network->delete();

        return $this->respond('Deleted');
    }

}

    