<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Log;
use Auth;
use App\User;
use App\Notification;
use App\Http\Requests;
use App\Transformers\NotificationTransformer;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;

class NotificationController extends ApiController
{

    protected $notificationTransformer;


    public function __construct(NotificationTransformer $transformer)
    {
        $this->middleware('jwt.auth');

        $this->middleware('notification.subject', ['only' => ['markAsSeen', 'deleteNotification']]);

        $this->notificationTransformer = $transformer;
    }

    /**
     * [getNotifications description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getNotifications(Request $request)
    {
        $user = Auth::user();

        $limit = $request->get('limit') ?: 20;

        $offset = $request->get('offset') ?: 0;

        $filter = $request->get('filter') ?: 'messages';

        switch (strtolower($filter)) {
            case 'messages':
                $all = $user->notifications()
                ->where('notification_type_id', function($query) { $query->select('id')->from('notification_types')->where('name', 'NEW_MESSAGE')->first(); })
                ->orderBy('seen', 'asc')->latest()->take($limit)->skip($offset)->get()->toArray();
                break;
            case 'testimonials':
                $all = $user->notifications()
                ->where('notification_type_id', function($query) { $query->select('id')->from('notification_types')->where('name', 'NEW_MESSAGE')->first(); })
                ->orderBy('seen', 'asc')->latest()->take($limit)->skip($offset)->get()->toArray();
                break;
            case 'invites':
                $all = $user->notifications()
                ->whereIn('notification_type_id', function($query) { $query->select('id')->from('notification_types')->where('name', 'NEW_FOLLOWER')->orWhere('name', 'FOLLOWED_BACK')->get(); })
                ->orderBy('seen', 'asc')->latest()->take($limit)->skip($offset)->get()->toArray();
                break;
            case 'referred':
                $all = $user->notifications()
                ->where('notification_type_id', function($query) { $query->select('id')->from('notification_types')->where('name', 'NEW_REFERRAL')->first(); })
                ->orderBy('seen', 'asc')->latest()->take($limit)->skip($offset)->get()->toArray();
                break;
            case 'reminders':
                $all = $user->notifications()
                ->where('notification_type_id', function($query) { $query->select('id')->from('notification_types')->where('name', 'NOTE_REMINDER')->first(); })
                ->orderBy('seen', 'asc')->latest()->take($limit)->skip($offset)->get()->toArray();
                break;
            case 'channel':
                $all = $user->notifications()
                ->where('notification_type_id', function($query) { $query->select('id')->from('notification_types')->where('name', 'NEW_CHANNEL_CHAT_MENTION')->first(); })
                ->orderBy('seen', 'asc')->latest()->take($limit)->skip($offset)->get()->toArray();
                break;
            default:
                $all = $user->notifications()->orderBy('seen', 'asc')->latest()->take($limit)->skip($offset)->get()->toArray();
                break;
        }
        //App\Notification::where('notification_type_id', function($query) { $query->select('id')->from('notification_types')->where('name', 'NEW_MESSAGE')->first(); })->get();
        return $this->respond([
                'notifications' => $this->notificationTransformer
                                    ->transformCollection($this->utf8_encode_deep($all))
            ]);

    }

    /**
     * [getCountOfUnseen description]
     * @return [type] [description]
     */
    public function getCountOfUnseen()
    {
        $unseenNotifications = Auth::user()->notifications()->where('seen', 0)->get();

        $categorized = (new \App\Services\NotificationCountService)->categorize($unseenNotifications);

        return $this->respond([
            "total" => $categorized['total'],
            "categories" => $categorized
        ]);
    }

    /**
     * [markAsSeen description]
     * @param  [type]  $id      [description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function markAsSeen($id, Request $request)
    {

        try {

            $notification = Notification::findOrFail($id);

            $notification->update(['seen' => true]);

            return $this->respond([
                        "message" => "notification updated"
                ]);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound('Notification not found');
        }

    }

    /**
     * [markAllAsSeen description]
     * @return [type] [description]
     */
    public function markAllAsSeen()
    {

        $unseenNotifications = Auth::user()->notifications()->where('seen', false)->get();

        $i = 0;

        foreach($unseenNotifications as $unseenNotification) {

            Notification::find($unseenNotification->id)->update(['seen' => true]);

            $i++;
        }

        return $this->respond([ 
                'message' => $i . ' notifications updated'
            ]);

    }

    

    /**
     * [deleteNotification description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function deleteNotification($id)
    {

        try {

            Notification::findOrFail($id)->delete();

            return $this->respond(['message' => 'Notification deleted']);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound('Could not find notification');
        }
        

    }
}
