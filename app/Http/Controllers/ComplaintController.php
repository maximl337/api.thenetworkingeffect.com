<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Complaint;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class ComplaintController extends ApiController
{

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function submitAbuse(Request $request)
    {
        $this->validate($request, [
                'body' => 'required'
            ]);

        $input = $request->input();

        $input['user_id'] = Auth::id();

        $complaint = Complaint::create($input);

        return $this->respondCreated('Abuse reported', $complaint->toArray());

    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
