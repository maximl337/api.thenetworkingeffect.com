<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Authorizer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PasswordVerifier extends Controller
{

      public function __construct()
      {
         $this->middleware('oauth', ['only' => ['getUser']]);
      }

   	public function verify($username, $password)
   	{
   		$credentials = [
   			"email" => $username,
   			"password" => $password
   		];

   		return Auth::attempt($credentials)
            ? Auth::id()
            : false;

   	}
}
