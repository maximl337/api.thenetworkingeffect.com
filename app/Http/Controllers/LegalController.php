<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Privacy_policy;
use App\Terms_conditions;
use App\Http\Requests;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;

class LegalController extends ApiController
{

    /**
     * get the latest version of terms and conditions
     * @return array terms and conditions
     */                                     
    public function getTermsAndConditions()
    {
        $tc = Terms_conditions::orderBy('created_at', 'desc')->first()->toArray();

        return $this->respond($tc);
    }

    /**
     * get the latest version of PrivacyPolicy
     * @return array terms and conditions
     */                                     
    public function getPrivacyPolicy()
    {
        $pp = Privacy_policy::orderBy('created_at', 'desc')->first()->toArray();

        return $this->respond($pp);
    }

    public function updateTermsAndConditions($id, Request $request)
    {
        $this->validate($request, [
            'body' => 'required'
        ]); 

        $tc = Terms_conditions::find($id);

        $tc->update($request->input());

        return $this->respond('Terms and Conditions updated.');
    }

    public function updatePrivacyPolicy($id, Request $request)
    {
        $this->validate($request, [
            'body' => 'required'
        ]); 

        $pp = Privacy_policy::find($id);

        $pp->update($request->input());

        return $this->respond('Privacy Policy updated.');
    }

    public function createTermsAndConditions(Request $request)
    {
        $this->validate($request, [
            'body' => 'required'
        ]); 

        $tc = Terms_conditions::create($request->input());

        return $this->respondCreated('Terms and conditions created', $tc->toArray());
    }

    public function createPrivacyPolicy(Request $request)
    {
        $this->validate($request, [
            'body' => 'required'
        ]); 

        $pp = Privacy_policy::create($request->input());

        return $this->respondCreated('Privacy Policy created.', $pp->toArray());
    }

    public function deleteTermsAndConditions($id)
    {

        $count = Terms_conditions::all()->count();

        if($count == 1) return $this->respondValidationError('Cannot delete last Terms and Conditions record');
        
        $tc = Terms_conditions::find($id);

        $tc->delete();

        return $this->respond('Terms and conditions deleted');
    }

    public function deletePrivacyPolicy($id)
    {

        $count = Privacy_policy::all()->count();

        if($count == 1) return $this->respondValidationError('Cannot delete last privacy policy record');

        $pp = Privacy_policy::find($id);

        $pp->delete();

        return $this->respond('Privacy Policy deleted');
    }

    public function showTermsAndConditions($id)
    {

        $tc = Terms_conditions::find($id);

        return $this->respond($tc);
    }

    public function showPrivacyPolicy($id)
    {

        $pp = Privacy_policy::find($id);

        return $this->respond($pp);
    }


    public function getLegal()
    {
        $tc = Terms_conditions::all();

        $pp = Privacy_policy::all();

        $data = [
            'tc' => $tc,

            'pp' => $pp
        ];

        return $this->respond($data);
    }
}
