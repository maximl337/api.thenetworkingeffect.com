<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\MandrillTemplate;
use App\Http\Requests;
use App\Http\Controllers\ApiController;
use App\Transformers\MailSubscriptionTransformer;

class MailController extends ApiController
{

    public $transformer;

    public function __construct(MailSubscriptionTransformer $transformer)
    {
        $this->middleware('jwt.auth');

        $this->transformer = $transformer;
    }

    /**
     * Get All Mandrill Templates
     * 
     * @return Response
     */
    public function getTemplates()
    {
        $templates = MandrillTemplate::all();

        return $this->respond($this->transformer->transformCollection($templates->toArray()));
    }

    /**
     * Get users unsubbed Mail
     * 
     * @return Response
     */
    public function getUnsubbed()
    {
        $user = Auth::user();

        $templates = $user->unsubbed_mail()->get();

        return $this->respond($this->transformer->transformCollection($templates->toArray()));
    }

    /**
     * Unsubscribe from Templates
     *
     * @param  Request $request 
     * @return Response
     */
    public function updateSubscription(Request $request)
    {
        $this->validate($request, [
                'template_ids' => 'array'
            ]);

        $user = Auth::user();

        $user->unsubbed_mail()->detach();

        $input = $request->input();

        $template_ids = array_values(array_filter($input['template_ids']));

        if( $template_ids )
            $user->unsubbed_mail()->attach($template_ids);

        return $this->respondCreated('Subscription updated');

    }
}
