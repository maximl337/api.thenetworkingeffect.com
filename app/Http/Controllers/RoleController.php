<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Channel;
use App\Role;
use App\Http\Requests;
use App\Http\Controllers\ApiController;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RoleController extends ApiController
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Get roles of a channel
     * 
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function index($id)
    {
        try {
            
            $channel = Channel::findOrFail($id);

            $roles = $channel->roles()->get();

            return $this->respond(compact('roles'));

        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound($e->getMessage());
        }
    }

    /**
     * show a channel
     * @param  [type] $channel_id [description]
     * @param  [type] $role_id    [description]
     * @return [type]             [description]
     */
    public function show($id)
    {
        try {
            
            $role = Role::findOrFail($id);

            return $this->respond(compact('role'));

        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {   
        $this->validate($request, [
                'name' => 'required|unique:roles,name,NULL,id,channel_id,'.$request->route('id')
            ]);

        try {

            $channel = Channel::findOrFail($id);

            $role = new Role(['name' => $request->name, 'user_id' => Auth::id()]);

            $channel->roles()->save($role);

            return $this->respondCreated('Role created successfully');

        } catch(ModelNotFoundException $e) {
            return $this->respondNotFound($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'name' => 'required|unique:roles,name,'.$request->route('id').',id,channel_id,'.$request->channel_id,
                'channel_id' => 'required'
            ]);

        try {

            $role = Role::findOrFail($id);

            $role->update(['name' => $request->name]);

            return $this->respond('Role updated successfully');

        } catch(ModelNotFoundException $e) {
            return $this->respondNotFound($e->getMessage());
        }
    }

    /**
     * [delete description]
     * 
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function destroy(Request $request, $id)
    {
        try {

            $role =Role::findOrFail($id);

            $role->channel()->dissociate();

            $role->delete();

            return $this->respond("Role deleted");
            
        } catch (Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }
}
