<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Network;
use App\Http\Requests;	
use App\SortingCategory;
use App\Services\SortingService;
use App\Events\NetworkUnfollowed;
use App\Events\NewNetworkFollower;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Transformers\NetworkTransformer;

class SortingController extends ApiController
{

	protected $sortingService;

	public function __construct(SortingService $sortingService)
	{
		$this->middleware('jwt.auth');

		$this->sortingService = $sortingService;
	}

	/**
	 * Categories 
	 * 
	 * @return [type] [description]
	 */
	public function getCategories()
	{
		$categories = SortingCategory::all();

		return $this->respond(['categories' => $categories]);
	}
	
	/**
	 * Sort a connection
	 * 
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function sort(Request $request)
	{

		// validate sorting id and subjected id
		$this->validate($request, [
				'subject_id' => 'required|exists:networks,subject_id,follower_id,' . Auth::id(),
				'sorting_category_id' => 'required|exists:sorting_categories,id'
			]);

		try {

			$input = $request->input();

			$user = Auth::user();

			// get network with subject id and auth id
			$network = Network::where('follower_id', Auth::id())
								->where('subject_id', $input['subject_id'])
								->first();

			// get sorting category
			$category = SortingCategory::find($input['sorting_category_id']);

			// make reminder date based on sorting category ( service )
			$remind_at = $this->sortingService->formatReminderDate($category, $user->reminder_notification_time, $user->timezone);

			// delete any unsent reminders
			$network->sorting()->newPivotStatement()->where('sent', false)->where('network_id', $network->id)->delete();

			// create reminder from the object
			$network->sorting()->attach($category, ['remind_at'=> $remind_at]);

			return $this->respond(['message' => 'Sorted successfully']);

		} catch(\Exception $e) {

			return $this->respondInternalError(['message' => 'Could not sort', 'error' => $e->getMessage(), 'error_stack' => $e->getTrace(), 'line' => $e->getLine()]);
		}

	}

	public function unsort(Request $request)
	{
		$this->validate($request, [
				'subject_id' => 'required|exists:networks,subject_id,follower_id,' . Auth::id()
			]);

		$input = $request->input();

		try {

			$network = Network::where('follower_id', Auth::id())
								->where('subject_id', $input['subject_id'])
								->first();

			// create reminder from the object
			$network->sorting()->newPivotStatement()->where('sent', false)->where('network_id', $network->id)->delete();

			return $this->respond(['message' => 'Parked successfully']);

		} catch(\Exception $e) {

			return $this->respondInternalError(['message' => 'Could not park.', 'error' => $e->getMessage()]);

		}
	}

	/**
	 * [markActive description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function markActive(Request $request)
	{
		$this->validate($request, [
				'subject_id' => 'required'
			]);

		try {

			$network = Auth::user()->network_following()->where('subject_id', $request->get('subject_id'))->firstOrFail();

			$sorting = $network->sorting()->firstOrFail();

			$network->sorting()->updateExistingPivot($sorting->id, ['active' => true], false);

			$this->respond([
					'message' => 'Marked active'
				]);

		} catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

			return $this->respondInternalError(['message' => 'Could not mark active', 'error' => $e->getMessage() ]);

		} catch(Exception $e) {

			return $this->respondInternalError(['message' => 'Could not mark active', 'error' => $e->getMessage() ]);

		}
	}
}