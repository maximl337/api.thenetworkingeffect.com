<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Tag;
use App\User;
use App\Message;
use App\Article;
use App\Network;
use App\Roaming_user;
use App\Http\Requests;
use App\Http\Controllers\ApiController;
use App\Services\GeoService;
use App\Events\NewUserJoined;
use App\Services\ImageService;
use App\Services\SanitizeUrlService;
use App\Http\Controllers\Controller;
use App\Contracts\Image;

class LegacyDataMigrationController extends ApiController
{

    protected $imageService;

    protected $permalink;

    public function __construct(Image $imageService, SanitizeUrlService $permalink)
    {
        $this->imageService = $imageService;

        $this->permalink = $permalink;
    }

    public function tags(Request $request)
    {
        $this->validate($request, [
                'name' => 'required'
            ]);

        $input = $request->input();

       

        $tag = new Tag;

        $tag->name = str_slug($input['name']);

        $tag->original_id = $input['original_id'];

        $tag->save();

        return $this->respondCreated('Created');

        
        
        
        
    }

    // Migrate Users
    public function users(Request $request, GeoService $geoService, SanitizeUrlService $sanitizeUrl)
    {

            $this->validate($request, [
                    'first_name'    =>  'required',
                    'last_name'     =>  'required',
                    'email'         =>  'required|unique:users,email',
                    'password'      =>  'required'
                ]);

            $input = $request->input();

            $exists = User::where('original_id', $input['original_id'])->exists();

            if($exists) return $this->respond('user exists');

            $user = new User;

            $user->password     = $input['password'];

            $user->email        = $input['email'];

            $user->first_name   = $input['first_name'];

            $user->last_name    = $input['last_name'];

            $user->save();

            $roamingCoordinatesGiven = (!empty($input['lat']) && !empty($input['lng']));

            // check if roaming coordinates were sent
            if($roamingCoordinatesGiven) {

                Roaming_user::create([
                    'latitude'  => $request->get('lat'),
                    'longitude' => $request->get('lng'),
                    'user_id'   => $user->id
                ]); 

                $input['latitude']  = $request->get('lat');
                
                $input['longitude'] = $request->get('lng');

            // check if address return coordinates
            } else {

                if(isset($input['street'])
                && isset($input['city'])
                && isset($input['state'])) {

                $geoCode = $geoService
                            ->getLatLang($input['street'], $input['city'], $input['state'], $request->get('zip_code'), $request->get('country'));
                } else {

                    $geoCode = [
                       'latitude' => '',
                       'longitude' => '' 
                    ];
                }

                $input['latitude']  = $geoCode['latitude'];
                
                $input['longitude'] = $geoCode['longitude'];

                Roaming_user::create([
                    'latitude'  => $geoCode['latitude'],
                    'longitude' => $geoCode['longitude'],
                    'user_id'   => $user->id
                ]);

            }
            

            $input['permalink'] = $sanitizeUrl->makePrettyUrl($user);

            $user->update($input);

            //event(new NewUserJoined($user));

            return $this->respondCreated('user created');
            
    }

    public function userTags(Request $request)
    {
        $this->validate($request, [
                'original_tag_id'   => 'required',
                'original_user_id'  => 'required'
            ]);

        $input = $request->input();

        $user = User::where('original_id', $input['original_user_id'])->first();

        $tag = Tag::where('original_id', $input['original_tag_id'])->first();

        if($tag && $user) {

            $user->tags()->attach($tag->id);    

            return $this->respond('done');
        }

        return $this->respondNotFound('Tag or user not found');
        
    }

    public function articles(Request $request)
    {
        $this->validate($request, [
                    'title'             =>  'required',
                    'body'              =>  'required',
                    'original_user_id'  =>  'required',
                    'original_id'       =>  'required'
                ]);

        $input = $request->input();

        $user = User::where('original_id', $input['original_user_id'])->first();

        $input['user_id'] = $user->id;

        $article = Article::create($input);

        $permalink = $this->permalink->makePrettyUrl($article);

        $article->update([
                'permalink' => $permalink
            ]);

        return $this->respondCreated('Article Created');
    }

    public function articleTags(Request $request)
    {
        $this->validate($request, [
                'original_tag_id'       => 'required',
                'original_article_id'   => 'required'
            ]);

        $input = $request->input();

        $article = Article::where('original_id', $input['original_article_id'])->first();

        $tag = Tag::where('original_id', $input['original_tag_id'])->first();

        if($tag && $article) {

            $article->tags()->attach($tag->id);    

            return $this->respond('done');
        }

        return $this->respondNotFound('Tag or article not found');
        
    }

    public function messages(Request $request)
    {
        $this->validate($request, [
                'original_sender_id'    => 'required',
                'original_recipient_id' => 'required',
                'body'                  => 'required',
                'original_id'           => 'required'
            ]);

        $input = $request->input();

        $sender = User::where('original_id', $input['original_sender_id'])->first();

        $recipient = User::where('original_id', $input['original_recipient_id'])->first();

        if($sender && $recipient) {

            $input['from'] = $sender->id;

            $input['to'] = $recipient->id;

            $message = Message::create($input);

            return $this->respondCreated('Message created');
        }
        
        return $this->respondNotFound('User not found');
    }   

    public function deletedMessages(Request $request)
    {
        $this->validate($request, [
                'original_message_id' => 'required',
                'original_user_id'    => 'required'
            ]);

        $input = $request->input();

        

            $user = User::where('original_id', $input['original_user_id'])->first();

            $message = Message::where('original_id', $input['original_message_id'])->first();

        if($user && $message) {
            
            $message->deleted_messages()->attach($user->id);

            return $this->respond('deleted');
        }

        return $this->respondNotFound('User or Message not found');
    }

    public function network(Request $request)
    {
        $this->validate($request, [
                'original_follower_id' => 'required',
                'original_subject_id'  => 'required',
                'approved'             => 'required',
                'created_at'           => 'required'
            ]);

        $input = $request->input();

        $follower = User::where('original_id', $input['original_follower_id'])->first();

        $subject = User::where('original_id', $input['original_subject_id'])->first();

        if($follower && $subject) {

            $input['follower_id'] = $follower->id;

            $input['subject_id'] = $subject->id;

            $network = Network::create($input);

            if($input['approved'] == 1) {

                $input['follower_id'] = $subject->id;

                $input['subject_id'] = $follower->id;

                $networkBack = Network::create($input);

            }

            return $this->respondCreated('network created');
        }

        return $this->respondNotFound('follower or subject not found');
    }

    public function password(Request $request)
    {
        $this->validate($request, [
                'password' => 'required',
                'original_id' => 'required'
            ]);

        $input = $request->input();

        $user = User::where('original_id', $input['original_id'])->first();

        if($user) {

            $user->password = $input['password'];

            $user->save();

            return $this->respond('user updated');
        }

        return $this->respondNotFound('Not found');



    }

    public function userDate(Request $request)
    {
        $this->validate($request, [
                'created_at' => 'required',
                'original_id' => 'required'
            ]);

        $input = $request->input();

        $user = User::where('original_id', $input['original_id'])->first();

        if($user) {

            $user->created_at = $input['created_at'];

            $user->save();

            return $this->respond('user updated');
        }

        return $this->respondNotFound('Not found');
    }

    public function articleDate(Request $request)
    {
        $this->validate($request, [
                'created_at' => 'required',
                'original_id' => 'required'
            ]);

        $input = $request->input();

        $article = Article::where('original_id', $input['original_id'])->first();

        if($article) {

            $article->created_at = $input['created_at'];

            $article->save();

            return $this->respond('article updated');
        }

        return $this->respondNotFound('Not found');
    }

    public function updateUser(Request $request)
    {
        $this->validate($request, [
                'original_id' => 'required'
            ]);

        $input = $request->input();

        $user = User::where('original_id', $input['original_id'])->first();

        if($user) {

            $user->business_name = $request->get('business_name');
            $user->street = $request->get('street');
            $user->city = $request->get('city');
            $user->state = $request->get('state');
            $user->country = $request->get('country');
            $user->zip_code = $request->get('zip_code');
            $user->logo = $request->get('logo');
            $user->about_business = $request->get('about_business');
            
            $user->save();

            return $this->respond('user updated');
        }

        return $this->respondNotFound('Not found');
    }

    public function messageDate(Request $request)
    {
        $this->validate($request, [
                'created_at' => 'required',
                'original_id' => 'required'
            ]);

        $input = $request->input();

        $message = Message::where('original_id', $input['original_id'])->first();

        if($message) {

            $message->created_at = $input['created_at'];

            $message->save();

            return $this->respond('message updated');
        }

        return $this->respondNotFound('Not found');
    }

}
