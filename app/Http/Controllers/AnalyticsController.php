<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Services\GeoService;
use App\Http\Requests;
use App\Http\Controllers\ApiController;

class AnalyticsController extends ApiController
{
    public function getUserWithCities(Request $request)
    {

    	$limit = $request->get('limit') ?: 100;

    	$offset = $request->get('offset') ?: 100;

    	// get users
    	$users = DB::table('users')
    				->select('id', 'first_name', 'last_name', 'email', 'city', 'latitude', 'longitude')
    				->skip($offset)
    				->take($limit)
    				->get();

    	
    	foreach($users as $user) {

    		if(empty($user->city)) {

    			$user->city = (new GeoService)->getUserCityByCoordinates($user->id);
    			
    		}
    	}	

    	return view('analytics.user-cities', compact('users'));

    }
}
