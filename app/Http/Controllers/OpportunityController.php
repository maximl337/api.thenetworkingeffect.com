<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;
use App\User;
use App\Opportunity;
use App\Http\Requests;
use App\Http\Controllers\ApiController;
use App\Transformers\OpportunityTransformer;

class OpportunityController extends ApiController
{

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Add the given id to opportunities
     * 
     * @param [type] $id [description]
     * @return 
     */
    public function add(Request $request)
    {
        try {
            
            $this->validate($request, [
                    'subject_id' => 'exists:users,id|not_in:' . Auth::id()
                ],
                [
                    'subject_id.not_in' => 'Cannot add self to opportunities'
                ]);

            $exists = Auth::user()->opportunities()->where('subject_id', $request->get('subject_id'))->exists();

            if(!$exists) {

                Auth::user()->opportunities()->save(new Opportunity(['subject_id' => $request->get('subject_id')]));
            }

            return $this->respondCreated("Opportunity added");

        } catch (\Exception $e) {
              
            return $this->respondInternalError($e->getMessage());
        }  
    }

    /**
     * [remove description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function remove($id)
    {
        try {
            
            Auth::user()->opportunities()->where('subject_id', $id)->delete();

            return $this->respond("Opportunity removed");

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound($e->getMessage());

        } catch (\Exception $e) {
            
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function getOpportunities(Request $request, OpportunityTransformer $transformer)
    {
        try {

            $limit = $request->get('limit') ?: 9;

            $offset = $request->get('offset') ?: 0;

            $query = $request->get('query') ?: '';

            $query = '%' . $query . '%';
            
            // $opportunities = Auth::user()
            //                         ->opportunities()
            //                         ->with(['subject' => function($q) use ($param) {
            //                             $q->where('first_name', 'LIKE', '%'.$param.'%')
            //                                         ->orWhere('last_name', 'LIKE', '%'.$param.'%');
            //                         }])
            //                         ->take($limit)
            //                         ->skip($offset)
            //                         ->get()
            //                         ->toArray();

            $opportunities = DB::select("SELECT 
                                        * 
                                        FROM 
                                        `opportunities` 
                                        WHERE
                                        `follower_id` = ?
                                        AND 
                                        `subject_id`
                                        IN
                                        (SELECT `id`
                                        FROM
                                        `users`
                                        WHERE
                                        `deleted_at` IS NULL
                                        AND
                                        (
                                        `first_name` LIKE ?
                                        OR
                                        `last_name` LIKE ?
                                        OR
                                        CONCAT(`first_name`, ' ', `last_name`) LIKE ?
                                        OR
                                        `job_title` LIKE ?
                                        OR
                                        `business_name` LIKE ?
                                        OR
                                        `business_category` LIKE ?
                                        )) LIMIT ? OFFSET ?", [Auth::id(), $query, $query, $query, $query, $query, $query, $limit, $offset]);

            
            return $this->respond([
                    "data" => $transformer->transformCollection($opportunities)
                ]);

        } catch (Exception $e) {
            
            return $this->respondInternalError($e->getMessage());
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        try {

            $opportunities = DB::table('opportunities as o')
                             ->leftJoin('users as u1', 'o.follower_id', '=', 'u1.id')
                             ->leftJoin('users as u2', 'o.subject_id', '=', 'u2.id')
                             ->select(  'o.*', 
                                        'u1.first_name as follower_fname', 
                                        'u1.last_name as follower_lname',
                                        'u2.first_name as subject_fname',
                                        'u2.last_name as subject_lname')
                             ->orderBy('o.created_at', 'desc')
                             ->get();
            
        } catch(\Illuminate\Database\QueryException $e) {

            return $this->respondInternalError($e->getMessage);

        } catch (\Exception $e) {
            
            return $this->respondInternalError($e->getMessage());
        }

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
