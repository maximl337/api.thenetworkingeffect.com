<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Log;
use App\Http\Requests;
use App\Utilities\Timezone;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class MiscController extends ApiController
{

	public function __construct()
    {
        $this->middleware('jwt.auth', ['only' => ['getHelp']]);
    }

    public function getTimezones()
    {
        $timezones = Timezone::getAll();

        return $this->respond([
                'timezones' => $timezones
            ]);
    }

    public function getHelp(Request $request)
    {
    	$this->validate($request, [
                'body' => 'required|max:500'
            ]);

        $input = $request->input();

        $user = Auth::user();

        $body = $request->get('body');

        try {

        	Mail::send('emails.help', compact('body'), function ($m) use ($user) {
            	$m->from($user->email, $user->first_name);

            	$m->to('support@thenetworkingeffect.com', 'Member Care')->subject('Help ticket');
        	});

        	return $this->respond(['message' => "Help ticket submitted"]);

        } catch(\Exception $e) {

        	Log::error('Get Help', [$e->getMessage()]);
        	
        	return $this->respondInternalError("Could not submit support ticket. There was an internal error");
        }

        
    }
    
}
