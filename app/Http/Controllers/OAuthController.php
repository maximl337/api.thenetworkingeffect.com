<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;
use Hash;
use Authorizer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class OAuthController extends Controller
{
    public function __construct()
	{
	 	//$this->middleware('oauth', ['only' => ['getUser']]);

		$this->middleware('csrf', ['only' => ['create', 'authenticate']]);
	}

	/**
	 * [verify description]
	 * @param  [type] $username [description]
	 * @param  [type] $password [description]
	 * @return [type]           [description]
	 */
   	public function verify($username, $password)
   	{
   		$credentials = [
   			"email" => $username,
   			"password" => $password
   		];

   		return Auth::attempt($credentials)
            ? Auth::id()
            : false;

   	}

   	/**
   	 * [register description]
   	 * 
   	 * @return [type] [description]
   	 */
   	public function register()
   	{
   		return view('oauth.register');
   	}

   	/**
   	 * [create description]
   	 * @param  Request $request [description]
   	 * @return [type]           [description]
   	 */
   	public function create(Request $request)
   	{

   		$this->validate($request, [
   				'name' => 'required',
   				'email' => 'required|unique:oauth_clients',
   				'password' => 'required|confirmed'
   			]);

   		try {
   			
   			//generate secret and id
   			$id = md5($request->get('email') . env('APP_KEY') . uniqid(rand()));

   			$secret = md5($request->get('email')).str_random().uniqid(rand());

   		    DB::table('oauth_clients')
		   		        ->insertGetId([
		 						'id' => $id,
		 						'secret' => $secret,
		 						'name' => $request->get('name'),
		 						'email' => $request->get('email'),
		 						'password' => bcrypt($request->get('password'))
		   		        	]);

		   	$user = DB::table('oauth_clients')->select('*')->where('id', $id)->first();

		   	return view('oauth.index', compact('user'));

   		} catch (Exception $e) {
   			
   			return redirect()->back()->withErrors([$e->getMessage()]);	
   		}
   	
   	}

   	/**
   	 * [login description]
   	 * @return [type] [description]
   	 */
   	public function login()
   	{
   		return view('oauth.login');
   	}

   	/**
   	 * [authenticate description]
   	 * @param  Request $request [description]
   	 * @return [type]           [description]
   	 */
   	public function authenticate(Request $request)
   	{
   		$this->validate($request, [
   				'email' => 'required|email|exists:oauth_clients',
   				'password' => 'required'
   			], [
   				'email.exists' => "Email not found"
   			]);

   		try {

   			$user = DB::table('oauth_clients')
			   			->select('*')
			   			->where('email', $request->get('email'))
			   			->first();
   			
	   		if(!$user) throw new \Exception("User not found");

			if (Hash::check($request->get('password'), $user->password))
			{
				
				return view('oauth.index', compact('user'));

			} else {

				throw new \Exception("Invalid credentials");
			}

   		} catch(\Exception $e) {

   			return redirect()->back()->withErrors([$e->getMessage()]);
   		}
   		
   	}
}
