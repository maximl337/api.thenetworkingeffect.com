<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;
use App\Tag;
use App\User;
use App\Event;
use App\Roaming_user;
use App\Http\Requests;
use App\Contracts\Search;
use AlgoliaSearch\Client;
use App\Events\EventJoin;
use App\Events\EventUpdate;
use App\Events\EventDelete;
use App\Events\UserUpdated;
use App\Utilities\Timezone;
use App\Events\EventCreated;
use App\Services\GeoService;
use App\Services\TagService;
use App\Events\EventUserLeft;
use App\Services\ImageService;
use App\Events\AdminEventAddUser;
use App\Http\Controllers\Controller;
use App\Services\SanitizeUrlService;
use App\Transformers\UserTransformer;
use App\Transformers\EventTransformer;
use App\Http\Requests\MakeEventRequest;
use App\Http\Controllers\ApiController;
use App\Http\Requests\UpdateEventRequest;
use App\Http\Requests\DeleteEventRequest;
use App\Transformers\UserSearchTransformer;
use App\Transformers\EventSearchTransformer;
use App\Transformers\AlgoliaEventTransformer;

class EventController extends ApiController
{

    protected $indices;

    protected $algolia;

    protected $imageService;

    protected $geoService;

    protected $tagService;

    protected $userTransformer;

    protected $userSearchTransformer;

    protected $algoliaTransformer;

    protected $eventTransformer;

    protected $search;

    public function __construct(ImageService $imageService, GeoService $geoService, TagService $tagService, UserTransformer $userTransformer, UserSearchTransformer $userSearchTransformer, Search $search, AlgoliaEventTransformer $algoliaTransformer, EventTransformer $eventTransformer)
    {

        $this->middleware('jwt.auth', ['except' => ['searchPublic', 'getEventPublic', 'getByPermalink']]);

        $this->middleware('event.owner', ['only' => ['updateEvent', 'deleteEvent', 'addUser', 'removeUser']]);

        $this->algolia = new Client(getenv('ALGOLIA_APP_ID'), getenv('ALGOLIA_SECRET_KEY'));

        $this->imageService = $imageService;

        $this->geoService = $geoService;

        $this->tagService = $tagService;

        $this->userTransformer = $userTransformer;

        $this->userSearchTransformer = $userSearchTransformer;

        $this->search = $search;

        $this->algoliaTransformer = $algoliaTransformer;

        $this->eventTransformer = $eventTransformer;

        $this->indices = getenv('ALGOLIA_EVENTS_INDEX');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getEvents(Request $request)
    {
        $limit = $request->get('limit') ?: 20;

        $page = $request->get('page') ?: 0;

        $events = Event::with(['tags' => function ($query) use ($limit, $page) {
                               $query->skip($limit * ($page-1))
                                    ->take($limit);
                                }])->paginate($limit);


        return $this->respond($events);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getEvent($id)
    {
        try {

            $event = Event::findOrFail($id);

            return $this->respond([
                    'data' => $this->eventTransformer->transform($event->toArray())
                ]);

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound('Event with that ID not found');

        } catch(\Exception $e) {

            return $this->respondInternalError("There was an Internal error:" . $e->getMessage());
            
        }
        
    }

    /**
     * get Event by id for public
     * 
     * @param  int $id Event id
     * @return App\Event    
     */
    public function getEventPublic($id)
    {
        $event = Event::findOrFail($id);

        return $this->respond([
                'data' => $this->eventTransformer->transform($event->toArray())
            ]);
    }

    /**
     * Get event by permalink
     * 
     * @param  str $permalink 
     * @return App\Event
     */
    public function getByPermalink($permalink)
    {
        $event = Event::where('permalink', $permalink)->first();

        if($event) {

            return $this->respond([
                'data' => $this->eventTransformer->transform($event->toArray())
            ]);
        }

        return $this->respondNotFound('No event found.');
    }

    /**
     * Make a new event
     * 
     * @param  MakeEventRequest   $request     
     * @param  SanitizeUrlService $sanitizeUrl
     * @return Response
     */
    public function makeEvent(MakeEventRequest $request, SanitizeUrlService $sanitizeUrl)
    {

        try {

            $input = $request->input();

            $input['user_id'] = Auth::user()->id;

            $street = isset($input['street']) ? $input['street'] : $input['venue'];

            $geoCode = $this->geoService
                        ->getLatLang($street, $input['city'], $input['state'], $request->get('zip_code'), $request->get('country'));

            if( empty($geoCode['latitude']) || empty($geoCode['longitude']) )
                    return response()->json([
                            'error' => [
                                'message' => 'Given address could not be converted to geo coordinates. Please provide a valid address', 
                                'status' => '422'
                            ]
                        ], 422);

            $input['latitude']  = $geoCode['latitude'];

            $input['longitude'] = $geoCode['longitude'];

            if($request->hasFile('banner_image')) 
                
                $input['banner_url'] = $this->imageService->upload($request->file('banner_image')->getRealPath());

            $event = Event::create($input);

            $event->update([
                    'permalink' => $sanitizeUrl->makePrettyUrl($event)
                ]);

            $event->users()->attach(Auth::user()->id, ['is_owner' => true]);

            $event->tags()->attach($this->tagService->getTagIds($input['tags']));

            event(new EventCreated($event));

            return $this->respondCreated('Event Created', [
                    'event' => $event
                ]);

        } catch(Exception $e) {

            return $this->respondInternalError('Could not create event');

        }

    }

    /**
     * Get authenticated users event
     * 
     * @param  Request $request
     * @return Response
     */
    public function myEvents(Request $request)
    {
        $limit = $request->get('limit') ?: 20;

        $page = $request->get('limit') ?: 0;

        $user = Auth::user();

        $userEvents = User::findOrFail($user->id)->my_events()->with(['tags' => function ($query) use ($limit, $page) {
                               $query->skip($limit * ($page-1))
                                    ->take($limit);
                                }])->paginate($limit);


        return $this->respond($userEvents);
    }

    /**
     * Update event
     * 
     * @param  int             $id
     * @param  UpdateEventRequest $request     
     * @param  SanitizeUrlService $sanitizeUrl 
     * @return Response
     */
    public function updateEvent($id, UpdateEventRequest $request, SanitizeUrlService $sanitizeUrl)
    {
        $event = Event::find($id);

        $input = $request->input();

        $street = isset($input['street']) ? $input['street'] : $input['venue'];

        $geoCode = $this->geoService
                    ->getLatLang($street, $input['city'], $input['state'], $request->get('zip_code'), $request->get('country'));

        if( empty($geoCode['latitude']) || empty($geoCode['longitude']) )
                    return response()->json([
                            'error' => [
                                'message' => 'Given address could not be converted to geo coordinates. Please provide a valid address', 
                                'status' => '422'
                            ]
                        ], 422);
        
        $input['latitude']  = $geoCode['latitude'];
        
        $input['longitude'] = $geoCode['longitude'];

        if($request->hasFile('banner_image'))
       
            $input['banner_url'] = $this->imageService->upload($request->file('banner_image')->getRealPath());

        $input['permalink'] = $sanitizeUrl->makePrettyUrl($event);

        $event->update($input);

        $event->tags()->detach();

        $event->tags()->attach($this->tagService->getTagIds($input['tags']));

        if(isset($input['remove_banner']) && $input['remove_banner'] == true) 
                $event->update([
                        'banner_url' => ''
                    ]);

        event(new EventUpdate($event));

        return $this->respond([
                'message' => 'Event updated',
                'event' => $event->toArray()
            ]);
    }

    /**
     * Delete an event
     * 
     * @param  int             $id      Events id
     * @param  DeleteEventRequest $request Request Validation
     * @return Response
     */
    public function deleteEvent($id, DeleteEventRequest $request)
    {
        $event = Event::find($id);

        event(new EventDelete($event));

        return $this->respond([
                'message' => 'Event deleted'
            ]);
    }

    
    /**
     * Join an event
     * @param  int $id Event id
     * @return Response
     */
    public function join($id)
    {
        try {

            $event = Event::findOrFail($id);

            $user = Auth::user();

            $alreadyJoined = $event->users()->where('user_id', $user->id)->exists();

            if($alreadyJoined) {

                return $this->respondValidationError('You have already joined this event');
            }

            $event->users()->attach($user->id);

            $data = [
                    'event' => $event, 
                    'user'  => Auth::user()
                ];

            event(new EventJoin($data));

            return $this->respondCreated('Joined Event');

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return $this->respondNotFound("Event not found");

        } catch(\Exception $e) {

            return $this->respondInternalError("There was an internal error");
        }

    }

    /**
     * Leave an event 
     * 
     * @param  int $id Event id
     * @return Response
     */
    public function leave($id)
    {
        $event = Event::find($id);

        $user = Auth::user();

        $event->users()->detach($user->id);

        $data = [
                'event' => $event,
                'user' => $user
            ];

        event(new EventUserLeft($data));

        return $this->respond(['message' => 'Left event']);
    }

    /**
     * Add a user to an event
     * 
     * @param int  $id Event id
     * @param Request $request
     * @return Response
     */
    public function addUser($id, Request $request)
    {
        
        if(!$request->get('user_id')) 
            return $this->respondValidationError('User id not given.');

        $input = $request->input();

        $event = Event::find($id);
        
        try {

            $exists = $event->users()->where('user_id', $input['user_id'])->exists();

            if($exists) {

                return $this->respondValidationError('User already added to the event');
            }

            $event->users()->attach($input['user_id']);

            $attendees = $event->users()->with('network_following')->latest()->get();

            //Build data for admin add
            $data = [
                'event' => $event, 
                'user'  => User::find($input['user_id'])
            ];

            event(new EventJoin($data));

            return $this->respondCreated('User added to event', [
                    'attendees' => $attendees,
                    'event'     => $event
                ]);
        
        } catch(Exception $e) {

            return $this->respondInternalError('Could not add user');
        }
    }

    /**
     * Remove attendee from event
     * 
     * @param  int  $id Event id
     * @param  Request $request 
     * @return Response
     */
    public function removeUser($id, Request $request)
    {   
        if(!$request->get('user_id')) 
            return $this->respondValidationError('User id not given.');

        $input = $request->input();

        $event = Event::find($id);

        try {

            $event->users()->detach($input['user_id']);

            // build data for event
            $data = [
                'event' => $event,
                'user' => User::find($input['user_id'])
            ];

            event(new EventUserLeft($data));

            return $this->respond(['message' => 'User removed']);
        
        } catch(Exception $e) {

            return $this->respondInternalError('Could not remove user');
        }
    }

    /**
     * Get event user created
     * 
     * @param  id  $id User id
     * @param  Request $request
     * @return Response
     */
    public function userEvents($id, Request $request)
    {
        $limit = $request->get('limit') ?: 20;

        $page = $request->get('page') ?: 0;

        $userEvents = User::findOrFail($id)->my_events()->with(['tags' => function ($query) use ($limit, $page) {
                               $query->skip($limit * ($page-1))
                                    ->take($limit);
                                }])->paginate($limit);

        return $this->respond($userEvents);
    }

    /**
     * Get event user is attending
     * 
     * @param  Request $request
     * @return Response
     */
    public function eventsAuthenticatedUserIsAttending(Request $request)
    {
        $limit = $request->get('limit') ?: 20;

        $page = $request->get('page') ?: 0;

        // Timezone offset
        $original_tz = date_default_timezone_get();

        date_default_timezone_set("America/Toronto");

        $timestamp = date('Y-m-d H:i:s');

        date_default_timezone_set($original_tz);

        $condition = $request->get('past_events')
                        ? '<'
                        : '>';
    
        $events = Auth::user()->events()
                            ->with('tags')
                            ->where(DB::raw('CONCAT(event_date, " ", end_time)'), $condition, $timestamp)
                            ->orderBy(DB::raw('CONCAT(event_date, " ", end_time)'), 'ASC')
                            ->paginate($limit);

        return $this->respond($events);
    }

    /**
     * Get Event Users
     * 
     * @param  int  $id Event id
     * @param  Request $request 
     * @return Response
     */
    public function eventUsers($id, Request $request)
    {
        $limit = $request->get('limit') ?: 1000;

        $users = Event::find($id)->users()
                                    ->with('articles')
                                    ->with('testimonials')
                                    ->with(['tags' => function($q) { $q->select('name'); }])
                                    ->orderBy('event_user.created_at', 'desc')
                                    ->paginate($limit);

        foreach($users as $user) {

            if(Auth::check()) {
        
                $user->distance = $this->geoService->getDistaneBetweenTwoUsers(Auth::id(), $user->id);
                            
            } // check Auth
        }

        return $this->respond($this->utf8_encode_deep($users));
    }

    /**
     * Get users of multiple events
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function eventsUsers(Request $request)
    {
        $input = $request->input();

        if(! is_array($input['event_ids']) )
            return $this->respondValidationError('Please provide an array of event ids');

        $eventIds = $input['event_ids'];
        
        $users = User::whereHas('events', function ($q) use ($eventIds) { 
                                    $q->whereIn('id', $eventIds);
                                })
                                ->with(['tags' => function($q) { $q->select('name'); }])
                                ->get()
                                ->toArray();

        return $this->respond($this->userTransformer->transformCollection($this->utf8_encode_deep($users)));
    }

    /**
     * Search events no auth
     * 
     * @param  Request $request 
     * @return mixed
     */
    public function searchPublic(Request $request, Timezone $timezone)
    {
        $pre_range = 100000;

        $range = (int) $pre_range * 1000;

        $timestamp = $timezone->get_normalized_unix_timestamp();

        $args = [
            "hitsPerPage" => $request->get('hitsPerPage') ?: 20, 
            "page" => $request->get('page') ?: 0,
            //"numericFilters" => "event_date>=".$timestamp
        ];

        $args["numericFilters"] = $request->get('past_events')
                                    ? "event_date<".$timestamp
                                    : "event_date>=".$timestamp;


        if($request->get('lat') && $request->get('lng')) {

            $args["aroundLatLng"] = $request->get('lat') . "," . $request->get('lng');

            $args["aroundRadius"] = $range;
        }

        $results = $this->search->index($this->indices)->search($request->get('query'), $args); 

        return $results;
    }

    /**
     * Search events 
     * 
     * @param  Request                $request               
     * @param  EventSearchTransformer $eventSearchTransformer 
     * @return mixed
     */
    public function search(Request $request, EventSearchTransformer $eventSearchTransformer, Timezone $timezone)
    {

        $pre_range = 100000;

        $range = (int) $pre_range * 1000;

        $timestamp = $timezone->get_normalized_unix_timestamp();

        $args = [
            "hitsPerPage" => $request->get('hitsPerPage') ?: 20, 
            "page" => $request->get('page') ?: 0,
            //"numericFilters" => "event_date>=".$timestamp
        ];

        $args["numericFilters"] = $request->get('past_events')
                                    ? "event_date<".$timestamp
                                    : "event_date>=".$timestamp;


        if($request->get('geo')) {

            $user = Auth::user();

            if(!$user->latitude || !$user->longitude) 
                return $this->respondValidationError('User does not have coordinates attached to their profile to perform a Geo search');
            
            $args["aroundLatLng"] = $user->latitude . "," . $user->longitude;

            $args["aroundRadius"] = $range;

        }

        if($request->get('lat') && $request->get('lng')) {

            $args["aroundLatLng"] = $request->get('lat') . "," . $request->get('lng');

            $args["aroundRadius"] = $range;

            $exists = Roaming_user::where([
                            'latitude'  => $request->get('lat'),
                            'longitude' => $request->get('lng'),
                            'user_id'   => Auth::id()
                        ])->exists();

            if(!$exists) {
                
                Roaming_user::create([
                        'latitude'  => $request->get('lat'),
                        'longitude' => $request->get('lng'),
                        'user_id'   => Auth::id()
                    ]);

                event(new UserUpdated(Auth::user()));
            }
        }

        $results = $this->search->index($this->indices)->search($request->get('query'), $args); 

        return $eventSearchTransformer->transformCollection($results);

    }

    /**
     * Search from users of events
     * 
     * @param  Request $request 
     * @return Response
     */
    public function eventsUsersSearch(Request $request)
    {
        $index = $this->algolia->initIndex(getenv('ALGOLIA_USERS_INDEX'));

        $hitsPerPage = $request->get('hitsPerPage') ?: 20;

        $page = $request->get('page') ?: 0;

        $geo = $request->get('geo') ?: 0;

        $lat = $request->get('lat') ?: 0;

        $lng = $request->get('lng') ?: 0;

        $eventIds = $request->get('event_ids');

        $pre_range = 1000;

        $range = (int) $pre_range * 1000;

        if(!is_array($eventIds))
                return $this->respondValidationError('Event Ids must be sent as an array');
        
        if(!count($eventIds))
                return $this->respondValidationError('No event ids given');

        $users = User::whereHas('events', function ($q) use ($eventIds) { 
                        $q->whereIn('id', $eventIds);
                    })->get()->toArray();

        $numericFilter = "(";

        $i=0;

        foreach($users as $user) {

            $numericFilter .= 'id=' . $user['id'];

            $i++;

            if($i < count($users)) $numericFilter .= ", ";
        }

        $numericFilter .= ")";
        
        if($geo) {

            $user = User::find(Auth::id());

            if(!$user->latitude || !$user->longitude) {

                return $this->respondValidationError('User does not have coordinates attached to their profile to perform a Geo search');
            }

            $coordinates = $user->latitude . "," . $user->longitude;

            $result = $index->search($request->get('query'),
                                    array(
                                      "numericFilters" => $numericFilter,
                                        "aroundLatLng" => $coordinates, 
                                        "aroundRadius" => $range, 
                                        "hitsPerPage" => $hitsPerPage, 
                                        "page" => $page
                                    ));

            return $this->userSearchTransformer->transformCollection($this->utf8_encode_deep($result));
        }

        if($lat && $lng) {
        
            $coordinates = $lat . "," . $lng;

            $result = $index->search($request->get('query'), 
                                    array(
                                        "numericFilters" => $numericFilter,
                                        "aroundLatLng" => $coordinates, 
                                        "aroundRadius" => $range, 
                                        "hitsPerPage" => $hitsPerPage, 
                                        "page" => $page
                                    ));

            return $this->userSearchTransformer->transformCollection($this->utf8_encode_deep($result));

        }

        $result = $index->search($request->get('query'),
                                array(
                                    "numericFilters" => $numericFilter,
                                    "hitsPerPage" => $hitsPerPage, 
                                    "page" => $page,
                                    ));

        return $this->userSearchTransformer->transformCollection($this->utf8_encode_deep($result));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(MakeEventRequest $request, SanitizeUrlService $sanitizeUrl)
    {
        try {

            $input = $request->input();

            if(!isset($input['user_id'])) return $this->respondValidationError('Please provide a user ID');

            $street = isset($input['street']) ? $input['street'] : $input['venue'];

            $geoCode = $this->geoService
                        ->getLatLang($street, $input['city'], $input['state'], $request->get('zip_code'), $request->get('country'));

            if( empty($geoCode['latitude']) || empty($geoCode['longitude']) )
                    return response()->json([
                            'error' => [
                                'message' => 'Given address could not be converted to geo coordinates. Please provide a valid address', 
                                'status' => '422'
                            ]
                        ], 422);

            $input['latitude']  = $geoCode['latitude'];

            $input['longitude'] = $geoCode['longitude'];

            if($request->hasFile('banner_image')) 
                
                $input['banner_url'] = $this->imageService->upload($request->file('banner_image')->getRealPath());

            $event = Event::create($input);

            $event->update([
                    'permalink' => $sanitizeUrl->makePrettyUrl($event)
                ]);

            $event->users()->attach($input['user_id'], ['is_owner' => true]);

            $event->tags()->attach($this->tagService->getTagIds($input['tags']));

            event(new EventCreated($event));

            return $this->respondCreated('Event Created', [
                    'event' => $event
                ]);

        } catch(Exception $e) {

            return $this->respondInternalError('Could not create event');

        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, UpdateEventRequest $request, SanitizeUrlService $sanitizeUrl)
    {
        $event = Event::find($id);

        $input = $request->input();

        if(!isset($input['user_id'])) return $this->respondValidationError('Please provide a user ID');

        // New Owner
        if($input['user_id'] != $event->user_id) {

            $event->users()->detach($event->user_id);
            
            $event->users()->attach($input['user_id'], ['is_owner' => true]);
        }

        $street = isset($input['street']) ? $input['street'] : $input['venue'];

        $geoCode = $this->geoService
                    ->getLatLang($street, $input['city'], $input['state'], $request->get('zip_code'), $request->get('country'));

        if( empty($geoCode['latitude']) || empty($geoCode['longitude']) )
                    return response()->json([
                            'error' => [
                                'message' => 'Given address could not be converted to geo coordinates. Please provide a valid address', 
                                'status' => '422'
                            ]
                        ], 422);
        
        $input['latitude']  = $geoCode['latitude'];
        
        $input['longitude'] = $geoCode['longitude'];

        if($request->hasFile('banner_image'))
       
            $input['banner_url'] = $this->imageService->upload($request->file('banner_image')->getRealPath());

        $input['permalink'] = $sanitizeUrl->makePrettyUrl($event);

        $event->update($input);

        $event->tags()->detach();

        $event->tags()->attach($this->tagService->getTagIds($input['tags']));

        if(isset($input['remove_banner']) && $input['remove_banner'] == true) 
                $event->update([
                        'banner_url' => ''
                    ]);

        event(new EventUpdate($event));

        return $this->respond([
                'message' => 'Event updated',
                'event' => $event->toArray()
            ]);
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        $events = Event::withTrashed()
                            ->with('user')
                            ->with('users')
                            ->get();

        return $this->respond($events);
    }

    /**
     * [show description]
     * @param  [type] $event [description]
     * @return [type]        [description]
     */
    public function show($event)
    {
        $event = Event::withTrashed()->findOrFail($event);

        $result['event'] = $event;

        $result['attendees'] = $event->users()
                                        ->with('network_following')
                                        ->with('sent_messages')
                                        ->with('sent_testimonials')
                                        ->with('notes')
                                        ->with('sent_card_shares')
                                        ->with('articles')
                                        ->get();

        foreach($result['attendees'] as $user) {

            $user->mutual_connections =  DB::select("SELECT * FROM `networks` WHERE 
                                                    `follower_id` = ? 
                                                    AND
                                                    `subject_id` IN (SELECT `follower_id` FROM `networks` WHERE `subject_id` = ?) GROUP BY `subject_id`", [$user->id, $user->id]);

        }

        $result['tags'] = $event->tags()->latest()->get();

        $result['allTags'] = DB::table('tags')->select('name')->get();

        $result['allUsers'] = DB::table('users')->select('id', 'first_name', 'last_name', 'email')->whereNull('deleted_at')->get();

        return $this->respond($result);
         
    }

    public function attendees($id)
    {
        $event = Event::withTrashed()->findOrFail($id);

        $result['event'] = $event;

        $result['attendees'] = $event->users()
                                        ->with('network_following')
                                        ->get();

        $result['tags'] = $event->tags()->latest()->get();

        return $this->respond($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);

        event(new EventDelete($event));

        return $this->respond([
                'message' => 'Event deleted'
            ]);
    }

    /**
     * [restore description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function restore($id)
    {
        $event = Event::withTrashed()->findOrFail($id);

        $event->restore();

        event(new EventCreated($event));

        return $this->respond("Event restored");
    }

}
