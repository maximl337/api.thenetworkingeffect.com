<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Tag;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Transformers\AssociationTagTransformer;

class TagController extends ApiController
{

    protected $tagService;

    public function __construct()
    {
        $this->middleware('jwt.auth', ['only' => ['getPopularTags']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $limit = $request->get('limit') ?: 20;

        $tags = Tag::paginate($limit);

        return $this->respond($tags);
        
    }

    /**
     * Return all Business Categories
     * 
     * @return [type] [description]
     */
    public function getBusinessCategories()
    {
        

        $data = [
            'data' => [
                [ 'name' => 'Advertising & Marketing' ],
                [ 'name' => 'Agriculture' ],
                [ 'name' => 'Apparel' ],
                [ 'name' => 'Associations/Non-Profits' ],
                [ 'name' => 'Automotive Services' ],
                [ 'name' => 'Beverages' ],
                [ 'name' => 'Building & Construction' ],
                [ 'name' => 'Business Services' ],
                [ 'name' => 'Computer Hardware' ],
                [ 'name' => 'Computer Software' ],
                [ 'name' => 'Consumer Products & Services' ],
                [ 'name' => 'E-Commerce & IT Outsourcing' ],
                [ 'name' => 'Educational Services' ],
                [ 'name' => 'Electrical' ],
                [ 'name' => 'Electronics' ],
                [ 'name' => 'Energy & Resources' ],
                [ 'name' => 'Environmental Markets' ],
                [ 'name' => 'Fabrication' ],
                [ 'name' => 'Financial' ],
                [ 'name' => 'Food' ],
                [ 'name' => 'Food Processing' ],
                [ 'name' => 'Furnishings' ],
                [ 'name' => 'Government' ],
                [ 'name' => 'Healthcare' ],
                [ 'name' => 'Housing' ],
                [ 'name' => 'Information Technology' ],
                [ 'name' => 'Insurance' ],
                [ 'name' => 'Legal' ],
                [ 'name' => 'Machinery & Equipment' ],
                [ 'name' => 'Manufacturing' ],
                [ 'name' => 'Materials & Chemicals' ],
                [ 'name' => 'Media' ],
                [ 'name' => 'Medical Equipment & Device' ],
                [ 'name' => 'Pharmaceuticals' ],
                [ 'name' => 'Printing & Publishing' ],
                [ 'name' => 'Professional Services' ],
                [ 'name' => 'Real Estate' ],
                [ 'name' => 'Restaurants & Bars' ],
                [ 'name' => 'Shopping & Stores' ],
                [ 'name' => 'Telecommunications & Wireless ' ],
                [ 'name' => 'Textiles' ],
                [ 'name' => 'Transportation & Shipping' ],
                [ 'name' => 'Travel & Leisure' ],
                [ 'name' => 'Warehousing & Storage' ]
            ]
        ];
        //$tags = Tag::where('business_category', true)->paginate($limit);

        return $this->respond($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]); 

        $input = $request->input();

        $input['name'] = str_slug($input['name']);

        $exists = Tag::where('name', $input['name'])->exists();

        if($exists) return $this->respondValidationError('Tag with that name already exists');

        $input['business_category'] = $request->get('business_category') ? true : false;

        $input['association'] = $request->get('association') ? true : false;

        $input['search_assist'] = $request->get('search_assist') ? true : false;

        $input['note_assist'] = $request->get('note_assist') ? true : false;

        $tag = Tag::create($input);

        return $this->respondCreated('Tag created', [
                'tag' => $tag
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $tag = Tag::findOrFail($id);

        return $this->respond($tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]); 

        $input = $request->input();

        $input['business_category'] = $request->get('business_category') ? true : false;

        $input['association'] = $request->get('association') ? true : false;

        $input['search_assist'] = $request->get('search_assist') ? true : false;

        $input['note_assist'] = $request->get('note_assist') ? true : false;

        $tag = Tag::findOrFail($id);

        $input['name'] = str_slug($input['name']);

        $tag->update($input);

        return $this->respond('Tag updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);

        $tag->delete();

        return $this->respond('Tag was deleted');
    }

    /**
     * Get tags by popularity
     *         
     * @return Illuminate\Http\Response
     */
    public function getPopularTags()
    {
        $popular = DB::select('SELECT t.`id`, t.`name`, t.`created_at`, count(tu.`tag_id`) as tag_count 
                                FROM `tags` t 
                                LEFT JOIN `tag_user` tu 
                                ON 
                                t.`id` = tu.`tag_id`
                                GROUP BY
                                t.`name`
                                HAVING
                                tag_count > ?
                                ORDER BY
                                tag_count DESC', [0]);

        return $this->respond(['tags' => $popular]);
    }

    /**
     * Get tags marked as association
     * 
     * @return Illuminate\Http\Response
     */
    public function getAssociationTags(AssociationTagTransformer $transformer)
    {
        $tags = Tag::where('association', true)->get()->toArray();

        return $this->respond(['tags' => $transformer->transformCollection($tags)]);
    }

    /**
     * [getSearchAssistTags description]
     * @param  AssociationTagTransformer $transformer [description]
     * @return [type]                                 [description]
     */
    public function getSearchAssistTags(AssociationTagTransformer $transformer)
    {
        $tags = Tag::searchAssist()->get()->toArray();

        return $this->respond(['tags' => $transformer->transformCollection($tags)]);
    }

    /**
     * [getNoteAssistTags description]
     * @param  AssociationTagTransformer $transformer [description]
     * @return [type]                                 [description]
     */
    public function getNoteAssistTags(AssociationTagTransformer $transformer)
    {
        $tags = Tag::noteAssist()->get()->toArray();

        return $this->respond(['tags' => $transformer->transformCollection($tags)]);
    }
}
