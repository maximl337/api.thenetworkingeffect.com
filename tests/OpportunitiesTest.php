<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tymon\JWTAuth\Facades\JWTAuth as JWTAuth;

class OpportunitiesTest extends ApiTester
{

    /** @test */
    public function it_gets_authenticated_users_opportunities()
    {
        $this->makeUserAndToken();

        do {

            $user = App\User::orderByRaw("RAND()")->first();
        
        } while($user->id == $this->user->id);


    	$opportunity = App\Opportunity::create([
                'follower_id' => $this->user->id,
                'subject_id' => $user->id
            ]);

        $params = [
            'token' => $this->token,
            'limit' => 100
        ];

    	$route = 'api/v1/opportunities?' . http_build_query($params);

    	$response = $this->call('GET', $route);




    	$this->seeJson([
    			'follower_id' => $this->user->id,
                'subject_id' => $user->id
    		]);
    }

    /** @test */
    public function it_gets_opportinities_by_query()
    {
        $this->makeUserAndToken();

        do {
            $user = App\User::orderByRaw("RAND()")->first();
        } while ($user->id == $this->user->id);

        App\Opportunity::create([
                'follower_id' => $this->user->id,
                'subject_id' => $user->id
            ]);

        $params = [
            'token' => $this->token,
            'limit' => 100,
            'query' => $user->first_name . ' ' . $user->last_name
        ];

        $route = 'api/v1/opportunities?' . http_build_query($params);

        $response = $this->call('GET', $route);

        $this->seeJson([
                'subject_id' => $user->id
            ]);

    }

    /** @test */    
    public function it_adds_user_to_opportunity()
    {
    	$this->makeUserAndToken();

    	do {
            $user = App\User::orderByRaw("RAND()")->first();
        } while ($user->id == $this->user->id);

        $opp = App\Opportunity::where('subject_id', $user->id)->where('follower_id', $this->user->id)->first();

        if($opp) $opp->delete();

        $route = 'api/v1/opportunities?token=' . $this->token; 

        $response = $this->call('POST', $route, ['subject_id' => $user->id]);

        $this->seeInDatabase('opportunities', [
        		'follower_id' => $this->user->id,
        		'subject_id' => $user->id
        	]);
    }

    /** @test */
    public function it_removes_a_user_from_opportunity()
    {
    	$this->makeUserAndToken();

    	do {
            $user = App\User::orderByRaw("RAND()")->first();
        } while ($user->id == $this->user->id);

        $opportunity = App\Opportunity::firstOrCreate(['follower_id' => $this->user->id, 'subject_id' => $user->id]);

        $route = 'api/v1/opportunities/'.$user->id.'?token=' . $this->token;

        $this->call('DELETE', $route);

        $this->dontSeeInDatabase('opportunities', [
        		'follower_id' => $this->user->id,
        		'subject_id' => $user->id
        	]);
    }
}
