<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tymon\JWTAuth\Facades\JWTAuth as JWTAuth;


class NoteTest extends ApiTester
{
    /** @test */
    public function it_creates_a_note()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/notes?token=' . $this->token;

        $data = [
            'title' => $this->faker->text,
            'subject_id' => 51,
            'body' => $this->faker->sentence
        ];

        $response = $this->call('POST', $route, $data);

        $this->assertEquals(201, $response->status());

        $this->seeInDatabase('notes', [
                'author_id' => $this->user->id,
                'subject_id' => $data['subject_id'],
                'body'  => $data['body']
            ]);
    }

    /** @test */
    public function it_gets_notes()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/notes?token=' . $this->token;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());

        $this->seeJson([
                'author_id' => $this->user->id,
            ]); 
    }

    /** @test */
    public function it_deletes_note()
    {
        $this->makeUserAndToken();

        $notes = App\User::find($this->user->id)->notes->toArray();

        $noteId = $notes[0]['id'];

        $route = 'api/v1/notes/' . $noteId . '?token=' . $this->token;

        $response = $this->call('delete', $route);

        $this->notSeeInDatabase('notes', [
                'author_id' => $this->user->id,
                'id' => $noteId
            ]);

        App\Note::create([
                'author_id' => $this->user->id,
                'subject_id' => 50,
                'body' => $this->faker->sentence
            ]);

        $this->assertEquals(200, $response->status());
    }

    /** @test */
    public function it_returns_403_on_request_update_by_non_author()
    {
        $this->makeUserAndToken();

        $note = App\Note::create([
                'author_id' => 50,
                'subject_id' => $this->user->id,
                'body' => $this->faker->sentence
            ]);

        $noteId = $note->id;

        $data = [
            'body' => $note->body . '_update'
        ];

        $route = 'api/v1/notes/' . $noteId . '?token=' . $this->token;

        $response = $this->call('POST', $route, $data);

        $this->assertEquals(403, $response->status());

        $this->notSeeInDatabase('notes', [
                'id' => $noteId,
                'body' => $data['body']
            ]);
    }

    /** @test */
    public function it_marks_note_reminder_notification_as_seen()
    {
        $notification = App\Notification::where('notification_type_id', function($q) {
            $q->select('id')->from('notification_types')->where('name', 'NOTE_REMINDER')->get();
        })->where('seen', false)->first();

        $note = App\Note::find($notification->object_id);

        $user = App\User::find($note->author_id);

        $token = JWTAuth::fromUser($user);

        $route = 'api/v1/notes/' . $note->id . '/seen?token=' . $token;

        $response = $this->call('POST', $route);

        //dd($response->getContent());

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('notifications', [
                'id' => $notification->id,
                'seen' => 1
            ]);
    }
}