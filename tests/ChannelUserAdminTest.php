<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

//use Faker\Factory as Faker;
use Tymon\JWTAuth\Facades\JWTAuth;

class ChannelUserAdminTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @test
     * @group ChannelUserAdmin
     */
    public function adminCanAddUserToChannel()
    {
        $admin = factory(App\User::class)->create(['is_admin' => 1]);

        $user = factory(App\User::class)->create();

        $token = JWTAuth::fromUser($admin);

        $channel = factory(App\Channel::class)->create([
                'user_id' => $admin->id
            ]);

        $url = '/api/v1/admin/channels/'.$channel->id.'/addMember?token=' . $token;

        $req = $this->call('POST', $url, [
                'user_id' => $user->id
            ]);

        $this->assertEquals(201, $req->status());   

        $this->seeInDatabase('channel_user', [
                'channel_id' => $channel->id,
                'user_id' => $user->id
            ]);
    }

    /**
     * @test
     * @group ChannelUserAdmin
     */
    public function adminCanRemoveUser()
    {
        $admin = factory(App\User::class)->create(['is_admin' => 1]);

        $user = factory(App\User::class)->create();

        $token = JWTAuth::fromUser($admin);

        $channel = factory(App\Channel::class)->create([
                'user_id' => $admin->id
            ]);

        $channel->users()->attach($user->id);

        $url = '/api/v1/admin/channels/'.$channel->id.'/removeMember?token=' . $token;

        $req = $this->call('POST', $url, [
                'user_id' => $user->id
            ]);

        $this->assertEquals(200, $req->status());   

        $this->dontSeeInDatabase('channel_user', [
                'channel_id' => $channel->id,
                'user_id' => $user->id
            ]);
    }

    /**
     * @test
     * @group ChannelUserAdmin
     */
    public function adminCanAddRoleWhenAddingMember()
    {
        $admin = factory(App\User::class)->create(['is_admin' => 1]);

        $user = factory(App\User::class)->create();

        $token = JWTAuth::fromUser($admin);

        $channel = factory(App\Channel::class)->create([
                'user_id' => $admin->id
            ]);

        $role = factory(App\Role::class)->create([
                'user_id' => $admin->id,
                'channel_id' => $channel->id
            ]);

        $url = '/api/v1/admin/channels/'.$channel->id.'/addMember?token=' . $token;

        $req = $this->call('POST', $url, [
                'user_id' => $user->id,
                'role_id' => $role->id
            ]);

        $this->assertEquals(201, $req->status());   

        $this->seeInDatabase('channel_user', [
                'channel_id' => $channel->id,
                'user_id' => $user->id,
                'role_id' => $role->id
            ]);
    }

    /**
     * @test
     * @group ChannelUserAdmin
     */
    public function adminCanUpdateMemberRole()
    {
        $admin = factory(App\User::class)->create(['is_admin' => 1]);

        $user = factory(App\User::class)->create();

        $token = JWTAuth::fromUser($admin);

        $channel = factory(App\Channel::class)->create([
                'user_id' => $admin->id
            ]);

        $role = factory(App\Role::class)->create([
                'user_id' => $admin->id,
                'channel_id' => $channel->id
            ]);

        $channel->users()->attach($user->id, ['role_id' => $role->id]);

        $role2 = factory(App\Role::class)->create([
                'user_id' => $admin->id,
                'channel_id' => $channel->id
            ]);

        $url = '/api/v1/admin/channels/'.$channel->id.'/updateMemberRole?token=' . $token;

        $req = $this->call('POST', $url, [
                'user_id' => $user->id,
                'role_id' => $role2->id
            ]);

        $this->assertEquals(200, $req->status());   

        $this->seeInDatabase('channel_user', [
                'channel_id' => $channel->id,
                'user_id' => $user->id,
                'role_id' => $role2->id
            ]);
    }

}