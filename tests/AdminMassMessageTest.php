<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminMassMessageTest extends AdminTester
{

    use DatabaseTransactions;

    /** @test */
    public function shouldSendMassMessage()
    {
        $this->makeAdminAndToken();
        
        $this->actingAs($this->user);

        $recipient_ids = [];

        // create 3 users
        $users = factory('App\User', 3)->create();

        foreach($users as $user) {

            array_push($recipient_ids, $user->id);
        }

        $data = [
            'from' => $this->user->id,
            'recipients' => $recipient_ids,
            'body' => 'foobar'
        ];

        $route = 'api/v1/admin/messages/mass_message?token=' . $this->token;

        $request = $this->json('POST', $route, $data);

        //dd($request->dump());

        foreach($recipient_ids as $to) {

            $this->seeInDatabase('messages', [
                'from' => $data['from'],
                'to' => $to,
                'body' =>  $data['body']
            ]);    

        }
        
    }
}
