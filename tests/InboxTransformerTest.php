<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

//use Faker\Factory as Faker;
use Tymon\JWTAuth\Facades\JWTAuth;

class InboxTransformerTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @test
     * @group inboxTransformer
     */
    public function getsTransformedPayload()
    {
        // create 3 users
        $users = factory(App\User::class, 7)->create();
        $_authenticated = $users->shift();
        $users = $users->toArray();
        // create 2 messages from authenticated to each user
        for($i=0; $i<5; $i++) {
            App\Message::create([
                'from' => $_authenticated->id,
                'to' => $users[$i]['id']
            ]);
        }

        $blockedUsers =  [$users[0], $users[1], $users[2]];

        foreach($blockedUsers as $blockedUser) {
            // block one of the users
            App\BlockedUser::create([
                'user_id' => $_authenticated->id,
                'blocked_id' => $blockedUser['id']
            ]);
        }
        
        $token = JWTAuth::fromUser($_authenticated);
        $route = 'api/v1/user/inbox?token='.$token;
        $response = $this->call('GET', $route);
        $payload = $response->getData();
        foreach($payload as $respItem) {
            foreach($blockedUsers as $blockedUser) {
                if($respItem->connection_id !== $blockedUser['id']) continue;
                $this->assertTrue($respItem->connection->is_blocked);
            }
        }
    }
}