<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

//use Faker\Factory as Faker;
use Tymon\JWTAuth\Facades\JWTAuth;

class DeleteChannelTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @test
     * @group channel
     */
    public function deletesChannelsForAdmins()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $token = JWTAuth::fromUser($adminUser);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $url = '/api/v1/admin/channels/'.$channel->id.'/delete?token=' . $token;

        $req = $this->call('POST', $url);

        $this->assertEquals(200, $req->status());

        $this->missingFromDatabase('channels', $channel->toArray());
    }
}