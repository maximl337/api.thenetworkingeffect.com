<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class EventTest extends ApiTester
{

    /** @test */
    public function it_gets_events()
    {
        
        $this->makeUserAndToken();

        $response = $this->call('GET', 'api/v1/events', ['token' => $this->token]);

        $this->assertEquals(200, $response->status());

    }

    /** @test */
    public function it_gets_a_single_event()
    {
        $this->makeUserAndToken();

        $event = App\Event::first()->toArray();

        $route = 'api/v1/events/' . $event['id'];

        $response = $this->call('GET', $route, ['token' => $this->token]);

        $this->assertEquals(200, $response->status());

        $this->assertEquals($event['id'], $response->getData(true)['data']['id']);

    }

    /** @test */
    public function it_creates_a_tne_event()
    {
        $this->makeUserAndToken();

        $unixTimestap = '1717952535';

        $tag = App\Tag::find(1);

        $data = [
            'name'              => $this->faker->sentence,
            'description'       => $this->faker->text,
            'event_date'        => $this->faker->date($format = 'Y-m-d'),
            'start_time'        => $this->faker->time,
            'end_time'          => $this->faker->time,
            'timezone'          => $this->faker->timezone,
            'street'            => $this->faker->streetAddress,
            'city'              => $this->faker->city,
            'state'             => $this->faker->state,
            'tags'              => [$tag->name]
        ];

        $route = 'api/v1/events/?token=' . $this->token;

        $response = $this->call('POST', $route, $data);

        $this->seeInDatabase('events', [
                                
                                'name' => $data['name'], 
                                'user_id' => $this->user->id

                            ]);

        $this->assertEquals(201, $response->status());

    }


    /** @test */
    public function it_updates_an_event()
    {
       $this->makeUserAndToken();

       $userEvent = App\User::find($this->user->id)->my_events()->first();

       $route = 'api/v1/events/' . $userEvent->id . '?token=' . $this->token;

       $updatedName = $userEvent->name .'_test';

       $tag = App\Tag::find(1);

       $data = [
            'name'              => $updatedName,
            'description'       => $userEvent->description,
            'event_date'        => $userEvent->event_date,
            'start_time'        => $userEvent->start_time,
            'end_time'          => $userEvent->end_time,
            'timezone'          => $userEvent->timezone,
            'street'            => $userEvent->street,
            'city'              => $userEvent->city,
            'state'             => $userEvent->state,
            'latitude'          => $userEvent->latitude,
            'longitude'         => $userEvent->longitude,
            'tags'              => [$tag->name]
        ];

       $sanitizer = new App\Services\SanitizeUrlService;


       $permalink = $sanitizer->makePrettyUrl($userEvent);

       $response = $this->call('POST', $route, $data);

       $this->assertEquals(200, $response->status());

       $this->seeInDatabase('events', [
                
                    'id' => $userEvent->id,
                    'name' => $updatedName,
                    'permalink' => $permalink

                ]);

       $this->seeInDatabase('event_tag', [
                'event_id' => $userEvent->id,
                'tag_id' => $tag->id
            ]);

    }

    /** @test */
    public function it_gets_users_events()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/user/events?token=' . $this->token;

        $response = $this->call('GET', $route, []);

        $this->assertEquals(200, $response->status());

        $this->seeJson([
                'user_id' => $this->user->id
            ]);
    }

    /** @test */
    public function it_returns_403_on_event_update_by_non_owner()
    {
        $this->makeUserAndToken();

        // user with id 1 will not be owner of event 2 by virtue of UserTableSeeder
        $route = 'api/v1/events/2?token=' . $this->token;

        $unixTimestap = '1717952535';

        $data = [
            'name'              => $this->faker->sentence.'_test',
            'description'       => $this->faker->text.'_test',
            'event_date'        => $this->faker->date($format = 'Y-m-d'),
            'start_time'        => $this->faker->time,
            'end_time'          => $this->faker->time,
            'timezone'          => $this->faker->timezone,
            'street'            => $this->faker->streetAddress.'_test',
            'city'              => $this->faker->city.'_test',
            'state'             => $this->faker->state.'_test',
            'latitude'          => $this->faker->latitude.'_test',
            'longitude'         => $this->faker->longitude.'_test'
        ];

        $response = $this->call('POST', $route, $data);

        $this->assertEquals(403, $response->status());

        $this->notseeInDatabase('events', [
                'id' => 2,
                'name' => $data['name']
            ]);
        
    }

    /** @test */
    public function it_deletes_an_event()
    {

        $this->makeUserAndToken();

        $userEvents = App\User::find($this->user->id)->my_events->toArray();

        $eventId = $userEvents[0]['id'];

        $route = 'api/v1/events/' . $eventId . '/delete?token=' . $this->token;

        $response = $this->call('delete', $route);

        $deletedEvent = App\Event::find($eventId);

        $this->assertEquals(false, $deletedEvent);   

        App\Event::withTrashed()->where('id', $eventId)->restore();

        $this->assertEquals(200, $response->status());

        
    }

    /** @test */
    public function it_lets_user_join_an_event()
    {
        $this->makeUserAndToken();

        $event = App\Event::first();

        $route = 'api/v1/events/' . $event->id .'/join?token=' . $this->token;

        $response = $this->call('POST', $route);

        $this->seeInDatabase('event_user', [
                'event_id' => $event->id,
                'user_id' => $this->user->id
            ]);
        
        $this->assertEquals(201, $response->status());
    }

    /** @test */
    public function it_lets_user_leave_an_event()
    {
        
        $this->makeUserAndToken();

        $events = App\User::find($this->user->id)->events->toArray();
        
        $eventId = $events[0]['id'];

        $route = 'api/v1/events/' . $eventId . '/leave?token=' . $this->token;

        $response = $this->call('POST', $route);

        $this->assertEquals(200, $response->status());

        $this->notSeeInDatabase('event_user', ['user_id' => $this->user->id, 'event_id' => $eventId]);


    }

    /** @test */
    public function it_lets_event_owner_add_user()
    {
        $this->makeUserAndToken();

        $events = App\User::find($this->user->id)->my_events->toArray();

        $eventId = $events[0]['id'];

        $testUserId = 50;

        $route = 'api/v1/events/' . $eventId . '/addUser?token=' . $this->token;

        $response = $this->call('POST', $route, ['user_id' => $testUserId ]);

        $this->seeInDatabase('event_user', [
                'event_id' => $eventId,
                'user_id' => $testUserId
            ]);

        App\Event::find($eventId)->users()->detach($testUserId);

        $this->assertEquals(201, $response->status());

    }

    /** @test */
    public function it_lets_event_owner_remove_user()
    {
        $this->makeUserAndToken();

        $events = App\User::find($this->user->id)->my_events->toArray();

        $eventId = $events[0]['id'];

        $eventUsers = App\Event::find($eventId)->users->toArray();

        $eventUserId = $eventUsers[0]['id'];

        $route = 'api/v1/events/' . $eventId . '/removeUser?token=' . $this->token;

        $response = $this->call('POST', $route, ['user_id' => $eventUserId ]);

        $this->notSeeInDatabase('event_user', [
                'event_id' => $eventId,
                'user_id' => $eventUserId
            ]);

        App\Event::find($eventId)->users()->attach($eventUserId);

        $this->assertEquals(200, $response->status());
    }

    /** @test */
    public function it_gets_event_users()
    {
        $this->makeUserAndToken();

        $event = App\Event::first();

        $route = 'api/v1/events/' . $event->id . '/users?token=' . $this->token;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());
    }

    /** @test */
    public function it_searches_for_event_users()
    {
        $this->makeUserAndToken();

        $event = App\Event::orderByRaw("RAND()")->first();


        $route = 'api/v1/search/events/users?token=' . $this->token . '&event_ids%5B%5D='. $event->id;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());

    }

    /** @test */
    public function it_gets_events_user_is_attending()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/user/events/attending?token=' . $this->token;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());
    }
}