<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class UserTest extends ApiTester
{

    use DatabaseTransactions;

    /** @test */
    public function it_should_get_users()
    {
        
        $this->makeUserAndToken();        

        $response = $this->call('GET', 'api/v1/users', ['token' => $this->token]);

        $this->assertArrayHasKey( 'data', $response->getData(true) );

        $this->assertEquals( 200, $response->status() );
        
    }

    /** @test */
    public function it_should_get_one_user()
    {
        $this->makeUserAndToken();   

        $route = 'api/v1/users/' . $this->user->id;

        $response = $this->call('GET', $route, ['token' => $this->token]);

        $this->seeJson([
                'id' => $this->user->id
            ]);
        
        $this->assertEquals(200, $response->status());
    }

    /**
     * @test
     * @group userTest
     */
    public function it_should_update_user_in_database()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/user/update/new?token=' . $this->token;

        $sendData = [
            'first_name'        => $this->user->first_name . '_test',
            'last_name'         => $this->user->last_name . '_test',
            'business_name'     => $this->user->business_name . '_test',
            'job_title'         => $this->user->job_title . '_test',
            'street'            => $this->user->street,
            'city'              => $this->user->city ,
            'state'             => $this->user->state,
            'business_category' => $this->user->business_category . '_test',
            'tags'              => [1, 2, 3]
        ];

        $sanituzeUrl = new App\Services\SanitizeUrlService;

        

        $response = $this->call('POST', $route, $sendData);

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('users', [
                'first_name' => $sendData['first_name']
            ]);

    }

    /** @test */
    public function it_should_delete_user()
    {
        $this->makeUserAndToken();

        $userId = $this->user->id;

        $route = 'api/v1/user/delete?token=' . $this->token;

        $response = $this->call('delete', $route);

        $deletedUser = App\User::find($userId);

        $this->assertEquals(false, $deletedUser);

        App\User::withTrashed()->where('id', $userId)->restore();

        $this->assertEquals(200, $response->status());

    }

    /** @test */
    public function it_should_update_user_without_address()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/user/update?token=' . $this->token;

        $sendData = [
            'first_name'        => $this->user->first_name . '_test',
            'last_name'         => $this->user->last_name . '_test',
            'business_name'     => $this->user->business_name . '_test',
            'job_title'         => $this->user->job_title . '_test',
            'business_category' => $this->user->business_category . '_test',
            'tags'              => [1, 2, 3]
        ];

        $sanituzeUrl = new App\Services\SanitizeUrlService;

        $permalink = $sanituzeUrl->makePrettyUrl($this->user);

        $response = $this->call('POST', $route, $sendData);

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('users', [
                'first_name' => $sendData['first_name'],
                'permalink' => $permalink
            ]);
    }

    /** @test */
    public function it_updates_user_email()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/user/email?token=' . $this->token;

        $data = ['email' => $this->faker->email];

        $response = $this->call('POST', $route, $data);

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('users', [
                'email' => $data['email'],
                'id'    => $this->user->id
            ]);

    }

    
    public function it_returns_302_on_duplicate_update_user_email()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/user/email?token=' . $this->token;

        $data = ['email' => $this->user->email];

        $response = $this->call('POST', $route, $data);

        $this->assertEquals(302, $response->status());

    }

    /** @test */
    public function it_updates_password()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/user/password?token=' . $this->token;

        $data = [

            'password' => 'test123',
            'password_confirmation' => 'test123'

            ];

        $response = $this->call('POST', $route, $data);

        $login = $this->call('POST', 'api/v1/auth/login', [ 
                        'email' => $this->user->email,
                        'password' => $data['password']
                    ]);

        $this->assertEquals(200, $response->status());

        $this->assertEquals(200, $login->status());

        $this->assertArrayHasKey( 'token', $login->getData(true) );
    }

    /** @test */
    public function it_should_return_user_by_email()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/user/email/' . $this->user->email;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());

        $this->seeJson([
                'first_name' => $this->user->first_name,
                'last_name'  => $this->user->last_name,
                'email'      => $this->user->email
            ]);
    }

    /** @test */
    public function it_should_hide_address()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/user/update?token='.$this->token;

        $response = $this->call('POST', $route, [
                'hide_address' => true
            ]);

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('users', [
                'id' => $this->user->id,
                'hide_address' => true
            ]);
    }

    /** @test */
    public function it_should_unhide_address()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/user/update?token=' . $this->token;

        $response = $this->call('POST', $route, ['hide_address' => false]);

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('users', [
                'id' => $this->user->id,
                'hide_address' => false
            ]); 
    }

    /** @test */
    public function it_should_not_return_address_if_hide_address_is_true()
    {
        $this->makeUserAndToken();

        $this->user->hide_address = true;

        $this->user->update();

        $route =  'api/v1/users/'. $this->user->id .'?token=' . $this->token;

        $response = $this->call('GET', $route);

        $this->dontSeeJson([
                'street' => $this->user->street,
                'city'  => $this->user->city,
                'state' => $this->user->state,
                'zip_code' => $this->user->zip_code,
                'country'  => $this->user->country
            ]);
    }

}
