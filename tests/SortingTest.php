<?php

use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class SortingTest extends ApiTester
{

    /** @test */
    public function it_sorts_single_network_contact()
    {
        $this->makeUserAndToken();

        $network = $this->user->network_following()->first();

        $sortingCategory = App\SortingCategory::first();

        $date = Carbon::now();

        $date->addDay(1);

        $remind_date = $date->format('Y-m-d') . ' 14:00:00';

        $route = 'api/v1/network/sort?token=' . $this->token;

        $data = [
            'subject_id'            => $network->subject_id,
            'sorting_category_id'   => $sortingCategory->id
        ];

        $response = $this->call('POST', $route, $data);

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('sorting_reminders', [
                
                    'network_id'                   => $network->id,
                    'sorting_category_id'          => $sortingCategory->id,
                    'remind_at'                    => $remind_date

                ]);
    }

    /** @test */
    public function it_marks_active()
    {
        $this->makeUserAndToken();

        $network = $this->user->network_following()->first();

        $data = [
            'subject_id' => $network->subject_id
        ];

        $route = 'api/v1/network/sort/active?token=' . $this->token;

        $response = $this->call('POST', $route, $data);

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('sorting_reminders', [
                'network_id' => $network->id,
                'active' => true
            ]);
    }

}