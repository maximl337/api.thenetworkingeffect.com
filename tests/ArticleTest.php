<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class ArticleTest extends ApiTester
{

    /** @test */
    public function it_gets_all_articles()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/articles?token=' . $this->token;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());
    }

    /** @test */
    public function it_gets_a_single_article()
    {
        $this->makeUserAndToken();

        $article = App\Article::find(50);

        $route = 'api/v1/articles/' . $article->id . '?token=' . $this->token;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());

       
    }

    /** @test */
    public function it_creates_an_article()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/articles?token=' . $this->token;

        $data = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->text,
            'teaser' => $this->faker->sentence,
            'tags' => [1, 2, 3]
        ];

        $response = $this->call('POST', $route, $data);

        $articleId = $response->getData(true)['data']['article']['id'];

        $this->seeInDatabase('articles', [
                'title' => $data['title'], 
                'id' => $articleId
                ]);

        $this->assertEquals(201, $response->status());

    }

    /** @test */
    public function it_updates_an_article()
    {
        $this->makeUserAndToken();

        $articles = App\User::find($this->user->id)->articles->toArray();

        $route = 'api/v1/articles/' . $articles[0]['id'] . '?token=' . $this->token;

        $articles[0]['title'] .= '_test';

        $articles[0]['tags'] = [3];

        $response = $this->call('POST', $route, $articles[0]);

        $this->seeInDatabase('articles', [
                'title' => $articles[0]['title']
            ]);

        $this->assertEquals(200, $response->status());

    }

    /** @test */
    public function it_returns_403_if_non_owner_makes_update_request()
    {
        $this->makeUserAndToken();

        $article = App\Article::find(50)->toArray(); 

        $article['tags'] = [1,2,3];

        $route = 'api/v1/articles/' . $article['id'] . '?token=' . $this->token;

        $response = $this->call('POST', $route, $article);

        $this->assertEquals(403, $response->status());
    }

    /** @test */
    public function it_returns_302_if_invalid_parameters_in_update_request()
    {
        $this->makeUserAndToken();

        $article = $this->user->articles()->first();

        $route = 'api/v1/articles/'.$article->id.'?token=' . $this->token;

        $response = $this->call('POST', $route, []);

        $this->assertEquals(302, $response->status());
    }

    /** @test */
    public function it_deletes_an_article()
    {
        $this->makeUserAndToken();

        $articles = App\User::find($this->user->id)->articles->toArray();

        $route = 'api/v1/articles/' . $articles[0]['id'] . '?token=' . $this->token;

        $response = $this->call('delete', $route, $articles[0]);

        $this->notSeeInDatabase('articles', [
                'title' => $articles[0]['title'],
                'id'    => $articles[0]['id']
            ]);


        App\Article::create([
            'title' => $this->faker->sentence, 
            'body' => $this->faker->text, 
            'teaser' => $this->faker->sentence, 
            'user_id' => 1
            ]);

        $this->assertEquals(200, $response->status());
    }

    /** @test */
    public function it_returns_403_if_non_owner_requests_to_delete()
    {
        $this->makeUserAndToken();

        $article = App\Article::find(50)->toArray(); 

        $route = 'api/v1/articles/' . $article['id'] . '?token=' . $this->token;

        $response = $this->call('delete', $route, $article);

        $this->assertEquals(403, $response->status());

        $this->seeInDatabase('articles', [
                'id' => $article['id']
            ]);
    }

    /** @test */
    public function it_gets_users_articles()
    {
        $this->makeUserAndToken();

        $userId = 2;

        $route = 'api/v1/users/' . $userId . '/articles?token=' . $this->token;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());

        $this->seeJson([
                'user_id' => $userId
            ]);
    }

    /** @test */        
    public function it_gets_my_articles()
    {
        $this->makeUserAndToken();


        $route = 'api/v1/user/articles?token=' . $this->token;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());

        $this->seeJson([
                'user_id' => $this->user->id
            ]);
    }

}