<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Carbon\Carbon;


class SortingServiceTest extends ApiTester
{

    /** @test */
    public function it_returns_reminder_date()
    {
        $service = new App\Services\SortingService;

        $sortingCat = new App\SortingCategory;

        $sortingCat->reminder_delay_days = 1;

        $sortingCat->reminder_time = '09:00:00';

        $reminderDate = $service->formatReminderDate($sortingCat);

        $date = Carbon::now();

        $date->addDay(1);

        $testAgainst = $date->format('Y-m-d') . ' 14:00:00';

        $this->assertEquals($testAgainst, $reminderDate);

    }

    public function it_sends_reminder_notifications()
    {
        $service = new App\Services\SortingService;

        $return_data = $service->sendReminders();

        $this->seeJson([
                ''
            ]);
    }

}