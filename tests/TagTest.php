<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class TagTest extends ApiTester
{

    /** @test */
    public function it_gets_tags()
    {
        $route = 'api/v1/tags';

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());

        $this->assertArrayHasKey( 'data', $response->getData(true) );
    }
}