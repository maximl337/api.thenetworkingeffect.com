<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tymon\JWTAuth\Facades\JWTAuth as JWTAuth;


class PlaceholderServiceTest extends ApiTester
{
    
    /** @test */
    public function it_creates_placeholder_user()
    {
    	// setup user
    	$this->makeUserAndToken();

    	// setup data
    	$data = [
    		'first_name' => $this->faker->firstName,
    		'last_name'	 => $this->faker->lastName,
    		'email'      => $this->faker->email		 
    	];

    	$route = 'api/v1/placeholder?token=' . $this->token;

    	// post data
    	$response = $this->call('POST', $route, $data);

    	//dd($response->getContent());

    	// validate reponse
    	$this->assertEquals(201, $response->status());

    	// validate in placeholder users
    	$this->seeInDatabase('placeholder_users', [
                'first_name' 	=> $data['first_name'],
                'last_name' 	=> $data['last_name'],
                'owner_id'		=> $this->user->id
            ]);

    	// validate in users
    	$this->seeInDatabase('users', [
                'first_name' 		=> $data['first_name'],
                'last_name' 		=> $data['last_name'],
                'email'      		=> $data['email'],
                'is_placeholder'	=> true
            ]);

    }

    /** @test */
    public function it_returns_transformed_placeholder_information_with_network()
    {
    	$this->makeUserAndToken();

    	$original_placeholder = $this->user->ownerPlaceholders()->latest()->first();

    	$placeholder = $original_placeholder->user()->first();

    	$data_two = [
    		'first_name'	=> 	$original_placeholder->first_name . '_test',
    		'last_name' 	=> 	$original_placeholder->last_name . '_test',
    		'email'			=>	$placeholder->email
    	];

    	//make a second user
    	$secondUser = App\User::find(2);

    	$secondUserToken = JWTAuth::fromUser($secondUser);

    	// post for second user
    	$route_two = 'api/v1/placeholder?token=' . $secondUserToken;

    	$response_two = $this->call('POST', $route_two, $data_two);

    	//get second users network
    	$route = 'api/v1/users/network/following?token=' . $secondUserToken;

    	$response = $this->call('GET', $route);

    	$this->seeJson([
                'first_name' => $data_two['first_name'],
                'last_name' => $data_two['last_name']
            ]);

    }

    /** @test */
    public function it_transform_data_to_make_placeholder()
    {
    	// setup user
    	$this->makeUserAndToken();

    	// setup data
    	$data = [
    		'first_name' => $this->faker->firstName,
    		'last_name'	 => $this->faker->lastName,
    		'email'      => $this->faker->email,
    		'foo'		 => "bar",
    	];

    	$route = 'api/v1/placeholder?token=' . $this->token;

    	// post data
    	$response = $this->call('POST', $route, $data);

    	// validate reponse
    	$this->assertEquals(201, $response->status());

    	// validate in placeholder users
    	$this->seeInDatabase('placeholder_users', [
                'first_name' 	=> $data['first_name'],
                'last_name' 	=> $data['last_name'],
                'owner_id'		=> $this->user->id
            ]);

    }

    /** @test */
    public function it_should_activate_placeholder_account_on_registration()
    {

    	$placeholder = App\User::where('is_placeholder', true)->first();

    	// build data
    	$data = [
    		'first_name' 	=> $placeholder->first_name,
    		'last_name'  	=> $placeholder->last_name,
    		'email'      	=> $placeholder->email,
    		'password'   	=> 'test123',
    		'business_name' => $this->faker->word
    	];

    	$route = 'api/v1/auth/register';

    	$response = $this->call('POST', $route, $data);

    	$this->assertEquals(200, $response->status());

    	$this->seeInDatabase('users', [
    			'first_name' 		=> $data['first_name'],
                'last_name' 		=> $data['last_name'],
                'business_name' 	=> $data['business_name'],
                'is_placeholder'	=> false
    		]);
    	
    }

    /** @test */
    public function it_should_activate_placeholder_account_on_social_sign_on()
    {
        $placeholder = App\User::where('is_placeholder', true)->first();

        // build data
        $data = [
            'first_name'    => $placeholder->first_name,
            'last_name'     => $placeholder->last_name,
            'email'         => $placeholder->email,
            'password'      => 'test123',
            'business_name' => $this->faker->word
        ];

        $route = 'api/v1/auth/social';

        $response = $this->call('POST', $route, $data);

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('users', [
                'first_name'        => $data['first_name'],
                'last_name'         => $data['last_name'],
                'business_name'     => $data['business_name'],
                'is_placeholder'    => false
            ]);
    }

    /** @test */
    public function it_sends_invitation_to_placeholder()
    {
        $this->makeUserAndToken();

        $placeholder = $this->user->ownerPlaceholders()->first();

        $data = [
            'placeholder_id' => $placeholder->user_id
        ];

        $route = 'api/v1/placeholder/invite?token=' . $this->token;

        $response = $this->call('POST', $route, $data);

        //dd(json_decode($response->getContent()));

        $this->assertEquals(200, $response->status());

    }

    /** @test */
    public function it_omits_placeholder_when_viewing_other_members_network()
    {

        $this->makeUserAndToken();

        $secondUser = App\User::find(2);

        $secondUserToken = JWTAuth::fromUser($secondUser);

        $placeholder = App\User::where('is_placeholder', true)->first();

        $route = 'api/v1/users/'. $this->user->id .'/network/following?token=' . $secondUserToken;

        $response = $this->call('GET', $route);

        $this->dontSeeJson([
                'user_id' => $placeholder->id
            ]);

    }
    
    
}

