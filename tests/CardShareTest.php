<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class CardShareTest extends ApiTester
{

    /** @test */
    public function it_creates_self_card_share_with_user_id()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/cardShare?token=' . $this->token;

        $data = [
            'recipient_ids' => [50]
        ];

        $response = $this->call('POST', $route, $data);

        //dd($response);

        $this->assertEquals(201, $response->status());

        $this->seeInDatabase('card_shares', [
                'sender_id' => $this->user->id,
                'recipient_id' => $data['recipient_ids'][0]
            ]);
    }

    /** @test */
    public function it_creates_self_card_share_with_email()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/cardShareEmail?token=' . $this->token;

        $data = [
            'recipient_emails' => [$this->faker->email]
        ];

        $response = $this->call('POST', $route, $data);

        $this->assertEquals(201, $response->status());

        $this->seeInDatabase('card_shares', [
                'sender_id' => $this->user->id,
                'recipient_email' => $data['recipient_emails'][0]
            ]);
    }

    /** @test */
    public function it_gets_sent_card_shares()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/user/cardShare/sent?token=' . $this->token;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());

        $this->seeJson([
                'sender_id' => $this->user->id
            ]);
    }

    /** @test */
    public function it_gets_received_card_shares()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/user/cardShare/received?token=' . $this->token;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());

        $this->seeJson([
                'recipient_id' => $this->user->id
            ]);
    }

    /** @test */
    public function it_refers_card_to_another_member()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/refer/ids?token=' . $this->token;

        $data = [
            'subject_id' => App\User::orderByRaw("RAND()")->first()->id,
            'recipient_ids' => [App\User::orderByRaw("RAND()")->first()->id],
            'message'       => $this->faker->sentence
        ];

        $response = $this->call('POST', $route, $data);

        $this->assertEquals(201, $response->status());

        $this->seeInDatabase('card_shares', [
                'sender_id' => $this->user->id,
                'subject_id' => $data['subject_id'],
                'recipient_id' => $data['recipient_ids'][0],
                'message'       => $data['message']
            ]);
    }

    /** @test */
    public function it_returns_error_on_bad_receipient_id()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/refer/ids?token=' . $this->token;

        $data = [
            'subject_id' => App\User::orderByRaw("RAND()")->first()->id,
            'recipient_ids' => [22222222222222],
            'message'       => $this->faker->sentence
        ];

        $response = $this->call('POST', $route, $data);

        $this->seeJson([
                "message" => [ $data['recipient_ids'][0] . ' does not exist' ]
            ]);
    }

    /** @test */
    public function it_refers_user_to_emails()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/refer/emails?token=' . $this->token;

        $data = [
            'subject_id' => App\User::orderByRaw("RAND()")->first()->id,
            'recipient_emails' => [$this->faker->email],
            'message'       => $this->faker->sentence
        ];

        $response = $this->call('POST', $route, $data);

        $this->assertEquals(201, $response->status());

        $this->seeInDatabase('card_shares', [
                'sender_id' => $this->user->id,
                'subject_id' => $data['subject_id'],
                'recipient_email' => $data['recipient_emails'][0],
                'message'       => $data['message']
            ]);
    }

    /** @test */
    public function it_returns_error_message_for_invalid_email()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/refer/emails?token=' . $this->token;

        $data = [
            'subject_id'        => App\User::orderByRaw("RAND()")->first()->id,
            'recipient_emails'  => [22222222222222],
            'message'           => $this->faker->sentence
        ];

        $response = $this->call('POST', $route, $data);

        $this->seeJson([
                "message" => [ $data['recipient_emails'][0] . ' is not a valid email' ]
            ]);
    }



}