<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

//use Faker\Factory as Faker;
use Tymon\JWTAuth\Facades\JWTAuth;

class ChannelsTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @test
     * @group channel
     */
    public function getsChannels()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $channel = factory(App\Channel::class, 3)->create(['user_id' => $adminUser->id]);

        $user = factory(App\User::class)->create(['is_admin' => 0]);
        $token = JWTAuth::fromUser($user);

        $url = '/api/v1/channels?token=' . $token;

        $req = $this->call('GET', $url);

        $this->assertEquals(200, $req->status());
    }

    /**
     * @test
     * @group channel
     */
    public function doesNotGetPrivateChannels()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);
        $channels = factory(App\Channel::class, 3)->create(['user_id' => $adminUser->id]);
        $privateChannel = factory(App\Channel::class)->create(['user_id' => $adminUser->id, 'private' => true]);
        $user = factory(App\User::class)->create(['is_admin' => 0]);
        $token = JWTAuth::fromUser($user);
        $url = '/api/v1/channels?token=' . $token;
        $req = $this->call('GET', $url);
        $this->dontSeeJson(['id' => $privateChannel->id]);
        $this->assertEquals(200, $req->status());
    }

    /**
     * @test
     * @group channel
     */
    public function getsPrivateChannelIfOwner()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);
        $channels = factory(App\Channel::class, 3)->create(['user_id' => $adminUser->id]);
        $privateChannel = factory(App\Channel::class)->create(['user_id' => $adminUser->id, 'private' => true]);
        $token = JWTAuth::fromUser($adminUser);
        $url = '/api/v1/channels?token=' . $token;
        $req = $this->call('GET', $url);
        $this->seeJsonContains(['id' => $privateChannel->id]);
        $this->assertEquals(200, $req->status());
    }

    /**
     * @test
     * @group channel
     */
    public function getsPrivateChannelIfMember()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);
        $channels = factory(App\Channel::class, 3)->create(['user_id' => $adminUser->id]);
        $privateChannel = factory(App\Channel::class)->create(['user_id' => $adminUser->id, 'private' => true]);
        $user = factory(App\User::class)->create(['is_admin' => 0]);
        $privateChannel->users()->attach($user);
        $token = JWTAuth::fromUser($user);
        $url = '/api/v1/channels?token=' . $token;
        $req = $this->call('GET', $url);
        $this->seeJsonContains(['id' => $privateChannel->id]);
        $this->assertEquals(200, $req->status());
    }

    /**
     * @test
     * @group channel
     */
    public function getAChannel()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $user = factory(App\User::class)->create(['is_admin' => 0]);

        $token = JWTAuth::fromUser($user);

        $url = '/api/v1/channels/'.$channel->id.'?token=' . $token;

        $req = $this->call('GET', $url);

        $this->assertEquals(200, $req->status());    

    }

    /**
     * @test
     * @group channel
     */
    public function getChannelMembers()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $user = factory(App\User::class)->create(['is_admin' => 0]);

        $channel->users()->attach([$user->id]);

        $token = JWTAuth::fromUser($user);

        $url = '/api/v1/channels/'.$channel->id.'/members?token=' . $token;

        $req = $this->call('GET', $url);

        $this->assertEquals(200, $req->status());

        $this->seeJsonContains([
                'id' => $user->id 
            ]);
    }

    /**
     * @test 
     * @group channel
     */
    public function joinChannel()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $user = factory(App\User::class)->create(['is_admin' => 0]);

        $token = JWTAuth::fromUser($user);

        $url = '/api/v1/channels/'.$channel->id.'/join?token=' . $token;

        $req = $this->call('POST', $url, []);

        $this->assertEquals(201, $req->status());

        $this->seeInDatabase('channel_user', [
                'user_id' => $user->id,
                'channel_id' => $channel->id
            ]);
    }

    /**
     * @test
     * @group channel
     */
    public function leaveChannel()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $user = factory(App\User::class)->create(['is_admin' => 0]);

        $channel->users()->attach([$user->id]);

        $token = JWTAuth::fromUser($user);

        $url = '/api/v1/channels/'.$channel->id.'/leave?token=' . $token;

        $req = $this->call('POST', $url, []);

        $this->assertEquals(200, $req->status());

        $this->dontSeeInDatabase('channel_user', [
                'user_id' => $user->id,
                'channel_id' => $channel->id 
            ]);
    }

    /**
     * @test
     * @group channel
     */
    public function addRole()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $user = factory(App\User::class)->create(['is_admin' => 0]);

        $role = factory(App\Role::class)->make(['user_id' => $adminUser->id]);

        $channel->users()->attach([$user->id]);

        $channel->roles()->save($role);

        $token = JWTAuth::fromUser($user);

        $url = '/api/v1/channels/'.$channel->id.'/roles/update?token=' . $token;

        $req = $this->call('POST', $url, ['role_id' => $role->id]);

        $this->assertEquals(200, $req->status());

        $this->seeInDatabase('channel_user', [
                'user_id' => $user->id,
                'channel_id' => $channel->id,
                'role_id' => $role->id
            ]);
    }

    /**
     * @test
     * @group channel
     */
    public function removeRole()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $user = factory(App\User::class)->create(['is_admin' => 0]);

        $role = factory(App\Role::class)->make(['user_id' => $adminUser->id]);

        $channel->users()->attach([$user->id]);

        $channel->roles()->save($role); 
        
        $channel->users()->updateExistingPivot($user->id, ['role_id' => $role->id]);

        $token = JWTAuth::fromUser($user);

        $url = '/api/v1/channels/'.$channel->id.'/roles/update?token=' . $token;

        $req = $this->call('POST', $url, ['role_id' => ''], [], [], $this->transformHeadersToServerVars(['Accept' => 'application/json']));

        $this->assertEquals(200, $req->status());

        $this->seeInDatabase('channel_user', [
                'user_id' => $user->id,
                'channel_id' => $channel->id,
                'role_id' => NULL
            ]);
    }
}