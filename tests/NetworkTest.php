<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tymon\JWTAuth\Facades\JWTAuth as JWTAuth;


class NetworkTest extends ApiTester
{

    /** @test */
    public function it_follows_user()
    {

        $this->makeUserAndToken();

        do {
            $user = App\User::orderByRaw("RAND()")->first();
        } while ($user->id == $this->user->id);
        

        App\Network::where('follower_id', $this->user->id)
                    ->where('subject_id', $user->id)
                    ->delete();

        $route = 'api/v1/users/'.$user->id.'/follow?token=' . $this->token;

        $response = $this->call('POST', $route);

        //dd($response->getContent());

        $this->assertEquals(201, $response->status());

        $this->seeInDatabase('networks', [
                'follower_id' => $this->user->id,
                'subject_id' => $user->id
            ]);

    }

    /** @test */
    public function it_unfollows_user()
    {
        $this->makeUserAndToken();

        $network = $this->user->network_following()->first();

        if(is_null($network)) {

            do {
                $user = App\User::orderByRaw("RAND()")->first();
            } while ($user->id == $this->user->id);
            

            $network = App\Network::create([
                    "follower_id" => $this->user->id,
                    "subject_id" => $user->id
                ]);
        }

        $route = 'api/v1/users/'.$network->subject_id.'/unfollow?token=' . $this->token;

        $response = $this->call('POST', $route);

        $this->assertEquals(200, $response->status());

        $this->notSeeInDatabase('networks', [
                'follower_id' => $this->user->id,
                'subject_id' => $network->subject_id
            ]);
    }

    /** @test */
    public function it_gets_users_followings_by_auth()
    {
        
        $this->makeUserAndToken();


        do {
            $user = App\User::orderByRaw("RAND()")->first();
        } while ($user->id == $this->user->id);

        App\Network::where('follower_id', $this->user->id)
                    ->where('subject_id', $user->id)
                    ->delete();

        App\Network::where('follower_id', $user->id)
                    ->where('subject_id', $this->user->id)
                    ->delete();

        App\Network::create([
                'follower_id' => $this->user->id,
                'subject_id' => $user->id
            ]);

        App\Network::create([
                'follower_id' => $user->id,
                'subject_id' => $this->user->id
            ]);        

        $route = 'api/v1/users/network/following?token=' . $this->token;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());

        $this->seeJson([
                'user_id' => $user->id
            ]);

    }

    /** @test */
    public function it_gets_users_followings_by_id()
    {
        $this->makeUserAndToken();

        do {
            $user2 = App\User::orderByRaw("RAND()")->first();
        } while($this->user->id == $user2->id);

        $route = 'api/v1/users/'.$this->user->id.'/network/following?token=' . $this->token;

        App\Network::where('follower_id', $user2->id)
                    ->where('subject_id', $this->user->id)
                    ->delete();

        App\Network::where('follower_id', $this->user->id)
                    ->where('subject_id', $user2->id)
                    ->delete();

        App\Network::create([
                'follower_id' => $this->user->id,
                'subject_id' => $user2->id
            ]);

        App\Network::create([
                'follower_id' => $user2->id,
                'subject_id' => $this->user->id
            ]);

        $response = $this->call('GET', $route);

        $this->seeJson([
                'user_id' => $user2->id
            ]);

    }

    /** @test */
    public function it_marks_network_as_seen()
    {
        $this->makeUserAndToken();

        $randomUser = App\User::orderByRaw("RAND()")->first();

        $network = App\Network::create([
                        'follower_id' => $randomUser->id,
                        'subject_id' => $this->user->id
                    ]);

        $route = 'api/v1/network/'. $network->id .'/seen/?token=' . $this->token;

        $response = $this->call('POST', $route);

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('networks', [
                'id' => $network->id,
                'seen' => true
            ]);
    }

    /** @test */
    public function it_marks_all_networks_as_seen()
    {
        $this->makeUserAndToken();

        $randomeUsers = App\User::orderByRaw("RAND()")->limit('5')->get();

        foreach($randomeUsers as $user) {
            App\Network::create([
                    'follower_id' => $user->id,
                    'subject_id' => $this->user->id
                ]);
        }

        $route = 'api/v1/network/all/seen?token=' . $this->token;

        $response = $this->call('POST', $route);

        $this->assertEquals(200, $response->status());

        $this->dontSeeInDatabase('networks', [
                'subject_id' => $this->user->id,
                'seen' => false
            ]);
    }

    /** @test */
    public function it_returns_403_for_non_subject_in_network()
    {
        $this->makeUserAndToken();

        do {
            $user = App\User::orderByRaw("RAND()")->first();
        } while ($user->id == $this->user->id);

        $network = App\Network::create([
                'follower_id' => $this->user->id,
                'subject_id' => $user->id
            ]);

        $route = 'api/v1/network/'. $network->id .'/seen/?token=' . $this->token;

        $response = $this->call('POST', $route);

        $this->assertEquals(403, $response->status());

        $this->dontSeeInDatabase('networks', [
                'id' => $network->id,
                'seen' => true
            ]);
        
    }

    /** @test */
    public function it_blocks_user()
    {
        $this->makeUserAndToken();

        do {
            $user = App\User::orderByRaw("RAND()")->first();
        } while ($user->id == $this->user->id);

        $route = 'api/v1/users/'.$user->id.'/block?token=' . $this->token;

        $response = $this->call('POST', $route);

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('blocked_users', [
                'user_id' => $this->user->id,
                'blocked_id' => $user->id
            ]);
    }

    /** @test */
    public function it_unblocks_user()
    {
        $this->makeUserAndToken();

        do {
            $user = App\User::orderByRaw("RAND()")->first();
        } while ($user->id == $this->user->id);

        $blockedUser = App\BlockedUser::firstOrCreate(['user_id' => $this->user->id, 'blocked_id' => $user->id]);

        $route = 'api/v1/users/'.$user->id.'/unblock?token=' . $this->token;

        $response = $this->call('POST', $route);

        $this->assertEquals(200, $response->status());

        $this->dontSeeInDatabase('blocked_users', [
                'id' => $blockedUser->id,
                'user_id' => $this->user->id,
                'blocked_id' => $user->id
            ]);
    }

    /** @test */
    public function it_blocks_user_from_following()
    {
        $this->makeUserAndToken();

        do {
            $user = App\User::orderByRaw("RAND()")->first();
        } while ($user->id == $this->user->id);

        $networkOne = App\Network::where('follower_id', $user->id)->where('subject_id', $this->user->id)->first();

        if($networkOne) $networkOne->delete();

        $networkTwo = App\Network::where('follower_id', $this->user->id)->where('subject_id', $user->id)->first();

        if($networkTwo) $networkTwo->delete();

        $blockedUser = App\BlockedUser::firstOrCreate(['user_id' => $user->id, 'blocked_id' => $this->user->id]);

        $route = 'api/v1/users/' . $user->id . '/follow?token=' . $this->token;

        $response = $this->call('POST', $route);

        $this->assertEquals(403, $response->status());

        $this->dontSeeInDatabase('networks', [
                'follower_id' => $this->user->id,
                'subject_id' => $user->id
            ]);

    }

    /** @test */
    public function it_sends_message_with_invitation()
    {
        $this->makeUserAndToken();

        do {
            $user = App\User::orderByRaw("RAND()")->first();
        } while ($user->id == $this->user->id);

        $networkOne = App\Network::where('follower_id', $user->id)->where('subject_id', $this->user->id)->first();

        if($networkOne) $networkOne->delete();

        $networkTwo = App\Network::where('follower_id', $this->user->id)->where('subject_id', $user->id)->first();

        if($networkTwo) $networkTwo->delete();

        $message = $this->faker->sentence;

        $route = 'api/v1/users/' . $user->id . '/follow?token=' . $this->token;

        $response = $this->call('POST', $route, ["message" => $message]);

        $this->assertEquals(201, $response->status());

        $this->seeInDatabase('networks', [
                'subject_id' => $user->id,
                'follower_id' => $this->user->id,
                'message' => $message
            ]);
    }

    /** @test */
    public function it_searches_own_network_by_query()
    {
        $this->makeUserAndToken();

        do {
            $user = App\User::orderByRaw("RAND()")->first();
        } while ($user->id == $this->user->id);

        App\Network::create([
                "follower_id" => $this->user->id,
                "subject_id"  => $user->id
            ]);

        App\Network::create([
                "follower_id" => $user->id,
                "subject_id"  => $this->user->id
            ]);

        $params = [
            'token' => $this->token,
            'query' => $user->first_name
        ];

        $route = 'api/v1/users/network/following?' . http_build_query($params);

        $response = $this->call('GET', $route);

        $this->seeJson([
                'user_id' => $user->id
            ]);

    }

    /** @test */
    public function it_searches_users_network_by_query()
    {
        
        $this->makeUserAndToken();

        $user1 = $this->user;

        do {
            $user2 = App\User::orderByRaw("RAND()")->first();
        } while ($user2->id == $this->user->id);

        App\Network::create([
                "follower_id" => $user2->id,
                "subject_id"  => $user1->id
            ]);

        App\Network::create([
                "follower_id" => $user1->id,
                "subject_id"  => $user2->id
            ]);

        $params = [
            'token' => $this->token,
            'query' => $user2->first_name
        ];

        $route = 'api/v1/users/'.$user1->id.'/network/following?' . http_build_query($params);

        $response = $this->call('GET', $route);

        //dd($response->getContent());

        $this->seeJson([
                'user_id' => $user2->id
            ]);

    }

    /**
     * No long valid since it will remove opportunity
     * 
     */
    public function it_does_not_remove_opportunity_when_following()
    {
        $this->makeUserAndToken();

        do {
            $user = App\User::orderByRaw("RAND()")->first();
        } while ($user->id == $this->user->id);

        $follower_id = $this->user->id;
        $subject_id = $user->id;

        App\Network::where(function($q) use($follower_id, $subject_id) {
                    $q->where('follower_id', $follower_id)
                        ->where('subject_id', $subject_id);
                })->orWhere(function($q) use($follower_id, $subject_id) {
                    $q->where('follower_id', $subject_id)
                        ->where('subject_id', $follower_id);
                })->delete();

        App\Opportunity::where(function($q) use($follower_id, $subject_id) {
                    $q->where('follower_id', $follower_id)
                        ->where('subject_id', $subject_id);
                })->orWhere(function($q) use($follower_id, $subject_id) {
                    $q->where('follower_id', $subject_id)
                        ->where('subject_id', $follower_id);
                })->delete();

        // add to opportunitie
        App\Opportunity::create([
                'follower_id' => $follower_id,
                'subject_id' => $subject_id
            ]);

        $route = 'api/v1/users/'.$user->id.'/follow?token=' . $this->token;

        $response = $this->call('POST', $route);

        $this->seeInDatabase('opportunities', [
                'follower_id' => $follower_id,
                'subject_id' => $subject_id
            ]);
    }

    /***/
    public function it_removes_opportunity_when_following_back()
    {
     
        $this->makeUserAndToken();

        do {
            $user = App\User::orderByRaw("RAND()")->first();
        } while ($user->id == $this->user->id);

        $follower_id = $this->user->id;

        $subject_id = $user->id;

        App\Network::where(function($q) use($follower_id, $subject_id) {
                    $q->where('follower_id', $follower_id)
                        ->where('subject_id', $subject_id);
                })->orWhere(function($q) use($follower_id, $subject_id) {
                    $q->where('follower_id', $subject_id)
                        ->where('subject_id', $follower_id);
                })->delete();

        App\Opportunity::where(function($q) use($follower_id, $subject_id) {
                    $q->where('follower_id', $follower_id)
                        ->where('subject_id', $subject_id);
                })->orWhere(function($q) use($follower_id, $subject_id) {
                    $q->where('follower_id', $subject_id)
                        ->where('subject_id', $follower_id);
                })->delete();

        // add to opportunities
        App\Opportunity::create([
                'follower_id' => $user->id,
                'subject_id' => $this->user->id
            ]);

        App\Network::create([
                'follower_id' => $user->id,
                'subject_id' => $this->user->id
            ]);

        $route = 'api/v1/users/'.$user->id.'/follow?token=' . $this->token;

        $response = $this->call('POST', $route);

        $this->dontSeeInDatabase('opportunities', [
                'follower_id' => $user->id,
                'subject_id' => $this->user->id
            ]);
    }

    /** @test */
    public function it_shows_invites_when_one_opportunity_adds_user_to_network()
    {
        $this->makeUserAndToken();

        $userA = $this->user;

        do {
            $userB = App\User::orderByRaw("RAND()")->first();
        } while ($userA->id == $userB->id);

        $follower_id = $userA->id;

        $subject_id = $userB->id;

        App\Network::where(function($q) use($follower_id, $subject_id) {
                    $q->where('follower_id', $follower_id)
                        ->where('subject_id', $subject_id);
                })->orWhere(function($q) use($follower_id, $subject_id) {
                    $q->where('follower_id', $subject_id)
                        ->where('subject_id', $follower_id);
                })->delete();

        App\Opportunity::where(function($q) use($follower_id, $subject_id) {
                    $q->where('follower_id', $follower_id)
                        ->where('subject_id', $subject_id);
                })->orWhere(function($q) use($follower_id, $subject_id) {
                    $q->where('follower_id', $subject_id)
                        ->where('subject_id', $follower_id);
                })->delete();


        // add to opportunities
        App\Opportunity::create([
                'follower_id' => $follower_id,
                'subject_id' => $subject_id
            ]);

        App\Opportunity::create([
                'follower_id' => $subject_id,
                'subject_id' => $follower_id
            ]);

        $userBToken = JWTAuth::fromUser($userB);

        Log::info("user A", [$userA->id]);
        Log::info("user B", [$userB->id]);

        // add one way network
        $routeOne = 'api/v1/users/'.$userA->id.'/follow?token=' . $userBToken;

        $response = $this->call('POST', $routeOne);

        $this->seeInDatabase('networks', [
                'follower_id' => $userB->id,
                'subject_id' => $userA->id,
                'seen' => false
            ]);
        
    }

}