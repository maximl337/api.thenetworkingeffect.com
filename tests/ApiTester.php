<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Faker\Factory as Faker;
use Tymon\JWTAuth\Facades\JWTAuth as JWTAuth;

class ApiTester extends TestCase
{

    protected $token, $user, $faker;

    public function __construct()
    {
        $this->faker = Faker::create();
    }

    public function makeUserAndToken()
    {
        $this->user = App\User::first();

        $this->token = JWTAuth::fromUser($this->user);
    }

}