<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

//use Faker\Factory as Faker;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\ChatMessage;

class ChatTest extends TestCase
{
    use DatabaseTransactions;

    protected $channel;
    protected $adminUser;
    protected $user;
    protected $token;

    public function createChannel()
    {
        $this->adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $this->channel = factory(App\Channel::class)->create(['user_id' => $this->adminUser->id]);

        $this->user = factory(App\User::class)->create(['is_admin' => 0]);

        $this->token = JWTAuth::fromUser($this->user);
    }

    /**
     * @test
     * @group chat
     */
    public function getsChannelsChat()
    {
        $this->createChannel();
        $messages = factory(ChatMessage::class)->create([
                'user_id' => $this->user->id,
                'channel_id' => $this->channel->id
            ]);
        $url = '/api/v1/channels/'.$this->channel->id.'/chat?token=' . $this->token;
        $resp = $this->json('GET', $url)
                ->assertResponseOk();
        $this->seeJsonContains($messages->toArray());
    }

    /**
     * @test
     * @group chat
     */
    public function sendChatMessage()
    {
        $this->createChannel();
        $url = '/api/v1/channels/'.$this->channel->id.'/chat?token=' . $this->token;
        $chatMessage = factory(ChatMessage::class)->make();
        $req = $this->json('POST', $url, ['message' => $chatMessage->message])
                ->assertResponseStatus(201);
        $test = [
                'message' => $chatMessage->message,
                'channel_id' => $this->channel->id,
                'user_id' => $this->user->id
            ];
        $this->seeJsonContains($test);
        $this->seeInDatabase('chat_messages', $test);
    }

    /**
     * @test
     * @group chat
     */
    public function deletesMessage()
    {
        $this->createChannel();
        $user2 = factory(App\User::class)->create(['is_admin' => 0]);
        $chatMessage = factory(ChatMessage::class)->create([
                'channel_id' => $this->channel->id,
                'user_id' => $user2->id
            ]);
        $url = '/api/v1/chat/'.$chatMessage->id.'?token=' . $this->token;
        $resp = $this->json('DELETE', $url)
                ->assertResponseOk();
        $this->seeInDatabase('deleted_chat_messages', [
                'chat_message_id' => $chatMessage->id,
                'user_id' => $this->user->id
            ]);
        $this->seeInDatabase('chat_messages', $chatMessage->toArray());
    }

    /**
     * @test
     * @group chat
     */
    public function doesNotGetDeletedMessage()
    {
        $this->createChannel();
        $user2 = factory(App\User::class)->create(['is_admin' => 0]);
        $chatMessage = factory(ChatMessage::class)->create([
                'user_id' => $user2->id,
                'channel_id' => $this->channel->id
            ]);
        \App\DeletedChatMessage::create([
                'chat_message_id' => $chatMessage->id,
                'user_id' => $this->user->id
            ]);
        $url = '/api/v1/channels/'.$this->channel->id.'/chat?token=' . $this->token;
        $resp = $this->json('GET', $url)
                    ->assertResponseOk();
        $this->dontSeeJson($chatMessage->toArray());
    }

    /**
     * @test
     * @group chat
     */
    public function getsMessageWhenDeletedByAnotherUser()
    {
        $this->createChannel();
        $user2 = factory(App\User::class)->create(['is_admin' => 0]);
        $chatMessage = factory(ChatMessage::class)->create([
                'user_id' => $user2->id,
                'channel_id' => $this->channel->id
            ]);
        $user3 = factory(App\User::class)->create(['is_admin' => 0]);
        \App\DeletedChatMessage::create([
                'chat_message_id' => $chatMessage->id,
                'user_id' => $user3->id
            ]);
        $url = '/api/v1/channels/'.$this->channel->id.'/chat?token=' . $this->token;
        $resp = $this->json('GET', $url)
                    ->assertResponseOk();
        $this->seeJsonContains($chatMessage->toArray());
    }

    /**
     * @test
     * @group chat
     */
    public function reportsMessage()
    {
        $this->createChannel();
        $user2 = factory(App\User::class)->create(['is_admin' => 0]);
        $chatMessage = factory(ChatMessage::class)->create([
                'channel_id' => $this->channel->id,
                'user_id' => $user2->id
            ]);
        $url = '/api/v1/chat/'.$chatMessage->id.'/report?token=' . $this->token;
        $resp = $this->json('POST', $url)
                ->assertResponseOk();
        $this->seeInDatabase('reported_chat_messages', [
                'chat_message_id' => $chatMessage->id,
                'user_id' => $this->user->id
            ]);
    }

    /**
     * @test
     * @group chat
     */
    public function sendsNotificationOnMention()
    {
        $this->createChannel();
        $user2 = factory(App\User::class)->create(['is_admin' => 0]);
        $chatMessage = factory(ChatMessage::class)->create([
                'channel_id' => $this->channel->id,
                'user_id' => $this->user->id
            ]);
        $url = '/api/v1/chat/'.$chatMessage->id.'/mention?token=' . $this->token;
        $data = [
            'user_id' => $user2->id,
            'channel_id' => $this->channel->id
        ];
        $resp = $this->json('POST', $url, $data)
                ->assertResponseStatus(201);
        $this->seeInDatabase('notifications', [
                'actor_id' => $this->user->id,
                'subject_id' => $user2->id,
                'object_id' => $chatMessage->id,
                'notification_type_id' => \App\Notification_type::where('name', 'NEW_CHANNEL_CHAT_MENTION')->first()->id
            ]);
    }

    /**
     * @test
     * @group chat
     */
    public function sendMentionNotificationToEveryone()
    {
        $this->createChannel();
        $channelUsers = factory(App\User::class, 3)->create(['is_admin' => 0]);
        foreach($channelUsers as $channelUser) {
            $this->channel->users()->attach($channelUser->id);
        }
        
        $chatMessage = factory(ChatMessage::class)->create([
                'channel_id' => $this->channel->id,
                'user_id' => $this->user->id
            ]);
        $url = '/api/v1/chat/'.$chatMessage->id.'/mention?token=' . $this->token;
        $data = [
            'everyone' => 1,
            'channel_id' => $this->channel->id
        ];
        $resp = $this->json('POST', $url, $data)
                ->assertResponseStatus(201);
        foreach($channelUsers as $channelUser) {
            if($channelUser->id == $this->user->id) {
                $this->dontSeeInDatabase('notifications', [
                    'actor_id' => $this->user->id,
                    'subject_id' => $channelUser->id,
                    'object_id' => $chatMessage->id,
                    'notification_type_id' => \App\Notification_type::where('name', 'NEW_CHANNEL_CHAT_MENTION')->first()->id
                ]);
            } else {
                $this->seeInDatabase('notifications', [
                    'actor_id' => $this->user->id,
                    'subject_id' => $channelUser->id,
                    'object_id' => $chatMessage->id,
                    'notification_type_id' => \App\Notification_type::where('name', 'NEW_CHANNEL_CHAT_MENTION')->first()->id
                ]);
            }
        }
        
    }
}