<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Tymon\JWTAuth\Facades\JWTAuth as JWTAuth;

class NotificationTest extends ApiTester
{

    //use DatabaseTransactions;

    /** @test */
    public function it_creates_a_notification_for_a_new_message()
    {
        $this->makeUserAndToken();

        $data = [
            'to' => 2,
            'body' => $this->faker->sentence
        ];

        $route = 'api/v1/messages?token=' . $this->token;

        $response = $this->call('POST', $route, $data);

        $message_id = $response->getData(true)['data']['message']['id'];

        $this->seeInDatabase('notifications', [
                'actor_id'              => $this->user->id,
                'subject_id'            => $data['to'],
                'object_id'             => $message_id,
                'notification_type_id'  => 1
            ]);
    }

    /** @test */
    public function it_creates_a_notification_for_new_follower()
    {
        $this->makeUserAndToken();

        $subject_id = 51;

        $route = 'api/v1/users/'. $subject_id .'/follow?token=' . $this->token;

        $response = $this->call('POST', $route);

        $content = json_decode($response->getContent());

        $this->seeInDatabase('notifications', [
                'actor_id'              => $this->user->id,
                'subject_id'            => $subject_id,
                'object_id'             => $content->data[0]->id,
                'notification_type_id'  => 2
            ]);
    }

    /** @test */
    public function it_creates_a_notification_for_new_card_share()
    {
        $this->makeUserAndToken();

        $randomUser = App\User::orderByRaw("RAND()")->get()->first();

        $data = [
            "recipient_ids" => [ $randomUser->id ]
        ];

        App\Card_shares::where('sender_id', $this->user->id)->delete();

        $route = 'api/v1/cardShare?token=' . $this->token;

        $response = $this->call('POST', $route, $data);

        $this->seeInDatabase('notifications', [
                'actor_id'              => $this->user->id,
                'subject_id'            => $randomUser->id,
                'notification_type_id'  => 4
            ]);

    }

    /** @test */
    public function it_creates_a_notification_for_new_testimonial()
    {
        $this->makeUserAndToken();

        $randomUser = App\User::orderByRaw("RAND()")->get()->first();

        $data = [
            "subject_id" => $randomUser->id,
            "body"      => $this->faker->sentence
        ];

        $route = 'api/v1/testimonials?token=' . $this->token;

        $response = $this->call('POST', $route, $data);

        $content = json_decode($response->getContent());

        $this->seeInDatabase('notifications', [
                'actor_id'              => $this->user->id,
                'subject_id'            => $randomUser->id,
                'object_id'             => $content->data->testimonial->id,
                'notification_type_id'  => 6
            ]);
    }

    /** @test */
    public function it_creates_a_notification_for_event_update()
    {
        $this->makeUserAndToken();

        $userEvents = App\User::find($this->user->id)->my_events->toArray();

        $route = 'api/v1/events/' . $userEvents[0]['id'] . '?token=' . $this->token;

        App\Notification::where('object_id', $userEvents[0]['id'])->delete();

        $data = [
            'name'              => $userEvents[0]['name'],
            'description'       => $userEvents[0]['description'],
            'event_date'        => $userEvents[0]['event_date'],
            'start_time'        => $userEvents[0]['start_time'],
            'end_time'          => $userEvents[0]['end_time'],
            'timezone'          => $userEvents[0]['timezone'],
            'street'            => $userEvents[0]['street'],
            'city'              => $userEvents[0]['city'],
            'state'             => $userEvents[0]['state'],
            'tags'              => [ $this->faker->word ]
        ];

        $response = $this->call('POST', $route, $data); 

        $user = App\Event::find($userEvents[0]['id'])->users()->get()->first();

        $this->seeInDatabase('notifications', [
                'actor_id'              => $this->user->id,
                'subject_id'            => $user->id,
                'object_id'             => $userEvents[0]['id'],
                'notification_type_id'  => 7
            ]);
    }

    /** @test */
    public function it_creates_notification_for_event_delete()
    {
        $this->makeUserAndToken();

        $userEvents = App\User::find($this->user->id)->my_events->toArray();

        $eventId = $userEvents[0]['id'];

        App\Notification::where('object_id', $eventId)->delete();

        $route = 'api/v1/events/' . $eventId . '/delete?token=' . $this->token;

        $response = $this->call('delete', $route);

        App\Event::withTrashed()->where('id', $eventId)->restore();

        $user = App\Event::find($userEvents[0]['id'])->users()->get()->first();

        $this->seeInDatabase('notifications', [
                'actor_id'              => $this->user->id,
                'subject_id'            => $user->id,
                'object_id'             => $eventId,
                'notification_type_id'  => 8
            ]);

    }

    /** @test */
    public function it_gets_users_notifications()
    {
        $this->makeUserAndToken();

        $notification = App\Notification::where('notification_type_id', function($query) {
                $query->select('id')->from('notification_types')->where('name', 'NEW_MESSAGE')->first();
        })->first();


        $user = App\User::find($notification->subject_id);

        $token = JWTAuth::fromUser($user);

        $route = 'api/v1/notifications?token=' . $token .'&filter=messages';

        // $response = $this->call('GET', $route);

        // dd($response->getContent());

        $this->get($route)
                ->seeJson([
                        'actor_id' => $notification->actor_id,
                        'subject_id' => $user->id,
                        'notification_type_id' => $notification->notification_type_id
                    ]);
    }

    /** @test */
    public function it_marks_notification_as_seen()
    {
        $this->makeUserAndToken();

        $notification = App\Notification::latest()->first();

        $user = App\User::find($notification->subject_id);

        $token = JWTAuth::fromUser($user);

        $route = 'api/v1/notifications/' . $notification->id . '?token=' . $token;

        $this->post($route, ['seen' => true])
                ->seeJson([
                        'message' => 'notification updated'
                    ]);
    }

    /** @test */
    public function it_deletes_a_notification()
    {
        $this->makeUserAndToken();

        $notification = App\Notification::latest()->first();

        $user = App\User::find($notification->subject_id);

        $token = JWTAuth::fromUser($user);

        $route = 'api/v1/notifications/' . $notification->id . '?token=' . $token;

        $this->delete($route)
                ->seeJson([
                        'message' => 'Notification deleted'
                    ]);
    }

    /** @test */
    public function it_gets_categorized_count_of_unseen_notifications()
    {
        $notification = App\Notification::where('notification_type_id', function($query) {
            $query->select('id')->from('notification_types')->where('name', 'NEW_MESSAGE')->first();
        })->first();

        $user = $notification->subject()->first();

        $count = $user->notifications()->where('seen', false)->where('notification_type_id', function($query) {
            $query->select('id')->from('notification_types')->where('name', 'NEW_MESSAGE')->first();
        })->count();

        $token = JWTAuth::fromUser($user);

        $route = 'api/v1/notifications/getCountOfUnseen?token=' . $token;

        $this->get($route)
                ->seeJson([
                        "messages" => $count
                    ]);
    }

}