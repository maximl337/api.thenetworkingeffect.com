<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class AdminAuthTest extends ApiTester
{

    
    /** @test */
    public function it_logs_in()
    {
        $data = [
            'email' => 'angad_dby@hotmail.com',
            'password' => 'test'
        ];

        $route = 'api/v1/admin/auth/login';

        $response = $this->call('POST', $route, $data);

        $this->assertResponseOk();
        
    }
}