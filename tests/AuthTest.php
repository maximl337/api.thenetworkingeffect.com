<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Faker\Factory as Faker;
use Tymon\JWTAuth\Facades\JWTAuth as JWTAuth;

class AuthTest extends ApiTester
{


    /** @test */
    public function it_returns_jwt_token_for_valid_login_request()
    {
        
        $data = [
            'email'         => $this->faker->email,
            'password'      => $this->faker->word,
            'first_name'    => $this->faker->firstName,
            'last_name'     => $this->faker->lastName
        ];

        $existingRecord = App\User::where('email', $data['email'])->first();

        if($existingRecord) $existingRecord->forceDelete();

        $user = new App\User;

        $user->password = bcrypt($data['password']);

        $user->email = $data['email'];

        $user->first_name = $data['first_name'];

        $user->last_name = $data['last_name'];

        $user->save();

        $token = JWTAuth::fromUser($user);

        $response = $this->call('POST', 'api/v1/auth/login', ['email' => $data['email'], 'password' => $data['password']] );
        
        $user->forceDelete();

        $this->assertEquals(200, $response->status());

        $this->assertArrayHasKey( 'token', $response->getData(true) ); 

    }

    /** @test */
    public function it_returns_302_for_empty_request_body_on_login()
    {
        $response = $this->call('POST', 'api/v1/auth/login', []);

        $this->assertEquals(302, $response->status());
    }

    /** @test */
    public function it_returns_401_for_invalid_credentials_on_login()
    {
        $response = $this->call('POST', 'api/v1/auth/login', ['email' => $this->faker->email, 'password' => $this->faker->word] );

        $this->assertEquals(401, $response->status());

    }

    /** @test */
    public function it_returns_405_for_invalid_method()
    {
        $response = $this->call('GET', 'api/v1/auth/login', []);

        $this->assertEquals(405, $response->status());

    }

    /** @test */
    public function it_registers_user()
    {
        $data = [

            'email'             => $this->faker->email,
            'password'          => $this->faker->password,
            'first_name'        => $this->faker->firstName,
            'last_name'         => $this->faker->lastName,
            'business_name'     => $this->faker->company,
            'job_title'         => $this->faker->word,
            'street'            => $this->faker->streetAddress,
            'city'              => $this->faker->city,
            'state'             => $this->faker->state,
            'business_category' => $this->faker->word
            
        ];

        $response = $this->call('POST', 'api/v1/auth/register', $data);

        unset($data['password']);

        $this->seeInDatabase('users', $data);

        App\User::where('email', $data['email'])->forceDelete();

        $this->assertEquals(201, $response->status());

        $this->assertArrayHasKey('token', $response->getData(true)['data']);

    }

    /** @test */
    public function it_creates_user_on_social_registration()
    {
        $data = [
            'email' => $this->faker->email,
            'provider' => 'google',
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName
        ];

        $response = $this->call('POST', 'api/v1/auth/social', $data);

        $this->seeInDatabase('users', $data);

        App\User::where('email', '=', $data['email'])->forceDelete();

        $this->assertArrayHasKey('token', $response->getData(true)['data']);

        $this->assertEquals(201, $response->status());
    }

    /** @test */
    public function it_returns_302_if_custom_request_validation_fails_on_register()
    {
        $response = $this->call('POST', 'api/v1/auth/register', []);

        $this->assertEquals(302, $response->status());
    }

    /** @test */
    public function it_returns_302_if_email_already_exists_on_register()
    {
        $user = App\User::first();

        $response = $this->call('POST', 'api/v1/auth/register', [
                'email' => $user->email,
                'password' => $this->faker->word,
                'first_name' => $this->faker->firstName,
                'last_name' => $this->faker->lastName
            ]);

        $this->assertEquals(302, $response->status()); 

    }

    /** @test */
    public function it_returns_405_with_invalid_method_on_register()
    {
        $response = $this->call('GET', 'api/v1/auth/register', []);

        $this->assertEquals(405, $response->status());
    }

    /** @test */
    public function it_returns_302_for_invalid_request_on_social_auth()
    {
        $response = $this->call('POST', 'api/v1/auth/social', []);

        $this->assertEquals(302, $response->status());
    }


    /** @test */
    public function it_updates_existing_user_on_social_auth()
    {
        $user = App\User::first()->toArray();

        $user['provider'] = 'social_auth_test';

        $response = $this->call('POST', 'api/v1/auth/social', $user);

        $this->seeInDatabase('users', [
                'email' => $user['email'],
                'provider' => 'social_auth_test'
            ]);

        $this->assertEquals(200, $response->status());

        $this->assertArrayHasKey('token', $response->getData(true));
    }

    /** @test */
    public function it_returns_405_with_invalid_method_on_social_auth()
    {
        $response = $this->call('GET', 'api/v1/auth/social', []);

        $this->assertEquals(405, $response->status());
    }

    /** @test */
    public function it_creates_password_reset_request()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/password/email?json=true';

        $response = $this->call('POST', $route, ['email' => $this->user->email]);

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('password_resets', [
                'email' => $this->user->email
            ]);
    }

    /** @test */
    public function it_resets_password()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/password/reset?json=true';

        //get token
        $token = DB::table('password_resets')->where('email', $this->user->email)->value('token');

        $data = [
            'token' => $token,
            'email' => $this->user->email,
            'password' => 'test123',
            'password_confirmation' => 'test123'
        ];

        $response = $this->call('POST', $route, $data);

        $this->assertEquals(200, $response->status());

    }

    /** @test */
    public function it_registers_user_without_address()
    {
        $data = [

            'email'             => $this->faker->email,
            'password'          => $this->faker->password,
            'first_name'        => $this->faker->firstName,
            'last_name'         => $this->faker->lastName,
            'business_name'     => $this->faker->company,
            'job_title'         => $this->faker->word,
            'business_category' => $this->faker->word
            
        ];

        $response = $this->call('POST', 'api/v1/auth/register', $data);

        unset($data['password']);

        $this->seeInDatabase('users', $data);

        App\User::where('email', $data['email'])->forceDelete();

        $this->assertEquals(201, $response->status());

        $this->assertArrayHasKey('token', $response->getData(true)['data']);

    }

}