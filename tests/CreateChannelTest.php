<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

//use Faker\Factory as Faker;
use Tymon\JWTAuth\Facades\JWTAuth;

class CreateChannelTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @test
     * @group channel
     */
    public function adminCanCreateChannel()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $token = JWTAuth::fromUser($adminUser);

        $url = '/api/v1/admin/channels?token=' . $token;

        $channel = factory(App\Channel::class)->make();

        $req = $this->call('POST', $url, [
                'name' => $channel->name
            ]);

        $this->assertEquals(201, $req->status());   

        $this->seeInDatabase('channels', [
                'name' => $channel->name,
                'user_id' => $adminUser->id
            ]);
    }

    /**
     * @test
     * @group channel
     */
    public function nonAdminCannotCreateChannel()
    {
        $nonAdminUser = factory(App\User::class)->create(['is_admin' => 0]);

        $token = JWTAuth::fromUser($nonAdminUser);

        $url = '/api/v1/admin/channels?token=' . $token;

        $channel = factory(App\Channel::class)->make();

        $req = $this->call('POST', $url, [
                'name' => $channel->name
            ]);

        $this->assertEquals(401, $req->status());   

        $this->missingFromDatabase('channels', [
                'name' => $channel->name,
                'user_id' => $nonAdminUser->id
            ]);
    }

}