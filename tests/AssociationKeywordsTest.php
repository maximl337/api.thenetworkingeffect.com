<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

//use Faker\Factory as Faker;
use Tymon\JWTAuth\Facades\JWTAuth;

class AssociationKeywordsTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @test
     * @group associationKeywords
     */
    public function getsKeywords()
    {
        $user = factory(App\User::class)->create();
        $token = JWTAuth::fromUser($user);
        $keyword = factory(\App\AssociationKeyword::class)->create();
        $url = '/api/v1/associationKeywords?token=' . $token;
        $req = $this->json('GET', $url)
            ->seeJsonContains([
                'id' => $keyword->id,
                'keyword' => $keyword->keyword
            ]);
            
    }

    /**
     * @test
     * @group associationKeywords
     */
    public function userShouldAttachAssociationKeyword()
    {
        $user = factory(\App\User::class)->create();
        $token = JWTAuth::fromUser($user);
        $keyword = factory(\App\AssociationKeyword::class)->create();
        $url = '/api/v1/user/associationKeywords/'.$keyword->id.'?token='.$token;
        $req = $this->json('POST', $url)
                ->assertResponseStatus(201);
        $this->seeInDatabase('association_keyword_user', [
            'user_id' => $user->id,
            'association_keyword_id' => $keyword->id
        ]);
    }

    /**
     * @test
     * @group associationKeywords
     */
    public function userShouldDetachAssociationKeyword()
    {
        $user = factory(\App\User::class)->create();
        $keyword = factory(\App\AssociationKeyword::class)->create();
        $user->association_keywords()->attach($keyword->id);
        $token = JWTAuth::fromUser($user);
        $url = '/api/v1/user/associationKeywords/'.$keyword->id.'?token='.$token;
        $req = $this->json('DELETE', $url)
            ->assertResponseStatus(200);
        $this->dontSeeInDatabase('association_keyword_user', [
                'user_id' => $user->id,
                'association_keyword_id' => $keyword->id
            ]);
    }

    /**
     * @test
     * @group associationKeywords
     */
    public function adminShouldGetsAllKeywords()
    {
        $admin = factory(\App\User::class)->create(['is_admin' => true]);
        $token = JWTAuth::fromUser($admin);
        $associationKeywords = factory(\App\AssociationKeyword::class, 5)->create();
        $route = "/api/v1/admin/associationKeywords?token=$token";
        $resp = $this->json('GET', $route)
                    ->assertResponseStatus(200);

        foreach($associationKeywords as $associationKeyword) {
            $this->seeJsonContains([
                'id' => $associationKeyword->id,
                'keyword' => $associationKeyword->keyword
            ]);    
        }
    }

    /**
     * @test
     * @group associationKeywords
     */
    public function adminShouldCreateKeyword()
    {
        $admin = factory(\App\User::class)->create(['is_admin' => true]);
        $token = JWTAuth::fromUser($admin);
        $data = factory(App\AssociationKeyword::class)->make(['keyword' => 'foo bar'])->toArray();
        $route = "/api/v1/admin/associationKeywords?token=$token";
        $resp = $this->json('POST', $route, $data)
                    ->assertResponseStatus(201);
        $this->seeInDatabase('association_keywords', $data);
    }

    /**
     * @test
     * @group associationKeywords 
     */
    public function adminShouldUpdateKeyword()
    {
        $admin = factory(\App\User::class)->create(['is_admin' => true]);
        $token = JWTAuth::fromUser($admin);
        $associationKeyword = factory(App\AssociationKeyword::class)->create();
        $id = $associationKeyword->id;
        $data = [
            'keyword' => $associationKeyword->keyword . ' updated'
        ];
        $route = "/api/v1/admin/associationKeywords/$id?token=$token";
        $resp = $this->json('PUT', $route, $data)
                    ->assertResponseStatus(200);
        $this->seeInDatabase('association_keywords', [
            'id' => $id,
            'keyword' => $data['keyword']
        ]);
    }

    /**
     * @test
     * @group associationKeywords
     */
    public function adminShouldDeleteKeyword()
    {
        $admin = factory(\App\User::class)->create(['is_admin' => true]);
        $token = JWTAuth::fromUser($admin);
        $associationKeyword = factory(App\AssociationKeyword::class)->create();
        $id = $associationKeyword->id;
        $route = "/api/v1/admin/associationKeywords/$id?token=$token";
        $resp = $this->json('DELETE', $route)
                    ->assertResponseStatus(200);
        $this->dontSeeInDatabase('association_keywords', $associationKeyword->toArray());
    }

    /**
     * @test
     * @group associationKeywords
     */
    public function adminShouldGetSingleKeyword()
    {
        $admin = factory(\App\User::class)->create(['is_admin' => true]);
        $token = JWTAuth::fromUser($admin);
        $associationKeyword = factory(App\AssociationKeyword::class)->create();
        $id = $associationKeyword->id;
        $route = "/api/v1/admin/associationKeywords/$id?token=$token";
        $resp = $this->json('GET', $route)
                    ->assertResponseStatus(200);
        $this->seeJsonContains($associationKeyword->toArray());
    }

    public function adminShouldGetUsersKeywords()
    {
        
    }

    public function adminShouldAttachKeywordToUser($value='')
    {
        # code...
    }

    public function adminShouldDetachKeywordFromUser($value='')
    {
        # code...
    }
}