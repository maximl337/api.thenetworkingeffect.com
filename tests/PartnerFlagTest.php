<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartnerFlagTest extends ApiTester
{

	/** @test */
    public function it_allows_registering_with_partner_flag()
    {

    	$partner = App\Partner::first();

    	$data = [
    		'first_name' 		=> $this->faker->firstName,
    		'last_name'	 		=> $this->faker->lastName,
    		'email' 			=> $this->faker->email,
    		'password' 			=> $this->faker->word(8),
    		'job_title' 		=> $this->faker->word,
    		'business_category' => $this->faker->word,
    		'business_name'     => $this->faker->word,
    		'partner' 			=> $partner->name
    	];

    	$route = 'api/v1/auth/register';

    	$response = $this->call("POST", $route, $data);

    	$body = json_decode($response->getContent());

    	$user_id = $body->data->user->id;

    	

    	$this->seeInDatabase('partner_user', [
    			'user_id' => $user_id,
    			'partner_id' => $partner->id
    		]);


    }

    /** @test */
    public function it_gets_users_partner()
    {
    	$this->makeUserAndToken();

    	$partner = App\Partner::first();

    	$this->user->partners()->delete();

    	$this->user->partners()->save($partner);

    	$route = 'api/v1/users/' . $this->user->id . '?token=' . $this->token;

    	$response = $this->call('GET', $route);

    	$this->seeJson([
    			'name' => $partner->name
    		]);
    }
}
