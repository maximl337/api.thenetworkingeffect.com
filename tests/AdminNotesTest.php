<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

//use Faker\Factory as Faker;
use Tymon\JWTAuth\Facades\JWTAuth;

class AdminNotesTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @test
     * @group adminNotes
     */
    public function admin_can_add_note()
    {
        $admin = factory(App\User::class)->create(['is_admin' => 1]);
        $user = factory(App\User::class)->create();
        $token = JWTAuth::fromUser($admin);
        $url = '/api/v1/admin/users/'.$user->id.'/addNote?token=' . $token;
        $adminNote = factory(App\AdminNote::class)->make();

        $req = $this->call('POST', $url, [
                'body' => $adminNote->body
            ]);

        $this->assertEquals(201, $req->status());   

        $this->seeInDatabase('admin_notes', [
                'admin_id' => $admin->id,
                'user_id' => $user->id,
                'body' => $adminNote->body
            ]);
    }

    /**
     * @test
     * @group adminNotes
     */
    public function returns_422_if_missing_body()
    {
        $admin = factory(App\User::class)->create(['is_admin' => 1]);
        $user = factory(App\User::class)->create();
        $token = JWTAuth::fromUser($admin);
        $url = '/api/v1/admin/users/'.$user->id.'/addNote?token=' . $token;
        $adminNote = factory(App\AdminNote::class)->make();
        $req = $this->json('POST', $url, [
                'body' => ''
            ]);
        $req->assertResponseStatus(422);
    }

    /**
     * @test
     * @group adminNotes
     */
    public function get_notes_by_user()
    {
        $admin = factory(App\User::class)->create(['is_admin' => 1]);
        $user = factory(App\User::class)->create();
        $token = JWTAuth::fromUser($admin);
        $url = '/api/v1/admin/users/'.$user->id.'/adminNotes?token=' . $token;
        $adminNotes = factory(App\AdminNote::class, 3)->create([
                'admin_id' => $admin->id,
                'user_id' => $user->id
            ]);

        $req = $this->get($url)
                ->seeJson([
                    'admin_id' => $admin->id,
                    'user_id' => $user->id,
                    'body' => $adminNotes->first()->body
                ]);
    }
}