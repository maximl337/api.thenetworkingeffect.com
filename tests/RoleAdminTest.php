<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

//use Faker\Factory as Faker;
use Tymon\JWTAuth\Facades\JWTAuth;

class RoleAdminTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @test
     * @group roles
     */
    public function createsRole()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $role = factory(App\Role::class)->make();

        $token = JWTAuth::fromUser($adminUser);

        $url = '/api/v1/admin/channels/'.$channel->id.'/roles?token=' . $token;

        $req = $this->call('POST', $url, ['name' => $role->name]);

        $this->assertEquals(201, $req->status());

        $this->seeInDatabase('roles', [
                'name' => $role->name,
                'user_id' => $adminUser->id,
                'channel_id' => $channel->id
            ]);
    }

    /**
     * @test
     * @group roles
     */
    public function cannotCreateRoleWithoutChannel()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $role = factory(App\Role::class)->make();

        $token = JWTAuth::fromUser($adminUser);

        $url = '/api/v1/admin/channels/foo/roles?token=' . $token;

        $req = $this->call('POST', $url, ['name' => $role->name]);

        $this->assertEquals(404, $req->status());
    }

    /**
     * @test
     * @group roles
     */
    public function cannotCreateDuplicateNamedRole()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $role = factory(App\Role::class)->make();

        $role->user_id = $adminUser->id;

        $channel->roles()->save($role);

        $token = JWTAuth::fromUser($adminUser);

        $url = '/api/v1/admin/channels/'.$channel->id.'/roles?token=' . $token;

        $server = $this->transformHeadersToServerVars(['Accept' => 'application/json']);

        $req = $this->call('POST', $url, ['name' => $role->name], [], [], $server);

        $this->assertEquals(422, $req->status());
    }

    /**
     * @test
     * @group roles
     */
    public function editsRole()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $role = factory(App\Role::class)->make();

        $role->user_id = $adminUser->id;

        $channel->roles()->save($role);

        $token = JWTAuth::fromUser($adminUser);

        $url = '/api/v1/admin/roles/'.$role->id.'/update?token=' . $token;

        $server = $this->transformHeadersToServerVars(['Accept' => 'application/json']);

        $updatedName = $role->name . 'foo';

        $data = [
            'name' => $updatedName,
            'channel_id' => $channel->id
        ];

        $req = $this->call('POST', $url, $data, [], [], $server);

        $this->seeInDatabase('roles', [
                'id' => $role->id,
                'channel_id' => $channel->id,
                'name' => $updatedName
            ]);
    }

    /**
     * @test
     * @group roles
     */
    public function deletesRole()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $role = factory(App\Role::class)->make();

        $role->user_id = $adminUser->id;

        $channel->roles()->save($role);

        $token = JWTAuth::fromUser($adminUser);

        $url = '/api/v1/admin/roles/'.$role->id.'/delete?token=' . $token;

        $server = $this->transformHeadersToServerVars(['Accept' => 'application/json']);

        $data = [
            'channel_id' => $channel->id
        ];

        $req = $this->call('POST', $url, $data, [], [], $server);

        $this->assertEquals(200, $req->status());

        $this->dontSeeInDatabase('roles', [
                'id' => $role->id,
                'channel_id' => $channel->id,
                'name' => $role->name
            ]);
    }

    /**
     * @test
     * @group roles
     */
    public function getsRoles()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $role = factory(App\Role::class)->make();

        $role->user_id = $adminUser->id;

        $channel->roles()->save($role);

        $token = JWTAuth::fromUser($adminUser);

        $url = '/api/v1/admin/channels/'.$channel->id.'/roles?token=' . $token;

        $server = $this->transformHeadersToServerVars(['Accept' => 'application/json']);

        $req = $this->call('GET', $url, [], [], [], $server);

        $this->assertEquals(200, $req->status());
        
        $this->seeJson([
                'id' => $role->id,
                'name' => $role->name,
                'channel_id' => $channel->id
            ]);

    }

    /**
     * @test
     * @group roles
     */
    public function getsRole()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $role = factory(App\Role::class)->make();

        $role->user_id = $adminUser->id;

        $channel->roles()->save($role);

        $token = JWTAuth::fromUser($adminUser);

        $url = '/api/v1/admin/roles/'.$role->id.'?token=' . $token;

        $server = $this->transformHeadersToServerVars(['Accept' => 'application/json']);

        $req = $this->call('GET', $url, [], [], [], $server);

        $this->assertEquals(200, $req->status());
        
        $this->seeJson([
                'id' => $role->id,
                'name' => $role->name,
                'channel_id' => $channel->id
            ]);
    }
}