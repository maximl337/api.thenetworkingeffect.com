<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Faker\Factory as Faker;
use Tymon\JWTAuth\Facades\JWTAuth as JWTAuth;

class AdminTester extends TestCase
{

    protected $token, $user, $faker;

    public function __construct()
    {
        $this->faker = Faker::create();

    }

    public function makeAdminAndToken()
    {
        $this->user = App\User::first();

        $this->user->is_admin = true;

        $this->user->save();

        $this->token = JWTAuth::fromUser($this->user);
    }

}