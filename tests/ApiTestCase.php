<?php

use Illuminate\Contracts\Auth\Authenticatable as UserContract;

abstract class ApiTestCase extends TestCase
{
    protected $headers = ['Accept' => 'application/json'];

    public function get($uri, array $data = [], array $headers = [])
    {
        $server = $this->transformHeadersToServerVars($headers);
        return $this->call('GET', $uri, $data, [], [], $server);
    }

    public function call($method, $uri, $parameters = [], $cookies = [], $files = [], $server = [], $content = null)
    {
        $applicationJson = $this->transformHeadersToServerVars($this->headers);
        $server = array_merge($server, $applicationJson);
        return parent::call($method, $uri, $parameters, $cookies, $files, $server, $content);
    }

    public function actingAs(UserContract $user, $driver = null)
    {
        $token = \JWTAuth::fromUser($user);
        \JWTAuth::setToken($token);
        $this->headers['Authorization'] = 'Bearer ' . $token;
      
        return $this;
    }
}