<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

//use Faker\Factory as Faker;
use Tymon\JWTAuth\Facades\JWTAuth;

class EditChannelTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @test
     * @group channel
     */
    public function adminCanEditChannel()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $token = JWTAuth::fromUser($adminUser);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $url = '/api/v1/admin/channels/'.$channel->id.'/update?token=' . $token;

        $data = ['name' =>  $channel->name . 'updated'];

        $req = $this->call('POST', $url, $data);

        $this->assertEquals(200, $req->status());   

        $this->seeInDatabase('channels', [
                'name' => $data['name'],
                'user_id' => $adminUser->id,
                'id' => $channel->id
            ]);
    }

    /**
     * @test
     * @group channel
     */
    public function cannotUpdateEmptyName()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $token = JWTAuth::fromUser($adminUser);

        $channel = factory(App\Channel::class)->create(['user_id' => $adminUser->id]);

        $url = '/api/v1/admin/channels/'.$channel->id.'/update?token=' . $token;

        $data = ['name' =>  ''];

        $req = $this->call('POST', $url, $data);

        $this->assertEquals(422, $req->status()); 

        $this->missingFromDatabase('channels', [
                'name' => $data['name'],
                'user_id' => $adminUser->id,
                'id' => $channel->id
            ]);
    }

    /**
     * @test
     * @group channel
     */
    public function canUpdateChannelsNotOwned()
    {
        $adminUser = factory(App\User::class)->create(['is_admin' => 1]);

        $token = JWTAuth::fromUser($adminUser);

        $owner = factory(App\User::class)->create();

        $channel = factory(App\Channel::class)->create([
                'user_id' => $owner->id
            ]);

        $url = '/api/v1/admin/channels/'.$channel->id.'/update?token=' . $token;

        $data = ['name' =>  'update'];

        $req = $this->call('POST', $url, $data);

        $this->assertEquals(200, $req->status()); 

        $this->seeInDatabase('channels', [
                'name' => $data['name'],
                'user_id' => $owner->id,
                'id' => $channel->id
            ]);
    }
}