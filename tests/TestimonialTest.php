<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class TestimonialTest extends ApiTester
{

    /** @test */
    public function it_creates_testimonial()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/testimonials?token=' . $this->token;

        $data = [
            'subject_id' => 50,
            'body'      => $this->faker->sentence
        ];

        $response = $this->call('POST', $route, $data);

        $this->assertEquals(201, $response->status());

        $this->seeInDatabase('testimonials', [
                'author_id' => $this->user->id,
                'subject_id' => $data['subject_id'],
                'body'      => $data['body']
            ]);
    }

    /** @test */
    public function it_gets_subjects_testimonials()
    {
    
        $this->makeUserAndToken();

        App\Testimonial::create([
                'author_id' => 50,
                'subject_id' => $this->user->id,
                'body' => $this->faker->sentence
            ]);

        $route = 'api/v1/user/testimonials?token=' . $this->token;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());

        $this->seeJson([
                'subject_id' => $this->user->id
            ]);
    }

    /** @test */
    public function it_gets_subjects_approved_testimonials()
    {
        $this->makeUserAndToken();

        App\Testimonial::create([
                'author_id' => 50,
                'subject_id' => $this->user->id,
                'body' => $this->faker->sentence,
                'approved' => true
            ]);

        $route = 'api/v1/user/testimonials/approved?token=' . $this->token;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());

        $this->seeJson([
                'subject_id' => $this->user->id,
                'approved' => 1
            ]);
    }

    /** @test */
    public function it_gets_subjects_unapproved_testimonials()
    {
        $this->makeUserAndToken();

        App\Testimonial::create([
                'author_id' => 50,
                'subject_id' => $this->user->id,
                'body' => $this->faker->sentence,
                'approved' => false
            ]);

        $route = 'api/v1/user/testimonials/unapproved?token=' . $this->token;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());

        $this->seeJson([
                'subject_id' => $this->user->id,
                'approved' => 0
            ]);
    }

    /** @test */
    public function it_approves_testimonial()
    {
        $this->makeUserAndToken();

        $testimonials = App\User::findOrFail($this->user->id)->testimonials->toArray();

        $testimonialId = $testimonials[0]['id'];

        App\Testimonial::find($testimonialId)->update(['approved' => false]);

        $route = 'api/v1/testimonials/' . $testimonialId. '/approve?token=' . $this->token;

        $response = $this->call('POST', $route);

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('testimonials', [
                'id'    => $testimonialId,
                'subject_id' => $this->user->id,
                'approved' => 1
            ]); 
    }

    /** @test */
    public function it_returns_403_if_non_subject_requests_to_approve()
    {

        $this->makeUserAndToken();

        $testimonials = App\User::findOrFail($this->user->id)->sent_testimonials->toArray();

        $testimonialId = $testimonials[0]['id'];

        $route = 'api/v1/testimonials/' . $testimonialId. '/approve?token=' . $this->token;

        $response = $this->call('POST', $route);

        $this->assertEquals(403, $response->status());
        
    }

    /** @test */
    public function it_unapproves_testimonial()
    {
        $this->makeUserAndToken();

        $testimonials = App\User::findOrFail($this->user->id)->testimonials->toArray();

        $testimonialId = $testimonials[0]['id'];

        App\Testimonial::find($testimonialId)->update(['approved' => true]);

        $route = 'api/v1/testimonials/' . $testimonialId. '/unapprove?token=' . $this->token;

        $response = $this->call('POST', $route);

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('testimonials', [
                'id'    => $testimonialId,
                'subject_id' => $this->user->id,
                'approved' => 0
            ]); 
    }

    /** @test */
    public function it_deletes_testimonial()
    {
        $this->makeUserAndToken();

        $testimonials = App\User::findOrFail($this->user->id)->testimonials->toArray();

        $testimonialId = $testimonials[0]['id'];

        $route = 'api/v1/testimonials/' . $testimonialId. '?token=' . $this->token;

        $response = $this->call('delete', $route);

        $this->assertEquals(200, $response->status());

        $this->notSeeInDatabase('testimonials', [
                'subject_id' => $this->user->id,
                'id'        => $testimonialId
            ]);

    }

    /** @test */
    public function it_returns_403_if_non_participant_request_delete()
    {
        $this->makeUserAndToken();

        $testimonial = App\Testimonial::create([
                            'author_id' => 10,
                            'subject_id' => 11,
                            'body' => $this->faker->sentence
                        ]);

        $route = 'api/v1/testimonials/' . $testimonial->id . '?token=' . $this->token;

        $response = $this->call('delete', $route);

        $this->assertEquals(403, $response->status());
    }

    /** @test */
    public function it_updates_testimonial()
    {
        $this->makeUserAndToken();

        $testimonials = App\User::findOrFail($this->user->id)->sent_testimonials->toArray();

        $testimonialId = $testimonials[0]['id'];

        App\Testimonial::findOrFail($testimonialId)->update(['approved' => false]);

        $route = 'api/v1/testimonials/' . $testimonialId . '?token=' . $this->token;

        $data = [
            'body' => $this->faker->sentence .'_update'
        ];

        $response = $this->call('post', $route, $data);

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('testimonials', [
                'id' => $testimonialId,
                'body'=> $data['body']
            ]);
    }

    /** @test */
    public function it_returns_403_if_author_requests_update_approved_testimonial()
    {
        $this->makeUserAndToken();

        $testimonials = App\User::findOrFail($this->user->id)->sent_testimonials->toArray();

        $testimonialId = $testimonials[0]['id'];

        App\Testimonial::findOrFail($testimonialId)->update(['approved' => true]);

        $route = 'api/v1/testimonials/' . $testimonialId . '?token=' . $this->token;

        $data = [
            'body' => $this->faker->sentence .'_update'
        ];

        $response = $this->call('post', $route, $data);

        $this->assertEquals(403, $response->status());

        $this->notSeeInDatabase('testimonials', [
                'id' => $testimonialId,
                'body'=> $data['body']
            ]);
    }

    /** @test */
    public function it_returns_403_if_non_author_requests_update_testimonial()
    {
        $this->makeUserAndToken();

        $testimonial = App\Testimonial::create([
                            'author_id' => 10,
                            'subject_id' => 11,
                            'body' => $this->faker->sentence
                        ]);

        $route = 'api/v1/testimonials/' . $testimonial->id . '?token=' . $this->token;

        $data = [
            'body' => $this->faker->sentence .'_update'
        ];

        $response = $this->call('post', $route);

        $this->assertEquals(403, $response->status());


        $this->notSeeInDatabase('testimonials', [
                'id' => $testimonial->id,
                'body'=> $data['body']
            ]);
    }

    /** @test */
    public function it_gets_users_approved_testimonials()
    {
        $this->makeUserAndToken();

        $testimonial = App\Testimonial::first();

        $userId = $testimonial->subject_id;
        
        $route = 'api/v1/users/' . $userId . '/testimonials?token=' . $this->token;

        $response = $this->call('GET', $route);
        
        $this->assertEquals(200, $response->status());

        $this->seeJson([
                'subject_id' => $userId,
                'approved' => 1
            ]);

    }
}