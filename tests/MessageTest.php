<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tymon\JWTAuth\Facades\JWTAuth as JWTAuth;


class MessageTest extends ApiTester
{
    /** @test */
    public function it_sends_a_message()
    {
        $this->makeUserAndToken();

        $data = [
            'to' => 2,
            'body' => $this->faker->sentence
        ];

        $route = 'api/v1/messages?token=' . $this->token;

        $response = $this->call('POST', $route, $data);

        $this->assertEquals(201, $response->status());

        $this->seeInDatabase('messages', [
                'to' => $data['to'],
                'body' => $data['body'],
                'from' => $this->user->id
            ]);
    }

    /** @test */
    public function it_returns_302_if_request_parameters_are_invalid_on_send()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/messages?token=' . $this->token;

        $response = $this->call('POST', $route, []);

        $this->assertEquals(302, $response->status());
    }

    /** @test */
    public function it_sends_a_reply()
    {
        $this->makeUserAndToken();

        $messages = App\User::find($this->user->id)->sent_messages->toArray();

        $data = [
            'to' => $messages[0]['to'],
            'body' => $this->faker->word
        ];

        $route = 'api/v1/messages/' . $messages[0]['id'] . '/reply?token=' . $this->token;

        $response = $this->call('POST', $route, $data);

        $this->assertEquals(201, $response->status());

        $this->seeInDatabase('messages', [
                'to' => $data['to'],
                'from' => $this->user->id,
                'body' => $data['body'],
                'parent_id' => $messages[0]['id']
            ]);
    }

    /** @test */
    public function it_gets_users_inbox()
    {
        $this->makeUserAndToken();

        $route = 'api/v1/user/inbox?token=' . $this->token;

        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());

        $this->seeJson([
                'user_id' => $this->user->id
            ]);
    }

    /** @test */
    public function it_gets_users_conversation()
    {
        $this->makeUserAndToken();

        $messages = App\User::find($this->user->id)->sent_messages->toArray();

        $connectionId = $messages[0]['to']; 

        $route = 'api/v1/messages/'. $connectionId . '/conversation?token=' . $this->token;
        
        $response = $this->call('GET', $route);

        $this->assertEquals(200, $response->status());

        $this->seeJson([
                'from' => $this->user->id,
                'to' => $messages[0]['to']
            ]);
 
    }

    /** @test */
    public function it_deletes_message()
    {
        $this->makeUserAndToken();

        $messages = App\User::find($this->user->id)->sent_messages->toArray();

        $messageToDelete = $messages[0]['id'];

        $route = 'api/v1/messages/' . $messageToDelete . '?token=' . $this->token;

        $response = $this->call('delete', $route);

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('deleted_messages', [
                'user_id' => $this->user->id,
                'message_id' => $messageToDelete
            ]); 
    }

    /** @test */
    public function it_marks_message_as_seen()
    {
        $message = App\Message::where('seen', false)->first();

        $user = $message->to()->first();

        $token = JWTAuth::fromUser($user);

        $route = 'api/v1/messages/' . $message->id . '/seen?token=' . $token;

        $response = $this->call('POST', $route);

        //dd($response->getContent());

        $this->assertEquals(200, $response->status());

        $this->seeInDatabase('messages', [
                'id' => $message->id,
                'seen' => true
            ]);
    }

}